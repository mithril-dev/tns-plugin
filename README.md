# Project Mithril
Timber and Stone mod.

## Releases
No releases yet.

## Install & Setup
__Using Visual Studio 2019__

1. Open a command line at a specified directory location and run `git clone https://gitlab.com/mithril-dev/tns-plugin.git`.
    - Note: I saved my files in a folder I created called "Mithril Projects".
2. Make sure to install the current Mithril-API from Discord in the #mithril-api-files text channel.
3. Open Visual Studio 2019.
4. Click `Continue without code`. (Looks blue and small below all the other options)
5. Click File > New > Project From Existing Code...
6. A prompt will show up, **MAKE SURE PROJECT TYPE IS IN VISUAL C#**, click `Next`.
7. Tell the prompt where the files are located.
8. Choose `TNSPlugin` as the name for the project.
9. Change the Output type to `Class Library`.
10. Click Finish.
11. Add necessary references to the project.
    - Make sure to reference `UnityEngine.dll`, `System.Drawing`, and `Assembly-CSharp.dll` (UnityEngine.dll and Assembly-CSharp.dll can be found at C:\Program Files (x86)\Steam\steamapps\common\Timber and Stone\Timber and Stone_Data\Managed)

## Contribute
Please make a branch for whatever feature, bug fixes, and or changes to the master branch.
Tags represent a released version of the master branch.

Contributers include in Discord usernames: Harvestminer (Lead), Anzarus, and Dream Haze

## Links
[Official Mithril Discord Server](https://discord.gg/yU2pUU7Ee2)
[Official Mithril Trello](https://trello.com/b/seCpBm8b/mithril-development)