﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Timber_and_Stone.Blocks;
using UnityEngine;

namespace TNSPlugin.Blocks
{
    public class RegisterBlocks : MonoBehaviour
    {
        public void Start()
        {
            BlockProperties.blocks[3] = new SlopeFieldStone(3);
            BlockProperties.blocks[62] = new SlopeFlatstone(62);
            BlockProperties.blocks[73] = new SlopeGreyPavestone(73);
            BlockProperties.blocks[74] = new SlopeBrownPavestone(74);
            BlockProperties.blocks[77] = new SlopeTimberPlanks(77);
            BlockProperties.blocks[78] = new SlopeTimberTiles(78);
            BlockProperties.blocks[79] = new SlopeTimberedFloor(79);
            BlockProperties.blocks[82] = new SlopeGreyCobble(82);
            BlockProperties.blocks[83] = new SlopeBrownCobble(83);
            BlockProperties.blocks[84] = new SlopeSmoothTimber(84);
            BlockProperties.blocks[7] = new StairBrownCobble(7);
            BlockProperties.blocks[5] = new StairGreyCobble(5);
            BlockProperties.blocks[39] = new StairTimberedFloor(39);
            BlockProperties.blocks[38] = new StairSmoothTimber(38);
            BlockProperties.blocks[37] = new StairTimberPlanks(37);
            BlockProperties.blocks[36] = new StairTimberTiles(36);
            BlockProperties.blocks[35] = new StairFlatstone(35);
            BlockProperties.blocks[12] = new StairFieldStone(12);
            BlockProperties.blocks[11] = new StairBrownPavestone(11);
            BlockProperties.blocks[9] = new StairGreyPavestone(9);

            Destroy(this);
        }
    }
}
