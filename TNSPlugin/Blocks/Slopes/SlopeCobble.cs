﻿using System.Collections.Generic;
using Timber_and_Stone.Texture;
using Timber_and_Stone.Blocks;

namespace TNSPlugin.Blocks
{
    class SlopeBrownCobble : ASlopeBlock
    {
        ATextureGroup tex;

        public SlopeBrownCobble(ushort id) : base(id) { }
        public override string getName()
        {
            return "Cobblestone Brown";
        }
        public override bool isBuildable()
        {
            return true;
        }

        public override eConstructionType getConstructionType()
        { return eConstructionType.FLOORING; }

        public override bool canUnitWalkThrough()
        {
            return false;
        }

        public override KeyValuePair<int, uint>[] getDrops()
        {
            return new KeyValuePair<int, uint>[] { new KeyValuePair<int, uint>(2, 2) };
        }

        public override Dictionary<Resource, ushort> getConstructingResource()
        {
            if (constructionResources == null)
            {
                constructionResources = new Dictionary<Resource, ushort>();
                constructionResources.Add(ResourceManager.getInstance().resources[2], 2);
            }

            return constructionResources;
        }

        public override ATextureGroup getTextures()
        {
            // TODO: replace with stone specific
            if (tex == null) tex = BlockProperties.BlockBrownCobble.getTextures();

            return tex;
        }
    }

    class SlopeGreyCobble : ASlopeBlock
    {
        ATextureGroup tex;

        public SlopeGreyCobble(ushort id) : base(id) { }
        public override string getName()
        {
            return "Cobblestone Grey";
        }
        public override bool isBuildable()
        {
            return true;
        }

        public override eConstructionType getConstructionType()
        { return eConstructionType.FLOORING; }

        public override bool canUnitWalkThrough()
        {
            return false;
        }

        public override KeyValuePair<int, uint>[] getDrops()
        {
            return new KeyValuePair<int, uint>[] { new KeyValuePair<int, uint>(2, 2) };
        }

        public override Dictionary<Resource, ushort> getConstructingResource()
        {
            if (constructionResources == null)
            {
                constructionResources = new Dictionary<Resource, ushort>();
                constructionResources.Add(ResourceManager.getInstance().resources[2], 2);
            }

            return constructionResources;
        }

        public override ATextureGroup getTextures()
        {
            // TODO: replace with stone specific
            if (tex == null) tex = BlockProperties.BlockGreyCobble.getTextures();

            return tex;
        }
    }
}
