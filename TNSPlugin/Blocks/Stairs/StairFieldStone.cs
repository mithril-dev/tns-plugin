﻿using System;
using System.Collections.Generic;
using System.Linq;
using Timber_and_Stone.Texture;
using Timber_and_Stone.Blocks;

namespace TNSPlugin.Blocks
{
    class StairFieldStone : AStairBlock
    {
        ATextureGroup tex;

        public StairFieldStone(ushort id) : base(id) { }
        public override string getName()
        {
            return "Fieldstone";
        }
        public override bool isBuildable()
        {
            return true;
        }

        public override eConstructionType getConstructionType()
        { return eConstructionType.WALLS; }

        public override bool canUnitWalkThrough()
        {
            return false;
        }

        public override KeyValuePair<int, uint>[] getDrops()
        {
            return new KeyValuePair<int, uint>[] { new KeyValuePair<int, uint>(2, 2) };
        }

        public override Dictionary<Resource, ushort> getConstructingResource()
        {
            if (constructionResources == null)
            {
                constructionResources = new Dictionary<Resource, ushort>();
                constructionResources.Add(ResourceManager.getInstance().resources[2], 2);
            }

            return constructionResources;
        }

        public override ATextureGroup getTextures()
        {
            // TODO: replace with stone specific
            if (tex == null) tex = BlockProperties.BlockFieldStone.getTextures();

            return tex;
        }
    }
}
