﻿using System;
using System.Collections.Generic;
using System.Linq;
using Timber_and_Stone.Texture;
using Timber_and_Stone.Blocks;

namespace TNSPlugin.Blocks
{
    class StairBrownPavestone : AStairBlock
    {
        ATextureGroup tex;

        public StairBrownPavestone(ushort id) : base(id) { }
        public override string getName()
        {
            return "Pavestone Brown";
        }
        public override bool isBuildable()
        {
            return true;
        }

        public override eConstructionType getConstructionType()
        { return eConstructionType.FLOORING; }

        public override bool canUnitWalkThrough()
        {
            return false;
        }

        public override KeyValuePair<int, uint>[] getDrops()
        {
            return new KeyValuePair<int, uint>[] { new KeyValuePair<int, uint>(40, 1) };
        }

        public override Dictionary<Resource, ushort> getConstructingResource()
        {
            if (constructionResources == null)
            {
                constructionResources = new Dictionary<Resource, ushort>();
                constructionResources.Add(ResourceManager.getInstance().resources[40], 1);
            }

            return constructionResources;
        }

        public override ATextureGroup getTextures()
        {
            // TODO: replace with stone specific
            if (tex == null) tex = BlockProperties.BlockBrownPavestone.getTextures();

            return tex;
        }
    }

    class StairGreyPavestone : AStairBlock
    {
        ATextureGroup tex;

        public StairGreyPavestone(ushort id) : base(id) { }
        public override string getName()
        {
            return "Pavestone Grey";
        }
        public override bool isBuildable()
        {
            return true;
        }

        public override eConstructionType getConstructionType()
        { return eConstructionType.FLOORING; }

        public override bool canUnitWalkThrough()
        {
            return false;
        }

        public override KeyValuePair<int, uint>[] getDrops()
        {
            return new KeyValuePair<int, uint>[] { new KeyValuePair<int, uint>(40, 1) };
        }

        public override Dictionary<Resource, ushort> getConstructingResource()
        {
            if (constructionResources == null)
            {
                constructionResources = new Dictionary<Resource, ushort>();
                constructionResources.Add(ResourceManager.getInstance().resources[40], 1);
            }

            return constructionResources;
        }

        public override ATextureGroup getTextures()
        {
            // TODO: replace with stone specific
            if (tex == null) tex = BlockProperties.BlockGreyPavestone.getTextures();

            return tex;
        }
    }
}
