﻿using System;
using System.Collections.Generic;
using System.Linq;
using Timber_and_Stone.Texture;

namespace Timber_and_Stone.Blocks
{
    class StairTimberedFloor : AStairBlock
    {
        ATextureGroup tex;

        public StairTimberedFloor(ushort id) : base(id) { }
        public override string getName()
        {
            return "Timbered Floor";
        }
        public override bool isBuildable()
        {
            return true;
        }

        public override eConstructionType getConstructionType()
        { return eConstructionType.FLOORING; }

        public override bool canUnitWalkThrough()
        {
            return false;
        }

        public override KeyValuePair<int, uint>[] getDrops()
        {
            return new KeyValuePair<int, uint>[] { new KeyValuePair<int, uint>(40, 1) };
        }

        public override Dictionary<Resource, ushort> getConstructingResource()
        {
            if (constructionResources == null)
            {
                constructionResources = new Dictionary<Resource, ushort>();
                constructionResources.Add(ResourceManager.getInstance().resources[40], 1);
            }

            return constructionResources;
        }

        public override ATextureGroup getTextures()
        {
            // TODO: replace with stone specific
            if (tex == null) tex = BlockProperties.BlockTimberedFloor.getTextures();

            return tex;
        }
    }
}
