﻿using System.Collections.Generic;
using Timber_and_Stone.API.Event;
using Timber_and_Stone.Event;
using Timber_and_Stone.GUIUtil;
using UnityEngine;

namespace TNSPlugin.Cinematic_Camera
{
    public class CinCamera : MonoBehaviour
    {
        public Spline spline;
        public float speed = 1f;

        private float distanceTravelled = 0f;

        private GUIManager guiManager;
        private ControlPlayer player;

        private bool isVisible;
        private bool isPlaying;

        private float min;
        private float max;

        private List<GameObject> gameObjects;

        private void Start()
        {
            isVisible = false;
            isPlaying = false;

            guiManager = GUIManager.getInstance();
            player = guiManager.controllerObj.GetComponent<ControlPlayer>();

            gameObjects = new List<GameObject>();
        }

        private void Update()
        {

            if (Input.GetKeyDown(KeyCode.L)) { isVisible = !isVisible; }

            if (!isPlaying) { distanceTravelled = 0; return; }

            distanceTravelled += speed * Time.deltaTime;
            player.currentCamera.transform.position = spline.GetPoint(distanceTravelled);
        }

        Rect window = new Rect(100, 100, 300, 162);
        void OnGUI()
        {
            if (isVisible)
            {
                window = GUI.Window(91037, window, playBackWindow, "");
            }
        }

        void playBackWindow(int windowID)
        {
            GUI.DragWindow(new Rect(0, 0, window.width - 24, 28));

            guiManager.DrawWindow(new Rect(0, 0, window.width, window.height), "Cinematic Camera Jig", false);

            if (GUI.Button(new Rect(window.width - 24, 0 + 4, 20, 20), "", guiManager.closeWindowButtonStyle))
            {
                isVisible = !isVisible;
                AEvent evt = new EventButtonPressed();
                EventManager.getInstance().InvokePre(evt);
                EventManager.getInstance().InvokePost(evt);
            }

            Rect playBackArea = new Rect(8, 28 + 4, window.width - 8 * 2, window.height - 32);
            GUILayout.BeginArea(playBackArea);
            {
                GUILayout.BeginHorizontal();
                {
                    DrawLayoutGUI.Text_Layout("Speed");
                    min = float.Parse(GUILayout.TextField("0.01"));
                    speed = DrawLayoutGUI.Slider((float)speed, min, max);
                    max = float.Parse(GUILayout.TextField("1"));
                }
                GUILayout.EndHorizontal();

                if (DrawLayoutGUI.Default_Layout_Button(!isPlaying ? "Play" : "Stop") && (spline != null && spline.points.Length >= 4))
                {
                    isPlaying = !isPlaying;
                    Time.timeScale = 1f;

                    foreach (var item in gameObjects)
                    {
                        var renderer = item.GetComponent<MeshRenderer>();
                        renderer.enabled = !isPlaying;
                    }
                }
                GUILayout.BeginHorizontal();
                {
                    if (DrawLayoutGUI.Default_Layout_Button("Add") && !isPlaying)
                    {
                        if (spline == null) spline = new Spline();

                        spline.AddPoint(player.currentCamera.transform.position);
                        var ob = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        ob.transform.position = player.currentCamera.transform.position;
                        ob.transform.localScale = new Vector3(0.09f, 0.09f, 0.09f);
                        gameObjects.Add(ob);
                    }
                    if (spline != null && DrawLayoutGUI.Default_Layout_Button("Clear"))
                    {
                        spline = null;
                        isPlaying = false;

                        foreach (var point in gameObjects)
                        {
                            Destroy(point);
                        }
                    }
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndArea();
        }
    }

    public class Spline
    {
        public Vector3[] points;
        public Vector3[] rotations;

        public int PointCount => points.Length;

        public Spline() { points = new Vector3[0]; }

        public Spline(Vector3[] points)
        {
            this.points = points;
        }

        public Vector3 GetPoint(float t)
        {
            int i;
            if (t >= 1f)
            {
                t = 1f;
                i = points.Length - 4;
            }
            else
            {
                t = Mathf.Clamp01(t) * (points.Length - 1) / 3f;
                i = (int)t;
                t -= i;
                i *= 3;
            }
            return transformPoint(points[i], points[i + 1], points[i + 2], points[i + 3], t);
        }

        public void AddPoint(Vector3 point)
        {
            Vector3[] newPoints = new Vector3[points.Length + 1];
            for (int i = 0; i < points.Length; i++)
            {
                newPoints[i] = points[i];
            }
            newPoints[points.Length] = point;
            points = newPoints;
        }

        private Vector3 transformPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
        {
            float u = 1f - t;
            float tt = t * t;
            float uu = u * u;
            float uuu = uu * u;
            float ttt = tt * t;

            Vector3 p = uuu * p0;
            p += 3f * uu * t * p1;
            p += 3f * u * tt * p2;
            p += ttt * p3;

            return p;
        }
    }
}
