﻿using MithrilAPI.Debug;
using System;
using System.Collections;
using System.Collections.Generic;
using Timber_and_Stone.API.Event;
using Timber_and_Stone.Event;
using TNSPlugin.Shaders;
using UnityEngine;

namespace TNSPlugin
{
	[RequireComponent(typeof(Light))]
	public class SkyController : MonoBehaviour, IEventListener
	{
		public bool overriedSkyColor = true;
		public Gradient topColor;
		public Gradient middleColor;
		public Gradient bottomColor;

		public bool overrideSunColor = true;
		public Gradient sunColor;

		public bool overrideLightColor = false;
		public Gradient lightColor;

		public bool overrideAmbientSkyColor = true;
		public Gradient ambientSkyColor;

		public bool overrideCloudsColor = true;
		public Gradient cloudsColor;

		public bool useSrub = false;
		public float scrub;

		private ControlPlayer player;

		public static GameObject SkySphereOBJ;
		public static Material SkyMaterial;


		private Light sun;
		public Light Sun
		{
			get
			{
				if (sun == null)
				{
					sun = GetComponent<Light>();
				}
				return sun;
			}
		}

		private static SkyController skyCon;
		public static SkyController getInstance()
        {
			return skyCon;
		}


		void Start()
		{
            topColor = new Gradient();
            middleColor = new Gradient();
            bottomColor = new Gradient();
            sunColor = new Gradient();
            lightColor = new Gradient();
            ambientSkyColor = new Gradient();
            cloudsColor = new Gradient();

            setColorKeys();

			EventManager.getInstance().Register(this);
			skyCon = this;

			player = GUIManager.getInstance().controllerObj.GetComponent<ControlPlayer>();

			SkySphereOBJ = new GameObject();
			SkySphereOBJ = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			GameObject.Destroy(SkySphereOBJ.collider);
			SkySphereOBJ.GetComponent<MeshRenderer>().sharedMaterial = SkyMaterial;
			SkySphereOBJ.transform.localScale = new Vector3(5, 5, 5);
			SkySphereOBJ.transform.localEulerAngles = new Vector3(0, 0, 0);
		}

		public void OnValidate()
		{
			if (useSrub)
			{
				UpdateGradients(scrub);
			}
		}

		private void Update()
		{
			Shader.SetGlobalVector("_SunDirection", Sun.transform.forward);

			Sun.transform.localEulerAngles = new Vector3(0 + (TimeManager.getInstance().time - 6) * 15, 0, 0);

			if (!useSrub && Sun.transform.hasChanged)
			{
				float pos = Vector3.Dot(Sun.transform.forward.normalized, Vector3.up) * 0.5f + 0.5f;
				UpdateGradients(pos);
			}

			if (SkySphereOBJ != null)
				SkySphereOBJ.transform.position = player.currentCamera.transform.position;
        }

        public void UpdateGradients(float pos)
		{
			if (overriedSkyColor)
			{
				Shader.SetGlobalColor("_ColorTop", topColor.Evaluate(pos));
				Shader.SetGlobalColor("_ColorMiddle", middleColor.Evaluate(pos));
				Shader.SetGlobalColor("_ColorBottom", bottomColor.Evaluate(pos));
			}
			if (overrideSunColor)
			{
				Shader.SetGlobalColor("_SunColor", sunColor.Evaluate(pos));
			}
			if (overrideLightColor)
			{
				Sun.color = lightColor.Evaluate(pos);
			}
			if (overrideAmbientSkyColor)
			{
				RenderSettings.ambientLight = ambientSkyColor.Evaluate(pos);
			}
			if (overrideCloudsColor)
			{
				Shader.SetGlobalColor("_CloudsColor", cloudsColor.Evaluate(pos));
			}
		}

		public GradientAlphaKey[] GetDefaultAlphaKeys()
		{
			return new GradientAlphaKey[]
			{
			new GradientAlphaKey ()
			{
				time = 0,
				alpha = 1
			},
			new GradientAlphaKey ()
			{
				time = 1,
				alpha = 1
			}
			};
		}

		public void setColorKeys()
		{
			topColor.colorKeys = new GradientColorKey[]
			{
			new GradientColorKey ()
			{
				time = 0,
				color = new Color(0.000f, 0.584f, 1.000f, 255.000f)
			},
			new GradientColorKey ()
			{
				time = 0.4705882f,
				color = new Color(0.675f, 0.722f, 0.847f, 255.000f)
			},
			new GradientColorKey ()
			{
				time = 0.6529488f,
				color = new Color(0.843f, 0.784f, 0.965f, 255.000f)
			},
			new GradientColorKey ()
			{
				time = 1,
				color = new Color(0.129f, 0.141f, 0.345f, 255.000f)
			}
			};
			topColor.alphaKeys = GetDefaultAlphaKeys();

			middleColor.colorKeys = new GradientColorKey[]
			{
			new GradientColorKey ()
			{
				time = 0,
				color = new Color(0.682f, 0.784f, 0.925f, 255.000f)
			},
			new GradientColorKey ()
			{
				time = 0.5029374f,
				color = new Color(0.867f, 0.655f, 0.682f, 255.000f)
			},
			new GradientColorKey ()
			{
				time = 0.5705959f,
				color = new Color(0.918f, 0.624f, 0.624f, 255.000f)
			},
			new GradientColorKey ()
			{
				time = 1,
				color = new Color(0.059f, 0.075f, 0.133f, 255.000f)
			}
			};
			middleColor.alphaKeys = GetDefaultAlphaKeys();

			bottomColor.colorKeys = new GradientColorKey[]
			{
			new GradientColorKey ()
			{
				time = 0,
				color = new Color(0.737f, 0.737f, 0.737f, 255.000f)
			},
			new GradientColorKey ()
			{
				time = 0.5000076f,
				color = new Color(0.737f, 0.737f, 0.737f, 255.000f)
			},
			new GradientColorKey ()
			{
				time = 0.5676509f,
				color = new Color(0.737f, 0.737f, 0.737f, 255.000f)
			},
			new GradientColorKey ()
			{
				time = 1,
				color = new Color(0.000f, 0.000f, 0.000f, 255.000f)
			}
			};
			bottomColor.alphaKeys = GetDefaultAlphaKeys();

			sunColor.colorKeys = new GradientColorKey[]
			{
			new GradientColorKey ()
			{
				time = 0,
				color = new Color(0.976f, 0.976f, 0.886f, 255.000f)
			},
			new GradientColorKey ()
			{
				time = 0.4117647f,
				color = new Color(1.000f, 0.843f, 0.502f, 255.000f)
			},
			new GradientColorKey ()
			{
				time = 1,
				color = new Color(0.000f, 0.000f, 0.000f, 255.000f)
			}
			};
			sunColor.alphaKeys = GetDefaultAlphaKeys();

			ambientSkyColor.colorKeys = new GradientColorKey[]
			{
			new GradientColorKey ()
			{
				time = 0,
				color = new Color(1f, 1f, 1f)
			},
			new GradientColorKey ()
			{
				time = 0.4852979f,
				color = new Color(1f, 0.7803922f, 0.7803922f)
			},
			new GradientColorKey ()
			{
				time = 1,
				color = new Color(0.345098f, 0.345098f, 0.345098f)
			}
			};
			ambientSkyColor.alphaKeys = GetDefaultAlphaKeys();

			cloudsColor.colorKeys = new GradientColorKey[]
			{
			new GradientColorKey ()
			{
				time = 0,
				color = new Color(0.8745098f, 0.8980392f, 1f)
			},
			new GradientColorKey ()
			{
				time = 0.4852979f,
				color = new Color(1f, 1f, 1f)
			},
			new GradientColorKey ()
			{
				time = 0.720592f,
				color = new Color(0.7529412f, 0.7490196f, 0.7803922f)
			},
			new GradientColorKey ()
			{
				time = 0.9852903f,
				color = new Color(0.09803922f, 0.08235294f, 0.2039216f)
			},
			new GradientColorKey ()
			{
				time = 1,
				color = new Color(0.06666667f, 0.05098039f, 0.1764706f)
			}
			};

			cloudsColor.alphaKeys = new GradientAlphaKey[]
			{
			new GradientAlphaKey ()
			{
				time = 0,
				alpha = 1
			},
			new GradientAlphaKey ()
			{
				time = 0.4882429f,
				alpha = 1
			},
			new GradientAlphaKey ()
			{
				time = 1,
				alpha = 0.1921569f
			}
			};
		}
	}
}