﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Timber_and_Stone.API.Event;

namespace TNSPlugin.Events
{
    public class EventChangedDifficulty : AEvent
    {
        private Difficulty difficulty;

        public EventChangedDifficulty(Difficulty difficulty)
        {
            this.difficulty = difficulty;
        }

        public Difficulty getDifficulty() { return difficulty; }
    }

    public enum Difficulty
    {
        Peaceful,
        Easy,
        Hard
    }
}