﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Timber_and_Stone;
using Timber_and_Stone.API.Event;
using Timber_and_Stone.API.Event.Task;
using Timber_and_Stone.Blocks;
using Timber_and_Stone.Event;
using Timber_and_Stone.Invasion;
using MithrilAPI.Debug;
using TNSPlugin.Events;
using UnityEngine;
using EventHandler = Timber_and_Stone.API.Event.EventHandler;
using TNSPlugin.Shaders;

namespace TNSPlugin
{
    public class GUIEventHandler : MonoBehaviour, IEventListener
    {
        private static readonly GUIManager guiManager = GUIManager.getInstance();
        private static readonly ControlPlayer controlPlayer = guiManager.controllerObj.GetComponent<ControlPlayer>();

        private static bool isWorldSettingsLoaded = false;

        public void Start()
        {
            EventManager.getInstance().Register(this);

            #region Put all objects (with name) into a file
            //var objects = FindObjectsOfType(typeof(GameObject));

            //foreach (GameObject go in objects)
            //{
            //    try
            //    {
            //        using (StreamWriter file = File.CreateText($@"go\{go.name}.txt"))
            //        {
            //            Component[] comps = go.GetComponents(typeof(Component));
            //            file.WriteLine($"Files in {go.name}: ");

            //            foreach (Component comp in comps)
            //            {
            //                file.WriteLine(comp.GetType().Name);
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        DebugConsole.Log($"{ex.Message} | {ex.Data}");
            //    }
            //}
            #endregion
        }

        //[EventHandler(Priority.Monitor)]
        //public void OnEntityDeath(EventEntityDeath evt)
        //{
        //    bool isPlayerUnit = evt.getUnit().faction == (IFaction)WorldManager.getInstance().PlayerFaction;

        //    if (isPlayerUnit)
        //    {
        //        APlayableEntity entity = evt.getUnit() as APlayableEntity;
        //        if (entity == null || entity.isAlive()) return;

        //        string name = entity.unitName;
        //        GUIInGame.NotificationLogs.Add($@"{name} died!");
        //    }
        //    else
        //    {
        //        if ((isPlayerUnit || evt.getUnit().isSpotted()) && evt.getDamager() != null)
        //        {
        //            if (evt.getDamager().isSpotted())
        //            {
        //                var random = UnityEngine.Random.Range(0, 4);

        //                switch (random)
        //                {
        //                    case 0:
        //                        GUIInGame.NotificationLogs.Add($"{evt.getDamager().unitName} slayed a {evt.getUnit().unitName}");
        //                        break;
        //                    case 1:
        //                        GUIInGame.NotificationLogs.Add($"{evt.getDamager().unitName} celebrate a {evt.getUnit().unitName}'s death!");
        //                        break;
        //                    case 2:
        //                        GUIInGame.NotificationLogs.Add($"{evt.getDamager().unitName} broke a {evt.getUnit().unitName}'s spirit");
        //                        break;
        //                    case 3:
        //                        GUIInGame.NotificationLogs.Add($"{evt.getDamager().unitName} sent a {evt.getUnit().unitName} to their grave");
        //                        break;
        //                    default:
        //                        GUIInGame.NotificationLogs.Add($"{evt.getDamager().unitName} sent a {evt.getUnit().unitName} to the other side");
        //                        break;
        //                }
        //            }
        //            else
        //            {
        //                GUIInGame.NotificationLogs.Add($"{evt.getUnit().unitName} was slain by an hidden entity");
        //            }
        //        }
        //    }
        //}

        //[EventHandler(Priority.Monitor)]
        //public void OnLevelUp(EventLevelUp evt)
        //{
        //    if (evt.getUnit().faction == (IFaction)WorldManager.getInstance().PlayerFaction)
        //        GUIInGame.NotificationLogs.Add($"Level Up! {evt.getUnit().unitName} is now a lvl. {evt.getLevel()} {((APlayableEntity)evt.getUnit()).getProfession().getProfessionName()}");
        //}

        [EventHandler(Priority.Monitor)]
        public void OnInvasion(EventInvasion evt)
        {
            if (GUINewGameSettings.difficulty == Difficulty.Hard && !isWorldSettingsLoaded)
            { new WorldSettings(WorldManager.getInstance().settlementName).LoadSettings(); isWorldSettingsLoaded = true; }

            if (GUINewGameSettings.difficulty == Difficulty.Peaceful)
            {
                evt.result = Result.Deny;
                evt.invasion.CancelInvasion();
                return;
            }

            string name = evt.invasion.getName();
            //GUIInGame.NotificationLogs.Add($"Arm the troops, a {name} invasion is happening!");
        }

        // Don't need but probably would still like this as an event....
        //[EventHandler(Priority.Monitor)]
        //public void OnEntitySpawned(EventEntitySpawned evt)
        //{
        //    if (GUINewGameSettings.isPeaceful)
        //    {
        //        if (evt.getALivingEntity().faction == WorldManager.getInstance().UndeadFaction)
        //        {
        //            evt.getALivingEntity().Destroy();
        //        }
        //    }
        //}

        //[EventHandler(Priority.Monitor)]
        //public void OnDesignCancelled(EventCancelledDesign evt)
        //{
        //    GUIInGame.CancelDesign();
        //}

        [EventHandler(Priority.Monitor)]
        public void OnGameSave(EventSave evt)
        {
            evt.result = Result.Deny;
            if (GUIManager.getInstance().inGame)
            {
                WorldSettings settings = new WorldSettings(WorldManager.getInstance().settlementName);
                settings.SaveSettings();
            }
        }

        [EventHandler(Priority.Monitor)]
        public void OnGameLoad(EventGameLoad evt)
        {
            WorldSettings settings = new WorldSettings(WorldManager.getInstance().settlementName);
            settings.LoadSettings();

            if (settings.worldSettings.difficulty == Difficulty.Peaceful)
            {
                List<ALivingEntity> enemies = new List<ALivingEntity>();
                enemies = FindObjectsOfType<ALivingEntity>().Where(x => WorldManager.getInstance().PlayerFaction.getAlignmentToward(x.faction) == Alignment.Enemy).ToList();

                foreach (ALivingEntity unit in enemies)
                {
                    unit.Destroy();
                }
            }

            FogOfWar.EnablePostProcessing(GUINewGameSettings.fogOfWar);
        }

        [EventHandler(Priority.Monitor)]
        public void OnFactionChange(EventEntityFactionChange evt)
        {
            if (evt.newFaction == (IFaction)WorldManager.getInstance().PlayerFaction && evt.unit is APlayableEntity)
            {
                GroupManager.settlement.Groups[0].Units.Add(new UnitData((APlayableEntity)evt.unit));

                evt.unit.sightRangeSphere.renderer.material = Outline.SightRangeShader;
                evt.unit.sightRangeSphere.renderer.castShadows = false;
                evt.unit.sightRangeSphere.renderer.receiveShadows = false;
            }
        }

        [EventHandler(Priority.Monitor)]
        public void OnDifficultyChange(EventChangedDifficulty evt)
        {
            switch (evt.getDifficulty())
            {
                case Difficulty.Peaceful:
                    GUINewGameSettings.difficulty = Difficulty.Peaceful;
                    break;
                case Difficulty.Easy:
                    GUINewGameSettings.difficulty = Difficulty.Easy;
                    break;
                case Difficulty.Hard:
                    GUINewGameSettings.difficulty = Difficulty.Hard;
                    break;
            }

            if (guiManager.inGame && !guiManager.gameOver)
            {
                if (GUINewGameSettings.difficulty == Difficulty.Peaceful)
                {
                    List<ALivingEntity> enemies = new List<ALivingEntity>();
                    enemies = FindObjectsOfType<ALivingEntity>().Where(x => WorldManager.getInstance().PlayerFaction.getAlignmentToward(x.faction) == Alignment.Enemy).ToList();

                    foreach (ALivingEntity unit in enemies)
                    {
                        unit.Destroy();
                    }
                }

                WorldSettings settings = new WorldSettings(WorldManager.getInstance().settlementName);
                settings.SaveSettings();
            }
        }

        [EventHandler(Priority.Monitor)]
        public void OnAcceptMigrant(EventMigrantAccept evt)
        {
            if (!GroupManager.settlement.Groups[0].Units.Contains(new UnitData(evt.unit)))
            {
                GroupManager.settlement.Groups[0].Units.Add(new UnitData(evt.unit));

                evt.unit.sightRangeSphere.renderer.material = Outline.SightRangeShader;
                evt.unit.sightRangeSphere.renderer.castShadows = false;
                evt.unit.sightRangeSphere.renderer.receiveShadows = false;
            }
        }

        //[EventHandler(Priority.Monitor)]
        //public void OnStorageChange(EventStorageChange evt)
        //{
        //    if (evt.GetResourceChangeType() == ResourceChangeType.Set || evt.GetResourceChangeType() == ResourceChangeType.Remove || evt.GetResourceChangeType() == ResourceChangeType.CheckForResource)
        //    {
        //        if (DebugConsole.mode == Debug.Cmd.GameMode.creative)
        //        {
        //            evt.result = Result.Deny;
        //        }
        //    }
        //}
    }
}
