﻿using UnityEngine;

namespace TNSPlugin
{
    public class GUIHide : MonoBehaviour
    {
        public static bool IsOldGUIHidden { get; private set; } = false;
        public static bool IsGUIHidden { get; private set; } = false;

        private static readonly GUIManager guiManager = AManager<GUIManager>.getInstance();

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.H) && guiManager.inGame)
            {
                HideMenus();
                IsGUIHidden = !IsGUIHidden;
            }
        }

        public void HideMenus()
        {
            if (GUIInGame.IsUsingNewUI)
            {
                this.GetComponent<GUIInGame>().enabled = !this.GetComponent<GUIInGame>().enabled;
            }
            else
            {
                guiManager.GetComponent<MainMenus>().enabled = !guiManager.GetComponent<MainMenus>().enabled;
                guiManager.GetComponent<NotificationWindow>().enabled = !guiManager.GetComponent<NotificationWindow>().enabled;
            }
        }
    }
}
