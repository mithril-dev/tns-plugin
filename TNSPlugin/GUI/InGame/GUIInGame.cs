﻿using System;
using System.Collections.Generic;
using System.Linq;
using Timber_and_Stone.API.Event;
using Timber_and_Stone.Blocks;
using Timber_and_Stone.Event;
using MithrilAPI.Debug;
using TNSPlugin.Events;
using TNSPlugin.Utilities;
using UnityEngine;
using EventHandler = Timber_and_Stone.API.Event.EventHandler;

namespace TNSPlugin
{
    public class GUIInGame : MonoBehaviour
    {
        public static bool IsUsingNewUI = false;

        private static List<GUILoad.GUIBlocks> guiBlocks = new List<GUILoad.GUIBlocks>();
        private static GUIContent[] content;
        private static readonly GUIManager guiManager = AManager<GUIManager>.getInstance();
        private static ControlPlayer controller = guiManager.controllerObj.GetComponent<ControlPlayer>();

        private static int BlockSelected = 0;
        private static bool isOptionsOpen = false;
        private static bool isBuildMenuOpen = false;
        private static bool isNotificationWindowOpen = false;
        private static bool isDigging = false;
        private static bool isChopping = false;

        private static Texture2D notificationIcon;
        private static Texture2D notificationIconHover;
        private static Texture2D notificationIconUnread;
        private static Texture2D mineOrDigIcon;
        private static Texture2D chopIcon;

        public static List<string> NotificationLogs;

        private static int lastCheckedLength = 0;

        private GameObject ob;

        public void Start()
        {
            NotificationLogs = new List<string>();

            new GUIEventHandler();

            notificationIcon = TextureHandler.ConvertToTexture2D("LetterIcon");
            notificationIconHover = TextureHandler.ConvertToTexture2D("LetterIconHover");
            notificationIconUnread = TextureHandler.ConvertToTexture2D("LetterIconUnread");
            mineOrDigIcon = TextureHandler.ConvertToTexture2D("mine_icon");
            chopIcon = TextureHandler.ConvertToTexture2D("chop_icon");

            notificationIcon.filterMode = FilterMode.Point;
            notificationIconHover.filterMode = FilterMode.Point;
            notificationIconUnread.filterMode = FilterMode.Point;
            mineOrDigIcon.filterMode = FilterMode.Point;
            chopIcon.filterMode = FilterMode.Point;
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (guiManager.inGame)
                {
                    CloseWindows();
                    isOptionsOpen = !isOptionsOpen;
                }
            }

            if (Input.GetMouseButtonDown(2))
            {
                if (guiManager.inGame)
                {
                    guiManager.controllerObj.GetComponent<ControlPlayer>().CancelDesigning(true);
                    isChopping = false;
                    isDigging = false;
                }
            }
        }

        void OnGUI()
        {
            if ((guiManager.inGame && !guiManager.gameOver))
            {
                if (IsUsingNewUI)
                {
                    guiManager.GetComponent<MainMenus>().enabled = false;
                    guiManager.GetComponent<NotificationWindow>().enabled = false;

                    guiBlocks = GUILoad.GUIBlockIcons.Where(t => t.design != eConstructionType.NOBUILD).ToList();
                    guiBlocks.MoveToLast(guiBlocks.Where(t => t.name == "Fence").Single());

                    // Get only images and put int a content array
                    content = guiBlocks.Select(t => new GUIContent(t.texture)).ToArray();
                }

                LeftGroupUI();
                RightGroupUI(IsUsingNewUI);
                ToolsGroupUI(IsUsingNewUI);
            }
        }

        // Mostly just info on time and day.
        private static void LeftGroupUI(bool enabled = true)
        {
            if (!enabled) return;

            if (GUI.Button(new Rect(0, 50 + 24, 82, 24), "Switch GUI"))
            {
                IsUsingNewUI = !IsUsingNewUI;
                guiManager.GetComponent<MainMenus>().enabled = !guiManager.GetComponent<MainMenus>().enabled;
                guiManager.GetComponent<NotificationWindow>().enabled = !guiManager.GetComponent<NotificationWindow>().enabled;
            }
        }

        // Managment and info type group.
        private static Vector2 scrollView = Vector2.zero;
        private static void RightGroupUI(bool enabled = true)
        {
            if (!enabled) return;

            Rect outside = new Rect(Screen.width - 138, 0, 138, 40);
            GUI.DrawTextureWithTexCoords(outside, guiManager.windowBG, new Rect(outside.x, outside.y, outside.width / guiManager.windowBG.width, outside.height / guiManager.windowBG.height));
            GUI.Box(outside, string.Empty, guiManager.windowBoxStyle);
            
            #region timeButtons
            int xPos = Screen.width - 120;
            int yPos = 3;

            if (GUI.Button(new Rect(xPos - 5, yPos + 4, 26, 26), "", guiManager.blankButtonStyle))
            {
                TimeManager.getInstance().pause();
            }
            // Play
            if (GUI.Button(new Rect(xPos + 27, yPos + 4, 26, 26), "", guiManager.blankButtonStyle))
            {
                TimeManager.getInstance().play(1.0f);
            }
            // Double Speed
            if (GUI.Button(new Rect(xPos + 58, yPos + 4, 26, 26), "", guiManager.blankButtonStyle))
            {
                TimeManager.getInstance().play(2.0f);
            }
            // Advance 1 Frame
            if (GUI.Button(new Rect(xPos + 84, yPos + 4, 26, 26), "", guiManager.blankButtonStyle))
            {
                TimeManager.getInstance().pause();
                //StartCoroutine(TimeManager.getInstance().AdvanceOneFrame());
            }

            if (Time.timeScale == 0.0f)
                GUI.DrawTexture(new Rect(xPos - 8, yPos + 1, 32, 32), guiManager.gameSpeedSelector);
            else if (Time.timeScale == 1.0f)
                GUI.DrawTexture(new Rect(xPos + 26, yPos + 1, 32, 32), guiManager.gameSpeedSelector);
            else
                GUI.DrawTexture(new Rect(xPos + 57, yPos + 1, 32, 32), guiManager.gameSpeedSelector);

            GUI.DrawTexture(new Rect(xPos - 20, yPos + 5, 128, 24), guiManager.gameSpeedButtons);
            #endregion

            Rect notificationWindow = new Rect(outside.x - 400 - 5, 45, 400, 300);
            Rect notificationButton = new Rect(outside.x - 32 - 5, 4, 32, 32);
            GUI.DrawTexture(notificationButton, isNotificationWindowOpen ? notificationIconHover : NotificationLogs.Count == lastCheckedLength ? notificationIcon : notificationIconUnread);
            if (GUI.Button(notificationButton, "", guiManager.blankButtonStyle))
            {
                isNotificationWindowOpen = !isNotificationWindowOpen;
            }

            if (isNotificationWindowOpen)
            {
                lastCheckedLength = NotificationLogs.Count;

                GUI.DrawTextureWithTexCoords(notificationWindow, guiManager.windowBG, new Rect(0, 0, notificationWindow.width / guiManager.windowBG.width, notificationWindow.height / guiManager.windowBG.height));
                GUI.Box(notificationWindow, string.Empty, guiManager.windowBoxStyle);
                GUILayout.BeginArea(new Rect(notificationWindow.x + 5, notificationWindow.y + 5, notificationWindow.width - 10, notificationWindow.height - 10));
                GUI.skin.verticalScrollbar = Styles.verticalScrollStyle;
                GUI.skin.verticalScrollbarThumb = Styles.verticalScrollThumbStyle;
                scrollView = GUILayout.BeginScrollView(scrollView, false, true, GUIStyle.none, Styles.verticalScrollStyle);
                GUILayout.BeginVertical();
                    for (int i = 0; i < NotificationLogs.Count; i++)
                    {
                        GUILayout.Box(NotificationLogs[i], Styles.unreadBoxStyle);
                        //scrollView = new Vector2(0, 1000 * i);
                    }
                GUILayout.EndVertical();
                GUILayout.EndScrollView();
                GUI.skin = guiManager.skin;
                GUILayout.EndArea();
            }

            if (notificationButton.Contains(Event.current.mousePosition) || notificationWindow.Contains(Event.current.mousePosition) || outside.Contains(Event.current.mousePosition))
                guiManager.mouseInGUI = true;
            if (notificationButton.Contains(Event.current.mousePosition))
                DrawToolTip("Notifications");
        }

        private static void ToolsGroupUI(bool enabled = true)
        {
            if (!enabled) return;

            Rect outside = new Rect(5, Screen.height - 155 - 5, 60, 155);

            GUI.DrawTextureWithTexCoords(outside, guiManager.windowBG, new Rect(0, 0, outside.width / guiManager.windowBG.width, outside.height / guiManager.windowBG.height));
            GUI.Box(outside, "", guiManager.windowBoxStyle);
            
            Rect buttonMineRect = new Rect(15f, Screen.height - 140 - 7f, 40, 40);
            if (guiManager.DrawButton(buttonMineRect, string.Empty, isDigging))
            {
                isDigging = !isDigging;
                isBuildMenuOpen = false;
                isChopping = false;

                if (isDigging)
                {
                    guiManager.controllerObj.GetComponent<ControlPlayer>().StartDesigning(eDesignType.MINE);
                }
                else
                    guiManager.controllerObj.GetComponent<ControlPlayer>().CancelDesigning(true);
            }
            GUI.DrawTexture(new Rect(buttonMineRect.x + 5, buttonMineRect.y + 5f, 32, 32), mineOrDigIcon);

            Rect buttonTreeChopRect = new Rect(15f, Screen.height - 95 - 7f, 40, 40);
            if (guiManager.DrawButton(buttonTreeChopRect, string.Empty, isChopping))
            {
                isChopping = !isChopping;
                isBuildMenuOpen = false;
                isDigging = false;

                if (isChopping)
                {
                    guiManager.controllerObj.GetComponent<ControlPlayer>().StartDesigning(eDesignType.CHOP);
                }
                else
                    guiManager.controllerObj.GetComponent<ControlPlayer>().CancelDesigning(true);
            }
            GUI.DrawTexture(new Rect(buttonTreeChopRect.x + 5, buttonTreeChopRect.y + 5f, 32, 32), chopIcon);

            Rect buttonRect = new Rect(15f, Screen.height - 50 - 7f, 40, 40);
            if (guiManager.DrawButton(buttonRect, string.Empty, isBuildMenuOpen))
            {
                isBuildMenuOpen = !isBuildMenuOpen;
                isDigging = false;
                isChopping = false;
                
                if (!isBuildMenuOpen)
                    guiManager.controllerObj.GetComponent<ControlPlayer>().CancelDesigning(true);
            }
            GUI.DrawTexture(new Rect(buttonRect.x + 5, buttonRect.y + 5f, 32, 32), guiManager.button_cube);

            if (buttonMineRect.Contains(Event.current.mousePosition))
                DrawToolTip("Dig / Mine");
            if (buttonTreeChopRect.Contains(Event.current.mousePosition))
                DrawToolTip("Chop Trees");
            if (buttonRect.Contains(Event.current.mousePosition))
                DrawToolTip("Build Blocks");

            if (isBuildMenuOpen)
                BuildWindow(buttonRect);
        }

        public static void CancelDesign()
        {
            isBuildMenuOpen = false;
            isChopping = false;
            isDigging = false;
        }

        private static void DrawToolTip(string text)
        {
            GUI.skin.box.font = guiManager.textStyle.font;
            GUI.skin.box.fontSize = guiManager.textStyle.fontSize;
            //GUI.skin.box.normal.background = guiManager.boxStyle.normal.background;
            GUI.skin.box.normal.textColor = Color.white;
            GUI.skin.box.padding = new RectOffset(5, 5, 5, 5);
            GUI.skin.box.stretchWidth = false;
            GUI.skin.box.stretchHeight = false;

            Rect tooltip = new Rect(Input.mousePosition.x + 10, Screen.height + 10 - Input.mousePosition.y, 200, 32);
            Vector2 size = GUI.skin.box.CalcSize(new GUIContent(text));
            if (tooltip.y + size.y > Screen.height)
                tooltip.y = Screen.height - size.y;
            if (tooltip.x + size.x > Screen.width)
                tooltip.x = Screen.width - size.x;

            guiManager.mouseInGUI = true;
            GUILayout.BeginArea(tooltip);
            GUILayout.Box(text);
            GUILayout.EndArea();
        }

        private static void BuildWindow(Rect buttonRect)
        {
            DesignMenu menu = guiManager.GetComponent<DesignMenu>();

            BlockProperties.BlockGroup propList = menu.blockList[guiBlocks[BlockSelected].design][guiBlocks[BlockSelected].name];

            controller.StartDesigning(eDesignType.BUILD);
            controller.buildingMaterial = propList.full;
            controller.buildTile = (propList.full.getID());

            Rect buildWindowRect = new Rect(buttonRect.x + buttonRect.width + 5, Screen.height - 155, Screen.width - 45 - 300, 150);
            GUI.DrawTextureWithTexCoords(new Rect(buildWindowRect.x, buildWindowRect.y, buildWindowRect.width, buildWindowRect.height), guiManager.windowBG, new Rect(0, 0, buildWindowRect.width / guiManager.windowBG.width, buildWindowRect.height / guiManager.windowBG.height));
            GUI.Box(buildWindowRect, string.Empty, guiManager.windowBoxStyle);

            BlockSelected = GUI.SelectionGrid(buildWindowRect, BlockSelected, content, 15, Styles.SelectionGridStyle);

            if (buildWindowRect.Contains(Event.current.mousePosition))
                guiManager.mouseInGUI = true;
        }

        private static void CloseWindows()
        {
            isBuildMenuOpen = false;
            isNotificationWindowOpen = false;
        }

        //enum DaysOfWeek
        //{
        //    Moon = 0,
        //    Mars = 1,
        //    Mercury = 2,
        //    Jupiter = 3,
        //    Venus = 4,
        //    Saturn = 5,
        //    Sun = 6
        //}

        #region copyAndPaste
        //    void OnGUI()
        //    {
        //        //if (guiManager.inGame && newUI && !guiManager.gameOver)
        //        //{
        //        //    TimeControls();
        //        //    RightGroup();
        //        //    TimeKeeper();

        //        //    BuildUI();
        //        //}
        //    }

        //    enum DaysOfWeek
        //    {
        //        Moon = 0,
        //        Mars = 1,
        //        Mercury = 2,
        //        Jupiter = 3,
        //        Venus = 4,
        //        Saturn = 5,
        //        Sun = 6
        //    }

        //    Texture2D dawn;
        //    Texture2D latedawn;
        //    Texture2D noon;
        //    Texture2D earlydusk;
        //    Texture2D dusk;
        //    Texture2D earlymidnight;
        //    Texture2D midnight;
        //    Texture2D latemidnight;

        //    private void TimeKeeper()
        //    {
        //        float padding = 5f;

        //        Rect outside = new Rect(0, 0, 230f, 40f);
        //        Rect inside = new Rect(padding, 5f, outside.width - (padding * 2), outside.height - padding);

        //        GUI.DrawTextureWithTexCoords(outside, guiManager.windowBG, new Rect(0, 0, outside.width / guiManager.windowBG.width, outside.height / guiManager.windowBG.height));
        //        GUI.Box(outside, string.Empty, guiManager.windowBoxStyle);

        //        GUILayout.BeginArea(outside);
        //            // Days of the week
        //            GUILayout.BeginArea(inside);
        //                guiManager.DrawTextLeftWhite(inside, "Day of " + Enum.GetName(typeof(DaysOfWeek), (guiManager.day - 1) % 6));
        //            GUILayout.EndArea();

        //            GUILayout.BeginArea(new Rect(inside.width - 37, 0, 32, 32));
        //                float posX = 0;
        //                float posY = 0;
        //                switch(guiManager.timeOfDay)
        //                {
        //                    case "Dawn":
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), dawn);
        //                        break;
        //                    case "Early Morning":
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), latedawn);
        //                        break;
        //                    case "Morning":
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), latedawn);
        //                        break;
        //                    case "Late Morning":
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), noon);
        //                        break;                   
        //                    case "Midday":               
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), noon);
        //                        break;                   
        //                    case "Afternoon":            
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), noon);
        //                        break;                   
        //                    case "Late Afternoon":       
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), earlydusk);
        //                        break;                   
        //                    case "Dusk":                 
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), dusk);
        //                        break;                   
        //                    case "Evening":              
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), dusk);
        //                        break;                   
        //                    case "Night":                
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), earlymidnight);
        //                        break;                   
        //                    case "Midnight":             
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), midnight);
        //                        break;                   
        //                    case "Late Night":           
        //                        GUI.DrawTexture(new Rect(posX, posY, 32, 32), latemidnight);
        //                        break;
        //                }
        //            GUILayout.EndArea();
        //        GUILayout.EndArea();
        //    }

        //    private void RightGroup()
        //    {
        //        Texture2D letter = TextureLoader.GetImage("letter");
        //        letter.filterMode = FilterMode.Point;

        //        // If merchant is here
        //        //Rect rect = new Rect(Screen.width - 40, 50f, 32, 32);
        //        //GUI.DrawTexture(rect, guiManager.merchantTexure);

        //        Rect rect = new Rect(Screen.width - 185f, 5f, 32, 32);
        //        GUI.DrawTexture(rect, guiManager.tile_preferences);

        //        Rect rect2 = new Rect(Screen.width - 225f, 5f, 32, 32);
        //        GUI.DrawTexture(rect2, guiManager.tile_information);

        //        Rect rect3 = new Rect(Screen.width - 265f, 5f, 32, 32);
        //        GUI.DrawTexture(rect3, letter);

        //        if (UnreadNotificiations)
        //        {
        //            //GUI.DrawTexture(rect3, TextureLoader.GetImage("alert"));
        //        }
        //    }

        //    readonly float boxWidth = 140f;
        //    readonly float boxHeight = 40f;

        //    void TimeControls()
        //    {
        //        GUI.Window(243, new Rect(Screen.width - boxWidth, 0, boxWidth, boxHeight), TimeControlsWindow, string.Empty);
        //    }

        //    void TimeControlsWindow(int windowID)
        //    {
        //        GUI.DrawTextureWithTexCoords(new Rect(0, 0, boxWidth, boxHeight), guiManager.windowBG, new Rect(0, 0, boxWidth / guiManager.windowBG.width, boxHeight / guiManager.windowBG.height));
        //        GUI.Box(new Rect(0, 0, boxWidth, boxHeight), string.Empty, guiManager.windowBoxStyle);

        //        int xPos = 20;
        //        int yPos = 44;
        //        if (Screen.width > 1520) yPos -= 40;

        //        // Pause
        //        if (GUI.Button(new Rect(xPos - 5, yPos + 4, 26, 26), "", guiManager.blankButtonStyle))
        //        {
        //            TimeManager.getInstance().pause();
        //            audio.PlayOneShot(PlayList.sound, 100f);
        //        }
        //        // Play
        //        if (GUI.Button(new Rect(xPos + 27, yPos + 4, 26, 26), "", guiManager.blankButtonStyle))
        //        {
        //            TimeManager.getInstance().play(1.0f);
        //            audio.PlayOneShot(PlayList.sound, 100f);
        //        }
        //        // Double Speed
        //        if (GUI.Button(new Rect(xPos + 58, yPos + 4, 26, 26), "", guiManager.blankButtonStyle))
        //        {
        //            TimeManager.getInstance().play(100.0f);
        //            audio.PlayOneShot(PlayList.sound, 100f);
        //        }
        //        // Advance 1 Frame
        //        if (GUI.Button(new Rect(xPos + 84, yPos + 4, 26, 26), "", guiManager.blankButtonStyle))
        //        {
        //            TimeManager.getInstance().pause();
        //            audio.PlayOneShot(PlayList.sound, 100f);
        //            StartCoroutine(TimeManager.getInstance().AdvanceOneFrame());
        //        }


        //        if (Time.timeScale == 0.0f)
        //            GUI.DrawTexture(new Rect(xPos - 8, yPos + 1, 32, 32), guiManager.gameSpeedSelector);
        //        else if (Time.timeScale == 1.0f)
        //            GUI.DrawTexture(new Rect(xPos + 26, yPos + 1, 32, 32), guiManager.gameSpeedSelector);
        //        else
        //            GUI.DrawTexture(new Rect(xPos + 57, yPos + 1, 32, 32), guiManager.gameSpeedSelector);

        //        GUI.DrawTexture(new Rect(xPos - 20, yPos + 5, 128, 24), guiManager.gameSpeedButtons);

        //        Rect rect = new Rect(xPos - 8, yPos - 2, 110, 28);
        //        if (rect.Contains(Event.current.mousePosition))
        //            guiManager.mouseInGUI = true;
        //    }

        //    private bool IsBuildMenuOpen = false;
        //    Rect buildWindowRect;

        //    void BuildUI()
        //    {
        //        float paddingRight = 300f;
        //        float paddingDown = 7f;

        //        float width = 52f;
        //        float height = 200f;

        //        buildWindowRect = new Rect(width, Screen.height - height - paddingDown, Screen.width - width - paddingRight, height);
        //        Rect buttonRect = new Rect(5f, Screen.height - 40 - 7f, 40, 40);

        //        if (guiManager.DrawButton(buttonRect, string.Empty, IsBuildMenuOpen))
        //        {
        //            audio.PlayOneShot(PlayList.sound);
        //            IsBuildMenuOpen = !IsBuildMenuOpen;
        //        }

        //        if (buttonRect.Contains(Event.current.mousePosition))
        //            guiManager.mouseInGUI = true;

        //        Rect cubeTex = new Rect(10f, Screen.height - 32 - 10f, 32, 32);
        //        GUI.DrawTexture(cubeTex, guiManager.button_cube);

        //        if (IsBuildMenuOpen)
        //        {
        //            GUI.Window(234, buildWindowRect, BuildMenu, string.Empty);

        //            if (buildWindowRect.Contains(Event.current.mousePosition))
        //                guiManager.mouseInGUI = true;
        //        }
        //    }

        //    string searchValue = "Search..";

        //    int selected;

        //    private Texture2D[] textures;

        //    void Start()
        //    {
        //        Texture2D all = TextureLoader.GetImage("all");
        //        Texture2D roofing = TextureLoader.GetImage("roofing");
        //        Texture2D walls = TextureLoader.GetImage("walls");
        //        Texture2D flooring = TextureLoader.GetImage("flooring");
        //        Texture2D fencing = TextureLoader.GetImage("fencing");
        //        Texture2D terrain = TextureLoader.GetImage("terrain");
        //        Texture2D utility = TextureLoader.GetImage("utility");

        //        dawn = TextureLoader.GetImage("dawn");
        //        latedawn = TextureLoader.GetImage("latedawn");
        //        noon = TextureLoader.GetImage("noon");
        //        earlydusk = TextureLoader.GetImage("earlydusk");
        //        dusk = TextureLoader.GetImage("dusk");
        //        earlymidnight = TextureLoader.GetImage("earlymidnight");
        //        midnight = TextureLoader.GetImage("midnight");
        //        latemidnight = TextureLoader.GetImage("latemidnight");

        //        textures = new Texture2D[7]
        //        {
        //            all,
        //            roofing,
        //            walls,
        //            flooring,
        //            fencing,
        //            terrain,
        //            utility
        //        };

        //        if (newUI)
        //        {
        //            guiManager.GetComponent<MainMenus>().enabled = !guiManager.GetComponent<MainMenus>().enabled;
        //            guiManager.GetComponent<NotificationWindow>().enabled = !guiManager.GetComponent<NotificationWindow>().enabled;
        //        }

        //        if (!AudioSourceSpawned && newUI)
        //        {
        //            GameObject gameObject = new GameObject();
        //            gameObject.AddComponent<AudioSource>();

        //            gameObject.transform.position = Camera.main.transform.position;

        //            gameObject.transform.parent = Camera.main.transform;
        //            audio = gameObject.GetComponent<AudioSource>();

        //            AudioSourceSpawned = true;
        //        }
        //    }

        //    void BuildMenu(int windowID)
        //    {
        //        GUI.DrawTextureWithTexCoords(new Rect(0, 0, buildWindowRect.width, buildWindowRect.height), guiManager.windowBG, new Rect(0, 0, buildWindowRect.width / guiManager.windowBG.width, buildWindowRect.height / guiManager.windowBG.height));

        //        Rect leftBoxGroup = new Rect(0, 30f, buildWindowRect.width, 40f);
        //        GUI.Box(leftBoxGroup, string.Empty, guiManager.boxStyle);

        //        Rect leftBoxInside = new Rect(5f, 17f, leftBoxGroup.width - 200f, leftBoxGroup.height + 10f);


        //        GUILayout.BeginArea(leftBoxInside);
        //            GUI.DrawTexture(new Rect((leftBoxInside.x - 5f) + 32 * selected, leftBoxInside.y - 5f, 32*1.3f, 32*1.3f), guiManager.gameSpeedSelector);
        //            GUILayout.BeginHorizontal();
        //            GUILayout.BeginArea(new Rect(leftBoxInside.x, leftBoxInside.y, 32 * 7, 32));
        //                // FILTER
        //                selected = GUILayout.Toolbar(selected, textures, guiManager.blankButtonStyle);
        //            GUILayout.EndArea();

        //            GUILayout.BeginArea(new Rect(leftBoxInside.x + 32 * 7 + 15f, leftBoxInside.y, 200f, leftBoxInside.height + 10f));
        //                GUILayout.Space(5f);
        //                // SEARCH
        //                searchValue = GUILayout.TextField(searchValue, guiManager.blankButtonStyle);
        //                if (searchValue == "" && (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))) searchValue = "Search..";
        //            GUILayout.EndArea();
        //            GUILayout.EndHorizontal();
        //        GUILayout.EndArea();


        //        GUILayout.BeginArea(new Rect(5f + leftBoxGroup.width, leftBoxInside.y, leftBoxGroup.width, leftBoxInside.height));
        //            //switch (selected)
        //            //{
        //            //    case 0:
        //            //    GUILayout.SelectionGrid()
        //            //    break;
        //            //}
        //        GUILayout.EndArea();

        //        // BlockTexture
        //        //Rect rect = new Rect(Screen.width - 40, 50f, 32, 32);
        //        //GUI.DrawTexture(rect, guiManager.constructionTexture);

        //        GUI.Box(new Rect(0, 0, buildWindowRect.width, buildWindowRect.height), string.Empty, guiManager.windowStyle);
        //    }
        #endregion
    }
}
