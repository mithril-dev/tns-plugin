﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Timber_and_Stone.Event;
using TNSPlugin.Events;
using UnityEngine;

namespace TNSPlugin
{
    public class GUIInGameSettings : MonoBehaviour
    {
        private static readonly GUIManager guiManager = GUIManager.getInstance();

        void OnGUI()
        {
            if ((guiManager.inGame && !guiManager.gameOver))
            {
                Rect window = new Rect(Screen.width / 2 - 120, 120, 210, 344);

                if (guiManager.escapeMenu == 28)
                {
                    if (guiManager.DrawButton(new Rect(window.xMin + 20, window.yMin + 222, 170, 28), "World Settings"))
                    {
                        guiManager.escapeMenu = 9;
                    }
                }

                if (guiManager.escapeMenu == 9)
                {
                    Rect settingRect = new Rect(Screen.width / 2 - 180, 120, 360, 160);
                    Rect difficultyRect = new Rect(settingRect.x + 32, settingRect.y + 38, 150, 30);

                    guiManager.DrawWindow(settingRect, "World Settings:", false);

                    guiManager.DrawTextLeftWhite(difficultyRect, "Difficulty:");

                    if (guiManager.DrawRadioButton(new Rect(settingRect.x + 32, difficultyRect.y + 24, 120, 24), "Peaceful", GUINewGameSettings.difficulty == Difficulty.Peaceful))
                    {
                        EventChangedDifficulty evt = new EventChangedDifficulty(Difficulty.Peaceful);
                        EventManager.getInstance().InvokePre(evt);
                        EventManager.getInstance().InvokePost(evt);
                    }
                    if (guiManager.DrawRadioButton(new Rect(settingRect.x + 160, difficultyRect.y + 24, 80, 24), "Hard", GUINewGameSettings.difficulty == Difficulty.Hard))
                    {
                        EventChangedDifficulty evt = new EventChangedDifficulty(Difficulty.Hard);
                        EventManager.getInstance().InvokePre(evt);
                        EventManager.getInstance().InvokePost(evt);
                    }

                    if (guiManager.DrawCheckBox(new Rect(settingRect.x + 32, difficultyRect.y + 48, 160, 24), "Fog Of War", ref GUINewGameSettings.fogOfWar))
                    {
                        FogOfWar.EnablePostProcessing(GUINewGameSettings.fogOfWar);
                    }
                    if (guiManager.DrawButton(new Rect(((settingRect.x + (settingRect.width / 2) - (85 / 2))), (settingRect.height + settingRect.y) - 48, 85, 28), "Back"))
                    {
                        guiManager.escapeMenu = 0;
                    }

                    if (window.Contains(Event.current.mousePosition))
                    {
                        guiManager.mouseInGUI = true;
                    }
                }
            }
        }
    }
}
