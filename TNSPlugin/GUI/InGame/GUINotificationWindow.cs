﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Timber_and_Stone.API.Event;
using Timber_and_Stone.Event;
using UnityEngine;

namespace TNSPlugin
{
    public class GUINotificationWindow : MonoBehaviour
    {
        private bool isWindowOpen = true;
        NotificationWindow window;

        void Start()
        {
            window = FindObjectOfType<NotificationWindow>();
        }

        public void OnGUI()
        {
            if (!GUIManager.getInstance().inGame) return;

            Rect chatbox = new Rect(4, Screen.height - window.windowSize.y - 32, window.windowSize.x + 12, window.windowSize.y + 30);
            Rect top = new Rect(7, chatbox.y - 4, 20, 20);
            Rect bot = new Rect(7, (chatbox.y + chatbox.height) - 24, 20, 20);
            if (GUI.Button(isWindowOpen ? top : bot, "", isWindowOpen ? GUIManager.getInstance().closeWindowButtonStyle : GUIManager.getInstance().checkBoxStyle))
            {
                window.enabled = !window.enabled;
                isWindowOpen = window.enabled;
                AEvent evt = new EventButtonPressed();
                EventManager.getInstance().InvokePre(evt);
                EventManager.getInstance().InvokePost(evt);
            }
        }
    }
}
