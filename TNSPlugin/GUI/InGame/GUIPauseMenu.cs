﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TNSPlugin
{
	class GUIPauseMenu : MonoBehaviour
	{
		GUIManager guiManager;

		void Start()
		{
			guiManager = GUIManager.getInstance();
		}

		void Update()
		{

		}

		void OnGUI()
		{
			if (guiManager.escapeMenu == 0)
			{
				guiManager.escapeMenu = 28;
			}

			if (guiManager.escapeMenu == 28)
			{
				Rect window = new Rect(Screen.width / 2 - 120, 120, 210, 344);
				guiManager.DrawWindow(window, "Menu", false);

				if (guiManager.DrawButton(new Rect(window.xMin + 20, window.yMin + 48, 170, 28), "Resume Game"))
				{
					guiManager.escapeMenu = -1;
					if (!guiManager.escapePaused)
						TimeManager.getInstance().play();
				}

				// 'X' button
				if (GUI.Button(new Rect(window.xMax - 24, window.yMin + 4, 20, 18), "", guiManager.closeWindowButtonStyle))
				{
					guiManager.escapeMenu = -1;
					if (!guiManager.escapePaused)
						TimeManager.getInstance().play();
				}

				if (guiManager.DrawButton(new Rect(window.xMin + 20, window.yMin + 102, 170, 28), "Options"))
					guiManager.escapeMenu = 1;
				if (guiManager.DrawButton(new Rect(window.xMin + 20, window.yMin + 142, 170, 28), "Controls"))
					guiManager.escapeMenu = 2;
				guiManager.DrawButton(new Rect(window.xMin + 20, window.yMin + 182, 170, 28), "Statistics");

				if (guiManager.DrawButton(new Rect(window.xMin + 20, window.yMin + 282, 170, 28), "Exit Game"))
					guiManager.escapeMenu = 6;

				if (window.Contains(Event.current.mousePosition))
				{
					guiManager.mouseInGUI = true;
				}
			}
		}
	}
}
