﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TNSPlugin.Utilities;
using UnityEngine;

namespace TNSPlugin
{
    public class GUIResourceMenu : MonoBehaviour
    {
        private static readonly GUIManager guiManager = GUIManager.getInstance();
        private Resource selectedResource;

        void Start()
        {
            GUIResourceMenu[] objects = FindObjectsOfType<GUIResourceMenu>();
            if (objects.Length > 1)
            {
                Destroy(this);
            }
        }

        Rect window = new Rect(50, 50, 600, 400);
        void OnGUI()
        {
            window = GUI.Window(21507, window, ResourceWindow, "");
        }

        Vector2 LeftAreaScroll;
        Vector2 RightAreaScroll;
        Vector2 BottomAreaScroll;
        void ResourceWindow(int windowID)
        {
            GUI.DrawTextureWithTexCoords(new Rect(0, 0, window.width, window.height), guiManager.windowBG, new Rect(0, 0, window.width / guiManager.windowBG.width, window.height / guiManager.windowBG.height));
            GUI.Box(new Rect(0, 0, window.width, window.height), "", guiManager.windowStyle);
            guiManager.DrawTextCenteredWhite(new Rect(0, 0 + 4, window.width, 28), "Resource Menu");
            {
                {
                    Rect LeftArea = new Rect(12, 40 + 28 + 32, (window.width / 2) - 24, window.height - (52 + 32 + 28 + 100));
                    GUI.Box(new Rect(0, 28, (window.width / 2), window.height - (28 + 100)), "", guiManager.windowStyle);
                    guiManager.DrawTextCenteredWhite(new Rect(0, 32, window.width / 2, 32), "Resources");
                    GUI.Box(new Rect(LeftArea.x, LeftArea.y - 32, LeftArea.width - 4, 24), "", guiManager.boxStyle);
                    //guiManager.DrawTextLeftWhite(new Rect(LeftArea.x + 4, LeftArea.y - 32, LeftArea.width - 4, 32), "Boxes_gj");
                    GUI.Box(new Rect(LeftArea.x, LeftArea.y, LeftArea.width - 4, LeftArea.height), "", guiManager.boxStyle);
                    GUI.Box(new Rect(LeftArea.x - 4, LeftArea.y - 4, LeftArea.width + 7, LeftArea.height + 8), "", guiManager.windowBoxStyle);
                    GUILayout.BeginArea(LeftArea);
                    GUI.skin.verticalScrollbar = Styles.verticalScrollStyle;
                    GUI.skin.verticalScrollbarThumb = Styles.verticalScrollThumbStyle;
                    LeftAreaScroll = GUILayout.BeginScrollView(LeftAreaScroll);
                    {
                        foreach (Resource resource in ResourceManager.getInstance().resources)
                        {
                            if (resource != null && resource.icon != null)
                            {
                                GUILayout.BeginHorizontal();
                                    if (GUILayout.Button(resource.icon, guiManager.hiddenButtonStyle))
                                    {
                                        selectedResource = resource;
                                    }
                                    GUILayout.Label(resource.name, guiManager.textStyle);
                                GUILayout.EndHorizontal();
                            }
                        }
                    }
                    GUILayout.EndScrollView();
                    GUILayout.EndArea();

                    Rect RightArea = new Rect((window.width / 2) + 12, 40 + 28, (window.width / 2) - 24, window.height - (52 + 28 + 100));
                    GUI.Box(new Rect(window.width / 2, 28, window.width / 2, window.height - (28 + 100)), "", guiManager.windowStyle);
                    guiManager.DrawTextCenteredWhite(new Rect(window.width / 2, 32, window.width / 2, 32), "Focused Resource");
                    GUILayout.BeginArea(RightArea);
                    RightAreaScroll = GUILayout.BeginScrollView(RightAreaScroll);
                    {
                        if (selectedResource != null)
                        {
                            GUILayout.BeginHorizontal();
                            GUILayout.Label(selectedResource.icon);
                            GUILayout.Label(selectedResource.name, guiManager.rightTextStyle);
                            GUILayout.EndHorizontal();

                            if (selectedResource.storageIndex == Timber_and_Stone.StorageType.Armor)
                            {
                                Armor res = ((Armor)selectedResource);

                                string strength = res.heavy ? "Heavy" : "Light";
                                GUILayout.Label($"{string.Concat(res.armorType.ToString())} ({strength})", guiManager.rightTextStyle);
                                GUILayout.Label($"{res.damageReduction * 100}% Damage Protection", guiManager.rightTextStyle);
                            }

                            GUILayout.Label($"{selectedResource.mass} Mass ({selectedResource.storageIndex.ToString()})", guiManager.rightTextStyle);
                        }
                    }
                    GUILayout.EndScrollView();
                    GUILayout.EndArea();
                }

                Rect BottomArea = new Rect(0, window.height - (100), window.width, window.height - (28 + 100));
                GUI.Box(BottomArea, "", guiManager.windowBoxStyle);
                GUILayout.BeginArea(BottomArea);
                BottomAreaScroll = GUILayout.BeginScrollView(BottomAreaScroll);
                {

                }
                GUILayout.EndScrollView();
                GUILayout.EndArea();
            }
            GUI.DragWindow(new Rect(0, 0, 100000, 28));
        }
    }
}
