﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Timber_and_Stone.API;
using Timber_and_Stone.Blocks;
using MithrilAPI.Debug;
using UnityEngine;

namespace TNSPlugin
{
    public class GUILoad : MonoBehaviour
    {
        public static List<GUIBlocks> GUIBlockIcons { get; private set; } = new List<GUIBlocks>();

        //private WorldManager.SaveFileData[] saveDataFiles;
        private static readonly GUIManager guiManager = AManager<GUIManager>.getInstance();

        private bool ResourcesBeingLoaded = false;
        private bool ResourcesLoaded = false;

        void Start()
        {
            this.gameObject.GetComponent<GUIMainMenu>().enabled = false;
            guiManager.startMenu = "LoadResources";
        }

        void OnGUI()
        {
            guiManager.DrawTextCenteredWhite(new Rect(Screen.width / 2 - 500/2, Screen.height / 2 - 24, 500, 24), "Loading Resources");
            
            if (!ResourcesBeingLoaded)
            {
                ResourcesBeingLoaded = true;
                StartCoroutine(LoadResources());
            }

            if (ResourcesLoaded)
            {
                this.gameObject.GetComponent<GUIMainMenu>().enabled = true;
                guiManager.startMenu = "Main";
                GUIInGame.IsUsingNewUI = Config.config.UsingNewUI;

                this.enabled = false;
            }
        }

        IEnumerator LoadGame(float delay, string name)
        {
            yield return new WaitForSeconds(delay);
            WorldManager.getInstance().LoadGame(name);
            guiManager.inGame = true;
            guiManager.controllerObj.GetComponent<ControlPlayer>().SwitchCamera();
            guiManager.controllerObj.GetComponent<ControlPlayer>().SwitchCamera();
        }

        IEnumerator LoadResources()
        {
            Transform cube = guiManager.cube;
            //SingleBlockRender sbr = new SingleBlockRender(guiManager.cube.gameObject);
            ControlPlayer controller = guiManager.controllerObj.GetComponent<ControlPlayer>();

            for (int i = 1; i < BlockProperties.blocks.Length - 1; i++)
            {
                yield return new WaitForEndOfFrame();
                
                BlockProperties prop = BlockProperties.blocks[i];

                if (i == 2 || i == 3 || i == 5 || i == 7 || i == 9 || i == 11 || i == 12)
                    continue;

                if ((i > 34 && i != 60) && i < 66)
                    continue;

                if (i > 72)
                    break;

                cube.localEulerAngles = new Vector3(0, -45, 0);

                try
                {
                    BlockProperties[,,] buildingBlocks; IBlockData[,,][] buildingData;
                    prop.getPreviewBlocks(controller.buildingVariationIndex, out buildingBlocks, out buildingData);
                    //sbr.SetBlocks(buildingBlocks, buildingData);
                }
                catch (Exception ex)
                {
                    DebugConsole.LogError($"[SBR]: {ex}");
                }

                cube.GetComponent<MeshRenderer>().sharedMaterials = ChunkManager.getInstance().materials;

                StartCoroutine(SaveIconToList(0.1f, prop.getName(), prop.getConstructionType(), prop.getID()));
            }

            ResourcesLoaded = true;
        }

        IEnumerator SaveIconToList(float delay, string name, eConstructionType eConstructionType, int id)
        {
            yield return new WaitForEndOfFrame();

            RenderTexture tmp = RenderTexture.GetTemporary(128, 128, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
            Graphics.Blit(guiManager.constructionTexture, tmp);

            RenderTexture previous = RenderTexture.active;
            RenderTexture.active = tmp;

            Texture2D texture = new Texture2D(128, 128);
            texture.ReadPixels(new Rect(0, 0, tmp.width, tmp.height), 0, 0);
            texture.Apply();

            RenderTexture.active = previous;
            RenderTexture.ReleaseTemporary(tmp);

            GUIBlockIcons.Add(new GUIBlocks()
            {
                name = name,
                texture = texture,
                design = eConstructionType,
                id = id
            });
        }

        public struct GUIBlocks
        {
            public Texture2D texture { get; set; }
            public string name { get; set; }
            public eConstructionType design { get; set; }
            public int id { get; set; }
        }
    }
}