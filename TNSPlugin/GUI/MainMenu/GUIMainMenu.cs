﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using MithrilAPI.Debug;
using Timber_and_Stone;
using System.Reflection;

namespace TNSPlugin
{
    public class GUIMainMenu : MonoBehaviour//, IGUI 
    {
        private static readonly GUIManager guiManager = AManager<GUIManager>.getInstance();
        private static readonly MapManager mapManager = AManager<MapManager>.getInstance();
        private Texture2D discordTexture;
        private Texture2D[] LogoAnimations;
        private Texture2D[] backgroundImages;
        private int moveAnim = 0;
        private int moveBackground = 0;
        
        public void Start()
        {
            guiManager.logo = null;
            
            discordTexture = TextureHandler.ConvertToTexture2D("DiscordIcon");
            discordTexture.filterMode = FilterMode.Point;

            List<Texture2D> iconTextures = new List<Texture2D>();
            List<Texture2D> backgroundTextures = new List<Texture2D>();

            for (int i = 0; i < 15; i++)
            {
                string name = $"LogoShineImg{i}";
                Texture2D texture = TextureHandler.ConvertToTexture2D(name);

                texture.filterMode = FilterMode.Point;
                iconTextures.Add(texture);
            }

            for (int i = 0; i < 2; i++)
            {
                string name = $"BackgroundIMG{i + 1}";
                Texture2D texture = TextureHandler.ConvertToTexture2D(name);

                backgroundTextures.Add(texture);
            }

            LogoAnimations = iconTextures.ToArray();
            backgroundImages = backgroundTextures.ToArray();

            StartCoroutine("animate");
            StartCoroutine("backgroundSwitch");
        }

        public void OnGUI()
        {
            if (guiManager.inStartMenu && guiManager.selectedBlock == null && !mapManager.showMap && !guiManager.inGame)
            {
                // Background image
                GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundImages[moveBackground]);
                GUI.DrawTexture(new Rect((Screen.width / 2) - ((LogoAnimations[0].width * 4) / 2), 20, LogoAnimations[0].width * 4, LogoAnimations[0].height * 4), LogoAnimations[moveAnim]);

                // Discord button / logo
                Rect discord = new Rect(Screen.width - 125f, Screen.height - 125f, 80f, 80f);
                GUI.DrawTexture(discord, discordTexture);
                if (GUI.Button(discord, string.Empty, guiManager.blankButtonStyle))
                {
                    Application.OpenURL("https://discord.gg/yU2pUU7Ee2");
                }

                // This will be moved to a mod settings button section on the main menu.
                //if (GUI.Button(new Rect(25, 25, 140, 25), string.Empty, guiManager.blankButtonStyle))
                //{
                //    GUIInGame.IsUsingNewUI = !GUIInGame.IsUsingNewUI;
                //    //DebugConsole.Log("TEST");
                //    try
                //    {
                //        Config.SaveSettings();
                //    }
                //    catch (Exception ex)
                //    {
                //        DebugConsole.LogError(ex.GetType().ToString());
                //    }
                //}
                //guiManager.DrawTextLeftWhite(new Rect(25 + 25, 25, 140, 25), "Use New UI");
                //guiManager.DrawCheckBox(new Rect(25, 25, 140, 25), string.Empty, ref GUIInGame.IsUsingNewUI);

                //GUI.Box(new Rect(0, 0, Screen.width, Screen.height), string.Empty);

                if (guiManager.startMenu == "Main")
                {
                    guiManager.startMenu = "MithrilMain";
                }
                else if (guiManager.startMenu == "MithrilMain")
                {
                    guiManager.DrawTextLeftWhite(new Rect(8, Screen.height - 56, 600, 28 * 2), "Mithril Mod " + Assembly.GetCallingAssembly().GetName().Version.ToString());

                    Rect newButton;
                    newButton = new Rect((Screen.width / 2 - 140), 342, 280, 36).Contains(Event.current.mousePosition) ?
                        newButton = new Rect((Screen.width / 2 - 140) - 5, 342 - 5, 280 + 10, 36 + 10) : newButton = new Rect(Screen.width / 2 - 140, 342, 280, 36);

                    Rect loadButton;
                    loadButton = new Rect((Screen.width / 2 - 140), 386, 280, 36).Contains(Event.current.mousePosition) ?
                        loadButton = new Rect((Screen.width / 2 - 140) - 5, 386 - 5, 280 + 10, 36 + 10) : loadButton = new Rect(Screen.width / 2 - 140, 386, 280, 36);

                    Rect optionsButton;
                    optionsButton = new Rect((Screen.width / 2 - 140), 430, 280, 36).Contains(Event.current.mousePosition) ?
                        optionsButton = new Rect((Screen.width / 2 - 140) - 5, 430 - 5, 280 + 10, 36 + 10) : optionsButton = new Rect(Screen.width / 2 - 140, 430, 280, 36);

                    Rect controlsButton;
                    controlsButton = new Rect((Screen.width / 2 - 140), 474, 280, 36).Contains(Event.current.mousePosition) ?
                        controlsButton = new Rect((Screen.width / 2 - 140) - 5, 474 - 5, 280 + 10, 36 + 10) : controlsButton = new Rect(Screen.width / 2 - 140, 474, 280, 36);

                    Rect creditsButton;
                    creditsButton = new Rect((Screen.width / 2 - 140), 518, 280, 36).Contains(Event.current.mousePosition) ?
                        creditsButton = new Rect((Screen.width / 2 - 140) - 5, 518 - 5, 280 + 10, 36 + 10) : creditsButton = new Rect(Screen.width / 2 - 140, 518, 280, 36);

                    Rect modsButton;
                    modsButton = new Rect((Screen.width / 2 - 140), 562, 280, 36).Contains(Event.current.mousePosition) ?
                        modsButton = new Rect((Screen.width / 2 - 140) - 5, 562 - 5, 280 + 10, 36 + 10) : modsButton = new Rect(Screen.width / 2 - 140, 562, 280, 36);

                    Rect quitButton;
                    quitButton = new Rect((Screen.width / 2 - 140), 650, 280, 36).Contains(Event.current.mousePosition) ?
                        quitButton = new Rect((Screen.width / 2 - 140) - 5, 650 - 5, 280 + 10, 36 + 10) : quitButton = new Rect(Screen.width / 2 - 140, 650, 280, 36);

                    if (guiManager.DrawButton(newButton, ""))
                    {
                        guiManager.startMenu = "New1";
                        guiManager.tempName = "New Settlement";
                    }

                    if (guiManager.DrawButton(loadButton, ""))
                    {
                        guiManager.startMenu = "Load";
                        guiManager.saveDataFiles = WorldManager.GetSaveDataFiles();
                    }
                    if (guiManager.DrawButton(optionsButton, ""))
                    {
                        guiManager.startMenu = "Options";
                    }
                    if (guiManager.DrawButton(controlsButton, ""))
                    {
                        guiManager.startMenu = "Controls";
                    }
                    if (guiManager.DrawButton(creditsButton, ""))
                    {
                        guiManager.startMenu = "Credits";
                        guiManager.creditsScrollPosition = Vector2.zero;
                    }
                    if (guiManager.DrawButton(modsButton, ""))
                    {
                        guiManager.startMenu = "Mods";
                        guiManager.creditsScrollPosition = Vector2.zero;
                    }

                    if (guiManager.DrawButton(quitButton, ""))
                    {
                        Application.Quit();
                    }

                    guiManager.DrawTextCenteredBlack(new Rect(0, 350, Screen.width, 24), "New Game");
                    guiManager.DrawTextCenteredBlack(new Rect(0, 394, Screen.width, 24), "Load Game");
                    guiManager.DrawTextCenteredBlack(new Rect(0, 438, Screen.width, 24), "Options");
                    guiManager.DrawTextCenteredBlack(new Rect(0, 482, Screen.width, 24), "Controls");
                    guiManager.DrawTextCenteredBlack(new Rect(0, 526, Screen.width, 24), "Credits");
                    guiManager.DrawTextCenteredBlack(new Rect(0, 570, Screen.width, 24), "Mods");
                    guiManager.DrawTextCenteredBlack(new Rect(0, 658, Screen.width, 24), "Exit");
                }
            }
        }

        IEnumerator animate()
        {
            for (moveAnim = 0; moveAnim < LogoAnimations.Length + 1; moveAnim++)
            {
                if (moveAnim == LogoAnimations.Length)
                {
                    moveAnim = 0;
                    yield return new WaitForSeconds(UnityEngine.Random.Range(3, 7));
                }
                yield return new WaitForSeconds(0.09f);
            }
        }
        IEnumerator backgroundSwitch()
        {
            for (moveBackground = 0; moveBackground < backgroundImages.Length + 1; moveBackground++)
            {
                if (moveBackground == backgroundImages.Length)
                {
                    moveBackground = 0;
                }
                
                yield return new WaitForSeconds(20);
            }
        }
    }
}
