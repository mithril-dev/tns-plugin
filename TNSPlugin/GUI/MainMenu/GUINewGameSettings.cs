﻿using Timber_and_Stone.Event;
using TNSPlugin.Events;
using UnityEngine;

namespace TNSPlugin
{
    public class GUINewGameSettings : MonoBehaviour
    {
        private static readonly GUIManager guiManager = AManager<GUIManager>.getInstance();

        public static Difficulty difficulty = Difficulty.Hard;
        public static bool fogOfWar = true;

        void Start()
        {
            
        }

        void OnGUI()
        {
            if (guiManager.startMenu == "New1")
            {
                Rect settingRect = new Rect(Screen.width / 2 - 180, 320 + 130, 360, 128);

                guiManager.DrawWindow(settingRect, "World Settings:", false);
                Rect difficultyRect = new Rect(settingRect.x + 32, settingRect.y + 38, 150, 30);

                guiManager.DrawTextLeftWhite(difficultyRect, "Difficulty:");

                if (guiManager.DrawRadioButton(new Rect(settingRect.x + 32, difficultyRect.y + 24, 120, 24), "Peaceful", difficulty == Difficulty.Peaceful))
                {
                    EventChangedDifficulty evt = new EventChangedDifficulty(Difficulty.Peaceful);
                    EventManager.getInstance().InvokePre(evt);
                    EventManager.getInstance().InvokePost(evt);
                }
                if (guiManager.DrawRadioButton(new Rect(settingRect.x + 160, difficultyRect.y + 24, 80, 24), "Hard", difficulty == Difficulty.Hard))
                {
                    EventChangedDifficulty evt = new EventChangedDifficulty(Difficulty.Hard);
                    EventManager.getInstance().InvokePre(evt);
                    EventManager.getInstance().InvokePost(evt);
                }
                //if (guiManager.DrawRadioButton(new Rect(settingRect.x + 250, difficultyRect.y + 24, 80, 24), "Hard", isHard))
                //{
                //    isPeaceful = false;
                //    isEasy = false;
                //    isHard = true;
                //}

                //guiManager.DrawTextLeftWhite(new Rect(difficultyRect.x, difficultyRect.y + 80, 150, 30), "Misc Settings:");

                if (guiManager.DrawCheckBox(new Rect(settingRect.x + 32, difficultyRect.y + 48, 160, 24), "Fog Of War", ref fogOfWar))
                {
                    FogOfWar.EnablePostProcessing(fogOfWar);
                }
            }
            
            if(guiManager.startMenu == "Main")
            {
                difficulty = Difficulty.Hard;
            }
        }
    }
}