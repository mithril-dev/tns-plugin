﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Timber_and_Stone.GUIUtil;
using UnityEngine;
using static System.Net.Mime.MediaTypeNames;

namespace TNSPlugin
{
    public class ModMenu : MonoBehaviour
    {
        Vector2 scrollPos;

        void OnGUI()
        {
            if (GUIManager.getInstance().startMenu != "Mods") return;
            Rect settingRect = new Rect(Screen.width / 2 - 290, 320 + 130, 580, 600);

            GUI.skin = GUIManager.getInstance().skin;
            GUILayout.BeginArea(settingRect);
            GUILayout.BeginScrollView(scrollPos);
            for (int i = 0; i < PluginManager.plugins.Count; i++)
            {
                var plugin = PluginManager.plugins[i];
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                    GUILayout.Label(plugin.ModName());
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                GUILayout.Label("v" + plugin.ModVersion().ToString());

                string stringSeparators = "\n";
                string lines = plugin.ModDesc().Replace(stringSeparators, "");
                lines = lines.Replace("\\n", "\n");
                GUILayout.Label(lines);
                GUILayout.Space(10);
            }
            GUILayout.EndScrollView();
            if (DrawLayoutGUI.Default_Layout_Button("Back"))
            {
                GUIManager.getInstance().startMenu = "Main";
            }
            GUILayout.EndArea();

        }
    }
}
