﻿using TNSPlugin.Blocks;
using TNSPlugin.GUIUtils;
using TNSPlugin.Sounds;
using TNSPlugin.Utilities;
using TNSPlugin.Weather;
using UnityEngine;

namespace TNSPlugin
{
    public class GUIHandler : MonoBehaviour
    {

        public void Awake()
        {
            PluginObject.Object.AddComponent<GUILoad>();
            PluginObject.Object.AddComponent<GUIEventHandler>();
            PluginObject.Object.AddComponent<GUINewGameSettings>();
            PluginObject.Object.AddComponent<ModMenu>();
            PluginObject.Object.AddComponent<GUIInGameSettings>();
            PluginObject.Object.AddComponent<GUIMainMenu>();
            PluginObject.Object.AddComponent<GUIHide>();
            //PluginObject.Object.AddComponent<GUIInGame>();
            //PluginObject.Object.AddComponent<GUIResourceMenu>();
            PluginObject.Object.AddComponent<GroupManager>();
            PluginObject.Object.AddComponent<GUIGroups>();
            PluginObject.Object.AddComponent<Cinematic_Camera.CinCamera>();
            PluginObject.Object.AddComponent<GUIPauseMenu>();
            PluginObject.Object.AddComponent<SaveGroup>();
            PluginObject.Object.AddComponent<RegisterBlocks>();
            PluginObject.Object.AddComponent<GUINotificationWindow>();
            //PluginObject.Object.AddComponent<ImportChair>(); 
            //PluginObject.Object.AddComponent<RainParticleSystem>();
            //PluginObject.Object.AddComponent<ImportStoneBench>();
        }
    }
}
