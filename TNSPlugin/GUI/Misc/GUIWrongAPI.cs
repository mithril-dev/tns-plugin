﻿using Timber_and_Stone;
using UnityEngine;

namespace TNSPlugin
{
    public class GUIWrongAPI : MonoBehaviour
    {
        Rect rect;

        public GUIWrongAPI()
        {
            float width = 240;
            float height = 32;

            rect = new Rect((Screen.width - width) / 2, 0, width, height);
        }

        public void DrawGUI()
        {
            GUI.Box(rect, string.Empty, GUIManager.getInstance().boxStyle);
            GUILayout.BeginArea(rect);
                GUILayout.BeginHorizontal();
                    GUILayout.Label("Mithril mod needs");
                    GUI.skin.button.normal.textColor = Color.magenta;
                    GUI.skin.button.hover.textColor = Color.magenta;
                    GUI.skin.button.active.textColor = Color.magenta;
                    GUI.skin.button.normal.background = null;
                    GUI.skin.button.hover.background = null;
                    GUI.skin.button.active.background = null;
                    GUI.skin.button.padding = new RectOffset(-4, 0, 4, 0);
                    if (GUILayout.Button("Mithril API"))
                    {
                        Application.OpenURL("https://www.curseforge.com/timber-and-stone/mods/mithril-api");
                    }
                    GUILayout.Label("to function.");
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }
    }
}