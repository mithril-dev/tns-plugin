﻿using System.Collections;
using UnityEngine;

namespace TNSPlugin.Groups
{
    public class BasicDespawner : MonoBehaviour
    {
        public APlayableEntity entity;

        // Update is called once per frame
        void Update()
        {
            Vector3 entPos = new Vector3(entity.transform.position.x, entity.transform.position.y + ChunkManager.getInstance().voxelSize, entity.transform.position.z);
            if (Vector3.Distance(entPos, this.transform.position) < ChunkManager.getInstance().voxelSize)
            {
                Destroy(this.gameObject);
            }
        }
    }
}