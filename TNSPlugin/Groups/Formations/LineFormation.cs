﻿using MithrilDebug.Debug.Cmd;
using System.Collections;
using System.Collections.Generic;
using Timber_and_Stone;
using Timber_and_Stone.Tasks;
using TNSPlugin.Groups;
using UnityEngine;
using static ALivingEntity;

namespace TNSPlugin
{
    public class LineFormation
    {
        public static void DistributeAlongFormation(APlayableEntity[] entities, Vector3[] pickBegin, Vector3[] pickEnd)
        {
            int index = 0;

            foreach (APlayableEntity ent in entities)
            {
                if (ent == null) continue;

                Coordinate begin = Coordinate.FromChunkBlock(pickBegin[0], (Vector3i)(pickBegin[1] + pickBegin[2]));
                Coordinate end = Coordinate.FromChunkBlock(pickEnd[0], (Vector3i)(pickEnd[1] + pickEnd[2]));

                Vector3 pos = Vector3.zero;

                float greaterX = begin.world.x > end.world.x ? begin.world.x : end.world.x;
                float lesserX = begin.world.x < end.world.x ? begin.world.x : end.world.x;

                float greaterZ = begin.world.z > end.world.z ? begin.world.z : end.world.z;
                float lesserZ = begin.world.z < end.world.z ? begin.world.z : end.world.z;

                float offset = index * ChunkManager.getInstance().voxelSize;

                if (greaterX - lesserX < greaterZ - lesserZ)
                {
                    pos = new Vector3(begin.world.x, begin.world.y, lesserZ + offset);
                }
                else if(greaterX - lesserX > greaterZ - lesserZ)
                {
                    pos = new Vector3(lesserX + offset, begin.world.y, begin.world.z);
                }

                var ob = GameObject.CreatePrimitive(PrimitiveType.Capsule);
                ob.transform.position = new Vector3(pos.x, pos.y + ChunkManager.getInstance().voxelSize, pos.z);
                ob.transform.localScale = new Vector3(ChunkManager.getInstance().voxelSize, ChunkManager.getInstance().voxelSize, ChunkManager.getInstance().voxelSize);
                BasicDespawner deSpawn = ob.AddComponent<BasicDespawner>();
                deSpawn.entity = ent;

                Coordinate final = Coordinate.FromWorld(pos);

                while (ent.taskStack.Count > 0) ent.taskStack.Pop().task.cleanup();
                ent.taskStack.Push(new TaskAndEnumerator(new TaskStopped()));
                ent.taskStack.Push(new TaskAndEnumerator(new TaskForcedMove(ent, final.chunk, final.block)));

                index++;
            }
        }
    }
}
