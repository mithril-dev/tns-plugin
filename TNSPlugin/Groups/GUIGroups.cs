﻿using MithrilAPI.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Timber_and_Stone.API.Event;
using Timber_and_Stone.Event;
using Timber_and_Stone.GUIUtil;
using TNSPlugin.Utilities;
using UnityEngine;

namespace TNSPlugin
{
    class GUIGroups : MonoBehaviour
    {
        private GUIManager guiManager;
        private ControlPlayer player;
        private Rect window;

        private GroupData focusedGroup;

        private bool isStoppingTasks = false;
        private bool isWindowOpen = false;
        private bool isCreatingGroup = false;
        private bool isAddingUnits = false;
        private bool isRenaming = false;

        private string groupName = "Group Name";
        private bool isGroupNameFieldFocused = false;

        void Start()
        {
            window = new Rect((Screen.width / 2) - 850 / 2, (Screen.height / 2) - 640 / 2, 850, 640);
            guiManager = GUIManager.getInstance();
            player = guiManager.controllerObj.GetComponent<ControlPlayer>();

            focusedGroup = GroupManager.settlement.Groups[0];
        }

        //APlayableEntity[] clipBoard;

        void Update()
        {
            if (!guiManager.inGame || guiManager.gameOver) { return; }

            if (Input.GetKeyDown(KeyCode.F2))
                isWindowOpen = !isWindowOpen;
            if (Options.getInstance().GetMenuKey())
                isWindowOpen = false;

            //if (focusedGroup != null && GroupManager.selectedUnits.Count != 0 && isWindowOpen && (Input.GetKeyDown(KeyCode.Delete) || Input.GetKeyDown(KeyCode.Backspace)))
            //{
            //    foreach (var unit in GroupManager.selectedUnits)
            //    {
            //        groupManager.focusedGroup.RemoveUnit(unit);
            //    }

            //    GroupManager.selectedUnits.Clear();
            //}

            //if (groupManager.focusedGroup != null && GroupManager.selectedUnits.Count != 0 && groupManager.isGroupWindowOpen && (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.C)))
            //{
            //    clipBoard = new APlayableEntity[GroupManager.selectedUnits.Count];
            //    GroupManager.selectedUnits.ToArray().CopyTo(clipBoard, 0);
            //}

            //if (groupManager.focusedGroup != null && groupManager.isGroupWindowOpen && (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.V)))
            //{
            //    if (!groupManager.focusedGroup.protect)
            //    {
            //        foreach (var unit in clipBoard)
            //        {
            //            groupManager.focusedGroup.AddUnit(unit);
            //        }
            //    }
            //}

            //if (groupManager.focusedGroup != null && GroupManager.selectedUnits.Count != 0 && groupManager.isGroupWindowOpen && (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.X)))
            //{
            //    if (GroupManager.selectedUnits != null)
            //    {
            //        if (!groupManager.focusedGroup.protect)
            //        {
            //            foreach (var unit in GroupManager.selectedUnits)
            //            {
            //                groupManager.focusedGroup.RemoveUnit(unit);
            //            }
            //        }

            //        clipBoard = new APlayableEntity[GroupManager.selectedUnits.Count];
            //        GroupManager.selectedUnits.ToArray().CopyTo(clipBoard, 0);
            //    }
            //}

            //if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Q))
            //{
            //    if (groupManager.focusedGroup != null)
            //    {
            //        if (GroupManager.selectedUnits.Count != groupManager.focusedGroup.units.Count)
            //        {
            //            foreach (var unit in groupManager.focusedGroup.units)
            //            {
            //                GroupManager.AddUnitToSelection(unit);
            //            }
            //        }
            //        else if (GroupManager.selectedUnits.Count != 0)
            //        {
            //            GroupManager.selectedUnits.Clear();
            //        }
            //    }
            //}
        }

        void OnGUI()
        {
            if (!guiManager.inGame || guiManager.gameOver || GUIHide.IsGUIHidden) return;
            DrawButton();

            if (focusedGroup == null) return;

            //if (guiManager.inGame && !guiManager.gameOver && focusedGroup != null && !GUIHide.IsGUIHidden)
            //{
            //    guiManager.DrawTextLeftWhite(new Rect(8, 50 + 28, 200, 28), $"Group: {groupManager.focusedGroup.Name}");

            //    if (GroupManager.selectedUnits.Count != 0)
            //    {
            //        string selection = GroupManager.selectedUnits.Count == groupManager.focusedGroup.units.Count ? "Everyone" : GroupManager.selectedUnits.Count.ToString();
            //        guiManager.DrawTextLeftWhite(new Rect(8, 50 + 56, 200, 28), $"Selected: {selection}");
            //    }
            //}

            if (!isWindowOpen) return;
            window = new Rect(window.x, window.y, 850, 640);
            window = GUI.Window(003165494, window, GroupsWindow, "", guiManager.windowStyleBlank);

            if (isCreatingGroup)
            {
                GUI.Window(216540534, new Rect(window.x + ((window.width / 2) - 300 / 2), window.y + ((window.height / 2) - 80 / 2), 300, 96), creatingNewGroup, "");
                GUI.BringWindowToFront(216540534);

                if ((Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Return) && (groupName != string.Empty && !GroupManager.settlement.Groups.Contains(new GroupData(groupName))))
                {
                    GroupManager.settlement.Groups.Add(new GroupData(groupName));
                    groupName = "Group Name";
                    isCreatingGroup = false;
                }
            }

            //if (isRenaming)
            //{
            //    GUI.Window(2165498, new Rect(window.x + ((window.width / 2) - 300 / 2), window.y + ((window.height / 2) - 80 / 2), 300, 96), renamingGroup, "");
            //    GUI.BringWindowToFront(2165498);
            //}

            if (window.Contains(Event.current.mousePosition))
                guiManager.mouseInGUI = true;
        }

        private void DrawButton()
        {
            GUI.depth = -100;

            Rect rect = new Rect(Screen.width - (184), 12 + 32, 32, 32);

            if (Screen.width < 1600)
                rect = new Rect(Screen.width - (184), 12 + 32, 32, 32);
            else
                rect = new Rect(Screen.width - (184), 4, 32, 32);

            GUI.DrawTexture(rect, guiManager.tile_information);
            if (GUI.Button(rect, "", guiManager.blankButtonStyle))
            {
                isWindowOpen = !isWindowOpen;

                AEvent evt = new EventButtonPressed();
                EventManager.getInstance().InvokePre(evt);
                EventManager.getInstance().InvokePost(evt);
            }
        }

        Vector2 scrollGroupUnits;
        void GroupsWindow(int windowID)
        {
            GUI.DragWindow(new Rect(0, 0, window.width - 24, 28));

            guiManager.DrawWindow(new Rect(0, 0, window.width, window.height), "Unit Groups", false);

            if (GUI.Button(new Rect(window.width - 24, 0 + 4, 20, 20), "", guiManager.closeWindowButtonStyle))
            {
                isWindowOpen = !isWindowOpen;
                AEvent evt = new EventButtonPressed();
                EventManager.getInstance().InvokePre(evt);
                EventManager.getInstance().InvokePost(evt);
            }

            LeftArea();
            RightArea();


            //if (GroupManager.selectedUnits.Count != 0 && groupManager.focusedGroup != null)
            //    ControlPanel();
        }

        void LeftArea()
        {
            // Left Area Box/Header
            Rect LeftArea = new Rect(0, 28, 200, window.height - 28);
            GUI.Box(LeftArea, "", guiManager.windowStyle);
            guiManager.DrawTextCenteredWhite(new Rect(0, LeftArea.y + 4, LeftArea.width, 28), "Groups");

            GUILayout.BeginArea(new Rect(LeftArea.x + 4, LeftArea.y + 28, LeftArea.width - 8, LeftArea.height - 28));
            //scrollGroupList = GUILayout.BeginScrollView(scrollGroupList);
            {
                // Group List Items
                foreach (GroupData group in GroupManager.settlement.Groups)
                {
                    GUILayout.BeginHorizontal();
                    {

                        GUILayout.Space(2);

                        GUILayout.Box("", guiManager.boxStyle);
                        Rect nameRect = GUILayoutUtility.GetLastRect();
                        if (GUI.Button(new Rect(nameRect.x + (group.Protect ? 0 : 20), nameRect.y, nameRect.width - (group.Protect ? 0 : 20), nameRect.height), "", guiManager.blankButtonStyle))
                        {
                            focusedGroup = group;
                            AEvent evt = new EventButtonPressed();
                            EventManager.getInstance().InvokePre(evt);
                            EventManager.getInstance().InvokePost(evt);
                        }

                        // Delete Group
                        GUI.Box(new Rect(nameRect.x + 4, nameRect.y, nameRect.width - 8, nameRect.height), (group.Protect ? "" : "    ") + group.Name, guiManager.textStyle);

                        if (!group.Protect)
                        {
                            Rect delRect = GUILayoutUtility.GetLastRect();
                            GUI.Box(new Rect(delRect.x + 1, delRect.y + 1, 20, 20), "", guiManager.closeWindowButtonStyle);

                            if (new Rect(delRect.x + 1, delRect.y + 1, 20, 20).Contains(Event.current.mousePosition) && Input.GetMouseButtonDown(0))
                            {
                                if (focusedGroup != null && focusedGroup == group)
                                {
                                    focusedGroup = null;
                                }

                                GroupManager.settlement.Groups.Remove(group);
                            }
                        }
                    }
                    GUILayout.EndHorizontal();
                    GUILayout.Space(4);
                }


                // Add Group Button
                if (GUILayout.Button("", guiManager.boxStyle))
                    isCreatingGroup = true;
                GUI.Label(GUILayoutUtility.GetLastRect(), "+", guiManager.centeredTextStyle);
            }
            //GUI.EndScrollView();
            GUILayout.EndArea();
        }

        void RightArea()
        {
            // Right Area
            Rect RightArea = new Rect(208, 28 + 28 + 8, window.width - 216, window.height - ((28 + 28 + 8) + (228)));
            GUI.Box(new Rect(200, 28, window.width - 200, window.height - 28), "", guiManager.windowStyle);
            GUILayout.BeginArea(RightArea);
            {
                GUILayout.BeginHorizontal();
                {
                    GUILayout.Box("", guiManager.boxStyle);
                    Rect titleRect = GUILayoutUtility.GetLastRect();
                    guiManager.DrawTextLeftWhite(new Rect(titleRect.x + 4, titleRect.y, titleRect.width - 8, titleRect.height), $"Group - {focusedGroup.Name}");

                    GUILayout.Space(8);

                    GUILayout.Box("", guiManager.boxStyle, GUILayout.Width(170));
                    Rect trashRect = GUILayoutUtility.GetLastRect();
                    guiManager.DrawTextLeftWhite(new Rect(trashRect.x + 4, trashRect.y, trashRect.width - 8, trashRect.height), $"Unit Count: {focusedGroup.Units.Count}");

                    //if (!focusedGroup.Protect && titleRect.Contains(Event.current.mousePosition) && Input.GetMouseButtonDown(0))
                    //    groupManager.isRenamingGroup = true;
                }
                GUILayout.EndHorizontal();
                GUILayout.Space(16);

                scrollGroupUnits = GUILayout.BeginScrollView(scrollGroupUnits);
                GUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button("", guiManager.blankButtonStyle, GUILayout.Width(20)))
                    {
                        if (GroupManager.selectedUnits.Count < focusedGroup.Units.Count)
                        {
                            foreach (UnitData entity in focusedGroup.Units)
                            {
                                GroupManager.selectedUnits.Add((APlayableEntity)entity.Entity);
                            }
                        }
                        else
                        {
                            foreach (UnitData entity in focusedGroup.Units)
                            {
                                GroupManager.selectedUnits.Remove((APlayableEntity)entity.Entity);
                            }
                        }
                    }
                    GUI.Toggle(GUILayoutUtility.GetLastRect(), GroupManager.selectedUnits.Count == focusedGroup.Units.Count, "", guiManager.checkBoxStyle);
                    GUILayout.Space(4);
                    GUILayout.Box("", guiManager.boxStyle, GUILayout.Width(180));
                    guiManager.DrawTextCenteredWhite(GUILayoutUtility.GetLastRect(), "Unit Name");
                    GUILayout.Space(4);
                    GUILayout.Box("", guiManager.boxStyle, GUILayout.Width(220));
                    guiManager.DrawTextCenteredWhite(GUILayoutUtility.GetLastRect(), "Profession");
                    GUILayout.Space(4);
                    GUILayout.Box("", guiManager.boxStyle, GUILayout.Width(180));
                    guiManager.DrawTextCenteredWhite(GUILayoutUtility.GetLastRect(), "Task");
                    GUILayout.Space(4);
                }
                GUILayout.EndHorizontal();

                GUILayout.Space(10);

                foreach (UnitData entity in focusedGroup.Units)
                {
                    GUILayout.BeginHorizontal();
                    {
                        if (GUILayout.Button("", guiManager.blankButtonStyle, GUILayout.Width(20)))
                        {
                            if (GroupManager.selectedUnits.Contains((APlayableEntity)entity.Entity))
                            {
                                GroupManager.selectedUnits.Remove((APlayableEntity)entity.Entity);
                            }
                            else
                            {
                                GroupManager.selectedUnits.Add((APlayableEntity)entity.Entity);
                            }
                        }
                        GUI.Toggle(GUILayoutUtility.GetLastRect(), GroupManager.selectedUnits.Contains((APlayableEntity)entity.Entity), "", guiManager.checkBoxStyle);
                        GUILayout.Space(4);
                        GUILayout.Box("", guiManager.boxStyle, GUILayout.Width(180));
                        GUI.Label(GUILayoutUtility.GetLastRect(), entity.Entity.unitName, guiManager.textStyle);
                        if (GUI.Button(GUILayoutUtility.GetLastRect(), "", guiManager.blankButtonStyle))
                        {
                            player.MoveToPosition(entity.Entity.transform.position);
                        }
                        GUILayout.Space(4);
                        GUILayout.Box("", guiManager.boxStyle, GUILayout.Width(220));
                        GUI.Label(GUILayoutUtility.GetLastRect(), $"Lvl. {((APlayableEntity)entity.Entity).getProfession().getLevel()} {((APlayableEntity)entity.Entity).getProfession().getProfessionName()}", guiManager.textStyle);
                        GUILayout.Space(4);
                        GUILayout.Box("", guiManager.boxStyle, GUILayout.Width(180));
                        GUI.Label(GUILayoutUtility.GetLastRect(), entity.Entity.peekTaskStack().name, guiManager.textStyle);
                        GUILayout.Space(4);
                    }
                    GUILayout.EndHorizontal();
                    GUILayout.Space(8);
                }

                // Adding Units Button
                if (!focusedGroup.Protect)
                {
                    if (GUILayout.Button("", guiManager.blankButtonStyle))
                        isAddingUnits = !isAddingUnits;
                    guiManager.DrawTextCenteredWhite(GUILayoutUtility.GetLastRect(), "Add Unit");

                    if (isAddingUnits)
                        addingUnit();
                }
                GUILayout.EndScrollView();
            }
            GUILayout.EndArea();
        }

        Vector2 scrollControl;
        //void ControlPanel()
        //{
        //    Rect box = new Rect(200, window.height - (228), window.width - 200, 228);
        //    Rect controlArea = new Rect(box.x + 8, box.y + 36, box.width - 16, box.height - 44);
        //    GUI.Box(box, "", guiManager.windowStyle);

        //    guiManager.DrawTextCenteredWhite(new Rect(box.x, box.y + 4, box.width, 28), "Selection Control Panel");

        //    GUILayout.BeginArea(controlArea);
        //    {
        //        if (!groupManager.focusedGroup.protect)
        //        {
        //            if (GUILayout.Button("", guiManager.buttonStyle, GUILayout.Width(100)))
        //            {
        //                foreach (var unit in GroupManager.selectedUnits)
        //                {
        //                    groupManager.focusedGroup.RemoveUnit(unit);
        //                }

        //                GroupManager.selectedUnits.Clear();
        //            }
        //            guiManager.DrawTextCenteredWhite(GUILayoutUtility.GetLastRect(), "Remove");
        //        }

        //        GUILayout.Box("", guiManager.boxStyle);
        //        guiManager.DrawTextCenteredWhite(GUILayoutUtility.GetLastRect(), "Unit Selection Tasks");

        //        GUILayout.BeginHorizontal();
        //        {
        //            GUILayout.Box("", guiManager.blankButtonStyle, GUILayout.Width(230));

        //            int count = 0;
        //            foreach (var unit in GroupManager.selectedUnits)
        //            {
        //                if (unit.preferences["preference.waitinhallwhileidle"] == true)
        //                    count++;
        //            }
        //            bool waitInHall = count == GroupManager.selectedUnits.Count();

        //            if (GUI.Button(GUILayoutUtility.GetLastRect(), "", guiManager.blankButtonStyle))
        //            {
        //                foreach (var unit in GroupManager.selectedUnits)
        //                {
        //                    unit.preferences["preference.waitinhallwhileidle"] = !waitInHall;
        //                }
        //            }
        //            guiManager.DrawCheckBox(GUILayoutUtility.GetLastRect(), "", ref waitInHall);
        //            guiManager.DrawTextRightWhite(GUILayoutUtility.GetLastRect(), "Wait in Hall While Idle");

        //            GUILayout.Space(4);

        //            // Stop Tasks Button
        //            if (GUILayout.Button("", guiManager.blankButtonStyle, GUILayout.Width(158)))
        //            {
        //                isStoppingTasks = !isStoppingTasks;

        //                foreach (APlayableEntity entity in GroupManager.selectedUnits)
        //                {
        //                    if (!isStoppingTasks && (entity.peekTaskStack() is Timber_and_Stone.Tasks.TaskStopped))
        //                    {
        //                        ((Timber_and_Stone.Tasks.TaskStopped)entity.peekTaskStack()).stopWaiting();
        //                    }
        //                    else if (!(entity.peekTaskStack() is Timber_and_Stone.Tasks.TaskStopped))
        //                    {
        //                        entity.interruptTask(new Timber_and_Stone.Tasks.TaskStopped());
        //                    }
        //                }
        //            }
        //            guiManager.DrawCheckBox(GUILayoutUtility.GetLastRect(), "", ref isStoppingTasks);
        //            guiManager.DrawTextRightWhite(GUILayoutUtility.GetLastRect(), "Hold Position");

        //            GUILayout.Space(4);

        //            GUILayout.Box("", guiManager.blankButtonStyle, GUILayout.Width(230));

        //            int count1 = 0;
        //            int chopperCount = 0;
        //            foreach (var unit in GroupManager.selectedUnits)
        //            {
        //                if (unit.getProfession().getProfessionName() != "Wood Chopper")
        //                    continue;
        //                else
        //                    chopperCount++;

        //                if (unit.preferences["preference.ChopClosestTree"] == true)
        //                    count1++;
        //            }
        //            bool chopTrees = count1 == chopperCount;

        //            if (GUI.Button(GUILayoutUtility.GetLastRect(), "", guiManager.blankButtonStyle))
        //            {
        //                foreach (var unit in GroupManager.selectedUnits)
        //                {
        //                    if (unit.getProfession().getProfessionName() != "Wood Chopper")
        //                        continue;

        //                    unit.preferences["preference.ChopClosestTree"] = !chopTrees;
        //                }
        //            }
        //            guiManager.DrawCheckBox(GUILayoutUtility.GetLastRect(), "", ref chopTrees);
        //            guiManager.DrawTextRightWhite(GUILayoutUtility.GetLastRect(), "Chop Nearest Trees");
        //        }
        //        GUILayout.EndHorizontal();


        //        GUILayout.Box("", guiManager.boxStyle);
        //        guiManager.DrawTextCenteredWhite(GUILayoutUtility.GetLastRect(), "Change Selected Units Professions");

        //        scrollControl = GUILayout.BeginScrollView(scrollControl);
        //        GUILayout.BeginHorizontal();
        //        foreach (var t in GroupManager.selectedUnits[0].professions)
        //        {
        //            GUILayout.Box("", guiManager.buttonStyle, GUILayout.Height(40), GUILayout.Width(40));
        //            Rect rect = GUILayoutUtility.GetLastRect();
        //            GUILayout.FlexibleSpace();
        //            GUI.DrawTexture(new Rect(rect.x + 4, rect.y + 4, 32, 32), AssetManager.getInstance().GetSkillIconFromProfession(t.Value.getProfessionName()));
        //            if (GUI.Button(new Rect(rect.x, rect.y, rect.width, rect.height), "", guiManager.hiddenButtonStyle))
        //            {
        //                foreach (var unit in GroupManager.selectedUnits)
        //                {
        //                    if (unit.getProfession().GetType() != t.Key)
        //                    {
        //                        unit.SetProfession(t.Key);
        //                    }
        //                }
        //            }
        //            //guiManager.DrawTextLeftWhite(new Rect(rect.x + 28, rect.y + 4, rect.width - 28, rect.height - 4), t.Value.getProfessionName());
        //        }
        //        GUILayout.EndHorizontal();
        //        GUILayout.EndScrollView();
        //    }
        //    GUILayout.EndArea();
        //}

        void creatingNewGroup(int id)
        {
            // Group Prompt Area/Header
            Rect newPrompt = new Rect(0, 0, 300, 96);
            GUI.DrawTextureWithTexCoords(new Rect(newPrompt.x, newPrompt.y, newPrompt.width, newPrompt.height), guiManager.windowBG, new Rect(0, 0, newPrompt.width / guiManager.windowBG.width, newPrompt.height / guiManager.windowBG.height));
            GUI.Box(newPrompt, "", guiManager.windowStyle);
            DrawGUI.Text(new Rect(newPrompt.x, newPrompt.y + 4, newPrompt.width, 28), "Create Group", TextAlignment.Center);

            GUI.FocusControl("GroupNameField");

            GUILayout.BeginArea(new Rect(newPrompt.x + 2, newPrompt.y + 28, newPrompt.width - 4, newPrompt.height));
            {
                // Name Box
                GUI.Box(new Rect(0, 0, newPrompt.width - 4, 28), "", guiManager.boxStyle);
                GUI.SetNextControlName("GroupNameField");
                groupName = GUILayout.TextField(groupName, guiManager.textStyle);

                // Accept/Decline
                GUILayout.BeginHorizontal();
                {
                    if (DrawLayoutGUI.Default_Layout_Button("Ok") && (groupName != string.Empty && !GroupManager.settlement.Groups.Contains(new GroupData(groupName))))
                    {
                        GroupManager.settlement.Groups.Add(new GroupData(groupName));
                        groupName = "Group Name";
                        isCreatingGroup = false;
                    }
                    if (DrawLayoutGUI.Default_Layout_Button("Cancel"))
                    {
                        groupName = "Group Name";
                        isCreatingGroup = false;
                    }
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndArea();
        }

        //void renamingGroup(int id)
        //{
        //    if (groupName == "Group Name")
        //        groupName = focusedGroup.Name;

        //    // Group Prompt Area/Header
        //    Rect newPrompt = new Rect(0, 0, 300, 96);
        //    GUI.DrawTextureWithTexCoords(new Rect(newPrompt.x, newPrompt.y, newPrompt.width, newPrompt.height), guiManager.windowBG, new Rect(0, 0, newPrompt.width / guiManager.windowBG.width, newPrompt.height / guiManager.windowBG.height));
        //    GUI.Box(newPrompt, "", guiManager.windowStyle);
        //    guiManager.DrawTextCenteredWhite(new Rect(newPrompt.x, newPrompt.y + 4, newPrompt.width, 28), "Rename Group");

        //    // Name Box
        //    GUI.Box(new Rect(newPrompt.x + 2, newPrompt.y + 28, newPrompt.width - 4, 28), "", guiManager.boxStyle);
        //    groupName = GUI.TextField(new Rect(newPrompt.x + 4, newPrompt.y + 28, newPrompt.width - 8, 28), groupName, guiManager.textStyle);

        //    // Accept/Decline
        //    if (GUI.Button(new Rect(newPrompt.x + 4, (newPrompt.y) + 28 + 32, (newPrompt.width / 2) - 4, 32), "Ok", guiManager.buttonStyle))
        //    {
        //        groupManager.focusedGroup.Name = groupName;
        //        groupName = "Group Name";
        //        groupManager.isRenamingGroup = false;
        //    }
        //    if (GUI.Button(new Rect((newPrompt.x + newPrompt.width / 2) + 2, (newPrompt.y) + 28 + 32, (newPrompt.width / 2) - 6, 32), "Cancel", guiManager.buttonStyle))
        //    {
        //        groupName = "Group Name";
        //        groupManager.isRenamingGroup = false;
        //    }
        //}

        void ContextGroupWindow(int id)
        {

        }

        void addingUnit()
        {
            Rect rect = GUILayoutUtility.GetLastRect();
            rect = new Rect(rect.x, rect.y + 28 - 8, rect.width, rect.height - (28));

            foreach (ALivingEntity unit in WorldManager.getInstance().PlayerFaction.units)
            {
                if (focusedGroup.Units.Contains(new UnitData((APlayableEntity)unit)))
                    continue;


                GUILayout.Box("", guiManager.boxStyle);

                if (GUI.Button(GUILayoutUtility.GetLastRect(), "", guiManager.blankButtonStyle))
                {
                    focusedGroup.Units.Add(new UnitData(unit));
                    isAddingUnits = false;
                }
                guiManager.DrawTextLeftWhite(GUILayoutUtility.GetLastRect(), "  " + unit.unitName);
            }


            if (WorldManager.getInstance().PlayerFaction.units.Count == focusedGroup.Units.Count)
            {
                GUILayout.Box("", guiManager.boxStyle);
                guiManager.DrawTextCenteredWhite(GUILayoutUtility.GetLastRect(), "  - No Units Available -");
            }

            rect = new Rect(rect.x, rect.y, rect.width, rect.height + (GUILayoutUtility.GetLastRect().y - rect.y) + 28);
            GUI.Box(rect, "", guiManager.windowBoxStyle);
        }
    }
}
