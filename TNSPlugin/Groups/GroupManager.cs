﻿using MithrilAPI.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Timber_and_Stone;
using Timber_and_Stone.Tasks;
using UnityEngine;

namespace TNSPlugin
{
    class GroupManager : MonoBehaviour
    {
        public static SettlementData settlement;
        public static List<APlayableEntity> selectedUnits = new List<APlayableEntity>();

        ControlPlayer player;
        GUIManager manager;

        Vector3[] pickBegin;
        Vector3[] pickEnd;

        void Awake()
        {
            settlement = new SettlementData();
            settlement.Name = WorldManager.getInstance().settlementName;

            settlement.Groups.Add(new GroupData("All", true));
        }

        void Start()
        {
            if (FindObjectsOfType<GroupManager>().Length > 1)
            {
                Destroy(this);
            }

            manager = GUIManager.getInstance();
            player = manager.controllerObj.GetComponent<ControlPlayer>();
        }

        // Test formations
        void Update()
        {
            if (!GUIManager.getInstance().inGame)
                return;


            LayerMask layerMask;
            RaycastHit hit;
            if (Input.GetMouseButtonDown(1) && Input.GetKey(KeyCode.LeftShift))
            {
                layerMask = (1 << 8);
                if (Physics.Raycast(player.currentCamera.ScreenPointToRay(Input.mousePosition), out hit, 300, layerMask))
                {
                    if (pickBegin != null && pickEnd == null) { pickEnd = ChunkManager.getInstance().Pick(hit.point, player.currentCamera.ScreenPointToRay(Input.mousePosition).direction, true); }
                    else if (pickBegin == null) { pickBegin = ChunkManager.getInstance().Pick(hit.point, player.currentCamera.ScreenPointToRay(Input.mousePosition).direction, true); }

                    if (pickBegin == null || pickEnd == null) { return; }

                    if (ChunkManager.getInstance().WalkableBlock(pickBegin[0], pickBegin[1], pickBegin[2], false, false, false, false) != 0)
                    { return; }
                    if (ChunkManager.getInstance().WalkableBlock(pickEnd[0], pickEnd[1], pickEnd[2], false, false, false, false) != 0)
                    { return; }

                    LineFormation.DistributeAlongFormation(selectedUnits.ToArray(), pickBegin, pickEnd);
                    pickBegin = null;
                    pickEnd = null;
                }
                else
                {
                    if (pickBegin != null && pickEnd == null) { pickEnd = ChunkManager.getInstance().Pick(player.currentCamera.transform.position, player.currentCamera.ScreenPointToRay(Input.mousePosition).direction, true); }
                    else if (pickBegin == null) { pickBegin = ChunkManager.getInstance().Pick(player.currentCamera.transform.position, player.currentCamera.ScreenPointToRay(Input.mousePosition).direction, true); }

                    if (pickBegin == null || pickEnd == null) { return; }

                    if (ChunkManager.getInstance().WalkableBlock(pickBegin[0], pickBegin[1], pickBegin[2], false, false, false, false) != 0)
                    { return; }
                    if (ChunkManager.getInstance().WalkableBlock(pickEnd[0], pickEnd[1], pickEnd[2], false, false, false, false) != 0)
                    { return; }

                    LineFormation.DistributeAlongFormation(selectedUnits.ToArray(), pickBegin, pickEnd);
                    pickBegin = null;
                    pickEnd = null;
                }
            }
        }
    }
}
