﻿using System.IO;
using Timber_and_Stone.API.Event;
using Timber_and_Stone.Event;
using EventHandler = Timber_and_Stone.API.Event.EventHandler;
using UnityEngine;
using MithrilAPI.Debug;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using LitJson;
using System.Collections.Generic;

namespace TNSPlugin
{
    public class SaveGroup : MonoBehaviour, IEventListener
    {
        string savesPath = "./plugins/";
        string fileName = "groups.json";

        void Start()
        {
            EventManager.getInstance().Register(this);
        }

        void Update()
        {

        }

        [EventHandler(Priority.Monitor)]
        public void OnGameSave(EventSave evt)
        {
            SettlementData data = new SettlementData();
            data.Name = WorldManager.getInstance().settlementName;
            //data.Groups = GroupManager.groups;

            //JsonData data = JsonMapper.ToJson();
        }

        [EventHandler(Priority.Monitor)]
        public void OnGameLoad(EventGameLoad evt)
        {
            //Load_Old();
        }
    }

    public class SettlementData
    {
        public string Name;
        public List<GroupData> Groups = new List<GroupData>();
    }

    public class GroupData
    {
        public string Name;
        public bool Protect;
        public List<UnitData> Units = new List<UnitData>();

        public GroupData() { }

        public GroupData(string name, bool protect = false)
        {
            Name = name;
            Protect = protect;
        }
    }

    public class UnitData
    {
        public UnitData() { }

        public UnitData(ALivingEntity enity)
        {
            Entity = enity;
        }

        public ALivingEntity Entity;
    }
}