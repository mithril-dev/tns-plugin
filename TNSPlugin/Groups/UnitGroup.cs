﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Timber_and_Stone;

namespace TNSPlugin
{
    public class UnitGroup
    {
        public string Name { get; set; }
        public List<APlayableEntity> units { get; private set; }
        public bool protect { get; private set; }

        public UnitGroup(string _name, bool _protect = false)
        {
            units = new List<APlayableEntity>();
            Name = _name;
            protect = _protect;
        }

        public void AddUnit(APlayableEntity _entity)
        {
            if (!units.Contains(_entity))
                units.Add(_entity);
        }

        public void RemoveUnit(APlayableEntity _entity)
        {
            if (units.Contains(_entity))
                units.Remove(_entity);
        }
    }
}
