﻿using System;
using System.Reflection;
using Timber_and_Stone.API;
using Timber_and_Stone.API.Event;
using Timber_and_Stone.Event;

namespace TNSPlugin
{
    public class MainPlugin : CSharpPlugin, IEventListener
    {
        public override void OnLoad()
        {
            VersionHelpers.VersionHandler.CheckCorrectAPIVersion(Assembly.GetCallingAssembly().GetName().Version);
        }

        public override void OnEnable()
        {
            EventManager.getInstance().Register(this);

            new MithrilManager();
        }

        public override string ModName()
        {
            return "Mithril Mod";
        }

        public override string ModDesc()
        {
            return @"A mod that adds quality of life things.\nPlans to Beautify the worlds settler's live in!";
        }

        public override Version ModVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version;
        }

        ~MainPlugin()
        {
            if (VersionHelpers.VersionHandler.CorrectAPIVersion)
            {
                Config.SaveSettings();
            }
        }
    }
}