﻿using System.Collections;
using UnityEngine;

namespace TNSPlugin.Utilities
{
    public class CreateMesh
    {
        public Mesh mesh { get; private set; }
        public GameObject gameObject { get; private set; }

        public CreateMesh(string resourcePath)
        {
            ImportMesh(resourcePath);
        }

        public CreateMesh(string resourcePath, GameObject _gameObject)
        {
            ImportMesh(resourcePath);
            gameObject = _gameObject;
        }

        private void ImportMesh(string path)
        {
            ObjImporter importer = new ObjImporter();
            mesh = importer.Import($"{path}");

            if (gameObject != null)
            {
                gameObject.AddComponent<MeshRenderer>();
                var filter = gameObject.AddComponent<MeshFilter>();
                filter.mesh = mesh;
            }
        }
    }
}