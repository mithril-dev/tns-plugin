﻿using MithrilAPI.Debug;
using System.Collections;
using TNSPlugin.Utilities;
using UnityEngine;

namespace TNSPlugin
{
    public class ImportChair : MonoBehaviour
    {
        public static Texture2D objTexture;
        CreateMesh cMesh;

        void Start()
        {
            cMesh = new CreateMesh("TNSPlugin.Resources.Mesh.chair.obj");
            objTexture = TextureHandler.ConvertToTexture2D("objecttextures");
            objTexture.name = "Object Textures";
            objTexture.filterMode = FilterMode.Point;

            Material objTextures = new Material(AssetManager.getInstance().cutoutShader);
            objTextures.mainTexture = objTexture;

            var transform = ResourceManager.getInstance().buildObjects[14];
            BuildStructure structure = transform.GetComponent<BuildStructure>();
            structure.textures = objTextures;

            MeshFilter filter = transform.GetComponent<MeshFilter>();
            filter.mesh = cMesh.mesh;
        }
    }
}