﻿using MithrilAPI.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TNSPlugin.Utilities;
using UnityEngine;

namespace TNSPlugin
{
    public class ImportStoneBench : MonoBehaviour
    {
        public static Texture2D objTexture;
        CreateMesh cMesh;

        void Start()
        {
            cMesh = new CreateMesh("TNSPlugin.Resources.Mesh.Stone_Bench.obj");
            objTexture = TextureHandler.ConvertToTexture2D("objecttextures");
            objTexture.name = "Object Textures";
            objTexture.filterMode = FilterMode.Point;

            Material objTextures = new Material(AssetManager.getInstance().cutoutShader);
            objTextures.mainTexture = objTexture;

            var transform = ResourceManager.getInstance().buildObjects[21];
            BuildStructure structure = transform.GetComponent<BuildStructure>();
            structure.textures = objTextures;

            MeshFilter filter = transform.GetComponent<MeshFilter>();
            filter.mesh = cMesh.mesh;

            //int count = 0;
            //foreach (var item in ResourceManager.getInstance().buildObjects)
            //{
            //    DebugConsole.Log($"{count} : {item.name}");
            //    count++;
            //}
        }
    }
}
