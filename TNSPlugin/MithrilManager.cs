﻿using TNSPlugin.GUIUtils;
using TNSPlugin.Shaders;
using TNSPlugin.Sounds;
using TNSPlugin.Utilities;

namespace TNSPlugin
{
    public class MithrilManager
    {
        public MithrilManager()
        {
            if (!VersionHelpers.VersionHandler.CorrectAPIVersion) return;

            new PluginObject();
            new SoundManager();

            Styles.ImplimentStyles();
            ShaderHandler.ApplyShaders();
            Config.LoadSettings();
            ModGUIManager.InitializeManager();
        }
    }
}
