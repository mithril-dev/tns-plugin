﻿Shader "Skybox/SkyShader"
{
    Properties
    {
        [Header(Sky color)]
//        _ColorTop("Color top", Color) = (0.58,1,1,1)
//        _ColorMiddle("Color middle", Color) = (0.71,0.83,0.98,1)
//        _ColorBottom("Color bottom", Color) = (0.74,0.74,0.74,1)
 
        _MiddleSmoothness("Middle smoothness", Range(0.0,1.0)) = 0.5
        _MiddleOffset("Middle offset", float) = -0.18
        _TopSmoothness("Top smoothness", Range(0.0, 1.0)) = 1
        _TopOffset("Top offset", float) = -0.44
 
        [Header(Sun)]
        _SunSize("Sun size", Range(0.0, 1.0)) = 0.3
//        _SunColor("Sun color", Color) = (0.98,0.91,0.69,1)
 
        [Header(Moon)]
        _MoonSize("Moon size", Range(0,1)) = 0.1
        _MoonColor("Moon color", Color) = (0.92,0.90,0.82,1)
        _MoonPhase("Moon phase", Range(0,1)) = 0.4
         
        [Header(Stars)]
        _Stars("Stars", 2D) = "black" {}
        _StarsIntensity("Stars intensity", float) = 1.43
 
        [Header(Clouds)]
//        _CloudsColor("Clouds color", Color) = (0.93,0.94,1,1)
        _CloudsTexture("Clouds texture", 2D) = "black" {}
        _CloudsThreshold("Clouds threshold", Range(0.0, 1.0)) = 0.05
        _CloudsSmoothness("Clouds smoothness", Range(0.0, 1.0)) = 0.58
        _SunCloudIntensity("Sun behind clouds intensity", Range(0, 1)) = 0.6
        _PanningSpeedX("Panning speed X", float) = 0.01
        _PanningSpeedY("Panning speed Y", float) = 0
    }
    SubShader
    {
		Tags { "Queue"="Background" }
        LOD 100
        
		ZWrite Off
        
        Cull front
 
        Pass
        {
            Program "vp" {
// Vertex combos: 1
//   opengl - ALU: 5 to 5
//   d3d9 - ALU: 5 to 5
//   d3d11 - ALU: 4 to 4, TEX: 0 to 0, FLOW: 1 to 1
SubProgram "opengl " {
Keywords { }
Bind "vertex" Vertex
Bind "normal" Normal
"3.0-!!ARBvp1.0
# 5 ALU
PARAM c[5] = { program.local[0],
		state.matrix.mvp };
MOV result.texcoord[0].xyz, vertex.normal;
DP4 result.position.w, vertex.position, c[4];
DP4 result.position.z, vertex.position, c[3];
DP4 result.position.y, vertex.position, c[2];
DP4 result.position.x, vertex.position, c[1];
END
# 5 instructions, 0 R-regs
"
}

SubProgram "d3d9 " {
Keywords { }
Bind "vertex" Vertex
Bind "normal" Normal
Matrix 0 [glstate_matrix_mvp]
"vs_3_0
; 5 ALU
dcl_texcoord0 o0
dcl_position o1
dcl_position0 v0
dcl_normal0 v1
mov o0.xyz, v1
dp4 o1.w, v0, c3
dp4 o1.z, v0, c2
dp4 o1.y, v0, c1
dp4 o1.x, v0, c0
"
}

SubProgram "d3d11 " {
Keywords { }
Bind "vertex" Vertex
Bind "normal" Normal
ConstBuffer "UnityPerDraw" 336 // 64 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
BindCB "UnityPerDraw" 0
// 6 instructions, 1 temp regs, 0 temp arrays:
// ALU 4 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0
eefiecedkcacljfbjlhcbheeaniokpnngadhnaeaabaaaaaaaeacaaaaadaaaaaa
cmaaaaaakaaaaaaapiaaaaaaejfdeheogmaaaaaaadaaaaaaaiaaaaaafaaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaafjaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahaaaaaagcaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahahaaaafaepfdejfeejepeoaafeeffiedepepfceeaaeoepfcenebemaaklklkl
epfdeheofaaaaaaaacaaaaaaaiaaaaaadiaaaaaaaaaaaaaaaaaaaaaaadaaaaaa
aaaaaaaaahaiaaaaebaaaaaaaaaaaaaaabaaaaaaadaaaaaaabaaaaaaapaaaaaa
feeffiedepepfceeaafdfgfpfaepfdejfeejepeoaaklklklfdeieefcaeabaaaa
eaaaabaaebaaaaaafjaaaaaeegiocaaaaaaaaaaaaeaaaaaafpaaaaadpcbabaaa
aaaaaaaafpaaaaadhcbabaaaacaaaaaagfaaaaadhccabaaaaaaaaaaaghaaaaae
pccabaaaabaaaaaaabaaaaaagiaaaaacabaaaaaadgaaaaafhccabaaaaaaaaaaa
egbcbaaaacaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaaegiocaaa
aaaaaaaaabaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaaaaaaaaaaaaaaaaa
agbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaa
aaaaaaaaacaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpccabaaa
abaaaaaaegiocaaaaaaaaaaaadaaaaaapgbpbaaaaaaaaaaaegaobaaaaaaaaaaa
doaaaaab"
}

SubProgram "gles " {
Keywords { }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 glstate_matrix_mvp;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  xlv_TEXCOORD0 = normalize(_glesNormal);
  gl_Position = (glstate_matrix_mvp * _glesVertex);
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD0;
uniform highp vec3 _SunDirection;
uniform highp float _PanningSpeedY;
uniform highp float _PanningSpeedX;
uniform highp float _SunCloudIntensity;
uniform highp float _CloudsThreshold;
uniform highp float _CloudsSmoothness;
uniform lowp vec4 _CloudsColor;
uniform sampler2D _CloudsTexture;
uniform highp float _StarsIntensity;
uniform sampler2D _Stars;
uniform highp float _MoonPhase;
uniform lowp vec4 _MoonColor;
uniform highp float _MoonSize;
uniform highp float _SunSize;
uniform lowp vec4 _SunColor;
uniform highp float _TopOffset;
uniform highp float _TopSmoothness;
uniform highp float _MiddleOffset;
uniform highp float _MiddleSmoothness;
uniform lowp vec4 _ColorTop;
uniform lowp vec4 _ColorMiddle;
uniform lowp vec4 _ColorBottom;
uniform highp vec4 _Time;
void main ()
{
  lowp vec4 cloudsCol_1;
  lowp vec2 starUV_2;
  highp float clouds_3;
  highp float cloudsTex_4;
  lowp vec4 col_5;
  lowp vec3 coords_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize(xlv_TEXCOORD0);
  coords_6 = tmpvar_7;
  lowp float r_8;
  if ((abs(coords_6.z) > (1e-08 * abs(coords_6.x)))) {
    lowp float y_over_x_9;
    y_over_x_9 = (coords_6.x / coords_6.z);
    lowp float s_10;
    lowp float x_11;
    x_11 = (y_over_x_9 * inversesqrt(((y_over_x_9 * y_over_x_9) + 1.0)));
    s_10 = (sign(x_11) * (1.5708 - (sqrt((1.0 - abs(x_11))) * (1.5708 + (abs(x_11) * (-0.214602 + (abs(x_11) * (0.0865667 + (abs(x_11) * -0.0310296)))))))));
    r_8 = s_10;
    if ((coords_6.z < 0.0)) {
      if ((coords_6.x >= 0.0)) {
        r_8 = (s_10 + 3.14159);
      } else {
        r_8 = (r_8 - 3.14159);
      };
    };
  } else {
    r_8 = (sign(coords_6.x) * 1.5708);
  };
  highp float tmpvar_12;
  highp float t_13;
  t_13 = max (min (((xlv_TEXCOORD0.y - _MiddleOffset) / (0.5 - ((1.0 - _MiddleSmoothness) / 2.0))), 1.0), 0.0);
  tmpvar_12 = (t_13 * (t_13 * (3.0 - (2.0 * t_13))));
  highp float t_14;
  t_14 = max (min ((((xlv_TEXCOORD0.y - _TopOffset) - 0.5) / ((1.0 - ((1.0 - _TopSmoothness) / 2.0)) - 0.5)), 1.0), 0.0);
  highp vec4 tmpvar_15;
  tmpvar_15 = mix (_ColorBottom, _ColorMiddle, vec4(tmpvar_12));
  col_5 = tmpvar_15;
  highp vec4 tmpvar_16;
  tmpvar_16 = mix (col_5, _ColorTop, vec4((t_14 * (t_14 * (3.0 - (2.0 * t_14))))));
  col_5 = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD0.y - _CloudsThreshold);
  lowp vec2 tmpvar_18;
  tmpvar_18.x = ((r_8 / 6.28318) * 5.0);
  tmpvar_18.y = (((sign(coords_6.y) * (1.5708 - (sqrt((1.0 - abs(coords_6.y))) * (1.5708 + (abs(coords_6.y) * (-0.214602 + (abs(coords_6.y) * (0.0865667 + (abs(coords_6.y) * -0.0310296))))))))) / 1.5708) * 5.0);
  highp vec2 tmpvar_19;
  tmpvar_19.x = _PanningSpeedX;
  tmpvar_19.y = _PanningSpeedY;
  highp vec2 P_20;
  P_20 = (tmpvar_18 + (tmpvar_19 * _Time.y));
  lowp float tmpvar_21;
  tmpvar_21 = texture2D (_CloudsTexture, P_20).x;
  cloudsTex_4 = tmpvar_21;
  highp float t_22;
  t_22 = max (min (((cloudsTex_4 - tmpvar_17) / ((tmpvar_17 + _CloudsSmoothness) - tmpvar_17)), 1.0), 0.0);
  clouds_3 = (t_22 * (t_22 * (3.0 - (2.0 * t_22))));
  lowp float r_23;
  if ((abs(coords_6.z) > (1e-08 * abs(coords_6.y)))) {
    lowp float y_over_x_24;
    y_over_x_24 = (coords_6.y / coords_6.z);
    lowp float s_25;
    lowp float x_26;
    x_26 = (y_over_x_24 * inversesqrt(((y_over_x_24 * y_over_x_24) + 1.0)));
    s_25 = (sign(x_26) * (1.5708 - (sqrt((1.0 - abs(x_26))) * (1.5708 + (abs(x_26) * (-0.214602 + (abs(x_26) * (0.0865667 + (abs(x_26) * -0.0310296)))))))));
    r_23 = s_25;
    if ((coords_6.z < 0.0)) {
      if ((coords_6.y >= 0.0)) {
        r_23 = (s_25 + 3.14159);
      } else {
        r_23 = (r_23 - 3.14159);
      };
    };
  } else {
    r_23 = (sign(coords_6.y) * 1.5708);
  };
  lowp float tmpvar_27;
  tmpvar_27 = (r_23 / 6.28318);
  lowp float tmpvar_28;
  tmpvar_28 = ((sign(coords_6.x) * (1.5708 - (sqrt((1.0 - abs(coords_6.x))) * (1.5708 + (abs(coords_6.x) * (-0.214602 + (abs(coords_6.x) * (0.0865667 + (abs(coords_6.x) * -0.0310296))))))))) / 1.5708);
  highp vec2 tmpvar_29;
  tmpvar_29.x = ((tmpvar_27 * 5.0) - (_Time.x / 5.0));
  tmpvar_29.y = (tmpvar_28 * 2.0);
  starUV_2 = tmpvar_29;
  lowp vec4 tmpvar_30;
  tmpvar_30 = texture2D (_Stars, starUV_2);
  highp float t_31;
  t_31 = max (min ((((xlv_TEXCOORD0.y * 10.0) - 0.5) / 0.5), 1.0), 0.0);
  highp float tmpvar_32;
  tmpvar_32 = ((((tmpvar_30 * _StarsIntensity) * clamp ((_SunDirection.y / 2.0), 0.0, 1.0)) * (1.0 - clouds_3)).x * (t_31 * (t_31 * (3.0 - (2.0 * t_31)))));
  highp float tmpvar_33;
  highp vec3 p_34;
  p_34 = (normalize(xlv_TEXCOORD0) - -(_SunDirection));
  tmpvar_33 = sqrt(dot (p_34, p_34));
  highp float t_35;
  t_35 = max (min ((tmpvar_33 / _SunSize), 1.0), 0.0);
  highp float tmpvar_36;
  tmpvar_36 = max ((clouds_3 * _CloudsColor.w), (t_35 * (t_35 * (3.0 - (2.0 * t_35)))));
  highp vec3 p_37;
  p_37 = (normalize(xlv_TEXCOORD0) - _SunDirection);
  highp vec3 p_38;
  p_38 = ((normalize(xlv_TEXCOORD0) - (vec3(0.0, 0.0, 0.1) * _MoonPhase)) - _SunDirection);
  highp float tmpvar_39;
  tmpvar_39 = clamp ((((float((_MoonSize >= sqrt(dot (p_37, p_37)))) - float((_MoonSize >= sqrt(dot (p_38, p_38))))) * _SunDirection.y) - clouds_3), 0.0, 1.0);
  highp float t_40;
  t_40 = max (min (((cloudsTex_4 - tmpvar_17) / (((tmpvar_17 + _CloudsSmoothness) + 0.1) - tmpvar_17)), 1.0), 0.0);
  highp float edge0_41;
  edge0_41 = ((tmpvar_17 + _CloudsSmoothness) + 0.1);
  highp float t_42;
  t_42 = max (min (((cloudsTex_4 - edge0_41) / (((tmpvar_17 + _CloudsSmoothness) + 0.4) - edge0_41)), 1.0), 0.0);
  highp float tmpvar_43;
  tmpvar_43 = ((t_40 * (t_40 * (3.0 - (2.0 * t_40)))) - (t_42 * (t_42 * (3.0 - (2.0 * t_42)))));
  highp float tmpvar_44;
  tmpvar_44 = ((mix (clouds_3, tmpvar_43, 0.5) * tmpvar_12) * _CloudsColor.w);
  clouds_3 = tmpvar_44;
  highp float t_45;
  t_45 = max (min (((cloudsTex_4 - tmpvar_17) / ((tmpvar_17 + _CloudsSmoothness) - tmpvar_17)), 1.0), 0.0);
  highp float edge0_46;
  edge0_46 = (tmpvar_17 + 0.02);
  highp float t_47;
  t_47 = max (min (((cloudsTex_4 - edge0_46) / (((tmpvar_17 + _CloudsSmoothness) + 0.02) - edge0_46)), 1.0), 0.0);
  highp float edge0_48;
  edge0_48 = (_SunSize * 3.0);
  highp float t_49;
  t_49 = max (min (((tmpvar_33 - edge0_48) / -(edge0_48)), 1.0), 0.0);
  highp vec4 tmpvar_50;
  tmpvar_50 = mix (_SunColor, col_5, vec4(tmpvar_36));
  col_5 = tmpvar_50;
  highp float t_51;
  t_51 = max (min (((tmpvar_33 - 0.2) / -0.2), 1.0), 0.0);
  highp vec4 tmpvar_52;
  tmpvar_52 = mix (_CloudsColor, (_CloudsColor + tmpvar_33), vec4(((tmpvar_43 * (t_51 * (t_51 * (3.0 - (2.0 * t_51))))) * _SunCloudIntensity)));
  cloudsCol_1 = tmpvar_52;
  highp vec4 tmpvar_53;
  tmpvar_53 = mix (col_5, cloudsCol_1, vec4(tmpvar_44));
  col_5 = tmpvar_53;
  highp vec4 tmpvar_54;
  tmpvar_54 = (col_5 + ((((t_45 * (t_45 * (3.0 - (2.0 * t_45)))) - (t_47 * (t_47 * (3.0 - (2.0 * t_47))))) * ((t_49 * (t_49 * (3.0 - (2.0 * t_49)))) * _CloudsColor.w)) * _SunColor));
  col_5 = tmpvar_54;
  highp vec4 tmpvar_55;
  tmpvar_55 = mix (col_5, _MoonColor, vec4(tmpvar_39));
  col_5 = tmpvar_55;
  highp vec4 tmpvar_56;
  tmpvar_56 = (col_5 + tmpvar_32);
  col_5 = tmpvar_56;
  gl_FragData[0] = col_5;
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 glstate_matrix_mvp;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  xlv_TEXCOORD0 = normalize(_glesNormal);
  gl_Position = (glstate_matrix_mvp * _glesVertex);
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD0;
uniform highp vec3 _SunDirection;
uniform highp float _PanningSpeedY;
uniform highp float _PanningSpeedX;
uniform highp float _SunCloudIntensity;
uniform highp float _CloudsThreshold;
uniform highp float _CloudsSmoothness;
uniform lowp vec4 _CloudsColor;
uniform sampler2D _CloudsTexture;
uniform highp float _StarsIntensity;
uniform sampler2D _Stars;
uniform highp float _MoonPhase;
uniform lowp vec4 _MoonColor;
uniform highp float _MoonSize;
uniform highp float _SunSize;
uniform lowp vec4 _SunColor;
uniform highp float _TopOffset;
uniform highp float _TopSmoothness;
uniform highp float _MiddleOffset;
uniform highp float _MiddleSmoothness;
uniform lowp vec4 _ColorTop;
uniform lowp vec4 _ColorMiddle;
uniform lowp vec4 _ColorBottom;
uniform highp vec4 _Time;
void main ()
{
  lowp vec4 cloudsCol_1;
  lowp vec2 starUV_2;
  highp float clouds_3;
  highp float cloudsTex_4;
  lowp vec4 col_5;
  lowp vec3 coords_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize(xlv_TEXCOORD0);
  coords_6 = tmpvar_7;
  lowp float r_8;
  if ((abs(coords_6.z) > (1e-08 * abs(coords_6.x)))) {
    lowp float y_over_x_9;
    y_over_x_9 = (coords_6.x / coords_6.z);
    lowp float s_10;
    lowp float x_11;
    x_11 = (y_over_x_9 * inversesqrt(((y_over_x_9 * y_over_x_9) + 1.0)));
    s_10 = (sign(x_11) * (1.5708 - (sqrt((1.0 - abs(x_11))) * (1.5708 + (abs(x_11) * (-0.214602 + (abs(x_11) * (0.0865667 + (abs(x_11) * -0.0310296)))))))));
    r_8 = s_10;
    if ((coords_6.z < 0.0)) {
      if ((coords_6.x >= 0.0)) {
        r_8 = (s_10 + 3.14159);
      } else {
        r_8 = (r_8 - 3.14159);
      };
    };
  } else {
    r_8 = (sign(coords_6.x) * 1.5708);
  };
  highp float tmpvar_12;
  highp float t_13;
  t_13 = max (min (((xlv_TEXCOORD0.y - _MiddleOffset) / (0.5 - ((1.0 - _MiddleSmoothness) / 2.0))), 1.0), 0.0);
  tmpvar_12 = (t_13 * (t_13 * (3.0 - (2.0 * t_13))));
  highp float t_14;
  t_14 = max (min ((((xlv_TEXCOORD0.y - _TopOffset) - 0.5) / ((1.0 - ((1.0 - _TopSmoothness) / 2.0)) - 0.5)), 1.0), 0.0);
  highp vec4 tmpvar_15;
  tmpvar_15 = mix (_ColorBottom, _ColorMiddle, vec4(tmpvar_12));
  col_5 = tmpvar_15;
  highp vec4 tmpvar_16;
  tmpvar_16 = mix (col_5, _ColorTop, vec4((t_14 * (t_14 * (3.0 - (2.0 * t_14))))));
  col_5 = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD0.y - _CloudsThreshold);
  lowp vec2 tmpvar_18;
  tmpvar_18.x = ((r_8 / 6.28318) * 5.0);
  tmpvar_18.y = (((sign(coords_6.y) * (1.5708 - (sqrt((1.0 - abs(coords_6.y))) * (1.5708 + (abs(coords_6.y) * (-0.214602 + (abs(coords_6.y) * (0.0865667 + (abs(coords_6.y) * -0.0310296))))))))) / 1.5708) * 5.0);
  highp vec2 tmpvar_19;
  tmpvar_19.x = _PanningSpeedX;
  tmpvar_19.y = _PanningSpeedY;
  highp vec2 P_20;
  P_20 = (tmpvar_18 + (tmpvar_19 * _Time.y));
  lowp float tmpvar_21;
  tmpvar_21 = texture2D (_CloudsTexture, P_20).x;
  cloudsTex_4 = tmpvar_21;
  highp float t_22;
  t_22 = max (min (((cloudsTex_4 - tmpvar_17) / ((tmpvar_17 + _CloudsSmoothness) - tmpvar_17)), 1.0), 0.0);
  clouds_3 = (t_22 * (t_22 * (3.0 - (2.0 * t_22))));
  lowp float r_23;
  if ((abs(coords_6.z) > (1e-08 * abs(coords_6.y)))) {
    lowp float y_over_x_24;
    y_over_x_24 = (coords_6.y / coords_6.z);
    lowp float s_25;
    lowp float x_26;
    x_26 = (y_over_x_24 * inversesqrt(((y_over_x_24 * y_over_x_24) + 1.0)));
    s_25 = (sign(x_26) * (1.5708 - (sqrt((1.0 - abs(x_26))) * (1.5708 + (abs(x_26) * (-0.214602 + (abs(x_26) * (0.0865667 + (abs(x_26) * -0.0310296)))))))));
    r_23 = s_25;
    if ((coords_6.z < 0.0)) {
      if ((coords_6.y >= 0.0)) {
        r_23 = (s_25 + 3.14159);
      } else {
        r_23 = (r_23 - 3.14159);
      };
    };
  } else {
    r_23 = (sign(coords_6.y) * 1.5708);
  };
  lowp float tmpvar_27;
  tmpvar_27 = (r_23 / 6.28318);
  lowp float tmpvar_28;
  tmpvar_28 = ((sign(coords_6.x) * (1.5708 - (sqrt((1.0 - abs(coords_6.x))) * (1.5708 + (abs(coords_6.x) * (-0.214602 + (abs(coords_6.x) * (0.0865667 + (abs(coords_6.x) * -0.0310296))))))))) / 1.5708);
  highp vec2 tmpvar_29;
  tmpvar_29.x = ((tmpvar_27 * 5.0) - (_Time.x / 5.0));
  tmpvar_29.y = (tmpvar_28 * 2.0);
  starUV_2 = tmpvar_29;
  lowp vec4 tmpvar_30;
  tmpvar_30 = texture2D (_Stars, starUV_2);
  highp float t_31;
  t_31 = max (min ((((xlv_TEXCOORD0.y * 10.0) - 0.5) / 0.5), 1.0), 0.0);
  highp float tmpvar_32;
  tmpvar_32 = ((((tmpvar_30 * _StarsIntensity) * clamp ((_SunDirection.y / 2.0), 0.0, 1.0)) * (1.0 - clouds_3)).x * (t_31 * (t_31 * (3.0 - (2.0 * t_31)))));
  highp float tmpvar_33;
  highp vec3 p_34;
  p_34 = (normalize(xlv_TEXCOORD0) - -(_SunDirection));
  tmpvar_33 = sqrt(dot (p_34, p_34));
  highp float t_35;
  t_35 = max (min ((tmpvar_33 / _SunSize), 1.0), 0.0);
  highp float tmpvar_36;
  tmpvar_36 = max ((clouds_3 * _CloudsColor.w), (t_35 * (t_35 * (3.0 - (2.0 * t_35)))));
  highp vec3 p_37;
  p_37 = (normalize(xlv_TEXCOORD0) - _SunDirection);
  highp vec3 p_38;
  p_38 = ((normalize(xlv_TEXCOORD0) - (vec3(0.0, 0.0, 0.1) * _MoonPhase)) - _SunDirection);
  highp float tmpvar_39;
  tmpvar_39 = clamp ((((float((_MoonSize >= sqrt(dot (p_37, p_37)))) - float((_MoonSize >= sqrt(dot (p_38, p_38))))) * _SunDirection.y) - clouds_3), 0.0, 1.0);
  highp float t_40;
  t_40 = max (min (((cloudsTex_4 - tmpvar_17) / (((tmpvar_17 + _CloudsSmoothness) + 0.1) - tmpvar_17)), 1.0), 0.0);
  highp float edge0_41;
  edge0_41 = ((tmpvar_17 + _CloudsSmoothness) + 0.1);
  highp float t_42;
  t_42 = max (min (((cloudsTex_4 - edge0_41) / (((tmpvar_17 + _CloudsSmoothness) + 0.4) - edge0_41)), 1.0), 0.0);
  highp float tmpvar_43;
  tmpvar_43 = ((t_40 * (t_40 * (3.0 - (2.0 * t_40)))) - (t_42 * (t_42 * (3.0 - (2.0 * t_42)))));
  highp float tmpvar_44;
  tmpvar_44 = ((mix (clouds_3, tmpvar_43, 0.5) * tmpvar_12) * _CloudsColor.w);
  clouds_3 = tmpvar_44;
  highp float t_45;
  t_45 = max (min (((cloudsTex_4 - tmpvar_17) / ((tmpvar_17 + _CloudsSmoothness) - tmpvar_17)), 1.0), 0.0);
  highp float edge0_46;
  edge0_46 = (tmpvar_17 + 0.02);
  highp float t_47;
  t_47 = max (min (((cloudsTex_4 - edge0_46) / (((tmpvar_17 + _CloudsSmoothness) + 0.02) - edge0_46)), 1.0), 0.0);
  highp float edge0_48;
  edge0_48 = (_SunSize * 3.0);
  highp float t_49;
  t_49 = max (min (((tmpvar_33 - edge0_48) / -(edge0_48)), 1.0), 0.0);
  highp vec4 tmpvar_50;
  tmpvar_50 = mix (_SunColor, col_5, vec4(tmpvar_36));
  col_5 = tmpvar_50;
  highp float t_51;
  t_51 = max (min (((tmpvar_33 - 0.2) / -0.2), 1.0), 0.0);
  highp vec4 tmpvar_52;
  tmpvar_52 = mix (_CloudsColor, (_CloudsColor + tmpvar_33), vec4(((tmpvar_43 * (t_51 * (t_51 * (3.0 - (2.0 * t_51))))) * _SunCloudIntensity)));
  cloudsCol_1 = tmpvar_52;
  highp vec4 tmpvar_53;
  tmpvar_53 = mix (col_5, cloudsCol_1, vec4(tmpvar_44));
  col_5 = tmpvar_53;
  highp vec4 tmpvar_54;
  tmpvar_54 = (col_5 + ((((t_45 * (t_45 * (3.0 - (2.0 * t_45)))) - (t_47 * (t_47 * (3.0 - (2.0 * t_47))))) * ((t_49 * (t_49 * (3.0 - (2.0 * t_49)))) * _CloudsColor.w)) * _SunColor));
  col_5 = tmpvar_54;
  highp vec4 tmpvar_55;
  tmpvar_55 = mix (col_5, _MoonColor, vec4(tmpvar_39));
  col_5 = tmpvar_55;
  highp vec4 tmpvar_56;
  tmpvar_56 = (col_5 + tmpvar_32);
  col_5 = tmpvar_56;
  gl_FragData[0] = col_5;
}



#endif"
}

SubProgram "gles3 " {
Keywords { }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal (normalize(_glesNormal))
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 322
struct v2f {
    highp vec3 uv;
    highp vec4 vertex;
};
#line 315
struct appdata {
    highp vec4 vertex;
    highp vec3 uv;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform highp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 328
uniform lowp vec4 _ColorBottom;
uniform lowp vec4 _ColorMiddle;
uniform lowp vec4 _ColorTop;
uniform highp float _MiddleSmoothness;
#line 332
uniform highp float _MiddleOffset;
uniform highp float _TopSmoothness;
uniform highp float _TopOffset;
uniform lowp vec4 _SunColor;
#line 336
uniform highp float _SunSize;
uniform highp float _MoonSize;
uniform lowp vec4 _MoonColor;
uniform highp float _MoonPhase;
#line 340
uniform sampler2D _Stars;
uniform highp vec4 _Stars_ST;
uniform highp float _StarsIntensity;
uniform sampler2D _CloudsTexture;
#line 344
uniform highp vec4 _CloudsTexture_ST;
uniform lowp vec4 _CloudsColor;
uniform highp float _CloudsSmoothness;
uniform highp float _CloudsThreshold;
#line 348
uniform highp float _SunCloudIntensity;
uniform highp float _PanningSpeedX;
uniform highp float _PanningSpeedY;
uniform highp vec3 _SunDirection;
#line 352
#line 352
v2f vert( in appdata v ) {
    v2f o;
    o.vertex = (glstate_matrix_mvp * v.vertex);
    #line 356
    o.uv = v.normal;
    return o;
}
out highp vec3 xlv_TEXCOORD0;
void main() {
    v2f xl_retval;
    appdata xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.uv = vec3(gl_MultiTexCoord0);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vert( xlt_v);
    xlv_TEXCOORD0 = vec3(xl_retval.uv);
    gl_Position = vec4(xl_retval.vertex);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 322
struct v2f {
    highp vec3 uv;
    highp vec4 vertex;
};
#line 315
struct appdata {
    highp vec4 vertex;
    highp vec3 uv;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform highp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 328
uniform lowp vec4 _ColorBottom;
uniform lowp vec4 _ColorMiddle;
uniform lowp vec4 _ColorTop;
uniform highp float _MiddleSmoothness;
#line 332
uniform highp float _MiddleOffset;
uniform highp float _TopSmoothness;
uniform highp float _TopOffset;
uniform lowp vec4 _SunColor;
#line 336
uniform highp float _SunSize;
uniform highp float _MoonSize;
uniform lowp vec4 _MoonColor;
uniform highp float _MoonPhase;
#line 340
uniform sampler2D _Stars;
uniform highp vec4 _Stars_ST;
uniform highp float _StarsIntensity;
uniform sampler2D _CloudsTexture;
#line 344
uniform highp vec4 _CloudsTexture_ST;
uniform lowp vec4 _CloudsColor;
uniform highp float _CloudsSmoothness;
uniform highp float _CloudsThreshold;
#line 348
uniform highp float _SunCloudIntensity;
uniform highp float _PanningSpeedX;
uniform highp float _PanningSpeedY;
uniform highp vec3 _SunDirection;
#line 352
#line 359
lowp vec4 frag( in v2f i ) {
    #line 361
    lowp vec3 coords = normalize(i.uv);
    lowp float tau = (atan( coords.x, coords.z) / 6.28318);
    lowp float pi = (asin(coords.y) / 1.5708);
    lowp vec2 skybox_uv = vec2( tau, pi);
    #line 365
    highp float middleThreshold = smoothstep( 0.0, (0.5 - ((1.0 - _MiddleSmoothness) / 2.0)), (i.uv.y - _MiddleOffset));
    highp float topThreshold = smoothstep( 0.5, (1.0 - ((1.0 - _TopSmoothness) / 2.0)), (i.uv.y - _TopOffset));
    lowp vec4 col = mix( _ColorBottom, _ColorMiddle, vec4( middleThreshold));
    col = mix( col, _ColorTop, vec4( topThreshold));
    #line 369
    highp float cloudsThreshold = (i.uv.y - _CloudsThreshold);
    highp float cloudsTex = float( texture( _CloudsTexture, (vec2( (skybox_uv.x * 5.0), (skybox_uv.y * 5.0)) + (vec2( _PanningSpeedX, _PanningSpeedY) * _Time.y))));
    highp float clouds = smoothstep( cloudsThreshold, (cloudsThreshold + _CloudsSmoothness), cloudsTex);
    lowp float star_tau = (atan( coords.y, coords.z) / 6.28318);
    #line 373
    lowp float star_pi = (asin(coords.x) / 1.5708);
    lowp vec2 starUV = vec2( ((star_tau * 5.0) - (_Time.x / 5.0)), (star_pi * 2.0));
    highp float stars = float( (((texture( _Stars, starUV) * _StarsIntensity) * xll_saturate_f((_SunDirection.y / 2.0))) * (1.0 - clouds)));
    stars *= smoothstep( 0.5, 1.0, (i.uv.y * 10.0));
    #line 377
    highp float sunSDF = distance( normalize(i.uv.xyz), (-_SunDirection));
    highp float sun = max( (clouds * _CloudsColor.w), smoothstep( 0.0, _SunSize, sunSDF));
    highp float moonSDF = distance( normalize(i.uv.xyz), _SunDirection);
    highp float moonPhaseSDF = distance( (normalize(i.uv.xyz) - (vec3( 0.0, 0.0, 0.1) * _MoonPhase)), _SunDirection);
    #line 381
    highp float moon = step( moonSDF, _MoonSize);
    moon -= step( moonPhaseSDF, _MoonSize);
    moon = xll_saturate_f(((moon * _SunDirection.y) - clouds));
    highp float cloudShading = (smoothstep( cloudsThreshold, ((cloudsThreshold + _CloudsSmoothness) + 0.1), cloudsTex) - smoothstep( ((cloudsThreshold + _CloudsSmoothness) + 0.1), ((cloudsThreshold + _CloudsSmoothness) + 0.4), cloudsTex));
    #line 385
    clouds = ((mix( clouds, cloudShading, 0.5) * middleThreshold) * _CloudsColor.w);
    highp float silverLining = (smoothstep( cloudsThreshold, (cloudsThreshold + _CloudsSmoothness), cloudsTex) - smoothstep( (cloudsThreshold + 0.02), ((cloudsThreshold + _CloudsSmoothness) + 0.02), cloudsTex));
    silverLining *= (smoothstep( (_SunSize * 3.0), 0.0, sunSDF) * _CloudsColor.w);
    col = mix( _SunColor, col, vec4( sun));
    #line 389
    lowp vec4 cloudsCol = mix( _CloudsColor, (_CloudsColor + sunSDF), vec4( ((cloudShading * smoothstep( 0.2, 0.0, sunSDF)) * _SunCloudIntensity)));
    col = mix( col, cloudsCol, vec4( clouds));
    col += (silverLining * _SunColor);
    col = mix( col, _MoonColor, vec4( moon));
    #line 393
    col += stars;
    return col;
}
in highp vec3 xlv_TEXCOORD0;
void main() {
    lowp vec4 xl_retval;
    v2f xlt_i;
    xlt_i.uv = vec3(xlv_TEXCOORD0);
    xlt_i.vertex = vec4(0.0);
    xl_retval = frag( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

}
Program "fp" {
// Fragment combos: 1
//   opengl - ALU: 208 to 208, TEX: 2 to 2
//   d3d9 - ALU: 198 to 198, TEX: 2 to 2
//   d3d11 - ALU: 146 to 146, TEX: 2 to 2, FLOW: 1 to 1
SubProgram "opengl " {
Keywords { }
Vector 0 [_Time]
Vector 1 [_ColorBottom]
Vector 2 [_ColorMiddle]
Vector 3 [_ColorTop]
Float 4 [_MiddleSmoothness]
Float 5 [_MiddleOffset]
Float 6 [_TopSmoothness]
Float 7 [_TopOffset]
Vector 8 [_SunColor]
Float 9 [_SunSize]
Float 10 [_MoonSize]
Vector 11 [_MoonColor]
Float 12 [_MoonPhase]
Float 13 [_StarsIntensity]
Vector 14 [_CloudsColor]
Float 15 [_CloudsSmoothness]
Float 16 [_CloudsThreshold]
Float 17 [_SunCloudIntensity]
Float 18 [_PanningSpeedX]
Float 19 [_PanningSpeedY]
Vector 20 [_SunDirection]
SetTexture 0 [_CloudsTexture] 2D
SetTexture 1 [_Stars] 2D
"3.0-!!ARBfp1.0
# 208 ALU, 2 TEX
PARAM c[28] = { program.local[0..20],
		{ 0, 0.1, -0.01348114, 0.0574646 },
		{ 0.12121582, 0.19567871, 0.33300781, 1 },
		{ 1.5703125, 3.140625, 0.79601991, 0 },
		{ -0.018722534, 0.074279785, 0.2121582, 2 },
		{ 3.1840796, 0, 3, 0.40000001 },
		{ 0.5, 0.2, -5, 0 },
		{ 0.02, 1.2736318, 0, 10 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
TEMP R6;
ADD R4.x, fragment.texcoord[0].y, -c[16];
ADD R4.y, R4.x, c[15].x;
DP3 R0.x, fragment.texcoord[0], fragment.texcoord[0];
RSQ R0.z, R0.x;
MUL R3.xyz, R0.z, fragment.texcoord[0];
ABS R0.w, R3.x;
ABS R0.y, R3.z;
MAX R0.x, R0.w, R0.y;
RCP R1.x, R0.x;
MIN R0.x, R0.w, R0.y;
MUL R0.x, R0, R1;
MUL R1.x, R0, R0;
MAD R1.y, R1.x, c[21].z, c[21].w;
MAD R1.y, R1, R1.x, -c[22].x;
MAD R1.y, R1, R1.x, c[22];
MAD R1.y, R1, R1.x, -c[22].z;
MAD R1.x, R1.y, R1, c[22].w;
MUL R1.x, R1, R0;
ABS R3.w, R3.y;
ADD R2.x, R4.y, c[21].y;
ADD R1.y, -R1.x, c[23].x;
ADD R0.x, R0.w, -R0.y;
CMP R0.x, -R0, R1.y, R1;
ADD R1.x, -R0, c[23].y;
CMP R0.x, R3.z, R1, R0;
CMP R0.x, R3, -R0, R0;
MUL R1.x, R0, c[23].z;
MAD R1.y, R3.w, c[24].x, c[24];
ADD R0.x, -R3.w, c[22].w;
MAD R1.y, R3.w, R1, -c[24].z;
RSQ R0.x, R0.x;
RCP R0.x, R0.x;
MAD R1.y, R3.w, R1, c[23].x;
MAD R1.y, -R0.x, R1, c[23].x;
SLT R0.x, R3.y, c[21];
MUL R0.x, R0, R1.y;
MAD R0.x, -R0, c[24].w, R1.y;
MUL R1.y, R0.x, c[25].x;
ADD R0.x, R4.y, -R2;
MOV R1.z, c[18].x;
MOV R1.w, c[19].x;
MAD R1.xy, R1.zwzw, c[0].y, R1;
ADD R1.z, R0.x, c[25].w;
TEX R0.x, R1, texture[0], 2D;
ADD R1.w, -R4.x, R4.y;
ADD R5.y, R0.x, -R4.x;
RCP R1.y, R1.z;
ADD R1.x, R0, -R2;
MUL_SAT R1.x, R1, R1.y;
MUL R1.y, -R1.x, c[24].w;
ADD R4.x, R4, c[27];
ADD R0.x, R0, -R4;
ADD R1.y, R1, c[25].z;
MUL R1.x, R1, R1;
MUL R2.x, R1, R1.y;
MAD R1.xyz, -R0.z, fragment.texcoord[0], -c[20];
DP3 R1.y, R1, R1;
ADD R1.w, R1, c[21].y;
RCP R1.x, R1.w;
RSQ R1.y, R1.y;
RCP R4.w, R1.y;
MUL_SAT R1.x, R5.y, R1;
MUL R1.y, R1.x, R1.x;
ADD R1.z, R4.w, -c[26].y;
MUL_SAT R1.z, R1, c[26];
MUL R1.x, -R1, c[24].w;
ADD R1.x, R1, c[25].z;
MAD R4.z, R1.y, R1.x, -R2.x;
MUL R1.w, -R1.z, c[24];
ADD R1.y, R1.w, c[25].z;
MUL R1.x, R1.z, R1.z;
MUL R1.x, R1, R1.y;
MUL R1.y, R4.z, R1.x;
MOV R2.x, c[22].w;
ADD R1.x, R2, -c[4];
MAD R2.y, -R1.x, c[26].x, c[26].x;
MUL R5.z, R1.y, c[17].x;
RCP R2.z, R2.y;
MOV R1, c[1];
ADD R2.y, fragment.texcoord[0], -c[5].x;
MUL_SAT R2.y, R2, R2.z;
MUL R2.z, -R2.y, c[24].w;
MUL R2.y, R2, R2;
ADD R2.z, R2, c[25];
MUL R5.x, R2.y, R2.z;
ADD R2.x, R2, -c[6];
MAD R2.y, -R2.x, c[26].x, c[26].x;
ADD R1, -R1, c[2];
ADD R2.x, fragment.texcoord[0].y, -c[7];
RCP R2.y, R2.y;
ADD R2.x, R2, -c[26];
MUL_SAT R5.w, R2.x, R2.y;
MUL R6.x, -R5.w, c[24].w;
MAD R1, R5.x, R1, c[1];
ADD R2, -R1, c[3];
MUL R5.w, R5, R5;
ADD R6.x, R6, c[25].z;
MUL R6.x, R5.w, R6;
MAD R1, R6.x, R2, R1;
RCP R5.w, c[15].x;
MUL_SAT R2.x, R5.y, R5.w;
MUL R5.w, R2.x, R2.x;
MUL R2.x, -R2, c[24].w;
ADD R6.x, R2, c[25].z;
MUL R5.y, R5.w, R6.x;
RCP R2.y, c[9].x;
MUL_SAT R2.y, R4.w, R2;
MUL R2.x, -R2.y, c[24].w;
ADD R2.z, R2.x, c[25];
MUL R2.x, R2.y, R2.y;
MUL R2.y, R2.x, R2.z;
MUL R2.x, R5.y, c[14].w;
ADD R1, R1, -c[8];
MAX R2.x, R2, R2.y;
MAD R2, R2.x, R1, c[8];
MAD R1, R5.z, R4.w, c[14];
ADD R1, R1, -R2;
ADD R5.z, R4.y, -R4.x;
ADD R4.z, -R5.y, R4;
MAD R4.y, R4.z, c[26].x, R5;
ADD R4.z, R5, c[27].x;
RCP R4.z, R4.z;
MUL_SAT R4.x, R0, R4.z;
MUL R4.y, R4, R5.x;
MUL R0.x, R4.y, c[14].w;
MAD R1, R0.x, R1, R2;
MUL R4.y, -R4.x, c[24].w;
MOV R2.z, c[25];
MUL R2.z, R2, c[9].x;
RCP R2.w, -R2.z;
ADD R2.z, R4.w, -R2;
MUL_SAT R2.z, R2, R2.w;
MUL R2.w, -R2.z, c[24];
ADD R2.x, R4.y, c[25].z;
MUL R0.x, R4, R4;
MUL R0.x, R0, R2;
MAX R2.x, R0.y, R3.w;
RCP R2.y, R2.x;
MIN R2.x, R0.y, R3.w;
MUL R2.x, R2, R2.y;
MUL R2.y, R2.x, R2.x;
MAD R4.x, R2.y, c[21].z, c[21].w;
MAD R4.x, R4, R2.y, -c[22];
MAD R4.x, R4, R2.y, c[22].y;
ADD R2.w, R2, c[25].z;
MUL R2.z, R2, R2;
MUL R2.z, R2, R2.w;
MAD R2.w, R4.x, R2.y, -c[22].z;
MAD R2.y, R2.w, R2, c[22].w;
MUL R4.x, R2.y, R2;
MUL R2.z, R2, c[14].w;
MAD R0.x, R5.w, R6, -R0;
MUL R0.x, R0, R2.z;
MUL R2, R0.x, c[8];
ADD R1, R1, R2;
ADD R0.x, -R0.y, R3.w;
ADD R4.y, -R4.x, c[23].x;
CMP R0.x, -R0, R4.y, R4;
ADD R0.y, -R0.x, c[23];
CMP R0.x, R3.z, R0.y, R0;
CMP R0.x, R3.y, -R0, R0;
MAD R3.z, R0.w, c[24].x, c[24].y;
ADD R3.y, -R0.w, c[22].w;
MAD R3.z, R0.w, R3, -c[24];
MAD R3.z, R0.w, R3, c[23].x;
RSQ R3.y, R3.y;
RCP R0.w, R3.y;
MAD R3.y, -R0.w, R3.z, c[23].x;
SLT R0.w, R3.x, c[21].x;
MUL R3.x, R0.w, R3.y;
MUL R0.w, R0.x, c[23].z;
MAD R3.y, -R3.x, c[24].w, R3;
MOV R0.x, c[0];
MAD R3.x, R0, -c[26].y, R0.w;
MUL R3.y, R3, c[27];
TEX R0.x, R3, texture[1], 2D;
MAD R3.xyz, -R0.z, fragment.texcoord[0], c[20];
DP3 R3.x, R3, R3;
MOV R0.w, c[20].y;
RSQ R3.x, R3.x;
RCP R3.x, R3.x;
ADD R0.y, -R5, c[22].w;
MUL_SAT R0.w, R0, c[26].x;
MUL R0.x, R0, c[13];
MUL R0.x, R0, R0.w;
MUL R0.w, R0.x, R0.y;
MOV R0.xy, c[21];
MUL R0.xy, R0, c[12].x;
MAD R0.xyz, R0.z, fragment.texcoord[0], -R0.xxyw;
ADD R0.xyz, -R0, c[20];
DP3 R0.x, R0, R0;
MUL R3.y, fragment.texcoord[0], c[27].w;
ADD R0.y, R3, -c[26].x;
MUL_SAT R0.y, R0, c[24].w;
MUL R0.z, -R0.y, c[24].w;
RSQ R0.x, R0.x;
RCP R0.x, R0.x;
SGE R3.x, c[10], R3;
SGE R0.x, c[10], R0;
ADD R0.x, R3, -R0;
ADD R0.z, R0, c[25];
MUL R0.y, R0, R0;
MUL R0.y, R0, R0.z;
MUL R3.x, R0.w, R0.y;
ADD R2, -R1, c[11];
MAD_SAT R0.x, R0, c[20].y, -R5.y;
MAD R0, R0.x, R2, R1;
ADD result.color, R0, R3.x;
END
# 208 instructions, 7 R-regs
"
}

SubProgram "d3d9 " {
Keywords { }
Vector 0 [_Time]
Vector 1 [_ColorBottom]
Vector 2 [_ColorMiddle]
Vector 3 [_ColorTop]
Float 4 [_MiddleSmoothness]
Float 5 [_MiddleOffset]
Float 6 [_TopSmoothness]
Float 7 [_TopOffset]
Vector 8 [_SunColor]
Float 9 [_SunSize]
Float 10 [_MoonSize]
Vector 11 [_MoonColor]
Float 12 [_MoonPhase]
Float 13 [_StarsIntensity]
Vector 14 [_CloudsColor]
Float 15 [_CloudsSmoothness]
Float 16 [_CloudsThreshold]
Float 17 [_SunCloudIntensity]
Float 18 [_PanningSpeedX]
Float 19 [_PanningSpeedY]
Vector 20 [_SunDirection]
SetTexture 0 [_CloudsTexture] 2D
SetTexture 1 [_Stars] 2D
"ps_3_0
; 198 ALU, 2 TEX
dcl_2d s0
dcl_2d s1
def c21, 0.00000000, 0.10000000, -0.01348114, 0.05746460
def c22, -0.12121582, 0.19567871, -0.33300781, 1.57031250
def c23, 3.14062500, 0.79601991, 0.00000000, 1.00000000
def c24, -0.01872253, 0.07427979, -0.21215820, 2.00000000
def c25, 3.18407965, 2.00000000, 3.00000000, 0.40000001
def c26, 0.50000000, -0.20000000, -5.00000000, -0.50000000
def c27, 0.02000000, 1.27363181, 10.00000000, -0.50000000
dcl_texcoord0 v0.xyz
add r4.x, v0.y, -c16
add r4.y, r4.x, c15.x
dp3 r0.x, v0, v0
rsq r0.z, r0.x
mul r3.xyz, r0.z, v0
abs_pp r0.y, r3.z
abs_pp r0.w, r3.x
max_pp r0.x, r0.w, r0.y
rcp_pp r1.x, r0.x
min_pp r0.x, r0.w, r0.y
mul_pp r0.x, r0, r1
mul_pp r1.x, r0, r0
mad_pp r1.y, r1.x, c21.z, c21.w
mad_pp r1.y, r1, r1.x, c22.x
mad_pp r1.y, r1, r1.x, c22
mad_pp r1.y, r1, r1.x, c22.z
mad_pp r1.x, r1.y, r1, c23.w
mul_pp r1.x, r1, r0
abs_pp r3.w, r3.y
add r2.x, r4.y, c21.y
add_pp r1.y, -r1.x, c22.w
add_pp r0.x, r0.w, -r0.y
cmp_pp r0.x, -r0, r1, r1.y
add_pp r1.x, -r0, c23
cmp_pp r0.x, r3.z, r0, r1
cmp_pp r0.x, r3, r0, -r0
mul_pp r1.x, r0, c23.y
mad_pp r1.y, r3.w, c24.x, c24
add_pp r0.x, -r3.w, c23.w
mad_pp r1.y, r3.w, r1, c24.z
rsq_pp r0.x, r0.x
rcp_pp r0.x, r0.x
mad_pp r1.y, r3.w, r1, c22.w
mad_pp r1.y, -r0.x, r1, c22.w
cmp_pp r0.x, r3.y, c23.z, c23.w
mul_pp r0.x, r0, r1.y
mad_pp r0.x, -r0, c24.w, r1.y
mul_pp r1.y, r0.x, c25.x
add r0.x, r4.y, -r2
mov r1.z, c18.x
mov r1.w, c19.x
mad r1.xy, r1.zwzw, c0.y, r1
add r1.z, r0.x, c25.w
texld r0.x, r1, s0
add r1.w, -r4.x, r4.y
add r5.y, r0.x, -r4.x
rcp r1.y, r1.z
add r1.x, r0, -r2
mul_sat r1.x, r1, r1.y
mad r1.y, -r1.x, c25, c25.z
add r4.x, r4, c27
mul r1.x, r1, r1
mul r2.x, r1, r1.y
mad r1.xyz, -r0.z, v0, -c20
dp3 r1.y, r1, r1
add r1.w, r1, c21.y
rcp r1.x, r1.w
rsq r1.y, r1.y
rcp r4.w, r1.y
mul_sat r1.x, r5.y, r1
mul r1.y, r1.x, r1.x
mad r1.x, -r1, c25.y, c25.z
add r1.z, r4.w, c26.y
mad r4.z, r1.y, r1.x, -r2.x
mul_sat r1.z, r1, c26
mul r1.x, r1.z, r1.z
mad r1.y, -r1.z, c25, c25.z
mul r1.y, r1.x, r1
mul r1.y, r4.z, r1
mov r1.x, c4
add r1.x, c23.w, -r1
mad r2.x, -r1, c26, c26
mul r5.z, r1.y, c17.x
rcp r2.y, r2.x
mov_pp r1, c2
add r2.x, v0.y, -c5
mul_sat r2.x, r2, r2.y
mul r2.y, r2.x, r2.x
mad r2.x, -r2, c25.y, c25.z
mul r5.x, r2.y, r2
mov r2.z, c6.x
add r2.x, c23.w, -r2.z
mad r2.y, -r2.x, c26.x, c26.x
add_pp r1, -c1, r1
add r2.x, v0.y, -c7
add r0.x, r0, -r4
mad_pp r1, r5.x, r1, c1
rcp r2.y, r2.y
add r2.x, r2, c26.w
mul_sat r5.w, r2.x, r2.y
mad r6.x, -r5.w, c25.y, c25.z
mul r5.w, r5, r5
mul r6.x, r5.w, r6
add_pp r2, -r1, c3
mad_pp r1, r6.x, r2, r1
rcp r5.w, c15.x
mul_sat r2.x, r5.y, r5.w
mul r5.w, r2.x, r2.x
mad r6.x, -r2, c25.y, c25.z
mul r5.y, r5.w, r6.x
rcp r2.y, c9.x
mul_sat r2.x, r4.w, r2.y
mad r2.y, -r2.x, c25, c25.z
mul r2.x, r2, r2
mul r2.y, r2.x, r2
mul r2.x, r5.y, c14.w
add r4.z, -r5.y, r4
add_pp r1, r1, -c8
max r2.x, r2, r2.y
mad_pp r2, r2.x, r1, c8
mad_pp r1, r5.z, r4.w, c14
add_pp r1, r1, -r2
add r5.z, r4.y, -r4.x
mad r4.z, r4, c26.x, r5.y
mul r4.y, r4.z, r5.x
mul r4.y, r4, c14.w
mad_pp r1, r4.y, r1, r2
max_pp r2.y, r0, r3.w
add r4.z, r5, c27.x
rcp r4.z, r4.z
mul_sat r0.x, r0, r4.z
mul r2.x, r0, r0
mad r0.x, -r0, c25.y, c25.z
mul r2.x, r2, r0
mov r2.z, c9.x
rcp_pp r2.y, r2.y
min_pp r0.x, r0.y, r3.w
mul_pp r0.x, r0, r2.y
mad r2.y, r5.w, r6.x, -r2.x
mul_pp r2.x, r0, r0
mad_pp r2.w, r2.x, c21.z, c21
mad_pp r4.x, r2.w, r2, c22
mul r2.z, c25, r2
rcp r2.w, -r2.z
add r2.z, r4.w, -r2
mul_sat r2.z, r2, r2.w
mad r2.w, -r2.z, c25.y, c25.z
mul r2.z, r2, r2
mul r2.z, r2, r2.w
mad_pp r4.x, r4, r2, c22.y
mad_pp r2.w, r4.x, r2.x, c22.z
mad_pp r2.w, r2, r2.x, c23
mul_pp r4.x, r2.w, r0
mul r2.z, r2, c14.w
mul r2.x, r2.y, r2.z
mul r2, r2.x, c8
add_pp r1, r1, r2
add_pp r0.x, -r0.y, r3.w
add_pp r4.y, -r4.x, c22.w
cmp_pp r0.x, -r0, r4, r4.y
add_pp r0.y, -r0.x, c23.x
cmp_pp r0.x, r3.z, r0, r0.y
cmp_pp r0.x, r3.y, r0, -r0
mad_pp r3.y, r0.w, c24.x, c24
add_pp r0.y, -r0.w, c23.w
mad_pp r3.y, r0.w, r3, c24.z
rsq_pp r0.y, r0.y
mad_pp r0.w, r0, r3.y, c22
rcp_pp r0.y, r0.y
mad_pp r0.w, -r0.y, r0, c22
cmp_pp r0.y, r3.x, c23.z, c23.w
mul_pp r3.x, r0.y, r0.w
mul_pp r0.y, r0.x, c23
mov r0.x, c26.y
mad_pp r0.w, -r3.x, c24, r0
mad r0.x, c0, r0, r0.y
mul_pp r0.y, r0.w, c27
texld r0.x, r0, s1
mov r0.w, c26.x
mul_sat r0.y, c20, r0.w
mul r0.x, r0, c13
mul r0.x, r0, r0.y
add r3.z, -r5.y, c23.w
mul r0.w, r0.x, r3.z
mad r3.xyz, -r0.z, v0, c20
mov r0.x, c12
mul r0.xy, c21, r0.x
dp3 r3.x, r3, r3
mad r0.xyz, r0.z, v0, -r0.xxyw
add r0.xyz, -r0, c20
dp3 r0.y, r0, r0
rsq r3.x, r3.x
rcp r3.x, r3.x
add r0.x, -r3, c10
rsq r0.y, r0.y
rcp r0.y, r0.y
mad r0.z, v0.y, c27, c27.w
mul_sat r0.z, r0, c24.w
mad r3.x, -r0.z, c25.y, c25.z
add r0.y, -r0, c10.x
mul r0.z, r0, r0
mul r0.z, r0, r3.x
cmp r0.y, r0, c23.w, c23.z
cmp r0.x, r0, c23.w, c23.z
add r0.x, r0, -r0.y
mul r3.x, r0.w, r0.z
add_pp r2, -r1, c11
mad_sat r0.x, r0, c20.y, -r5.y
mad_pp r0, r0.x, r2, r1
add_pp oC0, r0, r3.x
"
}

SubProgram "d3d11 " {
Keywords { }
ConstBuffer "$Globals" 240 // 240 used size, 23 vars
Vector 16 [_ColorBottom] 4
Vector 32 [_ColorMiddle] 4
Vector 48 [_ColorTop] 4
Float 64 [_MiddleSmoothness]
Float 68 [_MiddleOffset]
Float 72 [_TopSmoothness]
Float 76 [_TopOffset]
Vector 80 [_SunColor] 4
Float 96 [_SunSize]
Float 100 [_MoonSize]
Vector 112 [_MoonColor] 4
Float 128 [_MoonPhase]
Float 160 [_StarsIntensity]
Vector 192 [_CloudsColor] 4
Float 208 [_CloudsSmoothness]
Float 212 [_CloudsThreshold]
Float 216 [_SunCloudIntensity]
Float 220 [_PanningSpeedX]
Float 224 [_PanningSpeedY]
Vector 228 [_SunDirection] 3
ConstBuffer "UnityPerCamera" 128 // 16 used size, 8 vars
Vector 0 [_Time] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
SetTexture 0 [_CloudsTexture] 2D 1
SetTexture 1 [_Stars] 2D 0
// 151 instructions, 5 temp regs, 0 temp arrays:
// ALU 141 float, 0 int, 5 uint
// TEX 2 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0
eefiecedmjdojilphjpjofblkgfmempkdpfehfcgabaaaaaahmbfaaaaadaaaaaa
cmaaaaaaieaaaaaaliaaaaaaejfdeheofaaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaahahaaaaebaaaaaaaaaaaaaaabaaaaaa
adaaaaaaabaaaaaaapaaaaaafeeffiedepepfceeaafdfgfpfaepfdejfeejepeo
aaklklklepfdeheocmaaaaaaabaaaaaaaiaaaaaacaaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaaaaaaaaaapaaaaaafdfgfpfegbhcghgfheaaklklfdeieefclmbeaaaa
eaaaaaaacpafaaaafjaaaaaeegiocaaaaaaaaaaaapaaaaaafjaaaaaeegiocaaa
abaaaaaaabaaaaaafkaaaaadaagabaaaaaaaaaaafkaaaaadaagabaaaabaaaaaa
fibiaaaeaahabaaaaaaaaaaaffffaaaafibiaaaeaahabaaaabaaaaaaffffaaaa
gcbaaaadhcbabaaaaaaaaaaagfaaaaadpccabaaaaaaaaaaagiaaaaacafaaaaaa
baaaaaahbcaabaaaaaaaaaaaegbcbaaaaaaaaaaaegbcbaaaaaaaaaaaeeaaaaaf
bcaabaaaaaaaaaaaakaabaaaaaaaaaaadiaaaaahocaabaaaaaaaaaaaagaabaaa
aaaaaaaaagbjbaaaaaaaaaaadeaaaaajdcaabaaaabaaaaaapgapbaiaibaaaaaa
aaaaaaaajgafbaiaibaaaaaaaaaaaaaaaoaaaaakdcaabaaaabaaaaaaaceaaaaa
aaaaiadpaaaaiadpaaaaiadpaaaaiadpegaabaaaabaaaaaaddaaaaajmcaabaaa
abaaaaaapgapbaiaibaaaaaaaaaaaaaafgajbaiaibaaaaaaaaaaaaaadiaaaaah
dcaabaaaabaaaaaaegaabaaaabaaaaaaogakbaaaabaaaaaadiaaaaahmcaabaaa
abaaaaaaagaebaaaabaaaaaaagaebaaaabaaaaaadcaaaaapdcaabaaaacaaaaaa
ogakbaaaabaaaaaaaceaaaaafpkokkdmfpkokkdmaaaaaaaaaaaaaaaaaceaaaaa
dgfkkolndgfkkolnaaaaaaaaaaaaaaaadcaaaaamdcaabaaaacaaaaaaogakbaaa
abaaaaaaegaabaaaacaaaaaaaceaaaaaochgdidoochgdidoaaaaaaaaaaaaaaaa
dcaaaaamdcaabaaaacaaaaaaogakbaaaabaaaaaaegaabaaaacaaaaaaaceaaaaa
aebnkjloaebnkjloaaaaaaaaaaaaaaaadcaaaaammcaabaaaabaaaaaakgaobaaa
abaaaaaaagaebaaaacaaaaaaaceaaaaaaaaaaaaaaaaaaaaadiphhpdpdiphhpdp
diaaaaahdcaabaaaacaaaaaaogakbaaaabaaaaaaegaabaaaabaaaaaadcaaaaap
dcaabaaaacaaaaaaegaabaaaacaaaaaaaceaaaaaaaaaaamaaaaaaamaaaaaaaaa
aaaaaaaaaceaaaaanlapmjdpnlapmjdpaaaaaaaaaaaaaaaadbaaaaajmcaabaaa
acaaaaaapgapbaiaibaaaaaaaaaaaaaafgajbaiaibaaaaaaaaaaaaaaabaaaaah
dcaabaaaacaaaaaaogakbaaaacaaaaaaegaabaaaacaaaaaadcaaaaajdcaabaaa
abaaaaaaegaabaaaabaaaaaaogakbaaaabaaaaaaegaabaaaacaaaaaadbaaaaai
hcaabaaaacaaaaaalganbaaaaaaaaaaalganbaiaebaaaaaaaaaaaaaaabaaaaah
ecaabaaaabaaaaaaakaabaaaacaaaaaaabeaaaaanlapejmaaaaaaaahdcaabaaa
abaaaaaakgakbaaaabaaaaaaegaabaaaabaaaaaaddaaaaahmcaabaaaabaaaaaa
pgapbaaaaaaaaaaafgajbaaaaaaaaaaadbaaaaaimcaabaaaabaaaaaakgaobaaa
abaaaaaakgaobaiaebaaaaaaabaaaaaadeaaaaahjcaabaaaacaaaaaapgapbaaa
aaaaaaaafgajbaaaaaaaaaaabnaaaaaijcaabaaaacaaaaaaagambaaaacaaaaaa
agambaiaebaaaaaaacaaaaaaabaaaaahmcaabaaaabaaaaaakgaobaaaabaaaaaa
agambaaaacaaaaaadhaaaaakdcaabaaaabaaaaaaogakbaaaabaaaaaaegaabaia
ebaaaaaaabaaaaaaegaabaaaabaaaaaadiaaaaakfcaabaaaabaaaaaaagabbaaa
abaaaaaaaceaaaaaoplheldpaaaaaaaaoplheldpaaaaaaaadiaaaaajbcaabaaa
adaaaaaadkiacaaaaaaaaaaaanaaaaaabkiacaaaabaaaaaaaaaaaaaadiaaaaaj
ccaabaaaadaaaaaaakiacaaaaaaaaaaaaoaaaaaabkiacaaaabaaaaaaaaaaaaaa
dcaaaabajcaabaaaacaaaaaakgagbaiaibaaaaaaaaaaaaaaaceaaaaadagojjlm
aaaaaaaaaaaaaaaadagojjlmaceaaaaachbgjidnaaaaaaaaaaaaaaaachbgjidn
dcaaaaanjcaabaaaacaaaaaaagambaaaacaaaaaakgagbaiaibaaaaaaaaaaaaaa
aceaaaaaiedefjloaaaaaaaaaaaaaaaaiedefjlodcaaaaanjcaabaaaacaaaaaa
agambaaaacaaaaaakgagbaiaibaaaaaaaaaaaaaaaceaaaaakeanmjdpaaaaaaaa
aaaaaaaakeanmjdpaaaaaaalmcaabaaaadaaaaaakgagbaiambaaaaaaaaaaaaaa
aceaaaaaaaaaaaaaaaaaaaaaaaaaiadpaaaaiadpdcaaaaaoocaabaaaaaaaaaaa
agiacaiaebaaaaaaaaaaaaaaaiaaaaaaaceaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
mnmmmmdnfgaobaaaaaaaaaaaaaaaaaajocaabaaaaaaaaaaafgaobaaaaaaaaaaa
fgiocaiaebaaaaaaaaaaaaaaaoaaaaaabaaaaaahccaabaaaaaaaaaaajgahbaaa
aaaaaaaajgahbaaaaaaaaaaaelaaaaafccaabaaaaaaaaaaabkaabaaaaaaaaaaa
bnaaaaaiccaabaaaaaaaaaaabkiacaaaaaaaaaaaagaaaaaabkaabaaaaaaaaaaa
dhaaaaajccaabaaaaaaaaaaabkaabaaaaaaaaaaaabeaaaaaaaaaialpabeaaaaa
aaaaaaiaelaaaaafmcaabaaaaaaaaaaakgaobaaaadaaaaaadiaaaaahmcaabaaa
adaaaaaakgaobaaaaaaaaaaaagambaaaacaaaaaadcaaaaapmcaabaaaadaaaaaa
kgaobaaaadaaaaaaaceaaaaaaaaaaaaaaaaaaaaaaaaaaamaaaaaaamaaceaaaaa
aaaaaaaaaaaaaaaanlapejeanlapejeaabaaaaahgcaabaaaacaaaaaafgagbaaa
acaaaaaakgalbaaaadaaaaaadcaaaaajmcaabaaaaaaaaaaaagambaaaacaaaaaa
kgaobaaaaaaaaaaafgajbaaaacaaaaaaaaaaaaalmcaabaaaaaaaaaaakgaobaia
ebaaaaaaaaaaaaaaaceaaaaaaaaaaaaaaaaaaaaanlapmjdpnlapmjdpdiaaaaak
kcaabaaaabaaaaaapgalbaaaaaaaaaaaaceaaaaaaaaaaaaaimpjkcdpaaaaaaaa
oplheleaaaaaaaahmcaabaaaaaaaaaaaagambaaaabaaaaaaagaebaaaadaaaaaa
dcaaaaalbcaabaaaabaaaaaaakiacaiaebaaaaaaabaaaaaaaaaaaaaaabeaaaaa
mnmmemdockaabaaaabaaaaaaefaaaaajpcaabaaaabaaaaaaegaabaaaabaaaaaa
eghobaaaabaaaaaaaagabaaaaaaaaaaadiaaaaaibcaabaaaabaaaaaaakaabaaa
abaaaaaaakiacaaaaaaaaaaaakaaaaaaefaaaaajpcaabaaaacaaaaaaogakbaaa
aaaaaaaaeghobaaaaaaaaaaaaagabaaaabaaaaaaaaaaaaajecaabaaaaaaaaaaa
bkbabaaaaaaaaaaabkiacaiaebaaaaaaaaaaaaaaanaaaaaaaaaaaaaiicaabaaa
aaaaaaaackaabaiaebaaaaaaaaaaaaaaakaabaaaacaaaaaaaaaaaaaiccaabaaa
abaaaaaackaabaaaaaaaaaaaakiacaaaaaaaaaaaanaaaaaaaaaaaaakgcaabaaa
abaaaaaafgafbaaaabaaaaaaaceaaaaaaaaaaaaamnmmmmdnaknhkddmaaaaaaaa
aaaaaaaiicaabaaaabaaaaaackaabaiaebaaaaaaaaaaaaaabkaabaaaabaaaaaa
aaaaaaahecaabaaaaaaaaaaackaabaaaaaaaaaaaabeaaaaaaknhkddmaoaaaaak
icaabaaaabaaaaaaaceaaaaaaaaaiadpaaaaiadpaaaaiadpaaaaiadpdkaabaaa
abaaaaaadicaaaahicaabaaaabaaaaaadkaabaaaaaaaaaaadkaabaaaabaaaaaa
dcaaaaajccaabaaaacaaaaaadkaabaaaabaaaaaaabeaaaaaaaaaaamaabeaaaaa
aaaaeaeaaaaaaaaiccaabaaaabaaaaaabkaabaiaebaaaaaaabaaaaaaakaabaaa
acaaaaaaaaaaaaaibcaabaaaacaaaaaackaabaiaebaaaaaaaaaaaaaaakaabaaa
acaaaaaaaaaaaaaiecaabaaaaaaaaaaackaabaiaebaaaaaaaaaaaaaackaabaaa
abaaaaaaaoaaaaakecaabaaaaaaaaaaaaceaaaaaaaaaiadpaaaaiadpaaaaiadp
aaaaiadpckaabaaaaaaaaaaadicaaaahecaabaaaaaaaaaaackaabaaaaaaaaaaa
akaabaaaacaaaaaadicaaaahccaabaaaabaaaaaabkaabaaaabaaaaaaabeaaaaa
ffffffeadcaaaaajecaabaaaabaaaaaabkaabaaaabaaaaaaabeaaaaaaaaaaama
abeaaaaaaaaaeaeadiaaaaahkcaabaaaabaaaaaafganbaaaabaaaaaafganbaaa
abaaaaaadiaaaaahccaabaaaabaaaaaabkaabaaaabaaaaaackaabaaaabaaaaaa
dcaaaaakccaabaaaabaaaaaabkaabaaaacaaaaaadkaabaaaabaaaaaabkaabaia
ebaaaaaaabaaaaaadcaaaaakhcaabaaaacaaaaaaegbcbaaaaaaaaaaaagaabaaa
aaaaaaaajgihcaaaaaaaaaaaaoaaaaaadcaaaaalhcaabaaaadaaaaaaegbcbaaa
aaaaaaaaagaabaaaaaaaaaaajgihcaiaebaaaaaaaaaaaaaaaoaaaaaabaaaaaah
bcaabaaaaaaaaaaaegacbaaaadaaaaaaegacbaaaadaaaaaaelaaaaafbcaabaaa
aaaaaaaaakaabaaaaaaaaaaabnaaaaaibcaabaaaaaaaaaaabkiacaaaaaaaaaaa
agaaaaaaakaabaaaaaaaaaaaabaaaaahbcaabaaaaaaaaaaaakaabaaaaaaaaaaa
abeaaaaaaaaaiadpaaaaaaahbcaabaaaaaaaaaaabkaabaaaaaaaaaaaakaabaaa
aaaaaaaabaaaaaahccaabaaaaaaaaaaaegacbaaaacaaaaaaegacbaaaacaaaaaa
elaaaaafccaabaaaaaaaaaaabkaabaaaaaaaaaaaaaaaaaahecaabaaaabaaaaaa
bkaabaaaaaaaaaaaabeaaaaamnmmemlodiaaaaahecaabaaaabaaaaaackaabaaa
abaaaaaaabeaaaaaaaaakamadeaaaaahecaabaaaabaaaaaackaabaaaabaaaaaa
abeaaaaaaaaaaaaadcaaaaajicaabaaaabaaaaaackaabaaaabaaaaaaabeaaaaa
aaaaaamaabeaaaaaaaaaeaeadiaaaaahecaabaaaabaaaaaackaabaaaabaaaaaa
ckaabaaaabaaaaaadiaaaaahecaabaaaabaaaaaackaabaaaabaaaaaadkaabaaa
abaaaaaadiaaaaahecaabaaaabaaaaaackaabaaaabaaaaaabkaabaaaabaaaaaa
diaaaaaiecaabaaaabaaaaaackaabaaaabaaaaaackiacaaaaaaaaaaaanaaaaaa
dcaaaaakpcaabaaaacaaaaaakgakbaaaabaaaaaafgafbaaaaaaaaaaaegiocaaa
aaaaaaaaamaaaaaaaaaaaaammcaabaaaabaaaaaaagiicaiaebaaaaaaaaaaaaaa
aeaaaaaaaceaaaaaaaaaaaaaaaaaaaaaaaaaiadpaaaaiadpdcaaaabamcaabaaa
abaaaaaakgaobaiaebaaaaaaabaaaaaaaceaaaaaaaaaaaaaaaaaaaaaaaaaaadp
aaaaaadpaceaaaaaaaaaaaaaaaaaaaaaaaaaaadpaaaaaadpaoaaaaakmcaabaaa
abaaaaaaaceaaaaaaaaaiadpaaaaiadpaaaaiadpaaaaiadpkgaobaaaabaaaaaa
aaaaaaajdcaabaaaadaaaaaafgbfbaaaaaaaaaaangifcaiaebaaaaaaaaaaaaaa
aeaaaaaaaaaaaaahccaabaaaadaaaaaabkaabaaaadaaaaaaabeaaaaaaaaaaalp
dicaaaahmcaabaaaabaaaaaakgaobaaaabaaaaaaagaebaaaadaaaaaadcaaaaaj
bcaabaaaadaaaaaadkaabaaaabaaaaaaabeaaaaaaaaaaamaabeaaaaaaaaaeaea
diaaaaahicaabaaaabaaaaaadkaabaaaabaaaaaadkaabaaaabaaaaaadiaaaaah
icaabaaaabaaaaaadkaabaaaabaaaaaaakaabaaaadaaaaaadcaaaaajbcaabaaa
adaaaaaackaabaaaabaaaaaaabeaaaaaaaaaaamaabeaaaaaaaaaeaeadiaaaaah
ecaabaaaabaaaaaackaabaaaabaaaaaackaabaaaabaaaaaadiaaaaahecaabaaa
abaaaaaackaabaaaabaaaaaaakaabaaaadaaaaaaaaaaaaakpcaabaaaadaaaaaa
egiocaiaebaaaaaaaaaaaaaaabaaaaaaegiocaaaaaaaaaaaacaaaaaadcaaaaak
pcaabaaaadaaaaaakgakbaaaabaaaaaaegaobaaaadaaaaaaegiocaaaaaaaaaaa
abaaaaaaaaaaaaajpcaabaaaaeaaaaaaegaobaiaebaaaaaaadaaaaaaegiocaaa
aaaaaaaaadaaaaaadcaaaaajpcaabaaaadaaaaaapgapbaaaabaaaaaaegaobaaa
aeaaaaaaegaobaaaadaaaaaaaaaaaaajpcaabaaaadaaaaaaegaobaaaadaaaaaa
egiocaiaebaaaaaaaaaaaaaaafaaaaaaaoaaaaalicaabaaaabaaaaaaaceaaaaa
aaaaiadpaaaaiadpaaaaiadpaaaaiadpakiacaaaaaaaaaaaagaaaaaadicaaaah
icaabaaaabaaaaaabkaabaaaaaaaaaaadkaabaaaabaaaaaadcaaaaalccaabaaa
aaaaaaaaakiacaiaebaaaaaaaaaaaaaaagaaaaaaabeaaaaaaaaaeaeabkaabaaa
aaaaaaaadcaaaaajbcaabaaaaeaaaaaadkaabaaaabaaaaaaabeaaaaaaaaaaama
abeaaaaaaaaaeaeadiaaaaahicaabaaaabaaaaaadkaabaaaabaaaaaadkaabaaa
abaaaaaadiaaaaahicaabaaaabaaaaaadkaabaaaabaaaaaaakaabaaaaeaaaaaa
aoaaaaalbcaabaaaaeaaaaaaaceaaaaaaaaaiadpaaaaiadpaaaaiadpaaaaiadp
akiacaaaaaaaaaaaanaaaaaadicaaaahicaabaaaaaaaaaaadkaabaaaaaaaaaaa
akaabaaaaeaaaaaadcaaaaajbcaabaaaaeaaaaaadkaabaaaaaaaaaaaabeaaaaa
aaaaaamaabeaaaaaaaaaeaeadiaaaaahicaabaaaaaaaaaaadkaabaaaaaaaaaaa
dkaabaaaaaaaaaaadiaaaaahccaabaaaaeaaaaaadkaabaaaaaaaaaaaakaabaaa
aeaaaaaadiaaaaaiecaabaaaaeaaaaaabkaabaaaaeaaaaaadkiacaaaaaaaaaaa
amaaaaaadeaaaaahicaabaaaabaaaaaadkaabaaaabaaaaaackaabaaaaeaaaaaa
dcaaaaakpcaabaaaadaaaaaapgapbaaaabaaaaaaegaobaaaadaaaaaaegiocaaa
aaaaaaaaafaaaaaaaaaaaaaipcaabaaaacaaaaaaegaobaaaacaaaaaaegaobaia
ebaaaaaaadaaaaaadcaaaaakccaabaaaabaaaaaaakaabaiaebaaaaaaaeaaaaaa
dkaabaaaaaaaaaaabkaabaaaabaaaaaadcaaaaakicaabaaaaaaaaaaaakaabaia
ebaaaaaaaeaaaaaadkaabaaaaaaaaaaaabeaaaaaaaaaiadpdcaaaaajccaabaaa
abaaaaaabkaabaaaabaaaaaaabeaaaaaaaaaaadpbkaabaaaaeaaaaaadiaaaaah
ccaabaaaabaaaaaackaabaaaabaaaaaabkaabaaaabaaaaaadiaaaaaiccaabaaa
abaaaaaabkaabaaaabaaaaaadkiacaaaaaaaaaaaamaaaaaadcaaaaajpcaabaaa
acaaaaaafgafbaaaabaaaaaaegaobaaaacaaaaaaegaobaaaadaaaaaadcaaaaaj
ccaabaaaabaaaaaackaabaaaaaaaaaaaabeaaaaaaaaaaamaabeaaaaaaaaaeaea
diaaaaahecaabaaaaaaaaaaackaabaaaaaaaaaaackaabaaaaaaaaaaadcaaaaak
ecaabaaaaaaaaaaabkaabaiaebaaaaaaabaaaaaackaabaaaaaaaaaaabkaabaaa
aeaaaaaadccaaaalbcaabaaaaaaaaaaaakaabaaaaaaaaaaackiacaaaaaaaaaaa
aoaaaaaabkaabaiaebaaaaaaaeaaaaaadiaaaaaiccaabaaaabaaaaaaakiacaaa
aaaaaaaaagaaaaaaabeaaaaaaaaaeaeaaoaaaaalccaabaaaabaaaaaaaceaaaaa
aaaaiadpaaaaiadpaaaaiadpaaaaiadpbkaabaiaebaaaaaaabaaaaaadicaaaah
ccaabaaaaaaaaaaabkaabaaaaaaaaaaabkaabaaaabaaaaaadcaaaaajccaabaaa
abaaaaaabkaabaaaaaaaaaaaabeaaaaaaaaaaamaabeaaaaaaaaaeaeadiaaaaah
ccaabaaaaaaaaaaabkaabaaaaaaaaaaabkaabaaaaaaaaaaadiaaaaahccaabaaa
aaaaaaaabkaabaaaaaaaaaaabkaabaaaabaaaaaadiaaaaaiccaabaaaaaaaaaaa
bkaabaaaaaaaaaaadkiacaaaaaaaaaaaamaaaaaadiaaaaahccaabaaaaaaaaaaa
bkaabaaaaaaaaaaackaabaaaaaaaaaaadcaaaaakpcaabaaaacaaaaaafgafbaaa
aaaaaaaaegiocaaaaaaaaaaaafaaaaaaegaobaaaacaaaaaaaaaaaaajpcaabaaa
adaaaaaaegaobaiaebaaaaaaacaaaaaaegiocaaaaaaaaaaaahaaaaaadcaaaaaj
pcaabaaaacaaaaaaagaabaaaaaaaaaaaegaobaaaadaaaaaaegaobaaaacaaaaaa
dcaaaaajbcaabaaaaaaaaaaabkbabaaaaaaaaaaaabeaaaaaaaaacaebabeaaaaa
aaaaaalpaacaaaahbcaabaaaaaaaaaaaakaabaaaaaaaaaaaakaabaaaaaaaaaaa
dcaaaaajccaabaaaaaaaaaaaakaabaaaaaaaaaaaabeaaaaaaaaaaamaabeaaaaa
aaaaeaeadiaaaaahbcaabaaaaaaaaaaaakaabaaaaaaaaaaaakaabaaaaaaaaaaa
diaaaaahbcaabaaaaaaaaaaaakaabaaaaaaaaaaabkaabaaaaaaaaaaadicaaaai
ccaabaaaaaaaaaaackiacaaaaaaaaaaaaoaaaaaaabeaaaaaaaaaaadpdiaaaaah
ccaabaaaaaaaaaaabkaabaaaaaaaaaaaakaabaaaabaaaaaadiaaaaahccaabaaa
aaaaaaaadkaabaaaaaaaaaaabkaabaaaaaaaaaaadcaaaaajpccabaaaaaaaaaaa
fgafbaaaaaaaaaaaagaabaaaaaaaaaaaegaobaaaacaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { }
"!!GLES"
}

SubProgram "gles3 " {
Keywords { }
"!!GLES3"
}

}

#LINE 158

        }
    }
}