Shader "TAS/TerrianShader" {
	Properties {
		_MainTex ("Packed Texture", 2D) = "white" {}
		_ShdeTex ("Shadow Texture", 2D) = "white" {}
	}

	SubShader {
		Tags { "RenderType"="Opaque" }

		pass {
			Tags { "LightMode"="ForwardBase"}

			Program "vp" {
// Vertex combos: 12
//   opengl - ALU: 82 to 87
//   d3d9 - ALU: 90 to 96
SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" ATTR14
Bind "texcoord" TexCoord0
Bind "color" Color
Vector 13 [_WorldSpaceLightPos0]
Vector 14 [unity_4LightPosX0]
Vector 15 [unity_4LightPosY0]
Vector 16 [unity_4LightPosZ0]
Vector 17 [unity_4LightAtten0]
Vector 18 [unity_LightColor0]
Vector 19 [unity_LightColor1]
Vector 20 [unity_LightColor2]
Vector 21 [unity_LightColor3]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
"!!ARBvp1.0
# 82 ALU
PARAM c[23] = { { 0.0078125, 128, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..21],
		{ 0, 0.80000001 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
DP3 R0.w, vertex.normal, vertex.normal;
DP4 R1.z, vertex.position, c[7];
DP4 R1.y, vertex.position, c[6];
DP4 R1.x, vertex.position, c[5];
RSQ R0.w, R0.w;
MOV R2.z, c[16].y;
MOV R2.y, c[15];
MOV R0.x, c[14];
MOV R0.z, c[16].x;
MOV R0.y, c[15].x;
ADD R0.xyz, -R1, R0;
DP3 R1.w, R0, R0;
RSQ R2.x, R1.w;
MUL R3.xyz, R2.x, R0;
MUL R0.xyz, R0.w, vertex.normal;
MOV R2.x, c[14].y;
ADD R4.xyz, -R1, R2;
DP3 R2.z, R0, c[7];
DP3 R2.y, R0, c[6];
DP3 R2.x, R0, c[5];
DP3 R0.w, R2, R3;
DP3 R2.w, R4, R4;
RSQ R3.x, R2.w;
MUL R3.xyz, R3.x, R4;
DP3 R3.x, R2, R3;
MUL R2.w, R2, c[17].y;
ADD R2.w, R2, c[0];
MAX R3.w, R3.x, c[22].x;
RCP R2.w, R2.w;
MUL R3.xyz, R2.w, c[19];
MUL R4.xyz, R3, R3.w;
MUL R2.w, R1, c[17].x;
ADD R2.w, R2, c[0];
MAX R0.w, R0, c[22].x;
RCP R2.w, R2.w;
MOV R3.x, c[14].z;
MOV R3.z, c[16];
MOV R3.y, c[15].z;
ADD R3.xyz, -R1, R3;
DP3 R1.w, R3, R3;
RSQ R3.w, R1.w;
MUL R5.xyz, R3.w, R3;
MUL R3.xyz, R2.w, c[18];
MAD R3.xyz, R3, R0.w, R4;
DP3 R0.w, R2, R5;
MUL R1.w, R1, c[17].z;
ADD R1.w, R1, c[0];
RCP R2.w, R1.w;
MAX R0.w, R0, c[22].x;
MOV R4.x, c[14].w;
MOV R4.z, c[16].w;
MOV R4.y, c[15].w;
ADD R1.xyz, R4, -R1;
MUL R4.xyz, R2.w, c[20];
DP3 R1.w, R1, R1;
MAD R3.xyz, R4, R0.w, R3;
RSQ R0.w, R1.w;
MUL R1.xyz, R0.w, R1;
MUL R1.w, R1, c[17];
ADD R0.w, R1, c[0];
DP3 R1.x, R2, R1;
MAX R1.w, R1.x, c[22].x;
RCP R0.w, R0.w;
MUL R1.xyz, R0.w, c[21];
MAD R2.xyz, R1, R1.w, R3;
MOV R1, c[13];
MUL result.texcoord[5].xyz, R2, c[22].y;
MAD R2, vertex.attrib[14], c[0].y, c[0].z;
FLR R2, R2;
DP4 result.texcoord[1].z, R1, c[11];
DP4 result.texcoord[1].y, R1, c[10];
DP4 result.texcoord[1].x, R1, c[9];
MAD R1, vertex.color, c[0].y, c[0].z;
FLR R1, R1;
MUL result.texcoord[2], R1, c[0].x;
MUL result.texcoord[3], R2, c[0].x;
MOV result.texcoord[0].xyz, R0;
MOV result.texcoord[4].xy, vertex.texcoord[0];
DP4 result.position.w, vertex.position, c[4];
DP4 result.position.z, vertex.position, c[3];
DP4 result.position.y, vertex.position, c[2];
DP4 result.position.x, vertex.position, c[1];
END
# 82 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" TexCoord2
Bind "texcoord" TexCoord0
Bind "color" Color
Matrix 0 [glstate_matrix_mvp]
Vector 12 [_WorldSpaceLightPos0]
Vector 13 [unity_4LightPosX0]
Vector 14 [unity_4LightPosY0]
Vector 15 [unity_4LightPosZ0]
Vector 16 [unity_4LightAtten0]
Vector 17 [unity_LightColor0]
Vector 18 [unity_LightColor1]
Vector 19 [unity_LightColor2]
Vector 20 [unity_LightColor3]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
"vs_2_0
; 90 ALU
def c21, 128.00000000, 0.50000000, 0.00781250, 1.00000000
def c22, 0.00000000, 0.80000001, 0, 0
dcl_position0 v0
dcl_normal0 v1
dcl_tangent0 v2
dcl_texcoord0 v3
dcl_color0 v4
dp3 r0.w, v1, v1
dp4 r1.z, v0, c6
dp4 r1.y, v0, c5
dp4 r1.x, v0, c4
rsq r0.w, r0.w
mov r2.z, c15.y
mov r2.y, c14
mov r0.x, c13
mov r0.z, c15.x
mov r0.y, c14.x
add r0.xyz, -r1, r0
dp3 r1.w, r0, r0
rsq r2.x, r1.w
mul r3.xyz, r2.x, r0
mul r0.xyz, r0.w, v1
mov r2.x, c13.y
add r4.xyz, -r1, r2
dp3 r2.z, r0, c6
dp3 r2.y, r0, c5
dp3 r2.x, r0, c4
dp3 r0.w, r2, r3
dp3 r2.w, r4, r4
rsq r3.x, r2.w
mul r3.xyz, r3.x, r4
dp3 r3.x, r2, r3
mul r2.w, r2, c16.y
add r2.w, r2, c21
max r3.w, r3.x, c22.x
rcp r2.w, r2.w
mul r3.xyz, r2.w, c18
mul r5.xyz, r3, r3.w
mul r2.w, r1, c16.x
add r2.w, r2, c21
rcp r2.w, r2.w
max r0.w, r0, c22.x
mul r4.xyz, r2.w, c17
mad r4.xyz, r4, r0.w, r5
mov r3.x, c13.z
mov r3.z, c15
mov r3.y, c14.z
add r3.xyz, -r1, r3
dp3 r1.w, r3, r3
rsq r3.w, r1.w
mul r3.xyz, r3.w, r3
dp3 r0.w, r2, r3
mul r1.w, r1, c16.z
add r1.w, r1, c21
rcp r2.w, r1.w
max r0.w, r0, c22.x
mov r3.x, c13.w
mov r3.z, c15.w
mov r3.y, c14.w
add r1.xyz, r3, -r1
mul r3.xyz, r2.w, c19
dp3 r1.w, r1, r1
mad r3.xyz, r3, r0.w, r4
rsq r2.w, r1.w
mul r1.xyz, r2.w, r1
mul r0.w, r1, c16
dp3 r1.x, r2, r1
add r0.w, r0, c21
max r1.w, r1.x, c22.x
rcp r0.w, r0.w
mul r1.xyz, r0.w, c20
mad r2.xyz, r1, r1.w, r3
mad r1, v4, c21.x, c21.y
mul oT5.xyz, r2, c22.y
frc r2, r1
add r1, r1, -r2
mad r3, v2, c21.x, c21.y
frc r2, r3
add r2, r3, -r2
mul oT2, r1, c21.z
mov r1, c10
mul oT3, r2, c21.z
dp4 oT1.z, c12, r1
mov r1, c9
mov r2, c8
dp4 oT1.y, c12, r1
dp4 oT1.x, c12, r2
mov oT0.xyz, r0
mov oT4.xy, v3
dp4 oPos.w, v0, c3
dp4 oPos.z, v0, c2
dp4 oPos.y, v0, c1
dp4 oPos.x, v0, c0
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8.w = 1.0;
  tmpvar_8.xyz = tmpvar_5;
  highp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + (clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0)) * tmpvar_6.x) * tmpvar_8) * 2.0) + tmpvar_9);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8.w = 1.0;
  tmpvar_8.xyz = tmpvar_5;
  highp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + (clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0)) * tmpvar_6.x) * tmpvar_8) * 2.0) + tmpvar_9);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 329
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
};
#line 320
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 319
uniform highp vec4 _LightColor0;
#line 340
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 340
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 344
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 349
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 353
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 357
    return (lightColor * 0.8);
}
#line 359
vertex2fragment vertShadow( in vertexInput v ) {
    #line 361
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 365
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    #line 369
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 329
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
};
#line 320
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 319
uniform highp vec4 _LightColor0;
#line 340
#line 372
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 374
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 378
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 382
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 386
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * 1.0)) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" ATTR14
Bind "texcoord" TexCoord0
Bind "color" Color
Vector 13 [_WorldSpaceLightPos0]
Vector 14 [unity_4LightPosX0]
Vector 15 [unity_4LightPosY0]
Vector 16 [unity_4LightPosZ0]
Vector 17 [unity_4LightAtten0]
Vector 18 [unity_LightColor0]
Vector 19 [unity_LightColor1]
Vector 20 [unity_LightColor2]
Vector 21 [unity_LightColor3]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
"!!ARBvp1.0
# 82 ALU
PARAM c[23] = { { 0.0078125, 128, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..21],
		{ 0, 0.80000001 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
DP3 R0.w, vertex.normal, vertex.normal;
DP4 R1.z, vertex.position, c[7];
DP4 R1.y, vertex.position, c[6];
DP4 R1.x, vertex.position, c[5];
RSQ R0.w, R0.w;
MOV R2.z, c[16].y;
MOV R2.y, c[15];
MOV R0.x, c[14];
MOV R0.z, c[16].x;
MOV R0.y, c[15].x;
ADD R0.xyz, -R1, R0;
DP3 R1.w, R0, R0;
RSQ R2.x, R1.w;
MUL R3.xyz, R2.x, R0;
MUL R0.xyz, R0.w, vertex.normal;
MOV R2.x, c[14].y;
ADD R4.xyz, -R1, R2;
DP3 R2.z, R0, c[7];
DP3 R2.y, R0, c[6];
DP3 R2.x, R0, c[5];
DP3 R0.w, R2, R3;
DP3 R2.w, R4, R4;
RSQ R3.x, R2.w;
MUL R3.xyz, R3.x, R4;
DP3 R3.x, R2, R3;
MUL R2.w, R2, c[17].y;
ADD R2.w, R2, c[0];
MAX R3.w, R3.x, c[22].x;
RCP R2.w, R2.w;
MUL R3.xyz, R2.w, c[19];
MUL R4.xyz, R3, R3.w;
MUL R2.w, R1, c[17].x;
ADD R2.w, R2, c[0];
MAX R0.w, R0, c[22].x;
RCP R2.w, R2.w;
MOV R3.x, c[14].z;
MOV R3.z, c[16];
MOV R3.y, c[15].z;
ADD R3.xyz, -R1, R3;
DP3 R1.w, R3, R3;
RSQ R3.w, R1.w;
MUL R5.xyz, R3.w, R3;
MUL R3.xyz, R2.w, c[18];
MAD R3.xyz, R3, R0.w, R4;
DP3 R0.w, R2, R5;
MUL R1.w, R1, c[17].z;
ADD R1.w, R1, c[0];
RCP R2.w, R1.w;
MAX R0.w, R0, c[22].x;
MOV R4.x, c[14].w;
MOV R4.z, c[16].w;
MOV R4.y, c[15].w;
ADD R1.xyz, R4, -R1;
MUL R4.xyz, R2.w, c[20];
DP3 R1.w, R1, R1;
MAD R3.xyz, R4, R0.w, R3;
RSQ R0.w, R1.w;
MUL R1.xyz, R0.w, R1;
MUL R1.w, R1, c[17];
ADD R0.w, R1, c[0];
DP3 R1.x, R2, R1;
MAX R1.w, R1.x, c[22].x;
RCP R0.w, R0.w;
MUL R1.xyz, R0.w, c[21];
MAD R2.xyz, R1, R1.w, R3;
MOV R1, c[13];
MUL result.texcoord[5].xyz, R2, c[22].y;
MAD R2, vertex.attrib[14], c[0].y, c[0].z;
FLR R2, R2;
DP4 result.texcoord[1].z, R1, c[11];
DP4 result.texcoord[1].y, R1, c[10];
DP4 result.texcoord[1].x, R1, c[9];
MAD R1, vertex.color, c[0].y, c[0].z;
FLR R1, R1;
MUL result.texcoord[2], R1, c[0].x;
MUL result.texcoord[3], R2, c[0].x;
MOV result.texcoord[0].xyz, R0;
MOV result.texcoord[4].xy, vertex.texcoord[0];
DP4 result.position.w, vertex.position, c[4];
DP4 result.position.z, vertex.position, c[3];
DP4 result.position.y, vertex.position, c[2];
DP4 result.position.x, vertex.position, c[1];
END
# 82 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" TexCoord2
Bind "texcoord" TexCoord0
Bind "color" Color
Matrix 0 [glstate_matrix_mvp]
Vector 12 [_WorldSpaceLightPos0]
Vector 13 [unity_4LightPosX0]
Vector 14 [unity_4LightPosY0]
Vector 15 [unity_4LightPosZ0]
Vector 16 [unity_4LightAtten0]
Vector 17 [unity_LightColor0]
Vector 18 [unity_LightColor1]
Vector 19 [unity_LightColor2]
Vector 20 [unity_LightColor3]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
"vs_2_0
; 90 ALU
def c21, 128.00000000, 0.50000000, 0.00781250, 1.00000000
def c22, 0.00000000, 0.80000001, 0, 0
dcl_position0 v0
dcl_normal0 v1
dcl_tangent0 v2
dcl_texcoord0 v3
dcl_color0 v4
dp3 r0.w, v1, v1
dp4 r1.z, v0, c6
dp4 r1.y, v0, c5
dp4 r1.x, v0, c4
rsq r0.w, r0.w
mov r2.z, c15.y
mov r2.y, c14
mov r0.x, c13
mov r0.z, c15.x
mov r0.y, c14.x
add r0.xyz, -r1, r0
dp3 r1.w, r0, r0
rsq r2.x, r1.w
mul r3.xyz, r2.x, r0
mul r0.xyz, r0.w, v1
mov r2.x, c13.y
add r4.xyz, -r1, r2
dp3 r2.z, r0, c6
dp3 r2.y, r0, c5
dp3 r2.x, r0, c4
dp3 r0.w, r2, r3
dp3 r2.w, r4, r4
rsq r3.x, r2.w
mul r3.xyz, r3.x, r4
dp3 r3.x, r2, r3
mul r2.w, r2, c16.y
add r2.w, r2, c21
max r3.w, r3.x, c22.x
rcp r2.w, r2.w
mul r3.xyz, r2.w, c18
mul r5.xyz, r3, r3.w
mul r2.w, r1, c16.x
add r2.w, r2, c21
rcp r2.w, r2.w
max r0.w, r0, c22.x
mul r4.xyz, r2.w, c17
mad r4.xyz, r4, r0.w, r5
mov r3.x, c13.z
mov r3.z, c15
mov r3.y, c14.z
add r3.xyz, -r1, r3
dp3 r1.w, r3, r3
rsq r3.w, r1.w
mul r3.xyz, r3.w, r3
dp3 r0.w, r2, r3
mul r1.w, r1, c16.z
add r1.w, r1, c21
rcp r2.w, r1.w
max r0.w, r0, c22.x
mov r3.x, c13.w
mov r3.z, c15.w
mov r3.y, c14.w
add r1.xyz, r3, -r1
mul r3.xyz, r2.w, c19
dp3 r1.w, r1, r1
mad r3.xyz, r3, r0.w, r4
rsq r2.w, r1.w
mul r1.xyz, r2.w, r1
mul r0.w, r1, c16
dp3 r1.x, r2, r1
add r0.w, r0, c21
max r1.w, r1.x, c22.x
rcp r0.w, r0.w
mul r1.xyz, r0.w, c20
mad r2.xyz, r1, r1.w, r3
mad r1, v4, c21.x, c21.y
mul oT5.xyz, r2, c22.y
frc r2, r1
add r1, r1, -r2
mad r3, v2, c21.x, c21.y
frc r2, r3
add r2, r3, -r2
mul oT2, r1, c21.z
mov r1, c10
mul oT3, r2, c21.z
dp4 oT1.z, c12, r1
mov r1, c9
mov r2, c8
dp4 oT1.y, c12, r1
dp4 oT1.x, c12, r2
mov oT0.xyz, r0
mov oT4.xy, v3
dp4 oPos.w, v0, c3
dp4 oPos.z, v0, c2
dp4 oPos.y, v0, c1
dp4 oPos.x, v0, c0
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8.w = 1.0;
  tmpvar_8.xyz = tmpvar_5;
  highp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + (clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0)) * tmpvar_6.x) * tmpvar_8) * 2.0) + tmpvar_9);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8.w = 1.0;
  tmpvar_8.xyz = tmpvar_5;
  highp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + (clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0)) * tmpvar_6.x) * tmpvar_8) * 2.0) + tmpvar_9);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 329
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
};
#line 320
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 319
uniform highp vec4 _LightColor0;
#line 340
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 340
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 344
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 349
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 353
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 357
    return (lightColor * 0.8);
}
#line 359
vertex2fragment vertShadow( in vertexInput v ) {
    #line 361
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 365
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    #line 369
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 329
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
};
#line 320
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 319
uniform highp vec4 _LightColor0;
#line 340
#line 372
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 374
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 378
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 382
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 386
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * 1.0)) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_OFF" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" ATTR14
Bind "texcoord" TexCoord0
Bind "color" Color
Vector 13 [_WorldSpaceLightPos0]
Vector 14 [unity_4LightPosX0]
Vector 15 [unity_4LightPosY0]
Vector 16 [unity_4LightPosZ0]
Vector 17 [unity_4LightAtten0]
Vector 18 [unity_LightColor0]
Vector 19 [unity_LightColor1]
Vector 20 [unity_LightColor2]
Vector 21 [unity_LightColor3]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
"!!ARBvp1.0
# 82 ALU
PARAM c[23] = { { 0.0078125, 128, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..21],
		{ 0, 0.80000001 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
DP3 R0.w, vertex.normal, vertex.normal;
DP4 R1.z, vertex.position, c[7];
DP4 R1.y, vertex.position, c[6];
DP4 R1.x, vertex.position, c[5];
RSQ R0.w, R0.w;
MOV R2.z, c[16].y;
MOV R2.y, c[15];
MOV R0.x, c[14];
MOV R0.z, c[16].x;
MOV R0.y, c[15].x;
ADD R0.xyz, -R1, R0;
DP3 R1.w, R0, R0;
RSQ R2.x, R1.w;
MUL R3.xyz, R2.x, R0;
MUL R0.xyz, R0.w, vertex.normal;
MOV R2.x, c[14].y;
ADD R4.xyz, -R1, R2;
DP3 R2.z, R0, c[7];
DP3 R2.y, R0, c[6];
DP3 R2.x, R0, c[5];
DP3 R0.w, R2, R3;
DP3 R2.w, R4, R4;
RSQ R3.x, R2.w;
MUL R3.xyz, R3.x, R4;
DP3 R3.x, R2, R3;
MUL R2.w, R2, c[17].y;
ADD R2.w, R2, c[0];
MAX R3.w, R3.x, c[22].x;
RCP R2.w, R2.w;
MUL R3.xyz, R2.w, c[19];
MUL R4.xyz, R3, R3.w;
MUL R2.w, R1, c[17].x;
ADD R2.w, R2, c[0];
MAX R0.w, R0, c[22].x;
RCP R2.w, R2.w;
MOV R3.x, c[14].z;
MOV R3.z, c[16];
MOV R3.y, c[15].z;
ADD R3.xyz, -R1, R3;
DP3 R1.w, R3, R3;
RSQ R3.w, R1.w;
MUL R5.xyz, R3.w, R3;
MUL R3.xyz, R2.w, c[18];
MAD R3.xyz, R3, R0.w, R4;
DP3 R0.w, R2, R5;
MUL R1.w, R1, c[17].z;
ADD R1.w, R1, c[0];
RCP R2.w, R1.w;
MAX R0.w, R0, c[22].x;
MOV R4.x, c[14].w;
MOV R4.z, c[16].w;
MOV R4.y, c[15].w;
ADD R1.xyz, R4, -R1;
MUL R4.xyz, R2.w, c[20];
DP3 R1.w, R1, R1;
MAD R3.xyz, R4, R0.w, R3;
RSQ R0.w, R1.w;
MUL R1.xyz, R0.w, R1;
MUL R1.w, R1, c[17];
ADD R0.w, R1, c[0];
DP3 R1.x, R2, R1;
MAX R1.w, R1.x, c[22].x;
RCP R0.w, R0.w;
MUL R1.xyz, R0.w, c[21];
MAD R2.xyz, R1, R1.w, R3;
MOV R1, c[13];
MUL result.texcoord[5].xyz, R2, c[22].y;
MAD R2, vertex.attrib[14], c[0].y, c[0].z;
FLR R2, R2;
DP4 result.texcoord[1].z, R1, c[11];
DP4 result.texcoord[1].y, R1, c[10];
DP4 result.texcoord[1].x, R1, c[9];
MAD R1, vertex.color, c[0].y, c[0].z;
FLR R1, R1;
MUL result.texcoord[2], R1, c[0].x;
MUL result.texcoord[3], R2, c[0].x;
MOV result.texcoord[0].xyz, R0;
MOV result.texcoord[4].xy, vertex.texcoord[0];
DP4 result.position.w, vertex.position, c[4];
DP4 result.position.z, vertex.position, c[3];
DP4 result.position.y, vertex.position, c[2];
DP4 result.position.x, vertex.position, c[1];
END
# 82 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_OFF" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" TexCoord2
Bind "texcoord" TexCoord0
Bind "color" Color
Matrix 0 [glstate_matrix_mvp]
Vector 12 [_WorldSpaceLightPos0]
Vector 13 [unity_4LightPosX0]
Vector 14 [unity_4LightPosY0]
Vector 15 [unity_4LightPosZ0]
Vector 16 [unity_4LightAtten0]
Vector 17 [unity_LightColor0]
Vector 18 [unity_LightColor1]
Vector 19 [unity_LightColor2]
Vector 20 [unity_LightColor3]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
"vs_2_0
; 90 ALU
def c21, 128.00000000, 0.50000000, 0.00781250, 1.00000000
def c22, 0.00000000, 0.80000001, 0, 0
dcl_position0 v0
dcl_normal0 v1
dcl_tangent0 v2
dcl_texcoord0 v3
dcl_color0 v4
dp3 r0.w, v1, v1
dp4 r1.z, v0, c6
dp4 r1.y, v0, c5
dp4 r1.x, v0, c4
rsq r0.w, r0.w
mov r2.z, c15.y
mov r2.y, c14
mov r0.x, c13
mov r0.z, c15.x
mov r0.y, c14.x
add r0.xyz, -r1, r0
dp3 r1.w, r0, r0
rsq r2.x, r1.w
mul r3.xyz, r2.x, r0
mul r0.xyz, r0.w, v1
mov r2.x, c13.y
add r4.xyz, -r1, r2
dp3 r2.z, r0, c6
dp3 r2.y, r0, c5
dp3 r2.x, r0, c4
dp3 r0.w, r2, r3
dp3 r2.w, r4, r4
rsq r3.x, r2.w
mul r3.xyz, r3.x, r4
dp3 r3.x, r2, r3
mul r2.w, r2, c16.y
add r2.w, r2, c21
max r3.w, r3.x, c22.x
rcp r2.w, r2.w
mul r3.xyz, r2.w, c18
mul r5.xyz, r3, r3.w
mul r2.w, r1, c16.x
add r2.w, r2, c21
rcp r2.w, r2.w
max r0.w, r0, c22.x
mul r4.xyz, r2.w, c17
mad r4.xyz, r4, r0.w, r5
mov r3.x, c13.z
mov r3.z, c15
mov r3.y, c14.z
add r3.xyz, -r1, r3
dp3 r1.w, r3, r3
rsq r3.w, r1.w
mul r3.xyz, r3.w, r3
dp3 r0.w, r2, r3
mul r1.w, r1, c16.z
add r1.w, r1, c21
rcp r2.w, r1.w
max r0.w, r0, c22.x
mov r3.x, c13.w
mov r3.z, c15.w
mov r3.y, c14.w
add r1.xyz, r3, -r1
mul r3.xyz, r2.w, c19
dp3 r1.w, r1, r1
mad r3.xyz, r3, r0.w, r4
rsq r2.w, r1.w
mul r1.xyz, r2.w, r1
mul r0.w, r1, c16
dp3 r1.x, r2, r1
add r0.w, r0, c21
max r1.w, r1.x, c22.x
rcp r0.w, r0.w
mul r1.xyz, r0.w, c20
mad r2.xyz, r1, r1.w, r3
mad r1, v4, c21.x, c21.y
mul oT5.xyz, r2, c22.y
frc r2, r1
add r1, r1, -r2
mad r3, v2, c21.x, c21.y
frc r2, r3
add r2, r3, -r2
mul oT2, r1, c21.z
mov r1, c10
mul oT3, r2, c21.z
dp4 oT1.z, c12, r1
mov r1, c9
mov r2, c8
dp4 oT1.y, c12, r1
dp4 oT1.x, c12, r2
mov oT0.xyz, r0
mov oT4.xy, v3
dp4 oPos.w, v0, c3
dp4 oPos.z, v0, c2
dp4 oPos.y, v0, c1
dp4 oPos.x, v0, c0
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_OFF" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8.w = 1.0;
  tmpvar_8.xyz = tmpvar_5;
  highp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + (clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0)) * tmpvar_6.x) * tmpvar_8) * 2.0) + tmpvar_9);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_OFF" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8.w = 1.0;
  tmpvar_8.xyz = tmpvar_5;
  highp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + (clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0)) * tmpvar_6.x) * tmpvar_8) * 2.0) + tmpvar_9);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_OFF" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 329
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
};
#line 320
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 319
uniform highp vec4 _LightColor0;
#line 340
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 340
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 344
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 349
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 353
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 357
    return (lightColor * 0.8);
}
#line 359
vertex2fragment vertShadow( in vertexInput v ) {
    #line 361
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 365
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    #line 369
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 329
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
};
#line 320
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 319
uniform highp vec4 _LightColor0;
#line 340
#line 372
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 374
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 378
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 382
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 386
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * 1.0)) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" ATTR14
Bind "texcoord" TexCoord0
Bind "color" Color
Vector 13 [_ProjectionParams]
Vector 14 [_WorldSpaceLightPos0]
Vector 15 [unity_4LightPosX0]
Vector 16 [unity_4LightPosY0]
Vector 17 [unity_4LightPosZ0]
Vector 18 [unity_4LightAtten0]
Vector 19 [unity_LightColor0]
Vector 20 [unity_LightColor1]
Vector 21 [unity_LightColor2]
Vector 22 [unity_LightColor3]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
"!!ARBvp1.0
# 87 ALU
PARAM c[24] = { { 0.0078125, 128, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..22],
		{ 0, 0.80000001 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
DP3 R0.w, vertex.normal, vertex.normal;
DP4 R1.z, vertex.position, c[7];
DP4 R1.y, vertex.position, c[6];
DP4 R1.x, vertex.position, c[5];
RSQ R0.w, R0.w;
MOV R2.z, c[17].y;
MOV R2.y, c[16];
MOV R0.x, c[15];
MOV R0.z, c[17].x;
MOV R0.y, c[16].x;
ADD R0.xyz, -R1, R0;
DP3 R1.w, R0, R0;
RSQ R2.x, R1.w;
MUL R3.xyz, R2.x, R0;
MUL R0.xyz, R0.w, vertex.normal;
MOV R2.x, c[15].y;
ADD R4.xyz, -R1, R2;
DP3 R2.z, R0, c[7];
DP3 R2.y, R0, c[6];
DP3 R2.x, R0, c[5];
DP3 R0.w, R2, R3;
DP3 R2.w, R4, R4;
RSQ R3.x, R2.w;
MUL R3.xyz, R3.x, R4;
DP3 R3.x, R2, R3;
MUL R2.w, R2, c[18].y;
ADD R2.w, R2, c[0];
MAX R3.w, R3.x, c[23].x;
RCP R2.w, R2.w;
MUL R3.xyz, R2.w, c[20];
MUL R4.xyz, R3, R3.w;
MUL R2.w, R1, c[18].x;
ADD R2.w, R2, c[0];
MAX R0.w, R0, c[23].x;
RCP R2.w, R2.w;
MOV R3.x, c[15].z;
MOV R3.z, c[17];
MOV R3.y, c[16].z;
ADD R3.xyz, -R1, R3;
DP3 R1.w, R3, R3;
RSQ R3.w, R1.w;
MUL R5.xyz, R3.w, R3;
MUL R3.xyz, R2.w, c[19];
MAD R3.xyz, R3, R0.w, R4;
DP3 R0.w, R2, R5;
MUL R1.w, R1, c[18].z;
ADD R1.w, R1, c[0];
RCP R2.w, R1.w;
DP4 R3.w, vertex.position, c[4];
MAX R0.w, R0, c[23].x;
MOV R4.x, c[15].w;
MOV R4.z, c[17].w;
MOV R4.y, c[16].w;
ADD R1.xyz, R4, -R1;
MUL R4.xyz, R2.w, c[21];
MAD R3.xyz, R4, R0.w, R3;
DP3 R1.w, R1, R1;
RSQ R0.w, R1.w;
MUL R1.xyz, R0.w, R1;
MUL R1.w, R1, c[18];
ADD R0.w, R1, c[0];
DP3 R1.x, R2, R1;
MAX R1.w, R1.x, c[23].x;
RCP R0.w, R0.w;
MUL R1.xyz, R0.w, c[22];
MAD R2.xyz, R1, R1.w, R3;
DP4 R3.z, vertex.position, c[3];
MUL result.texcoord[5].xyz, R2, c[23].y;
MAD R2, vertex.attrib[14], c[0].y, c[0].z;
FLR R2, R2;
DP4 R3.x, vertex.position, c[1];
DP4 R3.y, vertex.position, c[2];
MUL R1.xyz, R3.xyww, c[0].z;
MUL R1.y, R1, c[13].x;
ADD result.texcoord[6].xy, R1, R1.z;
MOV R1, c[14];
DP4 result.texcoord[1].z, R1, c[11];
DP4 result.texcoord[1].y, R1, c[10];
DP4 result.texcoord[1].x, R1, c[9];
MAD R1, vertex.color, c[0].y, c[0].z;
FLR R1, R1;
MOV result.position, R3;
MUL result.texcoord[2], R1, c[0].x;
MUL result.texcoord[3], R2, c[0].x;
MOV result.texcoord[6].zw, R3;
MOV result.texcoord[0].xyz, R0;
MOV result.texcoord[4].xy, vertex.texcoord[0];
END
# 87 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" TexCoord2
Bind "texcoord" TexCoord0
Bind "color" Color
Matrix 0 [glstate_matrix_mvp]
Vector 12 [_ProjectionParams]
Vector 13 [_ScreenParams]
Vector 14 [_WorldSpaceLightPos0]
Vector 15 [unity_4LightPosX0]
Vector 16 [unity_4LightPosY0]
Vector 17 [unity_4LightPosZ0]
Vector 18 [unity_4LightAtten0]
Vector 19 [unity_LightColor0]
Vector 20 [unity_LightColor1]
Vector 21 [unity_LightColor2]
Vector 22 [unity_LightColor3]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
"vs_2_0
; 96 ALU
def c23, 128.00000000, 0.50000000, 0.00781250, 1.00000000
def c24, 0.00000000, 0.80000001, 0, 0
dcl_position0 v0
dcl_normal0 v1
dcl_tangent0 v2
dcl_texcoord0 v3
dcl_color0 v4
dp3 r0.w, v1, v1
dp4 r1.z, v0, c6
dp4 r1.y, v0, c5
dp4 r1.x, v0, c4
rsq r0.w, r0.w
mov r2.z, c17.y
mov r2.y, c16
mov r0.x, c15
mov r0.z, c17.x
mov r0.y, c16.x
add r0.xyz, -r1, r0
dp3 r1.w, r0, r0
rsq r2.x, r1.w
mul r3.xyz, r2.x, r0
mul r0.xyz, r0.w, v1
mov r2.x, c15.y
add r4.xyz, -r1, r2
dp3 r2.z, r0, c6
dp3 r2.y, r0, c5
dp3 r2.x, r0, c4
dp3 r0.w, r2, r3
dp3 r2.w, r4, r4
rsq r3.x, r2.w
mul r3.xyz, r3.x, r4
dp3 r3.x, r2, r3
mul r2.w, r2, c18.y
add r2.w, r2, c23
max r3.w, r3.x, c24.x
rcp r2.w, r2.w
mul r3.xyz, r2.w, c20
mul r5.xyz, r3, r3.w
mul r2.w, r1, c18.x
add r2.w, r2, c23
rcp r2.w, r2.w
max r0.w, r0, c24.x
mul r4.xyz, r2.w, c19
mad r4.xyz, r4, r0.w, r5
mov r3.x, c15.z
mov r3.z, c17
mov r3.y, c16.z
add r3.xyz, -r1, r3
dp3 r1.w, r3, r3
rsq r3.w, r1.w
mul r3.xyz, r3.w, r3
dp3 r0.w, r2, r3
mul r1.w, r1, c18.z
add r1.w, r1, c23
rcp r2.w, r1.w
max r0.w, r0, c24.x
mov r3.x, c15.w
mov r3.z, c17.w
mov r3.y, c16.w
add r1.xyz, r3, -r1
mul r3.xyz, r2.w, c21
dp3 r1.w, r1, r1
mad r3.xyz, r3, r0.w, r4
rsq r2.w, r1.w
mul r1.xyz, r2.w, r1
mul r0.w, r1, c18
dp3 r1.x, r2, r1
add r0.w, r0, c23
max r1.w, r1.x, c24.x
rcp r0.w, r0.w
mul r1.xyz, r0.w, c22
mad r2.xyz, r1, r1.w, r3
mad r3, v2, c23.x, c23.y
mad r1, v4, c23.x, c23.y
mul oT5.xyz, r2, c24.y
frc r2, r1
add r1, r1, -r2
frc r2, r3
mul oT2, r1, c23.z
add r1, r3, -r2
dp4 r3.w, v0, c3
dp4 r3.z, v0, c2
mul oT3, r1, c23.z
dp4 r3.x, v0, c0
dp4 r3.y, v0, c1
mul r2.xyz, r3.xyww, c23.y
mul r1.y, r2, c12.x
mov r1.x, r2
mad oT6.xy, r2.z, c13.zwzw, r1
mov r1, c10
dp4 oT1.z, c14, r1
mov r1, c9
mov r2, c8
mov oPos, r3
dp4 oT1.y, c14, r1
dp4 oT1.x, c14, r2
mov oT6.zw, r3
mov oT0.xyz, r0
mov oT4.xy, v3
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES


#ifdef VERTEX

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
  xlv_TEXCOORD6 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
}



#endif
#ifdef FRAGMENT

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp vec4 _LightShadowData;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp float tmpvar_8;
  mediump float lightShadowDataX_9;
  highp float dist_10;
  lowp float tmpvar_11;
  tmpvar_11 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD6).x;
  dist_10 = tmpvar_11;
  highp float tmpvar_12;
  tmpvar_12 = _LightShadowData.x;
  lightShadowDataX_9 = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = max (float((dist_10 > (xlv_TEXCOORD6.z / xlv_TEXCOORD6.w))), lightShadowDataX_9);
  tmpvar_8 = tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14.w = 1.0;
  tmpvar_14.xyz = tmpvar_5;
  highp vec4 tmpvar_15;
  tmpvar_15.w = 1.0;
  tmpvar_15.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_8)) * tmpvar_6.x) * tmpvar_14) * 2.0) + tmpvar_15);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES


#ifdef VERTEX

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = (glstate_matrix_mvp * _glesVertex);
  highp vec3 tmpvar_2;
  tmpvar_2 = normalize(_glesNormal);
  highp vec4 o_3;
  highp vec4 tmpvar_4;
  tmpvar_4 = (tmpvar_1 * 0.5);
  highp vec2 tmpvar_5;
  tmpvar_5.x = tmpvar_4.x;
  tmpvar_5.y = (tmpvar_4.y * _ProjectionParams.x);
  o_3.xy = (tmpvar_5 + tmpvar_4.w);
  o_3.zw = tmpvar_1.zw;
  highp vec3 lightColor_6;
  highp vec4 worldPos_7;
  highp vec3 worldN_8;
  mat3 tmpvar_9;
  tmpvar_9[0] = _Object2World[0].xyz;
  tmpvar_9[1] = _Object2World[1].xyz;
  tmpvar_9[2] = _Object2World[2].xyz;
  worldN_8 = (tmpvar_9 * tmpvar_2);
  worldPos_7 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.x;
  tmpvar_10.y = unity_4LightPosY0.x;
  tmpvar_10.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_7.xyz);
  lightColor_6 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_11))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.y;
  tmpvar_12.y = unity_4LightPosY0.y;
  tmpvar_12.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_13)))));
  highp vec3 tmpvar_14;
  tmpvar_14.x = unity_4LightPosX0.z;
  tmpvar_14.y = unity_4LightPosY0.z;
  tmpvar_14.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_15;
  tmpvar_15 = (tmpvar_14 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_15, tmpvar_15))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_15)))));
  highp vec3 tmpvar_16;
  tmpvar_16.x = unity_4LightPosX0.w;
  tmpvar_16.y = unity_4LightPosY0.w;
  tmpvar_16.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_17;
  tmpvar_17 = (tmpvar_16 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_17, tmpvar_17))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_17)))));
  gl_Position = tmpvar_1;
  xlv_TEXCOORD0 = tmpvar_2;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_6 * 0.8);
  xlv_TEXCOORD6 = o_3;
}



#endif
#ifdef FRAGMENT

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD6);
  lowp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = tmpvar_5;
  highp vec4 tmpvar_10;
  tmpvar_10.w = 1.0;
  tmpvar_10.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_8.x)) * tmpvar_6.x) * tmpvar_9) * 2.0) + tmpvar_10);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 349
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 353
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 358
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 362
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 366
    return (lightColor * 0.8);
}
#line 368
vertex2fragment vertShadow( in vertexInput v ) {
    #line 370
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 374
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    #line 379
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
out highp vec4 xlv_TEXCOORD6;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
    xlv_TEXCOORD6 = vec4(xl_retval._ShadowCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 317
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    #line 319
    highp float dist = textureProj( _ShadowMapTexture, shadowCoord).x;
    mediump float lightShadowDataX = _LightShadowData.x;
    return max( float((dist > (shadowCoord.z / shadowCoord.w))), lightShadowDataX);
}
#line 382
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 384
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 388
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 392
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 396
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * unitySampleShadow( i._ShadowCoord))) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
in highp vec4 xlv_TEXCOORD6;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD6);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" ATTR14
Bind "texcoord" TexCoord0
Bind "color" Color
Vector 13 [_ProjectionParams]
Vector 14 [_WorldSpaceLightPos0]
Vector 15 [unity_4LightPosX0]
Vector 16 [unity_4LightPosY0]
Vector 17 [unity_4LightPosZ0]
Vector 18 [unity_4LightAtten0]
Vector 19 [unity_LightColor0]
Vector 20 [unity_LightColor1]
Vector 21 [unity_LightColor2]
Vector 22 [unity_LightColor3]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
"!!ARBvp1.0
# 87 ALU
PARAM c[24] = { { 0.0078125, 128, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..22],
		{ 0, 0.80000001 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
DP3 R0.w, vertex.normal, vertex.normal;
DP4 R1.z, vertex.position, c[7];
DP4 R1.y, vertex.position, c[6];
DP4 R1.x, vertex.position, c[5];
RSQ R0.w, R0.w;
MOV R2.z, c[17].y;
MOV R2.y, c[16];
MOV R0.x, c[15];
MOV R0.z, c[17].x;
MOV R0.y, c[16].x;
ADD R0.xyz, -R1, R0;
DP3 R1.w, R0, R0;
RSQ R2.x, R1.w;
MUL R3.xyz, R2.x, R0;
MUL R0.xyz, R0.w, vertex.normal;
MOV R2.x, c[15].y;
ADD R4.xyz, -R1, R2;
DP3 R2.z, R0, c[7];
DP3 R2.y, R0, c[6];
DP3 R2.x, R0, c[5];
DP3 R0.w, R2, R3;
DP3 R2.w, R4, R4;
RSQ R3.x, R2.w;
MUL R3.xyz, R3.x, R4;
DP3 R3.x, R2, R3;
MUL R2.w, R2, c[18].y;
ADD R2.w, R2, c[0];
MAX R3.w, R3.x, c[23].x;
RCP R2.w, R2.w;
MUL R3.xyz, R2.w, c[20];
MUL R4.xyz, R3, R3.w;
MUL R2.w, R1, c[18].x;
ADD R2.w, R2, c[0];
MAX R0.w, R0, c[23].x;
RCP R2.w, R2.w;
MOV R3.x, c[15].z;
MOV R3.z, c[17];
MOV R3.y, c[16].z;
ADD R3.xyz, -R1, R3;
DP3 R1.w, R3, R3;
RSQ R3.w, R1.w;
MUL R5.xyz, R3.w, R3;
MUL R3.xyz, R2.w, c[19];
MAD R3.xyz, R3, R0.w, R4;
DP3 R0.w, R2, R5;
MUL R1.w, R1, c[18].z;
ADD R1.w, R1, c[0];
RCP R2.w, R1.w;
DP4 R3.w, vertex.position, c[4];
MAX R0.w, R0, c[23].x;
MOV R4.x, c[15].w;
MOV R4.z, c[17].w;
MOV R4.y, c[16].w;
ADD R1.xyz, R4, -R1;
MUL R4.xyz, R2.w, c[21];
MAD R3.xyz, R4, R0.w, R3;
DP3 R1.w, R1, R1;
RSQ R0.w, R1.w;
MUL R1.xyz, R0.w, R1;
MUL R1.w, R1, c[18];
ADD R0.w, R1, c[0];
DP3 R1.x, R2, R1;
MAX R1.w, R1.x, c[23].x;
RCP R0.w, R0.w;
MUL R1.xyz, R0.w, c[22];
MAD R2.xyz, R1, R1.w, R3;
DP4 R3.z, vertex.position, c[3];
MUL result.texcoord[5].xyz, R2, c[23].y;
MAD R2, vertex.attrib[14], c[0].y, c[0].z;
FLR R2, R2;
DP4 R3.x, vertex.position, c[1];
DP4 R3.y, vertex.position, c[2];
MUL R1.xyz, R3.xyww, c[0].z;
MUL R1.y, R1, c[13].x;
ADD result.texcoord[6].xy, R1, R1.z;
MOV R1, c[14];
DP4 result.texcoord[1].z, R1, c[11];
DP4 result.texcoord[1].y, R1, c[10];
DP4 result.texcoord[1].x, R1, c[9];
MAD R1, vertex.color, c[0].y, c[0].z;
FLR R1, R1;
MOV result.position, R3;
MUL result.texcoord[2], R1, c[0].x;
MUL result.texcoord[3], R2, c[0].x;
MOV result.texcoord[6].zw, R3;
MOV result.texcoord[0].xyz, R0;
MOV result.texcoord[4].xy, vertex.texcoord[0];
END
# 87 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" TexCoord2
Bind "texcoord" TexCoord0
Bind "color" Color
Matrix 0 [glstate_matrix_mvp]
Vector 12 [_ProjectionParams]
Vector 13 [_ScreenParams]
Vector 14 [_WorldSpaceLightPos0]
Vector 15 [unity_4LightPosX0]
Vector 16 [unity_4LightPosY0]
Vector 17 [unity_4LightPosZ0]
Vector 18 [unity_4LightAtten0]
Vector 19 [unity_LightColor0]
Vector 20 [unity_LightColor1]
Vector 21 [unity_LightColor2]
Vector 22 [unity_LightColor3]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
"vs_2_0
; 96 ALU
def c23, 128.00000000, 0.50000000, 0.00781250, 1.00000000
def c24, 0.00000000, 0.80000001, 0, 0
dcl_position0 v0
dcl_normal0 v1
dcl_tangent0 v2
dcl_texcoord0 v3
dcl_color0 v4
dp3 r0.w, v1, v1
dp4 r1.z, v0, c6
dp4 r1.y, v0, c5
dp4 r1.x, v0, c4
rsq r0.w, r0.w
mov r2.z, c17.y
mov r2.y, c16
mov r0.x, c15
mov r0.z, c17.x
mov r0.y, c16.x
add r0.xyz, -r1, r0
dp3 r1.w, r0, r0
rsq r2.x, r1.w
mul r3.xyz, r2.x, r0
mul r0.xyz, r0.w, v1
mov r2.x, c15.y
add r4.xyz, -r1, r2
dp3 r2.z, r0, c6
dp3 r2.y, r0, c5
dp3 r2.x, r0, c4
dp3 r0.w, r2, r3
dp3 r2.w, r4, r4
rsq r3.x, r2.w
mul r3.xyz, r3.x, r4
dp3 r3.x, r2, r3
mul r2.w, r2, c18.y
add r2.w, r2, c23
max r3.w, r3.x, c24.x
rcp r2.w, r2.w
mul r3.xyz, r2.w, c20
mul r5.xyz, r3, r3.w
mul r2.w, r1, c18.x
add r2.w, r2, c23
rcp r2.w, r2.w
max r0.w, r0, c24.x
mul r4.xyz, r2.w, c19
mad r4.xyz, r4, r0.w, r5
mov r3.x, c15.z
mov r3.z, c17
mov r3.y, c16.z
add r3.xyz, -r1, r3
dp3 r1.w, r3, r3
rsq r3.w, r1.w
mul r3.xyz, r3.w, r3
dp3 r0.w, r2, r3
mul r1.w, r1, c18.z
add r1.w, r1, c23
rcp r2.w, r1.w
max r0.w, r0, c24.x
mov r3.x, c15.w
mov r3.z, c17.w
mov r3.y, c16.w
add r1.xyz, r3, -r1
mul r3.xyz, r2.w, c21
dp3 r1.w, r1, r1
mad r3.xyz, r3, r0.w, r4
rsq r2.w, r1.w
mul r1.xyz, r2.w, r1
mul r0.w, r1, c18
dp3 r1.x, r2, r1
add r0.w, r0, c23
max r1.w, r1.x, c24.x
rcp r0.w, r0.w
mul r1.xyz, r0.w, c22
mad r2.xyz, r1, r1.w, r3
mad r3, v2, c23.x, c23.y
mad r1, v4, c23.x, c23.y
mul oT5.xyz, r2, c24.y
frc r2, r1
add r1, r1, -r2
frc r2, r3
mul oT2, r1, c23.z
add r1, r3, -r2
dp4 r3.w, v0, c3
dp4 r3.z, v0, c2
mul oT3, r1, c23.z
dp4 r3.x, v0, c0
dp4 r3.y, v0, c1
mul r2.xyz, r3.xyww, c23.y
mul r1.y, r2, c12.x
mov r1.x, r2
mad oT6.xy, r2.z, c13.zwzw, r1
mov r1, c10
dp4 oT1.z, c14, r1
mov r1, c9
mov r2, c8
mov oPos, r3
dp4 oT1.y, c14, r1
dp4 oT1.x, c14, r2
mov oT6.zw, r3
mov oT0.xyz, r0
mov oT4.xy, v3
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES


#ifdef VERTEX

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
  xlv_TEXCOORD6 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
}



#endif
#ifdef FRAGMENT

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp vec4 _LightShadowData;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp float tmpvar_8;
  mediump float lightShadowDataX_9;
  highp float dist_10;
  lowp float tmpvar_11;
  tmpvar_11 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD6).x;
  dist_10 = tmpvar_11;
  highp float tmpvar_12;
  tmpvar_12 = _LightShadowData.x;
  lightShadowDataX_9 = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = max (float((dist_10 > (xlv_TEXCOORD6.z / xlv_TEXCOORD6.w))), lightShadowDataX_9);
  tmpvar_8 = tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14.w = 1.0;
  tmpvar_14.xyz = tmpvar_5;
  highp vec4 tmpvar_15;
  tmpvar_15.w = 1.0;
  tmpvar_15.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_8)) * tmpvar_6.x) * tmpvar_14) * 2.0) + tmpvar_15);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES


#ifdef VERTEX

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = (glstate_matrix_mvp * _glesVertex);
  highp vec3 tmpvar_2;
  tmpvar_2 = normalize(_glesNormal);
  highp vec4 o_3;
  highp vec4 tmpvar_4;
  tmpvar_4 = (tmpvar_1 * 0.5);
  highp vec2 tmpvar_5;
  tmpvar_5.x = tmpvar_4.x;
  tmpvar_5.y = (tmpvar_4.y * _ProjectionParams.x);
  o_3.xy = (tmpvar_5 + tmpvar_4.w);
  o_3.zw = tmpvar_1.zw;
  highp vec3 lightColor_6;
  highp vec4 worldPos_7;
  highp vec3 worldN_8;
  mat3 tmpvar_9;
  tmpvar_9[0] = _Object2World[0].xyz;
  tmpvar_9[1] = _Object2World[1].xyz;
  tmpvar_9[2] = _Object2World[2].xyz;
  worldN_8 = (tmpvar_9 * tmpvar_2);
  worldPos_7 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.x;
  tmpvar_10.y = unity_4LightPosY0.x;
  tmpvar_10.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_7.xyz);
  lightColor_6 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_11))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.y;
  tmpvar_12.y = unity_4LightPosY0.y;
  tmpvar_12.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_13)))));
  highp vec3 tmpvar_14;
  tmpvar_14.x = unity_4LightPosX0.z;
  tmpvar_14.y = unity_4LightPosY0.z;
  tmpvar_14.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_15;
  tmpvar_15 = (tmpvar_14 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_15, tmpvar_15))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_15)))));
  highp vec3 tmpvar_16;
  tmpvar_16.x = unity_4LightPosX0.w;
  tmpvar_16.y = unity_4LightPosY0.w;
  tmpvar_16.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_17;
  tmpvar_17 = (tmpvar_16 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_17, tmpvar_17))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_17)))));
  gl_Position = tmpvar_1;
  xlv_TEXCOORD0 = tmpvar_2;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_6 * 0.8);
  xlv_TEXCOORD6 = o_3;
}



#endif
#ifdef FRAGMENT

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD6);
  lowp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = tmpvar_5;
  highp vec4 tmpvar_10;
  tmpvar_10.w = 1.0;
  tmpvar_10.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_8.x)) * tmpvar_6.x) * tmpvar_9) * 2.0) + tmpvar_10);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 349
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 353
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 358
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 362
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 366
    return (lightColor * 0.8);
}
#line 368
vertex2fragment vertShadow( in vertexInput v ) {
    #line 370
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 374
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    #line 379
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
out highp vec4 xlv_TEXCOORD6;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
    xlv_TEXCOORD6 = vec4(xl_retval._ShadowCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 317
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    #line 319
    highp float dist = textureProj( _ShadowMapTexture, shadowCoord).x;
    mediump float lightShadowDataX = _LightShadowData.x;
    return max( float((dist > (shadowCoord.z / shadowCoord.w))), lightShadowDataX);
}
#line 382
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 384
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 388
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 392
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 396
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * unitySampleShadow( i._ShadowCoord))) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
in highp vec4 xlv_TEXCOORD6;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD6);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" ATTR14
Bind "texcoord" TexCoord0
Bind "color" Color
Vector 13 [_ProjectionParams]
Vector 14 [_WorldSpaceLightPos0]
Vector 15 [unity_4LightPosX0]
Vector 16 [unity_4LightPosY0]
Vector 17 [unity_4LightPosZ0]
Vector 18 [unity_4LightAtten0]
Vector 19 [unity_LightColor0]
Vector 20 [unity_LightColor1]
Vector 21 [unity_LightColor2]
Vector 22 [unity_LightColor3]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
"!!ARBvp1.0
# 87 ALU
PARAM c[24] = { { 0.0078125, 128, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..22],
		{ 0, 0.80000001 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
DP3 R0.w, vertex.normal, vertex.normal;
DP4 R1.z, vertex.position, c[7];
DP4 R1.y, vertex.position, c[6];
DP4 R1.x, vertex.position, c[5];
RSQ R0.w, R0.w;
MOV R2.z, c[17].y;
MOV R2.y, c[16];
MOV R0.x, c[15];
MOV R0.z, c[17].x;
MOV R0.y, c[16].x;
ADD R0.xyz, -R1, R0;
DP3 R1.w, R0, R0;
RSQ R2.x, R1.w;
MUL R3.xyz, R2.x, R0;
MUL R0.xyz, R0.w, vertex.normal;
MOV R2.x, c[15].y;
ADD R4.xyz, -R1, R2;
DP3 R2.z, R0, c[7];
DP3 R2.y, R0, c[6];
DP3 R2.x, R0, c[5];
DP3 R0.w, R2, R3;
DP3 R2.w, R4, R4;
RSQ R3.x, R2.w;
MUL R3.xyz, R3.x, R4;
DP3 R3.x, R2, R3;
MUL R2.w, R2, c[18].y;
ADD R2.w, R2, c[0];
MAX R3.w, R3.x, c[23].x;
RCP R2.w, R2.w;
MUL R3.xyz, R2.w, c[20];
MUL R4.xyz, R3, R3.w;
MUL R2.w, R1, c[18].x;
ADD R2.w, R2, c[0];
MAX R0.w, R0, c[23].x;
RCP R2.w, R2.w;
MOV R3.x, c[15].z;
MOV R3.z, c[17];
MOV R3.y, c[16].z;
ADD R3.xyz, -R1, R3;
DP3 R1.w, R3, R3;
RSQ R3.w, R1.w;
MUL R5.xyz, R3.w, R3;
MUL R3.xyz, R2.w, c[19];
MAD R3.xyz, R3, R0.w, R4;
DP3 R0.w, R2, R5;
MUL R1.w, R1, c[18].z;
ADD R1.w, R1, c[0];
RCP R2.w, R1.w;
DP4 R3.w, vertex.position, c[4];
MAX R0.w, R0, c[23].x;
MOV R4.x, c[15].w;
MOV R4.z, c[17].w;
MOV R4.y, c[16].w;
ADD R1.xyz, R4, -R1;
MUL R4.xyz, R2.w, c[21];
MAD R3.xyz, R4, R0.w, R3;
DP3 R1.w, R1, R1;
RSQ R0.w, R1.w;
MUL R1.xyz, R0.w, R1;
MUL R1.w, R1, c[18];
ADD R0.w, R1, c[0];
DP3 R1.x, R2, R1;
MAX R1.w, R1.x, c[23].x;
RCP R0.w, R0.w;
MUL R1.xyz, R0.w, c[22];
MAD R2.xyz, R1, R1.w, R3;
DP4 R3.z, vertex.position, c[3];
MUL result.texcoord[5].xyz, R2, c[23].y;
MAD R2, vertex.attrib[14], c[0].y, c[0].z;
FLR R2, R2;
DP4 R3.x, vertex.position, c[1];
DP4 R3.y, vertex.position, c[2];
MUL R1.xyz, R3.xyww, c[0].z;
MUL R1.y, R1, c[13].x;
ADD result.texcoord[6].xy, R1, R1.z;
MOV R1, c[14];
DP4 result.texcoord[1].z, R1, c[11];
DP4 result.texcoord[1].y, R1, c[10];
DP4 result.texcoord[1].x, R1, c[9];
MAD R1, vertex.color, c[0].y, c[0].z;
FLR R1, R1;
MOV result.position, R3;
MUL result.texcoord[2], R1, c[0].x;
MUL result.texcoord[3], R2, c[0].x;
MOV result.texcoord[6].zw, R3;
MOV result.texcoord[0].xyz, R0;
MOV result.texcoord[4].xy, vertex.texcoord[0];
END
# 87 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" TexCoord2
Bind "texcoord" TexCoord0
Bind "color" Color
Matrix 0 [glstate_matrix_mvp]
Vector 12 [_ProjectionParams]
Vector 13 [_ScreenParams]
Vector 14 [_WorldSpaceLightPos0]
Vector 15 [unity_4LightPosX0]
Vector 16 [unity_4LightPosY0]
Vector 17 [unity_4LightPosZ0]
Vector 18 [unity_4LightAtten0]
Vector 19 [unity_LightColor0]
Vector 20 [unity_LightColor1]
Vector 21 [unity_LightColor2]
Vector 22 [unity_LightColor3]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
"vs_2_0
; 96 ALU
def c23, 128.00000000, 0.50000000, 0.00781250, 1.00000000
def c24, 0.00000000, 0.80000001, 0, 0
dcl_position0 v0
dcl_normal0 v1
dcl_tangent0 v2
dcl_texcoord0 v3
dcl_color0 v4
dp3 r0.w, v1, v1
dp4 r1.z, v0, c6
dp4 r1.y, v0, c5
dp4 r1.x, v0, c4
rsq r0.w, r0.w
mov r2.z, c17.y
mov r2.y, c16
mov r0.x, c15
mov r0.z, c17.x
mov r0.y, c16.x
add r0.xyz, -r1, r0
dp3 r1.w, r0, r0
rsq r2.x, r1.w
mul r3.xyz, r2.x, r0
mul r0.xyz, r0.w, v1
mov r2.x, c15.y
add r4.xyz, -r1, r2
dp3 r2.z, r0, c6
dp3 r2.y, r0, c5
dp3 r2.x, r0, c4
dp3 r0.w, r2, r3
dp3 r2.w, r4, r4
rsq r3.x, r2.w
mul r3.xyz, r3.x, r4
dp3 r3.x, r2, r3
mul r2.w, r2, c18.y
add r2.w, r2, c23
max r3.w, r3.x, c24.x
rcp r2.w, r2.w
mul r3.xyz, r2.w, c20
mul r5.xyz, r3, r3.w
mul r2.w, r1, c18.x
add r2.w, r2, c23
rcp r2.w, r2.w
max r0.w, r0, c24.x
mul r4.xyz, r2.w, c19
mad r4.xyz, r4, r0.w, r5
mov r3.x, c15.z
mov r3.z, c17
mov r3.y, c16.z
add r3.xyz, -r1, r3
dp3 r1.w, r3, r3
rsq r3.w, r1.w
mul r3.xyz, r3.w, r3
dp3 r0.w, r2, r3
mul r1.w, r1, c18.z
add r1.w, r1, c23
rcp r2.w, r1.w
max r0.w, r0, c24.x
mov r3.x, c15.w
mov r3.z, c17.w
mov r3.y, c16.w
add r1.xyz, r3, -r1
mul r3.xyz, r2.w, c21
dp3 r1.w, r1, r1
mad r3.xyz, r3, r0.w, r4
rsq r2.w, r1.w
mul r1.xyz, r2.w, r1
mul r0.w, r1, c18
dp3 r1.x, r2, r1
add r0.w, r0, c23
max r1.w, r1.x, c24.x
rcp r0.w, r0.w
mul r1.xyz, r0.w, c22
mad r2.xyz, r1, r1.w, r3
mad r3, v2, c23.x, c23.y
mad r1, v4, c23.x, c23.y
mul oT5.xyz, r2, c24.y
frc r2, r1
add r1, r1, -r2
frc r2, r3
mul oT2, r1, c23.z
add r1, r3, -r2
dp4 r3.w, v0, c3
dp4 r3.z, v0, c2
mul oT3, r1, c23.z
dp4 r3.x, v0, c0
dp4 r3.y, v0, c1
mul r2.xyz, r3.xyww, c23.y
mul r1.y, r2, c12.x
mov r1.x, r2
mad oT6.xy, r2.z, c13.zwzw, r1
mov r1, c10
dp4 oT1.z, c14, r1
mov r1, c9
mov r2, c8
mov oPos, r3
dp4 oT1.y, c14, r1
dp4 oT1.x, c14, r2
mov oT6.zw, r3
mov oT0.xyz, r0
mov oT4.xy, v3
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" }
"!!GLES


#ifdef VERTEX

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
  xlv_TEXCOORD6 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
}



#endif
#ifdef FRAGMENT

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp vec4 _LightShadowData;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp float tmpvar_8;
  mediump float lightShadowDataX_9;
  highp float dist_10;
  lowp float tmpvar_11;
  tmpvar_11 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD6).x;
  dist_10 = tmpvar_11;
  highp float tmpvar_12;
  tmpvar_12 = _LightShadowData.x;
  lightShadowDataX_9 = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = max (float((dist_10 > (xlv_TEXCOORD6.z / xlv_TEXCOORD6.w))), lightShadowDataX_9);
  tmpvar_8 = tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14.w = 1.0;
  tmpvar_14.xyz = tmpvar_5;
  highp vec4 tmpvar_15;
  tmpvar_15.w = 1.0;
  tmpvar_15.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_8)) * tmpvar_6.x) * tmpvar_14) * 2.0) + tmpvar_15);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" }
"!!GLES


#ifdef VERTEX

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = (glstate_matrix_mvp * _glesVertex);
  highp vec3 tmpvar_2;
  tmpvar_2 = normalize(_glesNormal);
  highp vec4 o_3;
  highp vec4 tmpvar_4;
  tmpvar_4 = (tmpvar_1 * 0.5);
  highp vec2 tmpvar_5;
  tmpvar_5.x = tmpvar_4.x;
  tmpvar_5.y = (tmpvar_4.y * _ProjectionParams.x);
  o_3.xy = (tmpvar_5 + tmpvar_4.w);
  o_3.zw = tmpvar_1.zw;
  highp vec3 lightColor_6;
  highp vec4 worldPos_7;
  highp vec3 worldN_8;
  mat3 tmpvar_9;
  tmpvar_9[0] = _Object2World[0].xyz;
  tmpvar_9[1] = _Object2World[1].xyz;
  tmpvar_9[2] = _Object2World[2].xyz;
  worldN_8 = (tmpvar_9 * tmpvar_2);
  worldPos_7 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.x;
  tmpvar_10.y = unity_4LightPosY0.x;
  tmpvar_10.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_7.xyz);
  lightColor_6 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_11))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.y;
  tmpvar_12.y = unity_4LightPosY0.y;
  tmpvar_12.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_13)))));
  highp vec3 tmpvar_14;
  tmpvar_14.x = unity_4LightPosX0.z;
  tmpvar_14.y = unity_4LightPosY0.z;
  tmpvar_14.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_15;
  tmpvar_15 = (tmpvar_14 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_15, tmpvar_15))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_15)))));
  highp vec3 tmpvar_16;
  tmpvar_16.x = unity_4LightPosX0.w;
  tmpvar_16.y = unity_4LightPosY0.w;
  tmpvar_16.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_17;
  tmpvar_17 = (tmpvar_16 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_17, tmpvar_17))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_17)))));
  gl_Position = tmpvar_1;
  xlv_TEXCOORD0 = tmpvar_2;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_6 * 0.8);
  xlv_TEXCOORD6 = o_3;
}



#endif
#ifdef FRAGMENT

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD6);
  lowp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = tmpvar_5;
  highp vec4 tmpvar_10;
  tmpvar_10.w = 1.0;
  tmpvar_10.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_8.x)) * tmpvar_6.x) * tmpvar_9) * 2.0) + tmpvar_10);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 349
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 353
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 358
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 362
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 366
    return (lightColor * 0.8);
}
#line 368
vertex2fragment vertShadow( in vertexInput v ) {
    #line 370
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 374
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    #line 379
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
out highp vec4 xlv_TEXCOORD6;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
    xlv_TEXCOORD6 = vec4(xl_retval._ShadowCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 317
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    #line 319
    highp float dist = textureProj( _ShadowMapTexture, shadowCoord).x;
    mediump float lightShadowDataX = _LightShadowData.x;
    return max( float((dist > (shadowCoord.z / shadowCoord.w))), lightShadowDataX);
}
#line 382
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 384
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 388
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 392
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 396
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * unitySampleShadow( i._ShadowCoord))) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
in highp vec4 xlv_TEXCOORD6;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD6);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" ATTR14
Bind "texcoord" TexCoord0
Bind "color" Color
Vector 13 [_WorldSpaceLightPos0]
Vector 14 [unity_4LightPosX0]
Vector 15 [unity_4LightPosY0]
Vector 16 [unity_4LightPosZ0]
Vector 17 [unity_4LightAtten0]
Vector 18 [unity_LightColor0]
Vector 19 [unity_LightColor1]
Vector 20 [unity_LightColor2]
Vector 21 [unity_LightColor3]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
"!!ARBvp1.0
# 82 ALU
PARAM c[23] = { { 0.0078125, 128, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..21],
		{ 0, 0.80000001 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
DP3 R0.w, vertex.normal, vertex.normal;
DP4 R1.z, vertex.position, c[7];
DP4 R1.y, vertex.position, c[6];
DP4 R1.x, vertex.position, c[5];
RSQ R0.w, R0.w;
MOV R2.z, c[16].y;
MOV R2.y, c[15];
MOV R0.x, c[14];
MOV R0.z, c[16].x;
MOV R0.y, c[15].x;
ADD R0.xyz, -R1, R0;
DP3 R1.w, R0, R0;
RSQ R2.x, R1.w;
MUL R3.xyz, R2.x, R0;
MUL R0.xyz, R0.w, vertex.normal;
MOV R2.x, c[14].y;
ADD R4.xyz, -R1, R2;
DP3 R2.z, R0, c[7];
DP3 R2.y, R0, c[6];
DP3 R2.x, R0, c[5];
DP3 R0.w, R2, R3;
DP3 R2.w, R4, R4;
RSQ R3.x, R2.w;
MUL R3.xyz, R3.x, R4;
DP3 R3.x, R2, R3;
MUL R2.w, R2, c[17].y;
ADD R2.w, R2, c[0];
MAX R3.w, R3.x, c[22].x;
RCP R2.w, R2.w;
MUL R3.xyz, R2.w, c[19];
MUL R4.xyz, R3, R3.w;
MUL R2.w, R1, c[17].x;
ADD R2.w, R2, c[0];
MAX R0.w, R0, c[22].x;
RCP R2.w, R2.w;
MOV R3.x, c[14].z;
MOV R3.z, c[16];
MOV R3.y, c[15].z;
ADD R3.xyz, -R1, R3;
DP3 R1.w, R3, R3;
RSQ R3.w, R1.w;
MUL R5.xyz, R3.w, R3;
MUL R3.xyz, R2.w, c[18];
MAD R3.xyz, R3, R0.w, R4;
DP3 R0.w, R2, R5;
MUL R1.w, R1, c[17].z;
ADD R1.w, R1, c[0];
RCP R2.w, R1.w;
MAX R0.w, R0, c[22].x;
MOV R4.x, c[14].w;
MOV R4.z, c[16].w;
MOV R4.y, c[15].w;
ADD R1.xyz, R4, -R1;
MUL R4.xyz, R2.w, c[20];
DP3 R1.w, R1, R1;
MAD R3.xyz, R4, R0.w, R3;
RSQ R0.w, R1.w;
MUL R1.xyz, R0.w, R1;
MUL R1.w, R1, c[17];
ADD R0.w, R1, c[0];
DP3 R1.x, R2, R1;
MAX R1.w, R1.x, c[22].x;
RCP R0.w, R0.w;
MUL R1.xyz, R0.w, c[21];
MAD R2.xyz, R1, R1.w, R3;
MOV R1, c[13];
MUL result.texcoord[5].xyz, R2, c[22].y;
MAD R2, vertex.attrib[14], c[0].y, c[0].z;
FLR R2, R2;
DP4 result.texcoord[1].z, R1, c[11];
DP4 result.texcoord[1].y, R1, c[10];
DP4 result.texcoord[1].x, R1, c[9];
MAD R1, vertex.color, c[0].y, c[0].z;
FLR R1, R1;
MUL result.texcoord[2], R1, c[0].x;
MUL result.texcoord[3], R2, c[0].x;
MOV result.texcoord[0].xyz, R0;
MOV result.texcoord[4].xy, vertex.texcoord[0];
DP4 result.position.w, vertex.position, c[4];
DP4 result.position.z, vertex.position, c[3];
DP4 result.position.y, vertex.position, c[2];
DP4 result.position.x, vertex.position, c[1];
END
# 82 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" TexCoord2
Bind "texcoord" TexCoord0
Bind "color" Color
Matrix 0 [glstate_matrix_mvp]
Vector 12 [_WorldSpaceLightPos0]
Vector 13 [unity_4LightPosX0]
Vector 14 [unity_4LightPosY0]
Vector 15 [unity_4LightPosZ0]
Vector 16 [unity_4LightAtten0]
Vector 17 [unity_LightColor0]
Vector 18 [unity_LightColor1]
Vector 19 [unity_LightColor2]
Vector 20 [unity_LightColor3]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
"vs_2_0
; 90 ALU
def c21, 128.00000000, 0.50000000, 0.00781250, 1.00000000
def c22, 0.00000000, 0.80000001, 0, 0
dcl_position0 v0
dcl_normal0 v1
dcl_tangent0 v2
dcl_texcoord0 v3
dcl_color0 v4
dp3 r0.w, v1, v1
dp4 r1.z, v0, c6
dp4 r1.y, v0, c5
dp4 r1.x, v0, c4
rsq r0.w, r0.w
mov r2.z, c15.y
mov r2.y, c14
mov r0.x, c13
mov r0.z, c15.x
mov r0.y, c14.x
add r0.xyz, -r1, r0
dp3 r1.w, r0, r0
rsq r2.x, r1.w
mul r3.xyz, r2.x, r0
mul r0.xyz, r0.w, v1
mov r2.x, c13.y
add r4.xyz, -r1, r2
dp3 r2.z, r0, c6
dp3 r2.y, r0, c5
dp3 r2.x, r0, c4
dp3 r0.w, r2, r3
dp3 r2.w, r4, r4
rsq r3.x, r2.w
mul r3.xyz, r3.x, r4
dp3 r3.x, r2, r3
mul r2.w, r2, c16.y
add r2.w, r2, c21
max r3.w, r3.x, c22.x
rcp r2.w, r2.w
mul r3.xyz, r2.w, c18
mul r5.xyz, r3, r3.w
mul r2.w, r1, c16.x
add r2.w, r2, c21
rcp r2.w, r2.w
max r0.w, r0, c22.x
mul r4.xyz, r2.w, c17
mad r4.xyz, r4, r0.w, r5
mov r3.x, c13.z
mov r3.z, c15
mov r3.y, c14.z
add r3.xyz, -r1, r3
dp3 r1.w, r3, r3
rsq r3.w, r1.w
mul r3.xyz, r3.w, r3
dp3 r0.w, r2, r3
mul r1.w, r1, c16.z
add r1.w, r1, c21
rcp r2.w, r1.w
max r0.w, r0, c22.x
mov r3.x, c13.w
mov r3.z, c15.w
mov r3.y, c14.w
add r1.xyz, r3, -r1
mul r3.xyz, r2.w, c19
dp3 r1.w, r1, r1
mad r3.xyz, r3, r0.w, r4
rsq r2.w, r1.w
mul r1.xyz, r2.w, r1
mul r0.w, r1, c16
dp3 r1.x, r2, r1
add r0.w, r0, c21
max r1.w, r1.x, c22.x
rcp r0.w, r0.w
mul r1.xyz, r0.w, c20
mad r2.xyz, r1, r1.w, r3
mad r1, v4, c21.x, c21.y
mul oT5.xyz, r2, c22.y
frc r2, r1
add r1, r1, -r2
mad r3, v2, c21.x, c21.y
frc r2, r3
add r2, r3, -r2
mul oT2, r1, c21.z
mov r1, c10
mul oT3, r2, c21.z
dp4 oT1.z, c12, r1
mov r1, c9
mov r2, c8
dp4 oT1.y, c12, r1
dp4 oT1.x, c12, r2
mov oT0.xyz, r0
mov oT4.xy, v3
dp4 oPos.w, v0, c3
dp4 oPos.z, v0, c2
dp4 oPos.y, v0, c1
dp4 oPos.x, v0, c0
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8.w = 1.0;
  tmpvar_8.xyz = tmpvar_5;
  highp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + (clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0)) * tmpvar_6.x) * tmpvar_8) * 2.0) + tmpvar_9);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8.w = 1.0;
  tmpvar_8.xyz = tmpvar_5;
  highp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + (clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0)) * tmpvar_6.x) * tmpvar_8) * 2.0) + tmpvar_9);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 329
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
};
#line 320
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 319
uniform highp vec4 _LightColor0;
#line 340
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 340
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 344
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 349
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 353
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 357
    return (lightColor * 0.8);
}
#line 359
vertex2fragment vertShadow( in vertexInput v ) {
    #line 361
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 365
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    #line 369
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 329
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
};
#line 320
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 319
uniform highp vec4 _LightColor0;
#line 340
#line 372
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 374
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 378
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 382
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 386
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * 1.0)) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" ATTR14
Bind "texcoord" TexCoord0
Bind "color" Color
Vector 13 [_ProjectionParams]
Vector 14 [_WorldSpaceLightPos0]
Vector 15 [unity_4LightPosX0]
Vector 16 [unity_4LightPosY0]
Vector 17 [unity_4LightPosZ0]
Vector 18 [unity_4LightAtten0]
Vector 19 [unity_LightColor0]
Vector 20 [unity_LightColor1]
Vector 21 [unity_LightColor2]
Vector 22 [unity_LightColor3]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
"!!ARBvp1.0
# 87 ALU
PARAM c[24] = { { 0.0078125, 128, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..22],
		{ 0, 0.80000001 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
DP3 R0.w, vertex.normal, vertex.normal;
DP4 R1.z, vertex.position, c[7];
DP4 R1.y, vertex.position, c[6];
DP4 R1.x, vertex.position, c[5];
RSQ R0.w, R0.w;
MOV R2.z, c[17].y;
MOV R2.y, c[16];
MOV R0.x, c[15];
MOV R0.z, c[17].x;
MOV R0.y, c[16].x;
ADD R0.xyz, -R1, R0;
DP3 R1.w, R0, R0;
RSQ R2.x, R1.w;
MUL R3.xyz, R2.x, R0;
MUL R0.xyz, R0.w, vertex.normal;
MOV R2.x, c[15].y;
ADD R4.xyz, -R1, R2;
DP3 R2.z, R0, c[7];
DP3 R2.y, R0, c[6];
DP3 R2.x, R0, c[5];
DP3 R0.w, R2, R3;
DP3 R2.w, R4, R4;
RSQ R3.x, R2.w;
MUL R3.xyz, R3.x, R4;
DP3 R3.x, R2, R3;
MUL R2.w, R2, c[18].y;
ADD R2.w, R2, c[0];
MAX R3.w, R3.x, c[23].x;
RCP R2.w, R2.w;
MUL R3.xyz, R2.w, c[20];
MUL R4.xyz, R3, R3.w;
MUL R2.w, R1, c[18].x;
ADD R2.w, R2, c[0];
MAX R0.w, R0, c[23].x;
RCP R2.w, R2.w;
MOV R3.x, c[15].z;
MOV R3.z, c[17];
MOV R3.y, c[16].z;
ADD R3.xyz, -R1, R3;
DP3 R1.w, R3, R3;
RSQ R3.w, R1.w;
MUL R5.xyz, R3.w, R3;
MUL R3.xyz, R2.w, c[19];
MAD R3.xyz, R3, R0.w, R4;
DP3 R0.w, R2, R5;
MUL R1.w, R1, c[18].z;
ADD R1.w, R1, c[0];
RCP R2.w, R1.w;
DP4 R3.w, vertex.position, c[4];
MAX R0.w, R0, c[23].x;
MOV R4.x, c[15].w;
MOV R4.z, c[17].w;
MOV R4.y, c[16].w;
ADD R1.xyz, R4, -R1;
MUL R4.xyz, R2.w, c[21];
MAD R3.xyz, R4, R0.w, R3;
DP3 R1.w, R1, R1;
RSQ R0.w, R1.w;
MUL R1.xyz, R0.w, R1;
MUL R1.w, R1, c[18];
ADD R0.w, R1, c[0];
DP3 R1.x, R2, R1;
MAX R1.w, R1.x, c[23].x;
RCP R0.w, R0.w;
MUL R1.xyz, R0.w, c[22];
MAD R2.xyz, R1, R1.w, R3;
DP4 R3.z, vertex.position, c[3];
MUL result.texcoord[5].xyz, R2, c[23].y;
MAD R2, vertex.attrib[14], c[0].y, c[0].z;
FLR R2, R2;
DP4 R3.x, vertex.position, c[1];
DP4 R3.y, vertex.position, c[2];
MUL R1.xyz, R3.xyww, c[0].z;
MUL R1.y, R1, c[13].x;
ADD result.texcoord[6].xy, R1, R1.z;
MOV R1, c[14];
DP4 result.texcoord[1].z, R1, c[11];
DP4 result.texcoord[1].y, R1, c[10];
DP4 result.texcoord[1].x, R1, c[9];
MAD R1, vertex.color, c[0].y, c[0].z;
FLR R1, R1;
MOV result.position, R3;
MUL result.texcoord[2], R1, c[0].x;
MUL result.texcoord[3], R2, c[0].x;
MOV result.texcoord[6].zw, R3;
MOV result.texcoord[0].xyz, R0;
MOV result.texcoord[4].xy, vertex.texcoord[0];
END
# 87 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "normal" Normal
Bind "tangent" TexCoord2
Bind "texcoord" TexCoord0
Bind "color" Color
Matrix 0 [glstate_matrix_mvp]
Vector 12 [_ProjectionParams]
Vector 13 [_ScreenParams]
Vector 14 [_WorldSpaceLightPos0]
Vector 15 [unity_4LightPosX0]
Vector 16 [unity_4LightPosY0]
Vector 17 [unity_4LightPosZ0]
Vector 18 [unity_4LightAtten0]
Vector 19 [unity_LightColor0]
Vector 20 [unity_LightColor1]
Vector 21 [unity_LightColor2]
Vector 22 [unity_LightColor3]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
"vs_2_0
; 96 ALU
def c23, 128.00000000, 0.50000000, 0.00781250, 1.00000000
def c24, 0.00000000, 0.80000001, 0, 0
dcl_position0 v0
dcl_normal0 v1
dcl_tangent0 v2
dcl_texcoord0 v3
dcl_color0 v4
dp3 r0.w, v1, v1
dp4 r1.z, v0, c6
dp4 r1.y, v0, c5
dp4 r1.x, v0, c4
rsq r0.w, r0.w
mov r2.z, c17.y
mov r2.y, c16
mov r0.x, c15
mov r0.z, c17.x
mov r0.y, c16.x
add r0.xyz, -r1, r0
dp3 r1.w, r0, r0
rsq r2.x, r1.w
mul r3.xyz, r2.x, r0
mul r0.xyz, r0.w, v1
mov r2.x, c15.y
add r4.xyz, -r1, r2
dp3 r2.z, r0, c6
dp3 r2.y, r0, c5
dp3 r2.x, r0, c4
dp3 r0.w, r2, r3
dp3 r2.w, r4, r4
rsq r3.x, r2.w
mul r3.xyz, r3.x, r4
dp3 r3.x, r2, r3
mul r2.w, r2, c18.y
add r2.w, r2, c23
max r3.w, r3.x, c24.x
rcp r2.w, r2.w
mul r3.xyz, r2.w, c20
mul r5.xyz, r3, r3.w
mul r2.w, r1, c18.x
add r2.w, r2, c23
rcp r2.w, r2.w
max r0.w, r0, c24.x
mul r4.xyz, r2.w, c19
mad r4.xyz, r4, r0.w, r5
mov r3.x, c15.z
mov r3.z, c17
mov r3.y, c16.z
add r3.xyz, -r1, r3
dp3 r1.w, r3, r3
rsq r3.w, r1.w
mul r3.xyz, r3.w, r3
dp3 r0.w, r2, r3
mul r1.w, r1, c18.z
add r1.w, r1, c23
rcp r2.w, r1.w
max r0.w, r0, c24.x
mov r3.x, c15.w
mov r3.z, c17.w
mov r3.y, c16.w
add r1.xyz, r3, -r1
mul r3.xyz, r2.w, c21
dp3 r1.w, r1, r1
mad r3.xyz, r3, r0.w, r4
rsq r2.w, r1.w
mul r1.xyz, r2.w, r1
mul r0.w, r1, c18
dp3 r1.x, r2, r1
add r0.w, r0, c23
max r1.w, r1.x, c24.x
rcp r0.w, r0.w
mul r1.xyz, r0.w, c22
mad r2.xyz, r1, r1.w, r3
mad r3, v2, c23.x, c23.y
mad r1, v4, c23.x, c23.y
mul oT5.xyz, r2, c24.y
frc r2, r1
add r1, r1, -r2
frc r2, r3
mul oT2, r1, c23.z
add r1, r3, -r2
dp4 r3.w, v0, c3
dp4 r3.z, v0, c2
mul oT3, r1, c23.z
dp4 r3.x, v0, c0
dp4 r3.y, v0, c1
mul r2.xyz, r3.xyww, c23.y
mul r1.y, r2, c12.x
mov r1.x, r2
mad oT6.xy, r2.z, c13.zwzw, r1
mov r1, c10
dp4 oT1.z, c14, r1
mov r1, c9
mov r2, c8
mov oPos, r3
dp4 oT1.y, c14, r1
dp4 oT1.x, c14, r2
mov oT6.zw, r3
mov oT0.xyz, r0
mov oT4.xy, v3
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
"!!GLES


#ifdef VERTEX

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
  xlv_TEXCOORD6 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
}



#endif
#ifdef FRAGMENT

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp vec4 _LightShadowData;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp float tmpvar_8;
  mediump float lightShadowDataX_9;
  highp float dist_10;
  lowp float tmpvar_11;
  tmpvar_11 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD6).x;
  dist_10 = tmpvar_11;
  highp float tmpvar_12;
  tmpvar_12 = _LightShadowData.x;
  lightShadowDataX_9 = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = max (float((dist_10 > (xlv_TEXCOORD6.z / xlv_TEXCOORD6.w))), lightShadowDataX_9);
  tmpvar_8 = tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14.w = 1.0;
  tmpvar_14.xyz = tmpvar_5;
  highp vec4 tmpvar_15;
  tmpvar_15.w = 1.0;
  tmpvar_15.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_8)) * tmpvar_6.x) * tmpvar_14) * 2.0) + tmpvar_15);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
"!!GLES


#ifdef VERTEX

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = (glstate_matrix_mvp * _glesVertex);
  highp vec3 tmpvar_2;
  tmpvar_2 = normalize(_glesNormal);
  highp vec4 o_3;
  highp vec4 tmpvar_4;
  tmpvar_4 = (tmpvar_1 * 0.5);
  highp vec2 tmpvar_5;
  tmpvar_5.x = tmpvar_4.x;
  tmpvar_5.y = (tmpvar_4.y * _ProjectionParams.x);
  o_3.xy = (tmpvar_5 + tmpvar_4.w);
  o_3.zw = tmpvar_1.zw;
  highp vec3 lightColor_6;
  highp vec4 worldPos_7;
  highp vec3 worldN_8;
  mat3 tmpvar_9;
  tmpvar_9[0] = _Object2World[0].xyz;
  tmpvar_9[1] = _Object2World[1].xyz;
  tmpvar_9[2] = _Object2World[2].xyz;
  worldN_8 = (tmpvar_9 * tmpvar_2);
  worldPos_7 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.x;
  tmpvar_10.y = unity_4LightPosY0.x;
  tmpvar_10.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_7.xyz);
  lightColor_6 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_11))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.y;
  tmpvar_12.y = unity_4LightPosY0.y;
  tmpvar_12.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_13)))));
  highp vec3 tmpvar_14;
  tmpvar_14.x = unity_4LightPosX0.z;
  tmpvar_14.y = unity_4LightPosY0.z;
  tmpvar_14.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_15;
  tmpvar_15 = (tmpvar_14 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_15, tmpvar_15))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_15)))));
  highp vec3 tmpvar_16;
  tmpvar_16.x = unity_4LightPosX0.w;
  tmpvar_16.y = unity_4LightPosY0.w;
  tmpvar_16.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_17;
  tmpvar_17 = (tmpvar_16 - worldPos_7.xyz);
  lightColor_6 = (lightColor_6 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_17, tmpvar_17))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_8, normalize(tmpvar_17)))));
  gl_Position = tmpvar_1;
  xlv_TEXCOORD0 = tmpvar_2;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_6 * 0.8);
  xlv_TEXCOORD6 = o_3;
}



#endif
#ifdef FRAGMENT

varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp vec4 tmpvar_8;
  tmpvar_8 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD6);
  lowp vec4 tmpvar_9;
  tmpvar_9.w = 1.0;
  tmpvar_9.xyz = tmpvar_5;
  highp vec4 tmpvar_10;
  tmpvar_10.w = 1.0;
  tmpvar_10.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_8.x)) * tmpvar_6.x) * tmpvar_9) * 2.0) + tmpvar_10);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 349
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 353
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 358
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 362
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 366
    return (lightColor * 0.8);
}
#line 368
vertex2fragment vertShadow( in vertexInput v ) {
    #line 370
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 374
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    #line 379
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
out highp vec4 xlv_TEXCOORD6;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
    xlv_TEXCOORD6 = vec4(xl_retval._ShadowCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 317
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    #line 319
    highp float dist = textureProj( _ShadowMapTexture, shadowCoord).x;
    mediump float lightShadowDataX = _LightShadowData.x;
    return max( float((dist > (shadowCoord.z / shadowCoord.w))), lightShadowDataX);
}
#line 382
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 384
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 388
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 392
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 396
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * unitySampleShadow( i._ShadowCoord))) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
in highp vec4 xlv_TEXCOORD6;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD6);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "SHADOWS_NATIVE" }
"!!GLES


#ifdef VERTEX

#extension GL_EXT_shadow_samplers : enable
varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
  xlv_TEXCOORD6 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
}



#endif
#ifdef FRAGMENT

#extension GL_EXT_shadow_samplers : enable
varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform lowp sampler2DShadow _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp vec4 _LightShadowData;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp float shadow_8;
  lowp float tmpvar_9;
  tmpvar_9 = shadow2DEXT (_ShadowMapTexture, xlv_TEXCOORD6.xyz);
  highp float tmpvar_10;
  tmpvar_10 = (_LightShadowData.x + (tmpvar_9 * (1.0 - _LightShadowData.x)));
  shadow_8 = tmpvar_10;
  lowp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_5;
  highp vec4 tmpvar_12;
  tmpvar_12.w = 1.0;
  tmpvar_12.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * shadow_8)) * tmpvar_6.x) * tmpvar_11) * 2.0) + tmpvar_12);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "SHADOWS_NATIVE" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 349
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 353
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 358
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 362
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 366
    return (lightColor * 0.8);
}
#line 368
vertex2fragment vertShadow( in vertexInput v ) {
    #line 370
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 374
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    #line 379
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
out highp vec4 xlv_TEXCOORD6;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
    xlv_TEXCOORD6 = vec4(xl_retval._ShadowCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_shadow2D(mediump sampler2DShadow s, vec3 coord) { return texture (s, coord); }
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 317
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    #line 319
    lowp float shadow = xll_shadow2D( _ShadowMapTexture, shadowCoord.xyz.xyz);
    shadow = (_LightShadowData.x + (shadow * (1.0 - _LightShadowData.x)));
    return shadow;
}
#line 382
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 384
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 388
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 392
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 396
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * unitySampleShadow( i._ShadowCoord))) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
in highp vec4 xlv_TEXCOORD6;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD6);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "SHADOWS_NATIVE" }
"!!GLES


#ifdef VERTEX

#extension GL_EXT_shadow_samplers : enable
varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
  xlv_TEXCOORD6 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
}



#endif
#ifdef FRAGMENT

#extension GL_EXT_shadow_samplers : enable
varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform lowp sampler2DShadow _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp vec4 _LightShadowData;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp float shadow_8;
  lowp float tmpvar_9;
  tmpvar_9 = shadow2DEXT (_ShadowMapTexture, xlv_TEXCOORD6.xyz);
  highp float tmpvar_10;
  tmpvar_10 = (_LightShadowData.x + (tmpvar_9 * (1.0 - _LightShadowData.x)));
  shadow_8 = tmpvar_10;
  lowp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_5;
  highp vec4 tmpvar_12;
  tmpvar_12.w = 1.0;
  tmpvar_12.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * shadow_8)) * tmpvar_6.x) * tmpvar_11) * 2.0) + tmpvar_12);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "SHADOWS_NATIVE" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 349
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 353
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 358
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 362
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 366
    return (lightColor * 0.8);
}
#line 368
vertex2fragment vertShadow( in vertexInput v ) {
    #line 370
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 374
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    #line 379
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
out highp vec4 xlv_TEXCOORD6;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
    xlv_TEXCOORD6 = vec4(xl_retval._ShadowCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_shadow2D(mediump sampler2DShadow s, vec3 coord) { return texture (s, coord); }
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 317
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    #line 319
    lowp float shadow = xll_shadow2D( _ShadowMapTexture, shadowCoord.xyz.xyz);
    shadow = (_LightShadowData.x + (shadow * (1.0 - _LightShadowData.x)));
    return shadow;
}
#line 382
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 384
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 388
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 392
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 396
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * unitySampleShadow( i._ShadowCoord))) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
in highp vec4 xlv_TEXCOORD6;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD6);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" "SHADOWS_NATIVE" }
"!!GLES


#ifdef VERTEX

#extension GL_EXT_shadow_samplers : enable
varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
  xlv_TEXCOORD6 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
}



#endif
#ifdef FRAGMENT

#extension GL_EXT_shadow_samplers : enable
varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform lowp sampler2DShadow _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp vec4 _LightShadowData;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp float shadow_8;
  lowp float tmpvar_9;
  tmpvar_9 = shadow2DEXT (_ShadowMapTexture, xlv_TEXCOORD6.xyz);
  highp float tmpvar_10;
  tmpvar_10 = (_LightShadowData.x + (tmpvar_9 * (1.0 - _LightShadowData.x)));
  shadow_8 = tmpvar_10;
  lowp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_5;
  highp vec4 tmpvar_12;
  tmpvar_12.w = 1.0;
  tmpvar_12.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * shadow_8)) * tmpvar_6.x) * tmpvar_11) * 2.0) + tmpvar_12);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" "SHADOWS_NATIVE" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 349
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 353
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 358
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 362
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 366
    return (lightColor * 0.8);
}
#line 368
vertex2fragment vertShadow( in vertexInput v ) {
    #line 370
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 374
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    #line 379
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
out highp vec4 xlv_TEXCOORD6;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
    xlv_TEXCOORD6 = vec4(xl_retval._ShadowCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_shadow2D(mediump sampler2DShadow s, vec3 coord) { return texture (s, coord); }
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 317
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    #line 319
    lowp float shadow = xll_shadow2D( _ShadowMapTexture, shadowCoord.xyz.xyz);
    shadow = (_LightShadowData.x + (shadow * (1.0 - _LightShadowData.x)));
    return shadow;
}
#line 382
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 384
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 388
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 392
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 396
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * unitySampleShadow( i._ShadowCoord))) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
in highp vec4 xlv_TEXCOORD6;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD6);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "SHADOWS_NATIVE" "VERTEXLIGHT_ON" }
"!!GLES


#ifdef VERTEX

#extension GL_EXT_shadow_samplers : enable
varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_4LightPosZ0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosX0;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec4 _glesTANGENT;
attribute vec4 _glesMultiTexCoord0;
attribute vec3 _glesNormal;
attribute vec4 _glesColor;
attribute vec4 _glesVertex;
void main ()
{
  highp vec3 tmpvar_1;
  tmpvar_1 = normalize(_glesNormal);
  highp vec3 lightColor_2;
  highp vec4 worldPos_3;
  highp vec3 worldN_4;
  mat3 tmpvar_5;
  tmpvar_5[0] = _Object2World[0].xyz;
  tmpvar_5[1] = _Object2World[1].xyz;
  tmpvar_5[2] = _Object2World[2].xyz;
  worldN_4 = (tmpvar_5 * tmpvar_1);
  worldPos_3 = (_Object2World * _glesVertex);
  highp vec3 tmpvar_6;
  tmpvar_6.x = unity_4LightPosX0.x;
  tmpvar_6.y = unity_4LightPosY0.x;
  tmpvar_6.z = unity_4LightPosZ0.x;
  highp vec3 tmpvar_7;
  tmpvar_7 = (tmpvar_6 - worldPos_3.xyz);
  lightColor_2 = (((1.0/((1.0 + (unity_4LightAtten0.x * dot (tmpvar_7, tmpvar_7))))) * unity_LightColor[0].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_7))));
  highp vec3 tmpvar_8;
  tmpvar_8.x = unity_4LightPosX0.y;
  tmpvar_8.y = unity_4LightPosY0.y;
  tmpvar_8.z = unity_4LightPosZ0.y;
  highp vec3 tmpvar_9;
  tmpvar_9 = (tmpvar_8 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.y * dot (tmpvar_9, tmpvar_9))))) * unity_LightColor[1].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_9)))));
  highp vec3 tmpvar_10;
  tmpvar_10.x = unity_4LightPosX0.z;
  tmpvar_10.y = unity_4LightPosY0.z;
  tmpvar_10.z = unity_4LightPosZ0.z;
  highp vec3 tmpvar_11;
  tmpvar_11 = (tmpvar_10 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.z * dot (tmpvar_11, tmpvar_11))))) * unity_LightColor[2].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_11)))));
  highp vec3 tmpvar_12;
  tmpvar_12.x = unity_4LightPosX0.w;
  tmpvar_12.y = unity_4LightPosY0.w;
  tmpvar_12.z = unity_4LightPosZ0.w;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_12 - worldPos_3.xyz);
  lightColor_2 = (lightColor_2 + (((1.0/((1.0 + (unity_4LightAtten0.w * dot (tmpvar_13, tmpvar_13))))) * unity_LightColor[3].xyz) * max (0.0, dot (worldN_4, normalize(tmpvar_13)))));
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = tmpvar_1;
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (floor(((_glesColor * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD3 = (floor(((_glesTANGENT * 128.0) + 0.5)) / 128.0);
  xlv_TEXCOORD4 = _glesMultiTexCoord0.xy;
  xlv_TEXCOORD5 = (lightColor_2 * 0.8);
  xlv_TEXCOORD6 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
}



#endif
#ifdef FRAGMENT

#extension GL_EXT_shadow_samplers : enable
varying highp vec4 xlv_TEXCOORD6;
varying highp vec3 xlv_TEXCOORD5;
varying highp vec2 xlv_TEXCOORD4;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform mediump vec4 _ShdeTex_ST;
uniform sampler2D _ShdeTex;
uniform sampler2D _MainTex;
uniform lowp sampler2DShadow _ShadowMapTexture;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp vec4 _LightShadowData;
void main ()
{
  lowp vec3 color_1;
  color_1 = texture2D (_MainTex, xlv_TEXCOORD2.xy).xyz;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD2.zw);
  if ((((tmpvar_2.x != 1.0) && (tmpvar_2.y != 0.0)) && (tmpvar_2.z != 1.0))) {
    color_1 = tmpvar_2.xyz;
  };
  color_1 = (color_1 * tmpvar_2.w);
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD3.xy);
  if ((((tmpvar_3.x != 1.0) && (tmpvar_3.y != 0.0)) && (tmpvar_3.z != 1.0))) {
    color_1 = tmpvar_3.xyz;
  };
  color_1 = (color_1 * tmpvar_3.w);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_MainTex, xlv_TEXCOORD3.zw);
  if ((((tmpvar_4.x != 1.0) && (tmpvar_4.y != 0.0)) && (tmpvar_4.z != 1.0))) {
    color_1 = tmpvar_4.xyz;
  };
  lowp vec3 tmpvar_5;
  tmpvar_5 = (color_1 * tmpvar_4.w);
  color_1 = tmpvar_5;
  lowp vec4 tmpvar_6;
  highp vec2 P_7;
  P_7 = ((xlv_TEXCOORD4 * _ShdeTex_ST.xy) + _ShdeTex_ST.zw);
  tmpvar_6 = texture2D (_ShdeTex, P_7);
  lowp float shadow_8;
  lowp float tmpvar_9;
  tmpvar_9 = shadow2DEXT (_ShadowMapTexture, xlv_TEXCOORD6.xyz);
  highp float tmpvar_10;
  tmpvar_10 = (_LightShadowData.x + (tmpvar_9 * (1.0 - _LightShadowData.x)));
  shadow_8 = tmpvar_10;
  lowp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_5;
  highp vec4 tmpvar_12;
  tmpvar_12.w = 1.0;
  tmpvar_12.xyz = xlv_TEXCOORD5;
  gl_FragData[0] = (((((glstate_lightmodel_ambient + ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * shadow_8)) * tmpvar_6.x) * tmpvar_11) * 2.0) + tmpvar_12);
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "SHADOWS_NATIVE" "VERTEXLIGHT_ON" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Color _glesColor
in vec4 _glesColor;
#define gl_Normal _glesNormal
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
#define TANGENT _glesTANGENT
in vec4 _glesTANGENT;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 349
highp vec3 ZShadeVertexLights( in highp vec4 vertex, in highp vec3 normal ) {
    highp vec3 worldN = (mat3( _Object2World) * normal);
    highp vec4 worldPos = (_Object2World * vertex);
    #line 353
    highp vec3 lightColor = vec3( 0.0, 0.0, 0.0);
    highp int index = 0;
    for ( ; (index < 4); (index++)) {
        #line 358
        highp vec3 lightPosition = vec3( unity_4LightPosX0[index], unity_4LightPosY0[index], unity_4LightPosZ0[index]);
        highp vec3 vertexToLightSource = (lightPosition - worldPos.xyz);
        highp vec3 lightDirection = normalize(vertexToLightSource);
        highp float squaredDistance = dot( vertexToLightSource, vertexToLightSource);
        #line 362
        highp float attenuation = (1.0 / (1.0 + (unity_4LightAtten0[index] * squaredDistance)));
        highp vec3 diffuseReflection = ((attenuation * vec3( unity_LightColor[index])) * max( 0.0, dot( worldN, lightDirection)));
        lightColor += diffuseReflection;
    }
    #line 366
    return (lightColor * 0.8);
}
#line 368
vertex2fragment vertShadow( in vertexInput v ) {
    #line 370
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    #line 374
    o.uvAB = (floor(((v.color * 128.0) + 0.5)) / 128.0);
    o.uvCD = (floor(((v.tangent * 128.0) + 0.5)) / 128.0);
    o.uvSh = v.texcoord.xy;
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    #line 379
    o.vertexLighting = ZShadeVertexLights( v.vertex, o.normal);
    return o;
}

out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
out highp vec4 xlv_TEXCOORD3;
out highp vec2 xlv_TEXCOORD4;
out highp vec3 xlv_TEXCOORD5;
out highp vec4 xlv_TEXCOORD6;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xlt_v.tangent = vec4(TANGENT);
    xlt_v.texcoord = vec4(gl_MultiTexCoord0);
    xlt_v.color = vec4(gl_Color);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval.uvAB);
    xlv_TEXCOORD3 = vec4(xl_retval.uvCD);
    xlv_TEXCOORD4 = vec2(xl_retval.uvSh);
    xlv_TEXCOORD5 = vec3(xl_retval.vertexLighting);
    xlv_TEXCOORD6 = vec4(xl_retval._ShadowCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_shadow2D(mediump sampler2DShadow s, vec3 coord) { return texture (s, coord); }
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 337
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 uvAB;
    highp vec4 uvCD;
    highp vec2 uvSh;
    highp vec3 vertexLighting;
    highp vec4 _ShadowCoord;
};
#line 328
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
    highp vec4 tangent;
    highp vec4 texcoord;
    highp vec4 color;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 323
uniform sampler2D _MainTex;
uniform mediump vec4 _MainTex_ST;
uniform sampler2D _ShdeTex;
uniform mediump vec4 _ShdeTex_ST;
#line 327
uniform highp vec4 _LightColor0;
#line 349
#line 317
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    #line 319
    lowp float shadow = xll_shadow2D( _ShadowMapTexture, shadowCoord.xyz.xyz);
    shadow = (_LightShadowData.x + (shadow * (1.0 - _LightShadowData.x)));
    return shadow;
}
#line 382
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 384
    lowp vec4 tex;
    lowp vec3 color = texture( _MainTex, i.uvAB.xy).xyz;
    tex = texture( _MainTex, i.uvAB.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    #line 388
    color *= tex.w;
    tex = texture( _MainTex, i.uvCD.xy);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    #line 392
    tex = texture( _MainTex, i.uvCD.zw);
    if ((((tex.x != 1.0) && (tex.y != 0.0)) && (tex.z != 1.0))){
        color = tex.xyz;
    }
    color *= tex.w;
    tex = texture( _ShdeTex, ((i.uvSh * _ShdeTex_ST.xy) + _ShdeTex_ST.zw));
    #line 396
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((((glstate_lightmodel_ambient + ((NdotL * _LightColor0) * unitySampleShadow( i._ShadowCoord))) * tex.x) * vec4( color, 1.0)) * 2.0) + vec4( i.vertexLighting, 1.0));
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
in highp vec4 xlv_TEXCOORD3;
in highp vec2 xlv_TEXCOORD4;
in highp vec3 xlv_TEXCOORD5;
in highp vec4 xlv_TEXCOORD6;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i.uvAB = vec4(xlv_TEXCOORD2);
    xlt_i.uvCD = vec4(xlv_TEXCOORD3);
    xlt_i.uvSh = vec2(xlv_TEXCOORD4);
    xlt_i.vertexLighting = vec3(xlv_TEXCOORD5);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD6);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

}
Program "fp" {
// Fragment combos: 6
//   opengl - ALU: 57 to 58, TEX: 5 to 6
//   d3d9 - ALU: 59 to 60, TEX: 5 to 6
SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
"!!ARBfp1.0
OPTION ARB_precision_hint_fastest;
# 57 ALU, 5 TEX
PARAM c[4] = { state.lightmodel.ambient,
		program.local[1..2],
		{ 0, 1, 2 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEX R2, fragment.texcoord[2].zwzw, texture[0], 2D;
TEX R0, fragment.texcoord[3].zwzw, texture[0], 2D;
TEX R1, fragment.texcoord[3], texture[0], 2D;
TEX R3.xyz, fragment.texcoord[2], texture[0], 2D;
MAD R4.xy, fragment.texcoord[4], c[1], c[1].zwzw;
ADD R3.w, R2.z, -c[3].y;
ABS R4.z, R2.y;
ABS R3.w, R3;
CMP R3.w, -R3, c[3].y, c[3].x;
CMP R4.z, -R4, c[3].y, c[3].x;
TEX R4.x, R4, texture[1], 2D;
ADD R4.y, R2.x, -c[3];
ABS R4.y, R4;
CMP R4.y, -R4, c[3], c[3].x;
MUL R4.y, R4, R4.z;
MUL R4.y, R4, R3.w;
CMP R2.xyz, -R4.y, R2, R3;
MUL R2.xyz, R2, R2.w;
ADD R3.w, R1.z, -c[3].y;
ABS R3.x, R3.w;
CMP R3.y, -R3.x, c[3], c[3].x;
ADD R2.w, R1.x, -c[3].y;
ABS R3.x, R1.y;
ABS R2.w, R2;
CMP R3.x, -R3, c[3].y, c[3];
CMP R2.w, -R2, c[3].y, c[3].x;
MUL R2.w, R2, R3.x;
MUL R2.w, R2, R3.y;
CMP R1.xyz, -R2.w, R1, R2;
MUL R3.xyz, R1, R1.w;
ADD R2.x, R0.z, -c[3].y;
ABS R1.w, R2.x;
DP3 R1.y, fragment.texcoord[1], fragment.texcoord[1];
RSQ R1.y, R1.y;
DP3 R1.x, fragment.texcoord[0], fragment.texcoord[0];
MUL R2.xyz, R1.y, fragment.texcoord[1];
RSQ R1.x, R1.x;
MUL R1.xyz, R1.x, fragment.texcoord[0];
DP3_SAT R1.x, R1, R2;
ADD R1.y, R0.x, -c[3];
ABS R1.z, R0.y;
ABS R1.y, R1;
CMP R1.w, -R1, c[3].y, c[3].x;
CMP R1.z, -R1, c[3].y, c[3].x;
CMP R1.y, -R1, c[3], c[3].x;
MUL R1.y, R1, R1.z;
MUL R1.y, R1, R1.w;
CMP R0.xyz, -R1.y, R0, R3;
MUL R0.xyz, R0, R0.w;
MUL R1, R1.x, c[2];
ADD R1, R1, c[0];
MOV R0.w, c[3].y;
MUL R1, R1, R4.x;
MUL R1, R1, R0;
MOV R0.w, c[3].y;
MOV R0.xyz, fragment.texcoord[5];
MAD result.color, R1, c[3].z, R0;
END
# 57 instructions, 5 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Vector 0 [glstate_lightmodel_ambient]
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
"ps_2_0
; 59 ALU, 5 TEX
dcl_2d s0
dcl_2d s1
def c3, -1.00000000, 0.00000000, 1.00000000, 2.00000000
dcl t0.xyz
dcl t1.xyz
dcl t2
dcl t3
dcl t4.xy
dcl t5.xyz
texld r6, t2, s0
texld r4, t3, s0
mov r0.y, t2.w
mov r0.x, t2.z
mov_pp r1.y, c1.w
mov_pp r1.x, c1.z
mad r2.xy, t4, c1, r1
mov r1.y, t3.w
mov r1.x, t3.z
mov r1.w, c3.z
texld r7, r2, s1
texld r5, r1, s0
texld r3, r0, s0
add_pp r0.x, r3.z, c3
add_pp r1.x, r3, c3
abs_pp r2.x, r3.y
abs_pp r0.x, r0
abs_pp r1.x, r1
cmp_pp r0.x, -r0, c3.y, c3.z
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
mul_pp r1.x, r1, r0
cmp_pp r1.xyz, -r1.x, r6, r3
mul_pp r3.xyz, r1, r3.w
add_pp r0.x, r4.z, c3
add_pp r1.x, r4, c3
abs_pp r2.x, r4.y
abs_pp r0.x, r0
abs_pp r1.x, r1
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
cmp_pp r0.x, -r0, c3.y, c3.z
mul_pp r0.x, r1, r0
cmp_pp r1.xyz, -r0.x, r3, r4
dp3 r2.x, t1, t1
rsq r2.x, r2.x
abs_pp r3.x, r5.y
add_pp r0.x, r5.z, c3
mul_pp r4.xyz, r1, r4.w
abs_pp r1.x, r0
dp3 r0.x, t0, t0
rsq r0.x, r0.x
mul r0.xyz, r0.x, t0
mul r2.xyz, r2.x, t1
dp3_sat r0.x, r0, r2
add_pp r2.x, r5, c3
abs_pp r2.x, r2
mul r0, r0.x, c2
cmp_pp r1.x, -r1, c3.y, c3.z
cmp_pp r3.x, -r3, c3.y, c3.z
cmp_pp r2.x, -r2, c3.y, c3.z
mul_pp r2.x, r2, r3
mul_pp r1.x, r2, r1
cmp_pp r1.xyz, -r1.x, r4, r5
mul_pp r1.xyz, r1, r5.w
add r0, r0, c0
mul r0, r0, r7.x
mul r1, r0, r1
mov r0.w, c3.z
mov r0.xyz, t5
mad r0, r1, c3.w, r0
mov oC0, r0
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES3"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
"!!ARBfp1.0
OPTION ARB_precision_hint_fastest;
# 57 ALU, 5 TEX
PARAM c[4] = { state.lightmodel.ambient,
		program.local[1..2],
		{ 0, 1, 2 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEX R2, fragment.texcoord[2].zwzw, texture[0], 2D;
TEX R0, fragment.texcoord[3].zwzw, texture[0], 2D;
TEX R1, fragment.texcoord[3], texture[0], 2D;
TEX R3.xyz, fragment.texcoord[2], texture[0], 2D;
MAD R4.xy, fragment.texcoord[4], c[1], c[1].zwzw;
ADD R3.w, R2.z, -c[3].y;
ABS R4.z, R2.y;
ABS R3.w, R3;
CMP R3.w, -R3, c[3].y, c[3].x;
CMP R4.z, -R4, c[3].y, c[3].x;
TEX R4.x, R4, texture[1], 2D;
ADD R4.y, R2.x, -c[3];
ABS R4.y, R4;
CMP R4.y, -R4, c[3], c[3].x;
MUL R4.y, R4, R4.z;
MUL R4.y, R4, R3.w;
CMP R2.xyz, -R4.y, R2, R3;
MUL R2.xyz, R2, R2.w;
ADD R3.w, R1.z, -c[3].y;
ABS R3.x, R3.w;
CMP R3.y, -R3.x, c[3], c[3].x;
ADD R2.w, R1.x, -c[3].y;
ABS R3.x, R1.y;
ABS R2.w, R2;
CMP R3.x, -R3, c[3].y, c[3];
CMP R2.w, -R2, c[3].y, c[3].x;
MUL R2.w, R2, R3.x;
MUL R2.w, R2, R3.y;
CMP R1.xyz, -R2.w, R1, R2;
MUL R3.xyz, R1, R1.w;
ADD R2.x, R0.z, -c[3].y;
ABS R1.w, R2.x;
DP3 R1.y, fragment.texcoord[1], fragment.texcoord[1];
RSQ R1.y, R1.y;
DP3 R1.x, fragment.texcoord[0], fragment.texcoord[0];
MUL R2.xyz, R1.y, fragment.texcoord[1];
RSQ R1.x, R1.x;
MUL R1.xyz, R1.x, fragment.texcoord[0];
DP3_SAT R1.x, R1, R2;
ADD R1.y, R0.x, -c[3];
ABS R1.z, R0.y;
ABS R1.y, R1;
CMP R1.w, -R1, c[3].y, c[3].x;
CMP R1.z, -R1, c[3].y, c[3].x;
CMP R1.y, -R1, c[3], c[3].x;
MUL R1.y, R1, R1.z;
MUL R1.y, R1, R1.w;
CMP R0.xyz, -R1.y, R0, R3;
MUL R0.xyz, R0, R0.w;
MUL R1, R1.x, c[2];
ADD R1, R1, c[0];
MOV R0.w, c[3].y;
MUL R1, R1, R4.x;
MUL R1, R1, R0;
MOV R0.w, c[3].y;
MOV R0.xyz, fragment.texcoord[5];
MAD result.color, R1, c[3].z, R0;
END
# 57 instructions, 5 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Vector 0 [glstate_lightmodel_ambient]
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
"ps_2_0
; 59 ALU, 5 TEX
dcl_2d s0
dcl_2d s1
def c3, -1.00000000, 0.00000000, 1.00000000, 2.00000000
dcl t0.xyz
dcl t1.xyz
dcl t2
dcl t3
dcl t4.xy
dcl t5.xyz
texld r6, t2, s0
texld r4, t3, s0
mov r0.y, t2.w
mov r0.x, t2.z
mov_pp r1.y, c1.w
mov_pp r1.x, c1.z
mad r2.xy, t4, c1, r1
mov r1.y, t3.w
mov r1.x, t3.z
mov r1.w, c3.z
texld r7, r2, s1
texld r5, r1, s0
texld r3, r0, s0
add_pp r0.x, r3.z, c3
add_pp r1.x, r3, c3
abs_pp r2.x, r3.y
abs_pp r0.x, r0
abs_pp r1.x, r1
cmp_pp r0.x, -r0, c3.y, c3.z
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
mul_pp r1.x, r1, r0
cmp_pp r1.xyz, -r1.x, r6, r3
mul_pp r3.xyz, r1, r3.w
add_pp r0.x, r4.z, c3
add_pp r1.x, r4, c3
abs_pp r2.x, r4.y
abs_pp r0.x, r0
abs_pp r1.x, r1
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
cmp_pp r0.x, -r0, c3.y, c3.z
mul_pp r0.x, r1, r0
cmp_pp r1.xyz, -r0.x, r3, r4
dp3 r2.x, t1, t1
rsq r2.x, r2.x
abs_pp r3.x, r5.y
add_pp r0.x, r5.z, c3
mul_pp r4.xyz, r1, r4.w
abs_pp r1.x, r0
dp3 r0.x, t0, t0
rsq r0.x, r0.x
mul r0.xyz, r0.x, t0
mul r2.xyz, r2.x, t1
dp3_sat r0.x, r0, r2
add_pp r2.x, r5, c3
abs_pp r2.x, r2
mul r0, r0.x, c2
cmp_pp r1.x, -r1, c3.y, c3.z
cmp_pp r3.x, -r3, c3.y, c3.z
cmp_pp r2.x, -r2, c3.y, c3.z
mul_pp r2.x, r2, r3
mul_pp r1.x, r2, r1
cmp_pp r1.xyz, -r1.x, r4, r5
mul_pp r1.xyz, r1, r5.w
add r0, r0, c0
mul r0, r0, r7.x
mul r1, r0, r1
mov r0.w, c3.z
mov r0.xyz, t5
mad r0, r1, c3.w, r0
mov oC0, r0
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES3"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_OFF" }
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
"!!ARBfp1.0
OPTION ARB_precision_hint_fastest;
# 57 ALU, 5 TEX
PARAM c[4] = { state.lightmodel.ambient,
		program.local[1..2],
		{ 0, 1, 2 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEX R2, fragment.texcoord[2].zwzw, texture[0], 2D;
TEX R0, fragment.texcoord[3].zwzw, texture[0], 2D;
TEX R1, fragment.texcoord[3], texture[0], 2D;
TEX R3.xyz, fragment.texcoord[2], texture[0], 2D;
MAD R4.xy, fragment.texcoord[4], c[1], c[1].zwzw;
ADD R3.w, R2.z, -c[3].y;
ABS R4.z, R2.y;
ABS R3.w, R3;
CMP R3.w, -R3, c[3].y, c[3].x;
CMP R4.z, -R4, c[3].y, c[3].x;
TEX R4.x, R4, texture[1], 2D;
ADD R4.y, R2.x, -c[3];
ABS R4.y, R4;
CMP R4.y, -R4, c[3], c[3].x;
MUL R4.y, R4, R4.z;
MUL R4.y, R4, R3.w;
CMP R2.xyz, -R4.y, R2, R3;
MUL R2.xyz, R2, R2.w;
ADD R3.w, R1.z, -c[3].y;
ABS R3.x, R3.w;
CMP R3.y, -R3.x, c[3], c[3].x;
ADD R2.w, R1.x, -c[3].y;
ABS R3.x, R1.y;
ABS R2.w, R2;
CMP R3.x, -R3, c[3].y, c[3];
CMP R2.w, -R2, c[3].y, c[3].x;
MUL R2.w, R2, R3.x;
MUL R2.w, R2, R3.y;
CMP R1.xyz, -R2.w, R1, R2;
MUL R3.xyz, R1, R1.w;
ADD R2.x, R0.z, -c[3].y;
ABS R1.w, R2.x;
DP3 R1.y, fragment.texcoord[1], fragment.texcoord[1];
RSQ R1.y, R1.y;
DP3 R1.x, fragment.texcoord[0], fragment.texcoord[0];
MUL R2.xyz, R1.y, fragment.texcoord[1];
RSQ R1.x, R1.x;
MUL R1.xyz, R1.x, fragment.texcoord[0];
DP3_SAT R1.x, R1, R2;
ADD R1.y, R0.x, -c[3];
ABS R1.z, R0.y;
ABS R1.y, R1;
CMP R1.w, -R1, c[3].y, c[3].x;
CMP R1.z, -R1, c[3].y, c[3].x;
CMP R1.y, -R1, c[3], c[3].x;
MUL R1.y, R1, R1.z;
MUL R1.y, R1, R1.w;
CMP R0.xyz, -R1.y, R0, R3;
MUL R0.xyz, R0, R0.w;
MUL R1, R1.x, c[2];
ADD R1, R1, c[0];
MOV R0.w, c[3].y;
MUL R1, R1, R4.x;
MUL R1, R1, R0;
MOV R0.w, c[3].y;
MOV R0.xyz, fragment.texcoord[5];
MAD result.color, R1, c[3].z, R0;
END
# 57 instructions, 5 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_OFF" }
Vector 0 [glstate_lightmodel_ambient]
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
"ps_2_0
; 59 ALU, 5 TEX
dcl_2d s0
dcl_2d s1
def c3, -1.00000000, 0.00000000, 1.00000000, 2.00000000
dcl t0.xyz
dcl t1.xyz
dcl t2
dcl t3
dcl t4.xy
dcl t5.xyz
texld r6, t2, s0
texld r4, t3, s0
mov r0.y, t2.w
mov r0.x, t2.z
mov_pp r1.y, c1.w
mov_pp r1.x, c1.z
mad r2.xy, t4, c1, r1
mov r1.y, t3.w
mov r1.x, t3.z
mov r1.w, c3.z
texld r7, r2, s1
texld r5, r1, s0
texld r3, r0, s0
add_pp r0.x, r3.z, c3
add_pp r1.x, r3, c3
abs_pp r2.x, r3.y
abs_pp r0.x, r0
abs_pp r1.x, r1
cmp_pp r0.x, -r0, c3.y, c3.z
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
mul_pp r1.x, r1, r0
cmp_pp r1.xyz, -r1.x, r6, r3
mul_pp r3.xyz, r1, r3.w
add_pp r0.x, r4.z, c3
add_pp r1.x, r4, c3
abs_pp r2.x, r4.y
abs_pp r0.x, r0
abs_pp r1.x, r1
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
cmp_pp r0.x, -r0, c3.y, c3.z
mul_pp r0.x, r1, r0
cmp_pp r1.xyz, -r0.x, r3, r4
dp3 r2.x, t1, t1
rsq r2.x, r2.x
abs_pp r3.x, r5.y
add_pp r0.x, r5.z, c3
mul_pp r4.xyz, r1, r4.w
abs_pp r1.x, r0
dp3 r0.x, t0, t0
rsq r0.x, r0.x
mul r0.xyz, r0.x, t0
mul r2.xyz, r2.x, t1
dp3_sat r0.x, r0, r2
add_pp r2.x, r5, c3
abs_pp r2.x, r2
mul r0, r0.x, c2
cmp_pp r1.x, -r1, c3.y, c3.z
cmp_pp r3.x, -r3, c3.y, c3.z
cmp_pp r2.x, -r2, c3.y, c3.z
mul_pp r2.x, r2, r3
mul_pp r1.x, r2, r1
cmp_pp r1.xyz, -r1.x, r4, r5
mul_pp r1.xyz, r1, r5.w
add r0, r0, c0
mul r0, r0, r7.x
mul r1, r0, r1
mov r0.w, c3.z
mov r0.xyz, t5
mad r0, r1, c3.w, r0
mov oC0, r0
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_OFF" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_OFF" }
"!!GLES"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_OFF" }
"!!GLES3"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
SetTexture 2 [_ShadowMapTexture] 2D
"!!ARBfp1.0
OPTION ARB_precision_hint_fastest;
# 58 ALU, 6 TEX
PARAM c[4] = { state.lightmodel.ambient,
		program.local[1..2],
		{ 0, 1, 2 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
TEX R4, fragment.texcoord[2].zwzw, texture[0], 2D;
TEX R2, fragment.texcoord[3].zwzw, texture[0], 2D;
TEX R3, fragment.texcoord[3], texture[0], 2D;
TEX R5.xyz, fragment.texcoord[2], texture[0], 2D;
TXP R1.x, fragment.texcoord[6], texture[2], 2D;
MAD R0.xy, fragment.texcoord[4], c[1], c[1].zwzw;
ABS R0.w, R4.y;
CMP R0.w, -R0, c[3].y, c[3].x;
TEX R0.x, R0, texture[1], 2D;
ADD R0.y, R4.z, -c[3];
ABS R0.y, R0;
CMP R0.z, -R0.y, c[3].y, c[3].x;
ADD R0.y, R4.x, -c[3];
ABS R0.y, R0;
CMP R0.y, -R0, c[3], c[3].x;
MUL R0.y, R0, R0.w;
MUL R0.z, R0.y, R0;
CMP R4.xyz, -R0.z, R4, R5;
ADD R0.y, R3.z, -c[3];
ABS R0.y, R0;
CMP R0.w, -R0.y, c[3].y, c[3].x;
ADD R0.y, R3.x, -c[3];
ABS R0.z, R3.y;
ABS R0.y, R0;
MUL R4.xyz, R4, R4.w;
CMP R0.z, -R0, c[3].y, c[3].x;
CMP R0.y, -R0, c[3], c[3].x;
MUL R0.y, R0, R0.z;
MUL R0.y, R0, R0.w;
CMP R3.xyz, -R0.y, R3, R4;
MUL R3.xyz, R3, R3.w;
ADD R0.z, R2, -c[3].y;
ABS R0.y, R0.z;
CMP R0.w, -R0.y, c[3].y, c[3].x;
ADD R0.y, R2.x, -c[3];
ABS R0.z, R2.y;
ABS R0.y, R0;
CMP R0.z, -R0, c[3].y, c[3].x;
CMP R0.y, -R0, c[3], c[3].x;
MUL R0.y, R0, R0.z;
MUL R0.y, R0, R0.w;
CMP R4.xyz, -R0.y, R2, R3;
DP3 R0.z, fragment.texcoord[1], fragment.texcoord[1];
RSQ R0.z, R0.z;
DP3 R0.y, fragment.texcoord[0], fragment.texcoord[0];
RSQ R0.y, R0.y;
MUL R3.xyz, R0.z, fragment.texcoord[1];
MUL R2.xyz, R0.y, fragment.texcoord[0];
DP3_SAT R0.y, R2, R3;
MUL R3.xyz, R4, R2.w;
MUL R2, R0.y, c[2];
MAD R1, R2, R1.x, c[0];
MUL R0, R1, R0.x;
MOV R3.w, c[3].y;
MUL R1, R0, R3;
MOV R0.w, c[3].y;
MOV R0.xyz, fragment.texcoord[5];
MAD result.color, R1, c[3].z, R0;
END
# 58 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Vector 0 [glstate_lightmodel_ambient]
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
SetTexture 2 [_ShadowMapTexture] 2D
"ps_2_0
; 60 ALU, 6 TEX
dcl_2d s0
dcl_2d s1
dcl_2d s2
def c3, -1.00000000, 0.00000000, 1.00000000, 2.00000000
dcl t0.xyz
dcl t1.xyz
dcl t2
dcl t3
dcl t4.xy
dcl t5.xyz
dcl t6
texld r4, t3, s0
texld r7, t2, s0
mov r0.y, t2.w
mov r0.x, t2.z
mov r1.y, t3.w
mov r1.x, t3.z
mov r2.xy, r1
mov_pp r1.y, c1.w
mov_pp r1.x, c1.z
mad r1.xy, t4, c1, r1
texld r6, r1, s1
texld r3, r0, s0
texld r5, r2, s0
texldp r2, t6, s2
dp3 r1.x, t1, t1
dp3 r0.x, t0, t0
rsq r1.x, r1.x
rsq r0.x, r0.x
mul r0.xyz, r0.x, t0
mul r1.xyz, r1.x, t1
dp3_sat r0.x, r0, r1
mul r0, r0.x, c2
mad r1, r0, r2.x, c0
add_pp r0.x, r3.z, c3
abs_pp r2.x, r3.y
mul r6, r1, r6.x
add_pp r1.x, r3, c3
abs_pp r0.x, r0
abs_pp r1.x, r1
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
cmp_pp r0.x, -r0, c3.y, c3.z
mul_pp r0.x, r1, r0
cmp_pp r1.xyz, -r0.x, r7, r3
mul_pp r3.xyz, r1, r3.w
add_pp r0.x, r4.z, c3
abs_pp r0.x, r0
add_pp r1.x, r4, c3
abs_pp r2.x, r4.y
abs_pp r1.x, r1
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
cmp_pp r0.x, -r0, c3.y, c3.z
mul_pp r1.x, r1, r0
cmp_pp r1.xyz, -r1.x, r3, r4
mul_pp r3.xyz, r1, r4.w
add_pp r0.x, r5.z, c3
abs_pp r0.x, r0
add_pp r1.x, r5, c3
abs_pp r2.x, r5.y
abs_pp r1.x, r1
mov r0.w, c3.z
cmp_pp r0.x, -r0, c3.y, c3.z
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
mul_pp r0.x, r1, r0
cmp_pp r0.xyz, -r0.x, r3, r5
mul_pp r0.xyz, r0, r5.w
mul r1, r6, r0
mov r0.w, c3.z
mov r0.xyz, t5
mad r0, r1, c3.w, r0
mov oC0, r0
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES3"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
SetTexture 2 [_ShadowMapTexture] 2D
"!!ARBfp1.0
OPTION ARB_precision_hint_fastest;
# 58 ALU, 6 TEX
PARAM c[4] = { state.lightmodel.ambient,
		program.local[1..2],
		{ 0, 1, 2 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
TEX R4, fragment.texcoord[2].zwzw, texture[0], 2D;
TEX R2, fragment.texcoord[3].zwzw, texture[0], 2D;
TEX R3, fragment.texcoord[3], texture[0], 2D;
TEX R5.xyz, fragment.texcoord[2], texture[0], 2D;
TXP R1.x, fragment.texcoord[6], texture[2], 2D;
MAD R0.xy, fragment.texcoord[4], c[1], c[1].zwzw;
ABS R0.w, R4.y;
CMP R0.w, -R0, c[3].y, c[3].x;
TEX R0.x, R0, texture[1], 2D;
ADD R0.y, R4.z, -c[3];
ABS R0.y, R0;
CMP R0.z, -R0.y, c[3].y, c[3].x;
ADD R0.y, R4.x, -c[3];
ABS R0.y, R0;
CMP R0.y, -R0, c[3], c[3].x;
MUL R0.y, R0, R0.w;
MUL R0.z, R0.y, R0;
CMP R4.xyz, -R0.z, R4, R5;
ADD R0.y, R3.z, -c[3];
ABS R0.y, R0;
CMP R0.w, -R0.y, c[3].y, c[3].x;
ADD R0.y, R3.x, -c[3];
ABS R0.z, R3.y;
ABS R0.y, R0;
MUL R4.xyz, R4, R4.w;
CMP R0.z, -R0, c[3].y, c[3].x;
CMP R0.y, -R0, c[3], c[3].x;
MUL R0.y, R0, R0.z;
MUL R0.y, R0, R0.w;
CMP R3.xyz, -R0.y, R3, R4;
MUL R3.xyz, R3, R3.w;
ADD R0.z, R2, -c[3].y;
ABS R0.y, R0.z;
CMP R0.w, -R0.y, c[3].y, c[3].x;
ADD R0.y, R2.x, -c[3];
ABS R0.z, R2.y;
ABS R0.y, R0;
CMP R0.z, -R0, c[3].y, c[3].x;
CMP R0.y, -R0, c[3], c[3].x;
MUL R0.y, R0, R0.z;
MUL R0.y, R0, R0.w;
CMP R4.xyz, -R0.y, R2, R3;
DP3 R0.z, fragment.texcoord[1], fragment.texcoord[1];
RSQ R0.z, R0.z;
DP3 R0.y, fragment.texcoord[0], fragment.texcoord[0];
RSQ R0.y, R0.y;
MUL R3.xyz, R0.z, fragment.texcoord[1];
MUL R2.xyz, R0.y, fragment.texcoord[0];
DP3_SAT R0.y, R2, R3;
MUL R3.xyz, R4, R2.w;
MUL R2, R0.y, c[2];
MAD R1, R2, R1.x, c[0];
MUL R0, R1, R0.x;
MOV R3.w, c[3].y;
MUL R1, R0, R3;
MOV R0.w, c[3].y;
MOV R0.xyz, fragment.texcoord[5];
MAD result.color, R1, c[3].z, R0;
END
# 58 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Vector 0 [glstate_lightmodel_ambient]
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
SetTexture 2 [_ShadowMapTexture] 2D
"ps_2_0
; 60 ALU, 6 TEX
dcl_2d s0
dcl_2d s1
dcl_2d s2
def c3, -1.00000000, 0.00000000, 1.00000000, 2.00000000
dcl t0.xyz
dcl t1.xyz
dcl t2
dcl t3
dcl t4.xy
dcl t5.xyz
dcl t6
texld r4, t3, s0
texld r7, t2, s0
mov r0.y, t2.w
mov r0.x, t2.z
mov r1.y, t3.w
mov r1.x, t3.z
mov r2.xy, r1
mov_pp r1.y, c1.w
mov_pp r1.x, c1.z
mad r1.xy, t4, c1, r1
texld r6, r1, s1
texld r3, r0, s0
texld r5, r2, s0
texldp r2, t6, s2
dp3 r1.x, t1, t1
dp3 r0.x, t0, t0
rsq r1.x, r1.x
rsq r0.x, r0.x
mul r0.xyz, r0.x, t0
mul r1.xyz, r1.x, t1
dp3_sat r0.x, r0, r1
mul r0, r0.x, c2
mad r1, r0, r2.x, c0
add_pp r0.x, r3.z, c3
abs_pp r2.x, r3.y
mul r6, r1, r6.x
add_pp r1.x, r3, c3
abs_pp r0.x, r0
abs_pp r1.x, r1
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
cmp_pp r0.x, -r0, c3.y, c3.z
mul_pp r0.x, r1, r0
cmp_pp r1.xyz, -r0.x, r7, r3
mul_pp r3.xyz, r1, r3.w
add_pp r0.x, r4.z, c3
abs_pp r0.x, r0
add_pp r1.x, r4, c3
abs_pp r2.x, r4.y
abs_pp r1.x, r1
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
cmp_pp r0.x, -r0, c3.y, c3.z
mul_pp r1.x, r1, r0
cmp_pp r1.xyz, -r1.x, r3, r4
mul_pp r3.xyz, r1, r4.w
add_pp r0.x, r5.z, c3
abs_pp r0.x, r0
add_pp r1.x, r5, c3
abs_pp r2.x, r5.y
abs_pp r1.x, r1
mov r0.w, c3.z
cmp_pp r0.x, -r0, c3.y, c3.z
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
mul_pp r0.x, r1, r0
cmp_pp r0.xyz, -r0.x, r3, r5
mul_pp r0.xyz, r0, r5.w
mul r1, r6, r0
mov r0.w, c3.z
mov r0.xyz, t5
mad r0, r1, c3.w, r0
mov oC0, r0
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES3"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" }
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
SetTexture 2 [_ShadowMapTexture] 2D
"!!ARBfp1.0
OPTION ARB_precision_hint_fastest;
# 58 ALU, 6 TEX
PARAM c[4] = { state.lightmodel.ambient,
		program.local[1..2],
		{ 0, 1, 2 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
TEX R4, fragment.texcoord[2].zwzw, texture[0], 2D;
TEX R2, fragment.texcoord[3].zwzw, texture[0], 2D;
TEX R3, fragment.texcoord[3], texture[0], 2D;
TEX R5.xyz, fragment.texcoord[2], texture[0], 2D;
TXP R1.x, fragment.texcoord[6], texture[2], 2D;
MAD R0.xy, fragment.texcoord[4], c[1], c[1].zwzw;
ABS R0.w, R4.y;
CMP R0.w, -R0, c[3].y, c[3].x;
TEX R0.x, R0, texture[1], 2D;
ADD R0.y, R4.z, -c[3];
ABS R0.y, R0;
CMP R0.z, -R0.y, c[3].y, c[3].x;
ADD R0.y, R4.x, -c[3];
ABS R0.y, R0;
CMP R0.y, -R0, c[3], c[3].x;
MUL R0.y, R0, R0.w;
MUL R0.z, R0.y, R0;
CMP R4.xyz, -R0.z, R4, R5;
ADD R0.y, R3.z, -c[3];
ABS R0.y, R0;
CMP R0.w, -R0.y, c[3].y, c[3].x;
ADD R0.y, R3.x, -c[3];
ABS R0.z, R3.y;
ABS R0.y, R0;
MUL R4.xyz, R4, R4.w;
CMP R0.z, -R0, c[3].y, c[3].x;
CMP R0.y, -R0, c[3], c[3].x;
MUL R0.y, R0, R0.z;
MUL R0.y, R0, R0.w;
CMP R3.xyz, -R0.y, R3, R4;
MUL R3.xyz, R3, R3.w;
ADD R0.z, R2, -c[3].y;
ABS R0.y, R0.z;
CMP R0.w, -R0.y, c[3].y, c[3].x;
ADD R0.y, R2.x, -c[3];
ABS R0.z, R2.y;
ABS R0.y, R0;
CMP R0.z, -R0, c[3].y, c[3].x;
CMP R0.y, -R0, c[3], c[3].x;
MUL R0.y, R0, R0.z;
MUL R0.y, R0, R0.w;
CMP R4.xyz, -R0.y, R2, R3;
DP3 R0.z, fragment.texcoord[1], fragment.texcoord[1];
RSQ R0.z, R0.z;
DP3 R0.y, fragment.texcoord[0], fragment.texcoord[0];
RSQ R0.y, R0.y;
MUL R3.xyz, R0.z, fragment.texcoord[1];
MUL R2.xyz, R0.y, fragment.texcoord[0];
DP3_SAT R0.y, R2, R3;
MUL R3.xyz, R4, R2.w;
MUL R2, R0.y, c[2];
MAD R1, R2, R1.x, c[0];
MUL R0, R1, R0.x;
MOV R3.w, c[3].y;
MUL R1, R0, R3;
MOV R0.w, c[3].y;
MOV R0.xyz, fragment.texcoord[5];
MAD result.color, R1, c[3].z, R0;
END
# 58 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" }
Vector 0 [glstate_lightmodel_ambient]
Vector 1 [_ShdeTex_ST]
Vector 2 [_LightColor0]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_ShdeTex] 2D
SetTexture 2 [_ShadowMapTexture] 2D
"ps_2_0
; 60 ALU, 6 TEX
dcl_2d s0
dcl_2d s1
dcl_2d s2
def c3, -1.00000000, 0.00000000, 1.00000000, 2.00000000
dcl t0.xyz
dcl t1.xyz
dcl t2
dcl t3
dcl t4.xy
dcl t5.xyz
dcl t6
texld r4, t3, s0
texld r7, t2, s0
mov r0.y, t2.w
mov r0.x, t2.z
mov r1.y, t3.w
mov r1.x, t3.z
mov r2.xy, r1
mov_pp r1.y, c1.w
mov_pp r1.x, c1.z
mad r1.xy, t4, c1, r1
texld r6, r1, s1
texld r3, r0, s0
texld r5, r2, s0
texldp r2, t6, s2
dp3 r1.x, t1, t1
dp3 r0.x, t0, t0
rsq r1.x, r1.x
rsq r0.x, r0.x
mul r0.xyz, r0.x, t0
mul r1.xyz, r1.x, t1
dp3_sat r0.x, r0, r1
mul r0, r0.x, c2
mad r1, r0, r2.x, c0
add_pp r0.x, r3.z, c3
abs_pp r2.x, r3.y
mul r6, r1, r6.x
add_pp r1.x, r3, c3
abs_pp r0.x, r0
abs_pp r1.x, r1
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
cmp_pp r0.x, -r0, c3.y, c3.z
mul_pp r0.x, r1, r0
cmp_pp r1.xyz, -r0.x, r7, r3
mul_pp r3.xyz, r1, r3.w
add_pp r0.x, r4.z, c3
abs_pp r0.x, r0
add_pp r1.x, r4, c3
abs_pp r2.x, r4.y
abs_pp r1.x, r1
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
cmp_pp r0.x, -r0, c3.y, c3.z
mul_pp r1.x, r1, r0
cmp_pp r1.xyz, -r1.x, r3, r4
mul_pp r3.xyz, r1, r4.w
add_pp r0.x, r5.z, c3
abs_pp r0.x, r0
add_pp r1.x, r5, c3
abs_pp r2.x, r5.y
abs_pp r1.x, r1
mov r0.w, c3.z
cmp_pp r0.x, -r0, c3.y, c3.z
cmp_pp r2.x, -r2, c3.y, c3.z
cmp_pp r1.x, -r1, c3.y, c3.z
mul_pp r1.x, r1, r2
mul_pp r0.x, r1, r0
cmp_pp r0.xyz, -r0.x, r3, r5
mul_pp r0.xyz, r0, r5.w
mul r1, r6, r0
mov r0.w, c3.z
mov r0.xyz, t5
mad r0, r1, c3.w, r0
mov oC0, r0
"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" }
"!!GLES"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_ON" "DIRLIGHTMAP_ON" "SHADOWS_SCREEN" }
"!!GLES3"
}

}

#LINE 125

		}
		
		pass {		
			Tags { "LightMode"="ForwardAdd"}
			Fog {Mode Off}
			BlenD DstColor One
			
			Program "vp" {
// Vertex combos: 5
//   opengl - ALU: 11 to 20
//   d3d9 - ALU: 13 to 22
//   d3d11 - ALU: 11 to 20, TEX: 0 to 0, FLOW: 1 to 1
//   d3d11_9x - ALU: 11 to 20, TEX: 0 to 0, FLOW: 1 to 1
SubProgram "opengl " {
Keywords { "POINT" }
Bind "vertex" Vertex
Bind "normal" Normal
Vector 17 [_WorldSpaceLightPos0]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
Vector 18 [unity_Scale]
Matrix 13 [_LightMatrix0]
"!!ARBvp1.0
# 19 ALU
PARAM c[19] = { program.local[0],
		state.matrix.mvp,
		program.local[5..18] };
TEMP R0;
TEMP R1;
MOV R1, c[17];
DP4 R0.z, R1, c[11];
DP4 R0.x, R1, c[9];
DP4 R0.y, R1, c[10];
MAD result.texcoord[1].xyz, R0, c[18].w, -vertex.position;
DP3 R1.x, vertex.normal, vertex.normal;
RSQ R1.x, R1.x;
DP4 R0.w, vertex.position, c[8];
DP4 R0.z, vertex.position, c[7];
DP4 R0.x, vertex.position, c[5];
DP4 R0.y, vertex.position, c[6];
DP4 result.texcoord[2].z, R0, c[15];
DP4 result.texcoord[2].y, R0, c[14];
DP4 result.texcoord[2].x, R0, c[13];
MUL result.texcoord[0].xyz, R1.x, vertex.normal;
DP4 result.position.w, vertex.position, c[4];
DP4 result.position.z, vertex.position, c[3];
DP4 result.position.y, vertex.position, c[2];
DP4 result.position.x, vertex.position, c[1];
END
# 19 instructions, 2 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "POINT" }
Bind "vertex" Vertex
Bind "normal" Normal
Matrix 0 [glstate_matrix_mvp]
Vector 16 [_WorldSpaceLightPos0]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
Vector 17 [unity_Scale]
Matrix 12 [_LightMatrix0]
"vs_2_0
; 21 ALU
dcl_position0 v0
dcl_normal0 v1
mov r0, c10
dp4 r2.z, c16, r0
mov r0, c9
dp4 r2.y, c16, r0
mov r1, c8
dp4 r2.x, c16, r1
dp3 r1.x, v1, v1
rsq r1.x, r1.x
dp4 r0.w, v0, c7
dp4 r0.z, v0, c6
dp4 r0.x, v0, c4
dp4 r0.y, v0, c5
mad oT1.xyz, r2, c17.w, -v0
dp4 oT2.z, r0, c14
dp4 oT2.y, r0, c13
dp4 oT2.x, r0, c12
mul oT0.xyz, r1.x, v1
dp4 oPos.w, v0, c3
dp4 oPos.z, v0, c2
dp4 oPos.y, v0, c1
dp4 oPos.x, v0, c0
"
}

SubProgram "d3d11 " {
Keywords { "POINT" }
Bind "vertex" Vertex
Bind "normal" Normal
ConstBuffer "$Globals" 96 // 80 used size, 3 vars
Matrix 16 [_LightMatrix0] 4
ConstBuffer "UnityLighting" 720 // 16 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
ConstBuffer "UnityPerDraw" 336 // 336 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
Matrix 256 [_World2Object] 4
Vector 320 [unity_Scale] 4
BindCB "$Globals" 0
BindCB "UnityLighting" 1
BindCB "UnityPerDraw" 2
// 21 instructions, 2 temp regs, 0 temp arrays:
// ALU 20 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0
eefiecediejnfnlnnibnmjldodabjlgijddeajhdabaaaaaaieaeaaaaadaaaaaa
cmaaaaaahmaaaaaaaeabaaaaejfdeheoeiaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaaebaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaafaepfdejfeejepeoaaeoepfcenebemaaepfdeheo
iaaaaaaaaeaaaaaaaiaaaaaagiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaaheaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahaiaaaaheaaaaaa
abaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaheaaaaaaacaaaaaaaaaaaaaa
adaaaaaaadaaaaaaahaiaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfcee
aaklklklfdeieefchiadaaaaeaaaabaanoaaaaaafjaaaaaeegiocaaaaaaaaaaa
afaaaaaafjaaaaaeegiocaaaabaaaaaaabaaaaaafjaaaaaeegiocaaaacaaaaaa
bfaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaadhcbabaaaabaaaaaaghaaaaae
pccabaaaaaaaaaaaabaaaaaagfaaaaadhccabaaaabaaaaaagfaaaaadhccabaaa
acaaaaaagfaaaaadhccabaaaadaaaaaagiaaaaacacaaaaaadiaaaaaipcaabaaa
aaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaaabaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaacaaaaaaaaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaacaaaaaakgbkbaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpccabaaaaaaaaaaaegiocaaaacaaaaaaadaaaaaa
pgbpbaaaaaaaaaaaegaobaaaaaaaaaaabaaaaaahbcaabaaaaaaaaaaaegbcbaaa
abaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaa
diaaaaahhccabaaaabaaaaaaagaabaaaaaaaaaaaegbcbaaaabaaaaaadiaaaaaj
hcaabaaaaaaaaaaafgifcaaaabaaaaaaaaaaaaaaegiccaaaacaaaaaabbaaaaaa
dcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaabaaaaaaaagiacaaaabaaaaaa
aaaaaaaaegacbaaaaaaaaaaadcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaa
bcaaaaaakgikcaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaadcaaaaalhcaabaaa
aaaaaaaaegiccaaaacaaaaaabdaaaaaapgipcaaaabaaaaaaaaaaaaaaegacbaaa
aaaaaaaadcaaaaalhccabaaaacaaaaaaegacbaaaaaaaaaaapgipcaaaacaaaaaa
beaaaaaaegbcbaiaebaaaaaaaaaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaa
aaaaaaaaegiocaaaacaaaaaaanaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaa
acaaaaaaamaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaacaaaaaaaoaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaapaaaaaapgbpbaaaaaaaaaaa
egaobaaaaaaaaaaadiaaaaaihcaabaaaabaaaaaafgafbaaaaaaaaaaaegiccaaa
aaaaaaaaacaaaaaadcaaaaakhcaabaaaabaaaaaaegiccaaaaaaaaaaaabaaaaaa
agaabaaaaaaaaaaaegacbaaaabaaaaaadcaaaaakhcaabaaaaaaaaaaaegiccaaa
aaaaaaaaadaaaaaakgakbaaaaaaaaaaaegacbaaaabaaaaaadcaaaaakhccabaaa
adaaaaaaegiccaaaaaaaaaaaaeaaaaaapgapbaaaaaaaaaaaegacbaaaaaaaaaaa
doaaaaab"
}

SubProgram "gles " {
Keywords { "POINT" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _LightMatrix0;
uniform highp vec4 unity_Scale;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 _WorldSpaceLightPos0;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = normalize(_glesNormal);
  xlv_TEXCOORD1 = (((_World2Object * _WorldSpaceLightPos0).xyz * unity_Scale.w) - _glesVertex.xyz);
  xlv_TEXCOORD2 = (_LightMatrix0 * (_Object2World * _glesVertex)).xyz;
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform sampler2D _LightTexture0;
void main ()
{
  highp float tmpvar_1;
  tmpvar_1 = dot (xlv_TEXCOORD2, xlv_TEXCOORD2);
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_LightTexture0, vec2(tmpvar_1));
  gl_FragData[0] = (((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_2.w) * 2.0);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "POINT" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _LightMatrix0;
uniform highp vec4 unity_Scale;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 _WorldSpaceLightPos0;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = normalize(_glesNormal);
  xlv_TEXCOORD1 = (((_World2Object * _WorldSpaceLightPos0).xyz * unity_Scale.w) - _glesVertex.xyz);
  xlv_TEXCOORD2 = (_LightMatrix0 * (_Object2World * _glesVertex)).xyz;
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform sampler2D _LightTexture0;
void main ()
{
  highp float tmpvar_1;
  tmpvar_1 = dot (xlv_TEXCOORD2, xlv_TEXCOORD2);
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_LightTexture0, vec2(tmpvar_1));
  gl_FragData[0] = (((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_2.w) * 2.0);
}



#endif"
}

SubProgram "d3d11_9x " {
Keywords { "POINT" }
Bind "vertex" Vertex
Bind "normal" Normal
ConstBuffer "$Globals" 96 // 80 used size, 3 vars
Matrix 16 [_LightMatrix0] 4
ConstBuffer "UnityLighting" 720 // 16 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
ConstBuffer "UnityPerDraw" 336 // 336 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
Matrix 256 [_World2Object] 4
Vector 320 [unity_Scale] 4
BindCB "$Globals" 0
BindCB "UnityLighting" 1
BindCB "UnityPerDraw" 2
// 21 instructions, 2 temp regs, 0 temp arrays:
// ALU 20 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0_level_9_1
eefiecedncpkemgbjdhkgnblejjjjgjbmdgonjbbabaaaaaakeagaaaaaeaaaaaa
daaaaaaaemacaaaammafaaaabmagaaaaebgpgodjbeacaaaabeacaaaaaaacpopp
lmabaaaafiaaaaaaaeaaceaaaaaafeaaaaaafeaaaaaaceaaabaafeaaaaaaabaa
aeaaabaaaaaaaaaaabaaaaaaabaaafaaaaaaaaaaacaaaaaaaeaaagaaaaaaaaaa
acaaamaaajaaakaaaaaaaaaaaaaaaaaaaaacpoppbpaaaaacafaaaaiaaaaaapja
bpaaaaacafaaabiaabaaapjaaiaaaaadaaaaabiaabaaoejaabaaoejaahaaaaac
aaaaabiaaaaaaaiaafaaaaadaaaaahoaaaaaaaiaabaaoejaabaaaaacaaaaapia
afaaoekaafaaaaadabaaahiaaaaaffiaapaaoekaaeaaaaaeabaaahiaaoaaoeka
aaaaaaiaabaaoeiaaeaaaaaeaaaaahiabaaaoekaaaaakkiaabaaoeiaaeaaaaae
aaaaahiabbaaoekaaaaappiaaaaaoeiaaeaaaaaeabaaahoaaaaaoeiabcaappka
aaaaoejbafaaaaadaaaaapiaaaaaffjaalaaoekaaeaaaaaeaaaaapiaakaaoeka
aaaaaajaaaaaoeiaaeaaaaaeaaaaapiaamaaoekaaaaakkjaaaaaoeiaaeaaaaae
aaaaapiaanaaoekaaaaappjaaaaaoeiaafaaaaadabaaahiaaaaaffiaacaaoeka
aeaaaaaeabaaahiaabaaoekaaaaaaaiaabaaoeiaaeaaaaaeaaaaahiaadaaoeka
aaaakkiaabaaoeiaaeaaaaaeacaaahoaaeaaoekaaaaappiaaaaaoeiaafaaaaad
aaaaapiaaaaaffjaahaaoekaaeaaaaaeaaaaapiaagaaoekaaaaaaajaaaaaoeia
aeaaaaaeaaaaapiaaiaaoekaaaaakkjaaaaaoeiaaeaaaaaeaaaaapiaajaaoeka
aaaappjaaaaaoeiaaeaaaaaeaaaaadmaaaaappiaaaaaoekaaaaaoeiaabaaaaac
aaaaammaaaaaoeiappppaaaafdeieefchiadaaaaeaaaabaanoaaaaaafjaaaaae
egiocaaaaaaaaaaaafaaaaaafjaaaaaeegiocaaaabaaaaaaabaaaaaafjaaaaae
egiocaaaacaaaaaabfaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaadhcbabaaa
abaaaaaaghaaaaaepccabaaaaaaaaaaaabaaaaaagfaaaaadhccabaaaabaaaaaa
gfaaaaadhccabaaaacaaaaaagfaaaaadhccabaaaadaaaaaagiaaaaacacaaaaaa
diaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaaabaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaaaaaaaaaagbabaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaacaaaaaa
kgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpccabaaaaaaaaaaaegiocaaa
acaaaaaaadaaaaaapgbpbaaaaaaaaaaaegaobaaaaaaaaaaabaaaaaahbcaabaaa
aaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaaaaaaaaaa
akaabaaaaaaaaaaadiaaaaahhccabaaaabaaaaaaagaabaaaaaaaaaaaegbcbaaa
abaaaaaadiaaaaajhcaabaaaaaaaaaaafgifcaaaabaaaaaaaaaaaaaaegiccaaa
acaaaaaabbaaaaaadcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaabaaaaaaa
agiacaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaadcaaaaalhcaabaaaaaaaaaaa
egiccaaaacaaaaaabcaaaaaakgikcaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaa
dcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaabdaaaaaapgipcaaaabaaaaaa
aaaaaaaaegacbaaaaaaaaaaadcaaaaalhccabaaaacaaaaaaegacbaaaaaaaaaaa
pgipcaaaacaaaaaabeaaaaaaegbcbaiaebaaaaaaaaaaaaaadiaaaaaipcaabaaa
aaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaaanaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaacaaaaaaamaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaaoaaaaaakgbkbaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaapaaaaaa
pgbpbaaaaaaaaaaaegaobaaaaaaaaaaadiaaaaaihcaabaaaabaaaaaafgafbaaa
aaaaaaaaegiccaaaaaaaaaaaacaaaaaadcaaaaakhcaabaaaabaaaaaaegiccaaa
aaaaaaaaabaaaaaaagaabaaaaaaaaaaaegacbaaaabaaaaaadcaaaaakhcaabaaa
aaaaaaaaegiccaaaaaaaaaaaadaaaaaakgakbaaaaaaaaaaaegacbaaaabaaaaaa
dcaaaaakhccabaaaadaaaaaaegiccaaaaaaaaaaaaeaaaaaapgapbaaaaaaaaaaa
egacbaaaaaaaaaaadoaaaaabejfdeheoeiaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaaebaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaafaepfdejfeejepeoaaeoepfcenebemaaepfdeheo
iaaaaaaaaeaaaaaaaiaaaaaagiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaaheaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahaiaaaaheaaaaaa
abaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaheaaaaaaacaaaaaaaaaaaaaa
adaaaaaaadaaaaaaahaiaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfcee
aaklklkl"
}

SubProgram "gles3 " {
Keywords { "POINT" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal _glesNormal
in vec3 _glesNormal;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 324
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec3 _LightCoord;
};
#line 318
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform highp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _LightTexture0;
uniform highp mat4 _LightMatrix0;
uniform highp vec4 _LightColor0;
#line 332
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return ((objSpaceLightPos.xyz * unity_Scale.w) - v.xyz);
}
#line 332
vertex2fragment vertShadow( in vertexInput v ) {
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    #line 336
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    o._LightCoord = (_LightMatrix0 * (_Object2World * v.vertex)).xyz;
    #line 340
    return o;
}
out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec3 xlv_TEXCOORD2;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec3(xl_retval._LightCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 324
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec3 _LightCoord;
};
#line 318
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform highp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _LightTexture0;
uniform highp mat4 _LightMatrix0;
uniform highp vec4 _LightColor0;
#line 332
#line 342
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 344
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((NdotL * _LightColor0) * (texture( _LightTexture0, vec2( dot( i._LightCoord, i._LightCoord))).w * 1.0)) * 2.0);
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec3 xlv_TEXCOORD2;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i._LightCoord = vec3(xlv_TEXCOORD2);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" }
Bind "vertex" Vertex
Bind "normal" Normal
Vector 9 [_WorldSpaceLightPos0]
Matrix 5 [_World2Object]
"!!ARBvp1.0
# 11 ALU
PARAM c[10] = { program.local[0],
		state.matrix.mvp,
		program.local[5..9] };
TEMP R0;
TEMP R1;
MOV R0, c[9];
DP3 R1.x, vertex.normal, vertex.normal;
RSQ R1.x, R1.x;
DP4 result.texcoord[1].z, R0, c[7];
DP4 result.texcoord[1].y, R0, c[6];
DP4 result.texcoord[1].x, R0, c[5];
MUL result.texcoord[0].xyz, R1.x, vertex.normal;
DP4 result.position.w, vertex.position, c[4];
DP4 result.position.z, vertex.position, c[3];
DP4 result.position.y, vertex.position, c[2];
DP4 result.position.x, vertex.position, c[1];
END
# 11 instructions, 2 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" }
Bind "vertex" Vertex
Bind "normal" Normal
Matrix 0 [glstate_matrix_mvp]
Vector 8 [_WorldSpaceLightPos0]
Matrix 4 [_World2Object]
"vs_2_0
; 13 ALU
dcl_position0 v0
dcl_normal0 v1
mov r0, c6
mov r1, c5
dp4 oT1.z, c8, r0
dp4 oT1.y, c8, r1
dp3 r1.x, v1, v1
mov r0, c4
rsq r1.x, r1.x
dp4 oT1.x, c8, r0
mul oT0.xyz, r1.x, v1
dp4 oPos.w, v0, c3
dp4 oPos.z, v0, c2
dp4 oPos.y, v0, c1
dp4 oPos.x, v0, c0
"
}

SubProgram "d3d11 " {
Keywords { "DIRECTIONAL" }
Bind "vertex" Vertex
Bind "normal" Normal
ConstBuffer "UnityLighting" 720 // 16 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
ConstBuffer "UnityPerDraw" 336 // 320 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 256 [_World2Object] 4
BindCB "UnityLighting" 0
BindCB "UnityPerDraw" 1
// 12 instructions, 1 temp regs, 0 temp arrays:
// ALU 11 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0
eefiecedjomcclppogkkgejbodkinnpffjebcpnmabaaaaaapeacaaaaadaaaaaa
cmaaaaaahmaaaaaaomaaaaaaejfdeheoeiaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaaebaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaafaepfdejfeejepeoaaeoepfcenebemaaepfdeheo
giaaaaaaadaaaaaaaiaaaaaafaaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaafmaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahaiaaaafmaaaaaa
abaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaafdfgfpfaepfdejfeejepeoaa
feeffiedepepfceeaaklklklfdeieefcaaacaaaaeaaaabaaiaaaaaaafjaaaaae
egiocaaaaaaaaaaaabaaaaaafjaaaaaeegiocaaaabaaaaaabeaaaaaafpaaaaad
pcbabaaaaaaaaaaafpaaaaadhcbabaaaabaaaaaaghaaaaaepccabaaaaaaaaaaa
abaaaaaagfaaaaadhccabaaaabaaaaaagfaaaaadhccabaaaacaaaaaagiaaaaac
abaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaaegiocaaaabaaaaaa
abaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaabaaaaaaaaaaaaaaagbabaaa
aaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaabaaaaaa
acaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpccabaaaaaaaaaaa
egiocaaaabaaaaaaadaaaaaapgbpbaaaaaaaaaaaegaobaaaaaaaaaaabaaaaaah
bcaabaaaaaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaa
aaaaaaaaakaabaaaaaaaaaaadiaaaaahhccabaaaabaaaaaaagaabaaaaaaaaaaa
egbcbaaaabaaaaaadiaaaaajhcaabaaaaaaaaaaafgifcaaaaaaaaaaaaaaaaaaa
egiccaaaabaaaaaabbaaaaaadcaaaaalhcaabaaaaaaaaaaaegiccaaaabaaaaaa
baaaaaaaagiacaaaaaaaaaaaaaaaaaaaegacbaaaaaaaaaaadcaaaaalhcaabaaa
aaaaaaaaegiccaaaabaaaaaabcaaaaaakgikcaaaaaaaaaaaaaaaaaaaegacbaaa
aaaaaaaadcaaaaalhccabaaaacaaaaaaegiccaaaabaaaaaabdaaaaaapgipcaaa
aaaaaaaaaaaaaaaaegacbaaaaaaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 glstate_matrix_mvp;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = normalize(_glesNormal);
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
void main ()
{
  gl_FragData[0] = ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * 2.0);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _World2Object;
uniform highp mat4 glstate_matrix_mvp;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = normalize(_glesNormal);
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
void main ()
{
  gl_FragData[0] = ((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * 2.0);
}



#endif"
}

SubProgram "d3d11_9x " {
Keywords { "DIRECTIONAL" }
Bind "vertex" Vertex
Bind "normal" Normal
ConstBuffer "UnityLighting" 720 // 16 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
ConstBuffer "UnityPerDraw" 336 // 320 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 256 [_World2Object] 4
BindCB "UnityLighting" 0
BindCB "UnityPerDraw" 1
// 12 instructions, 1 temp regs, 0 temp arrays:
// ALU 11 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0_level_9_1
eefiecedhpbdpfhipahamiifpibjckldfimhcjdjabaaaaaafmaeaaaaaeaaaaaa
daaaaaaajeabaaaajmadaaaaomadaaaaebgpgodjfmabaaaafmabaaaaaaacpopp
baabaaaaemaaaaaaadaaceaaaaaaeiaaaaaaeiaaaaaaceaaabaaeiaaaaaaaaaa
abaaabaaaaaaaaaaabaaaaaaaeaaacaaaaaaaaaaabaabaaaaeaaagaaaaaaaaaa
aaaaaaaaaaacpoppbpaaaaacafaaaaiaaaaaapjabpaaaaacafaaabiaabaaapja
aiaaaaadaaaaabiaabaaoejaabaaoejaahaaaaacaaaaabiaaaaaaaiaafaaaaad
aaaaahoaaaaaaaiaabaaoejaabaaaaacaaaaapiaabaaoekaafaaaaadabaaahia
aaaaffiaahaaoekaaeaaaaaeabaaahiaagaaoekaaaaaaaiaabaaoeiaaeaaaaae
aaaaahiaaiaaoekaaaaakkiaabaaoeiaaeaaaaaeabaaahoaajaaoekaaaaappia
aaaaoeiaafaaaaadaaaaapiaaaaaffjaadaaoekaaeaaaaaeaaaaapiaacaaoeka
aaaaaajaaaaaoeiaaeaaaaaeaaaaapiaaeaaoekaaaaakkjaaaaaoeiaaeaaaaae
aaaaapiaafaaoekaaaaappjaaaaaoeiaaeaaaaaeaaaaadmaaaaappiaaaaaoeka
aaaaoeiaabaaaaacaaaaammaaaaaoeiappppaaaafdeieefcaaacaaaaeaaaabaa
iaaaaaaafjaaaaaeegiocaaaaaaaaaaaabaaaaaafjaaaaaeegiocaaaabaaaaaa
beaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaadhcbabaaaabaaaaaaghaaaaae
pccabaaaaaaaaaaaabaaaaaagfaaaaadhccabaaaabaaaaaagfaaaaadhccabaaa
acaaaaaagiaaaaacabaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaa
egiocaaaabaaaaaaabaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaabaaaaaa
aaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaa
egiocaaaabaaaaaaacaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaak
pccabaaaaaaaaaaaegiocaaaabaaaaaaadaaaaaapgbpbaaaaaaaaaaaegaobaaa
aaaaaaaabaaaaaahbcaabaaaaaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaa
eeaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaadiaaaaahhccabaaaabaaaaaa
agaabaaaaaaaaaaaegbcbaaaabaaaaaadiaaaaajhcaabaaaaaaaaaaafgifcaaa
aaaaaaaaaaaaaaaaegiccaaaabaaaaaabbaaaaaadcaaaaalhcaabaaaaaaaaaaa
egiccaaaabaaaaaabaaaaaaaagiacaaaaaaaaaaaaaaaaaaaegacbaaaaaaaaaaa
dcaaaaalhcaabaaaaaaaaaaaegiccaaaabaaaaaabcaaaaaakgikcaaaaaaaaaaa
aaaaaaaaegacbaaaaaaaaaaadcaaaaalhccabaaaacaaaaaaegiccaaaabaaaaaa
bdaaaaaapgipcaaaaaaaaaaaaaaaaaaaegacbaaaaaaaaaaadoaaaaabejfdeheo
eiaaaaaaacaaaaaaaiaaaaaadiaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaa
apapaaaaebaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahahaaaafaepfdej
feejepeoaaeoepfcenebemaaepfdeheogiaaaaaaadaaaaaaaiaaaaaafaaaaaaa
aaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaafmaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahaiaaaafmaaaaaaabaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahaiaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfceeaaklklkl"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal _glesNormal
in vec3 _glesNormal;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 322
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
};
#line 316
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _LightColor0;
#line 329
#line 338
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 329
vertex2fragment vertShadow( in vertexInput v ) {
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    #line 333
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    return o;
}
out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 322
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
};
#line 316
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform highp vec4 _LightColor0;
#line 329
#line 338
#line 338
highp vec4 fragShadow( in vertex2fragment i ) {
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((NdotL * _LightColor0) * 1.0) * 2.0);
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "SPOT" }
Bind "vertex" Vertex
Bind "normal" Normal
Vector 17 [_WorldSpaceLightPos0]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
Vector 18 [unity_Scale]
Matrix 13 [_LightMatrix0]
"!!ARBvp1.0
# 20 ALU
PARAM c[19] = { program.local[0],
		state.matrix.mvp,
		program.local[5..18] };
TEMP R0;
TEMP R1;
MOV R1, c[17];
DP4 R0.w, vertex.position, c[8];
DP4 R0.z, R1, c[11];
DP4 R0.x, R1, c[9];
DP4 R0.y, R1, c[10];
MAD result.texcoord[1].xyz, R0, c[18].w, -vertex.position;
DP3 R1.x, vertex.normal, vertex.normal;
RSQ R1.x, R1.x;
DP4 R0.z, vertex.position, c[7];
DP4 R0.x, vertex.position, c[5];
DP4 R0.y, vertex.position, c[6];
DP4 result.texcoord[2].w, R0, c[16];
DP4 result.texcoord[2].z, R0, c[15];
DP4 result.texcoord[2].y, R0, c[14];
DP4 result.texcoord[2].x, R0, c[13];
MUL result.texcoord[0].xyz, R1.x, vertex.normal;
DP4 result.position.w, vertex.position, c[4];
DP4 result.position.z, vertex.position, c[3];
DP4 result.position.y, vertex.position, c[2];
DP4 result.position.x, vertex.position, c[1];
END
# 20 instructions, 2 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "SPOT" }
Bind "vertex" Vertex
Bind "normal" Normal
Matrix 0 [glstate_matrix_mvp]
Vector 16 [_WorldSpaceLightPos0]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
Vector 17 [unity_Scale]
Matrix 12 [_LightMatrix0]
"vs_2_0
; 22 ALU
dcl_position0 v0
dcl_normal0 v1
mov r0, c10
dp4 r2.z, c16, r0
mov r0, c9
dp4 r2.y, c16, r0
mov r1, c8
dp4 r2.x, c16, r1
dp3 r1.x, v1, v1
rsq r1.x, r1.x
dp4 r0.w, v0, c7
dp4 r0.z, v0, c6
dp4 r0.x, v0, c4
dp4 r0.y, v0, c5
mad oT1.xyz, r2, c17.w, -v0
dp4 oT2.w, r0, c15
dp4 oT2.z, r0, c14
dp4 oT2.y, r0, c13
dp4 oT2.x, r0, c12
mul oT0.xyz, r1.x, v1
dp4 oPos.w, v0, c3
dp4 oPos.z, v0, c2
dp4 oPos.y, v0, c1
dp4 oPos.x, v0, c0
"
}

SubProgram "d3d11 " {
Keywords { "SPOT" }
Bind "vertex" Vertex
Bind "normal" Normal
ConstBuffer "$Globals" 96 // 80 used size, 3 vars
Matrix 16 [_LightMatrix0] 4
ConstBuffer "UnityLighting" 720 // 16 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
ConstBuffer "UnityPerDraw" 336 // 336 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
Matrix 256 [_World2Object] 4
Vector 320 [unity_Scale] 4
BindCB "$Globals" 0
BindCB "UnityLighting" 1
BindCB "UnityPerDraw" 2
// 21 instructions, 2 temp regs, 0 temp arrays:
// ALU 20 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0
eefiecedphcmaaehmoihemhjjoieclgakckokemhabaaaaaaieaeaaaaadaaaaaa
cmaaaaaahmaaaaaaaeabaaaaejfdeheoeiaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaaebaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaafaepfdejfeejepeoaaeoepfcenebemaaepfdeheo
iaaaaaaaaeaaaaaaaiaaaaaagiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaaheaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahaiaaaaheaaaaaa
abaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaheaaaaaaacaaaaaaaaaaaaaa
adaaaaaaadaaaaaaapaaaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfcee
aaklklklfdeieefchiadaaaaeaaaabaanoaaaaaafjaaaaaeegiocaaaaaaaaaaa
afaaaaaafjaaaaaeegiocaaaabaaaaaaabaaaaaafjaaaaaeegiocaaaacaaaaaa
bfaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaadhcbabaaaabaaaaaaghaaaaae
pccabaaaaaaaaaaaabaaaaaagfaaaaadhccabaaaabaaaaaagfaaaaadhccabaaa
acaaaaaagfaaaaadpccabaaaadaaaaaagiaaaaacacaaaaaadiaaaaaipcaabaaa
aaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaaabaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaacaaaaaaaaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaacaaaaaakgbkbaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpccabaaaaaaaaaaaegiocaaaacaaaaaaadaaaaaa
pgbpbaaaaaaaaaaaegaobaaaaaaaaaaabaaaaaahbcaabaaaaaaaaaaaegbcbaaa
abaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaa
diaaaaahhccabaaaabaaaaaaagaabaaaaaaaaaaaegbcbaaaabaaaaaadiaaaaaj
hcaabaaaaaaaaaaafgifcaaaabaaaaaaaaaaaaaaegiccaaaacaaaaaabbaaaaaa
dcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaabaaaaaaaagiacaaaabaaaaaa
aaaaaaaaegacbaaaaaaaaaaadcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaa
bcaaaaaakgikcaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaadcaaaaalhcaabaaa
aaaaaaaaegiccaaaacaaaaaabdaaaaaapgipcaaaabaaaaaaaaaaaaaaegacbaaa
aaaaaaaadcaaaaalhccabaaaacaaaaaaegacbaaaaaaaaaaapgipcaaaacaaaaaa
beaaaaaaegbcbaiaebaaaaaaaaaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaa
aaaaaaaaegiocaaaacaaaaaaanaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaa
acaaaaaaamaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaacaaaaaaaoaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaapaaaaaapgbpbaaaaaaaaaaa
egaobaaaaaaaaaaadiaaaaaipcaabaaaabaaaaaafgafbaaaaaaaaaaaegiocaaa
aaaaaaaaacaaaaaadcaaaaakpcaabaaaabaaaaaaegiocaaaaaaaaaaaabaaaaaa
agaabaaaaaaaaaaaegaobaaaabaaaaaadcaaaaakpcaabaaaabaaaaaaegiocaaa
aaaaaaaaadaaaaaakgakbaaaaaaaaaaaegaobaaaabaaaaaadcaaaaakpccabaaa
adaaaaaaegiocaaaaaaaaaaaaeaaaaaapgapbaaaaaaaaaaaegaobaaaabaaaaaa
doaaaaab"
}

SubProgram "gles " {
Keywords { "SPOT" }
"!!GLES


#ifdef VERTEX

varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _LightMatrix0;
uniform highp vec4 unity_Scale;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 _WorldSpaceLightPos0;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = normalize(_glesNormal);
  xlv_TEXCOORD1 = (((_World2Object * _WorldSpaceLightPos0).xyz * unity_Scale.w) - _glesVertex.xyz);
  xlv_TEXCOORD2 = (_LightMatrix0 * (_Object2World * _glesVertex));
}



#endif
#ifdef FRAGMENT

varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform sampler2D _LightTextureB0;
uniform sampler2D _LightTexture0;
void main ()
{
  lowp vec4 tmpvar_1;
  highp vec2 P_2;
  P_2 = ((xlv_TEXCOORD2.xy / xlv_TEXCOORD2.w) + 0.5);
  tmpvar_1 = texture2D (_LightTexture0, P_2);
  highp float tmpvar_3;
  tmpvar_3 = dot (xlv_TEXCOORD2.xyz, xlv_TEXCOORD2.xyz);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_LightTextureB0, vec2(tmpvar_3));
  gl_FragData[0] = (((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * ((float((xlv_TEXCOORD2.z > 0.0)) * tmpvar_1.w) * tmpvar_4.w)) * 2.0);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "SPOT" }
"!!GLES


#ifdef VERTEX

varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _LightMatrix0;
uniform highp vec4 unity_Scale;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 _WorldSpaceLightPos0;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = normalize(_glesNormal);
  xlv_TEXCOORD1 = (((_World2Object * _WorldSpaceLightPos0).xyz * unity_Scale.w) - _glesVertex.xyz);
  xlv_TEXCOORD2 = (_LightMatrix0 * (_Object2World * _glesVertex));
}



#endif
#ifdef FRAGMENT

varying highp vec4 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform sampler2D _LightTextureB0;
uniform sampler2D _LightTexture0;
void main ()
{
  lowp vec4 tmpvar_1;
  highp vec2 P_2;
  P_2 = ((xlv_TEXCOORD2.xy / xlv_TEXCOORD2.w) + 0.5);
  tmpvar_1 = texture2D (_LightTexture0, P_2);
  highp float tmpvar_3;
  tmpvar_3 = dot (xlv_TEXCOORD2.xyz, xlv_TEXCOORD2.xyz);
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2D (_LightTextureB0, vec2(tmpvar_3));
  gl_FragData[0] = (((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * ((float((xlv_TEXCOORD2.z > 0.0)) * tmpvar_1.w) * tmpvar_4.w)) * 2.0);
}



#endif"
}

SubProgram "d3d11_9x " {
Keywords { "SPOT" }
Bind "vertex" Vertex
Bind "normal" Normal
ConstBuffer "$Globals" 96 // 80 used size, 3 vars
Matrix 16 [_LightMatrix0] 4
ConstBuffer "UnityLighting" 720 // 16 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
ConstBuffer "UnityPerDraw" 336 // 336 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
Matrix 256 [_World2Object] 4
Vector 320 [unity_Scale] 4
BindCB "$Globals" 0
BindCB "UnityLighting" 1
BindCB "UnityPerDraw" 2
// 21 instructions, 2 temp regs, 0 temp arrays:
// ALU 20 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0_level_9_1
eefiecedefjnpdgnoifgcfifipeimhkmejdkgkifabaaaaaakeagaaaaaeaaaaaa
daaaaaaaemacaaaammafaaaabmagaaaaebgpgodjbeacaaaabeacaaaaaaacpopp
lmabaaaafiaaaaaaaeaaceaaaaaafeaaaaaafeaaaaaaceaaabaafeaaaaaaabaa
aeaaabaaaaaaaaaaabaaaaaaabaaafaaaaaaaaaaacaaaaaaaeaaagaaaaaaaaaa
acaaamaaajaaakaaaaaaaaaaaaaaaaaaaaacpoppbpaaaaacafaaaaiaaaaaapja
bpaaaaacafaaabiaabaaapjaaiaaaaadaaaaabiaabaaoejaabaaoejaahaaaaac
aaaaabiaaaaaaaiaafaaaaadaaaaahoaaaaaaaiaabaaoejaabaaaaacaaaaapia
afaaoekaafaaaaadabaaahiaaaaaffiaapaaoekaaeaaaaaeabaaahiaaoaaoeka
aaaaaaiaabaaoeiaaeaaaaaeaaaaahiabaaaoekaaaaakkiaabaaoeiaaeaaaaae
aaaaahiabbaaoekaaaaappiaaaaaoeiaaeaaaaaeabaaahoaaaaaoeiabcaappka
aaaaoejbafaaaaadaaaaapiaaaaaffjaalaaoekaaeaaaaaeaaaaapiaakaaoeka
aaaaaajaaaaaoeiaaeaaaaaeaaaaapiaamaaoekaaaaakkjaaaaaoeiaaeaaaaae
aaaaapiaanaaoekaaaaappjaaaaaoeiaafaaaaadabaaapiaaaaaffiaacaaoeka
aeaaaaaeabaaapiaabaaoekaaaaaaaiaabaaoeiaaeaaaaaeabaaapiaadaaoeka
aaaakkiaabaaoeiaaeaaaaaeacaaapoaaeaaoekaaaaappiaabaaoeiaafaaaaad
aaaaapiaaaaaffjaahaaoekaaeaaaaaeaaaaapiaagaaoekaaaaaaajaaaaaoeia
aeaaaaaeaaaaapiaaiaaoekaaaaakkjaaaaaoeiaaeaaaaaeaaaaapiaajaaoeka
aaaappjaaaaaoeiaaeaaaaaeaaaaadmaaaaappiaaaaaoekaaaaaoeiaabaaaaac
aaaaammaaaaaoeiappppaaaafdeieefchiadaaaaeaaaabaanoaaaaaafjaaaaae
egiocaaaaaaaaaaaafaaaaaafjaaaaaeegiocaaaabaaaaaaabaaaaaafjaaaaae
egiocaaaacaaaaaabfaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaadhcbabaaa
abaaaaaaghaaaaaepccabaaaaaaaaaaaabaaaaaagfaaaaadhccabaaaabaaaaaa
gfaaaaadhccabaaaacaaaaaagfaaaaadpccabaaaadaaaaaagiaaaaacacaaaaaa
diaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaaabaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaaaaaaaaaagbabaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaacaaaaaa
kgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpccabaaaaaaaaaaaegiocaaa
acaaaaaaadaaaaaapgbpbaaaaaaaaaaaegaobaaaaaaaaaaabaaaaaahbcaabaaa
aaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaaaaaaaaaa
akaabaaaaaaaaaaadiaaaaahhccabaaaabaaaaaaagaabaaaaaaaaaaaegbcbaaa
abaaaaaadiaaaaajhcaabaaaaaaaaaaafgifcaaaabaaaaaaaaaaaaaaegiccaaa
acaaaaaabbaaaaaadcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaabaaaaaaa
agiacaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaadcaaaaalhcaabaaaaaaaaaaa
egiccaaaacaaaaaabcaaaaaakgikcaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaa
dcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaabdaaaaaapgipcaaaabaaaaaa
aaaaaaaaegacbaaaaaaaaaaadcaaaaalhccabaaaacaaaaaaegacbaaaaaaaaaaa
pgipcaaaacaaaaaabeaaaaaaegbcbaiaebaaaaaaaaaaaaaadiaaaaaipcaabaaa
aaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaaanaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaacaaaaaaamaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaaoaaaaaakgbkbaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaapaaaaaa
pgbpbaaaaaaaaaaaegaobaaaaaaaaaaadiaaaaaipcaabaaaabaaaaaafgafbaaa
aaaaaaaaegiocaaaaaaaaaaaacaaaaaadcaaaaakpcaabaaaabaaaaaaegiocaaa
aaaaaaaaabaaaaaaagaabaaaaaaaaaaaegaobaaaabaaaaaadcaaaaakpcaabaaa
abaaaaaaegiocaaaaaaaaaaaadaaaaaakgakbaaaaaaaaaaaegaobaaaabaaaaaa
dcaaaaakpccabaaaadaaaaaaegiocaaaaaaaaaaaaeaaaaaapgapbaaaaaaaaaaa
egaobaaaabaaaaaadoaaaaabejfdeheoeiaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaaebaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaafaepfdejfeejepeoaaeoepfcenebemaaepfdeheo
iaaaaaaaaeaaaaaaaiaaaaaagiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaaheaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahaiaaaaheaaaaaa
abaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaheaaaaaaacaaaaaaaaaaaaaa
adaaaaaaadaaaaaaapaaaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfcee
aaklklkl"
}

SubProgram "gles3 " {
Keywords { "SPOT" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal _glesNormal
in vec3 _glesNormal;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 333
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 _LightCoord;
};
#line 327
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform highp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _LightTexture0;
uniform highp mat4 _LightMatrix0;
uniform sampler2D _LightTextureB0;
uniform highp vec4 _LightColor0;
#line 341
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return ((objSpaceLightPos.xyz * unity_Scale.w) - v.xyz);
}
#line 341
vertex2fragment vertShadow( in vertexInput v ) {
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    #line 345
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    o._LightCoord = (_LightMatrix0 * (_Object2World * v.vertex));
    #line 349
    return o;
}
out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec4 xlv_TEXCOORD2;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec4(xl_retval._LightCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 333
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec4 _LightCoord;
};
#line 327
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform highp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _LightTexture0;
uniform highp mat4 _LightMatrix0;
uniform sampler2D _LightTextureB0;
uniform highp vec4 _LightColor0;
#line 341
#line 322
lowp float UnitySpotAttenuate( in highp vec3 LightCoord ) {
    #line 324
    return texture( _LightTextureB0, vec2( dot( LightCoord, LightCoord))).w;
}
#line 318
lowp float UnitySpotCookie( in highp vec4 LightCoord ) {
    #line 320
    return texture( _LightTexture0, ((LightCoord.xy / LightCoord.w) + 0.5)).w;
}
#line 351
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 353
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((NdotL * _LightColor0) * (((float((i._LightCoord.z > 0.0)) * UnitySpotCookie( i._LightCoord)) * UnitySpotAttenuate( i._LightCoord.xyz)) * 1.0)) * 2.0);
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec4 xlv_TEXCOORD2;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i._LightCoord = vec4(xlv_TEXCOORD2);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "POINT_COOKIE" }
Bind "vertex" Vertex
Bind "normal" Normal
Vector 17 [_WorldSpaceLightPos0]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
Vector 18 [unity_Scale]
Matrix 13 [_LightMatrix0]
"!!ARBvp1.0
# 19 ALU
PARAM c[19] = { program.local[0],
		state.matrix.mvp,
		program.local[5..18] };
TEMP R0;
TEMP R1;
MOV R1, c[17];
DP4 R0.z, R1, c[11];
DP4 R0.x, R1, c[9];
DP4 R0.y, R1, c[10];
MAD result.texcoord[1].xyz, R0, c[18].w, -vertex.position;
DP3 R1.x, vertex.normal, vertex.normal;
RSQ R1.x, R1.x;
DP4 R0.w, vertex.position, c[8];
DP4 R0.z, vertex.position, c[7];
DP4 R0.x, vertex.position, c[5];
DP4 R0.y, vertex.position, c[6];
DP4 result.texcoord[2].z, R0, c[15];
DP4 result.texcoord[2].y, R0, c[14];
DP4 result.texcoord[2].x, R0, c[13];
MUL result.texcoord[0].xyz, R1.x, vertex.normal;
DP4 result.position.w, vertex.position, c[4];
DP4 result.position.z, vertex.position, c[3];
DP4 result.position.y, vertex.position, c[2];
DP4 result.position.x, vertex.position, c[1];
END
# 19 instructions, 2 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "POINT_COOKIE" }
Bind "vertex" Vertex
Bind "normal" Normal
Matrix 0 [glstate_matrix_mvp]
Vector 16 [_WorldSpaceLightPos0]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
Vector 17 [unity_Scale]
Matrix 12 [_LightMatrix0]
"vs_2_0
; 21 ALU
dcl_position0 v0
dcl_normal0 v1
mov r0, c10
dp4 r2.z, c16, r0
mov r0, c9
dp4 r2.y, c16, r0
mov r1, c8
dp4 r2.x, c16, r1
dp3 r1.x, v1, v1
rsq r1.x, r1.x
dp4 r0.w, v0, c7
dp4 r0.z, v0, c6
dp4 r0.x, v0, c4
dp4 r0.y, v0, c5
mad oT1.xyz, r2, c17.w, -v0
dp4 oT2.z, r0, c14
dp4 oT2.y, r0, c13
dp4 oT2.x, r0, c12
mul oT0.xyz, r1.x, v1
dp4 oPos.w, v0, c3
dp4 oPos.z, v0, c2
dp4 oPos.y, v0, c1
dp4 oPos.x, v0, c0
"
}

SubProgram "d3d11 " {
Keywords { "POINT_COOKIE" }
Bind "vertex" Vertex
Bind "normal" Normal
ConstBuffer "$Globals" 96 // 80 used size, 3 vars
Matrix 16 [_LightMatrix0] 4
ConstBuffer "UnityLighting" 720 // 16 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
ConstBuffer "UnityPerDraw" 336 // 336 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
Matrix 256 [_World2Object] 4
Vector 320 [unity_Scale] 4
BindCB "$Globals" 0
BindCB "UnityLighting" 1
BindCB "UnityPerDraw" 2
// 21 instructions, 2 temp regs, 0 temp arrays:
// ALU 20 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0
eefiecediejnfnlnnibnmjldodabjlgijddeajhdabaaaaaaieaeaaaaadaaaaaa
cmaaaaaahmaaaaaaaeabaaaaejfdeheoeiaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaaebaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaafaepfdejfeejepeoaaeoepfcenebemaaepfdeheo
iaaaaaaaaeaaaaaaaiaaaaaagiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaaheaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahaiaaaaheaaaaaa
abaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaheaaaaaaacaaaaaaaaaaaaaa
adaaaaaaadaaaaaaahaiaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfcee
aaklklklfdeieefchiadaaaaeaaaabaanoaaaaaafjaaaaaeegiocaaaaaaaaaaa
afaaaaaafjaaaaaeegiocaaaabaaaaaaabaaaaaafjaaaaaeegiocaaaacaaaaaa
bfaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaadhcbabaaaabaaaaaaghaaaaae
pccabaaaaaaaaaaaabaaaaaagfaaaaadhccabaaaabaaaaaagfaaaaadhccabaaa
acaaaaaagfaaaaadhccabaaaadaaaaaagiaaaaacacaaaaaadiaaaaaipcaabaaa
aaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaaabaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaacaaaaaaaaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaacaaaaaakgbkbaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpccabaaaaaaaaaaaegiocaaaacaaaaaaadaaaaaa
pgbpbaaaaaaaaaaaegaobaaaaaaaaaaabaaaaaahbcaabaaaaaaaaaaaegbcbaaa
abaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaa
diaaaaahhccabaaaabaaaaaaagaabaaaaaaaaaaaegbcbaaaabaaaaaadiaaaaaj
hcaabaaaaaaaaaaafgifcaaaabaaaaaaaaaaaaaaegiccaaaacaaaaaabbaaaaaa
dcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaabaaaaaaaagiacaaaabaaaaaa
aaaaaaaaegacbaaaaaaaaaaadcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaa
bcaaaaaakgikcaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaadcaaaaalhcaabaaa
aaaaaaaaegiccaaaacaaaaaabdaaaaaapgipcaaaabaaaaaaaaaaaaaaegacbaaa
aaaaaaaadcaaaaalhccabaaaacaaaaaaegacbaaaaaaaaaaapgipcaaaacaaaaaa
beaaaaaaegbcbaiaebaaaaaaaaaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaa
aaaaaaaaegiocaaaacaaaaaaanaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaa
acaaaaaaamaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaacaaaaaaaoaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaapaaaaaapgbpbaaaaaaaaaaa
egaobaaaaaaaaaaadiaaaaaihcaabaaaabaaaaaafgafbaaaaaaaaaaaegiccaaa
aaaaaaaaacaaaaaadcaaaaakhcaabaaaabaaaaaaegiccaaaaaaaaaaaabaaaaaa
agaabaaaaaaaaaaaegacbaaaabaaaaaadcaaaaakhcaabaaaaaaaaaaaegiccaaa
aaaaaaaaadaaaaaakgakbaaaaaaaaaaaegacbaaaabaaaaaadcaaaaakhccabaaa
adaaaaaaegiccaaaaaaaaaaaaeaaaaaapgapbaaaaaaaaaaaegacbaaaaaaaaaaa
doaaaaab"
}

SubProgram "gles " {
Keywords { "POINT_COOKIE" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _LightMatrix0;
uniform highp vec4 unity_Scale;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 _WorldSpaceLightPos0;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = normalize(_glesNormal);
  xlv_TEXCOORD1 = (((_World2Object * _WorldSpaceLightPos0).xyz * unity_Scale.w) - _glesVertex.xyz);
  xlv_TEXCOORD2 = (_LightMatrix0 * (_Object2World * _glesVertex)).xyz;
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform sampler2D _LightTextureB0;
uniform samplerCube _LightTexture0;
void main ()
{
  highp float tmpvar_1;
  tmpvar_1 = dot (xlv_TEXCOORD2, xlv_TEXCOORD2);
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_LightTextureB0, vec2(tmpvar_1));
  lowp vec4 tmpvar_3;
  tmpvar_3 = textureCube (_LightTexture0, xlv_TEXCOORD2);
  gl_FragData[0] = (((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * (tmpvar_2.w * tmpvar_3.w)) * 2.0);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "POINT_COOKIE" }
"!!GLES


#ifdef VERTEX

varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _LightMatrix0;
uniform highp vec4 unity_Scale;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 _WorldSpaceLightPos0;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = normalize(_glesNormal);
  xlv_TEXCOORD1 = (((_World2Object * _WorldSpaceLightPos0).xyz * unity_Scale.w) - _glesVertex.xyz);
  xlv_TEXCOORD2 = (_LightMatrix0 * (_Object2World * _glesVertex)).xyz;
}



#endif
#ifdef FRAGMENT

varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform sampler2D _LightTextureB0;
uniform samplerCube _LightTexture0;
void main ()
{
  highp float tmpvar_1;
  tmpvar_1 = dot (xlv_TEXCOORD2, xlv_TEXCOORD2);
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_LightTextureB0, vec2(tmpvar_1));
  lowp vec4 tmpvar_3;
  tmpvar_3 = textureCube (_LightTexture0, xlv_TEXCOORD2);
  gl_FragData[0] = (((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * (tmpvar_2.w * tmpvar_3.w)) * 2.0);
}



#endif"
}

SubProgram "d3d11_9x " {
Keywords { "POINT_COOKIE" }
Bind "vertex" Vertex
Bind "normal" Normal
ConstBuffer "$Globals" 96 // 80 used size, 3 vars
Matrix 16 [_LightMatrix0] 4
ConstBuffer "UnityLighting" 720 // 16 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
ConstBuffer "UnityPerDraw" 336 // 336 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
Matrix 256 [_World2Object] 4
Vector 320 [unity_Scale] 4
BindCB "$Globals" 0
BindCB "UnityLighting" 1
BindCB "UnityPerDraw" 2
// 21 instructions, 2 temp regs, 0 temp arrays:
// ALU 20 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0_level_9_1
eefiecedncpkemgbjdhkgnblejjjjgjbmdgonjbbabaaaaaakeagaaaaaeaaaaaa
daaaaaaaemacaaaammafaaaabmagaaaaebgpgodjbeacaaaabeacaaaaaaacpopp
lmabaaaafiaaaaaaaeaaceaaaaaafeaaaaaafeaaaaaaceaaabaafeaaaaaaabaa
aeaaabaaaaaaaaaaabaaaaaaabaaafaaaaaaaaaaacaaaaaaaeaaagaaaaaaaaaa
acaaamaaajaaakaaaaaaaaaaaaaaaaaaaaacpoppbpaaaaacafaaaaiaaaaaapja
bpaaaaacafaaabiaabaaapjaaiaaaaadaaaaabiaabaaoejaabaaoejaahaaaaac
aaaaabiaaaaaaaiaafaaaaadaaaaahoaaaaaaaiaabaaoejaabaaaaacaaaaapia
afaaoekaafaaaaadabaaahiaaaaaffiaapaaoekaaeaaaaaeabaaahiaaoaaoeka
aaaaaaiaabaaoeiaaeaaaaaeaaaaahiabaaaoekaaaaakkiaabaaoeiaaeaaaaae
aaaaahiabbaaoekaaaaappiaaaaaoeiaaeaaaaaeabaaahoaaaaaoeiabcaappka
aaaaoejbafaaaaadaaaaapiaaaaaffjaalaaoekaaeaaaaaeaaaaapiaakaaoeka
aaaaaajaaaaaoeiaaeaaaaaeaaaaapiaamaaoekaaaaakkjaaaaaoeiaaeaaaaae
aaaaapiaanaaoekaaaaappjaaaaaoeiaafaaaaadabaaahiaaaaaffiaacaaoeka
aeaaaaaeabaaahiaabaaoekaaaaaaaiaabaaoeiaaeaaaaaeaaaaahiaadaaoeka
aaaakkiaabaaoeiaaeaaaaaeacaaahoaaeaaoekaaaaappiaaaaaoeiaafaaaaad
aaaaapiaaaaaffjaahaaoekaaeaaaaaeaaaaapiaagaaoekaaaaaaajaaaaaoeia
aeaaaaaeaaaaapiaaiaaoekaaaaakkjaaaaaoeiaaeaaaaaeaaaaapiaajaaoeka
aaaappjaaaaaoeiaaeaaaaaeaaaaadmaaaaappiaaaaaoekaaaaaoeiaabaaaaac
aaaaammaaaaaoeiappppaaaafdeieefchiadaaaaeaaaabaanoaaaaaafjaaaaae
egiocaaaaaaaaaaaafaaaaaafjaaaaaeegiocaaaabaaaaaaabaaaaaafjaaaaae
egiocaaaacaaaaaabfaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaadhcbabaaa
abaaaaaaghaaaaaepccabaaaaaaaaaaaabaaaaaagfaaaaadhccabaaaabaaaaaa
gfaaaaadhccabaaaacaaaaaagfaaaaadhccabaaaadaaaaaagiaaaaacacaaaaaa
diaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaaabaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaaaaaaaaaagbabaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaacaaaaaa
kgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpccabaaaaaaaaaaaegiocaaa
acaaaaaaadaaaaaapgbpbaaaaaaaaaaaegaobaaaaaaaaaaabaaaaaahbcaabaaa
aaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaaaaaaaaaa
akaabaaaaaaaaaaadiaaaaahhccabaaaabaaaaaaagaabaaaaaaaaaaaegbcbaaa
abaaaaaadiaaaaajhcaabaaaaaaaaaaafgifcaaaabaaaaaaaaaaaaaaegiccaaa
acaaaaaabbaaaaaadcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaabaaaaaaa
agiacaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaadcaaaaalhcaabaaaaaaaaaaa
egiccaaaacaaaaaabcaaaaaakgikcaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaa
dcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaabdaaaaaapgipcaaaabaaaaaa
aaaaaaaaegacbaaaaaaaaaaadcaaaaalhccabaaaacaaaaaaegacbaaaaaaaaaaa
pgipcaaaacaaaaaabeaaaaaaegbcbaiaebaaaaaaaaaaaaaadiaaaaaipcaabaaa
aaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaaanaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaacaaaaaaamaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaaoaaaaaakgbkbaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaapaaaaaa
pgbpbaaaaaaaaaaaegaobaaaaaaaaaaadiaaaaaihcaabaaaabaaaaaafgafbaaa
aaaaaaaaegiccaaaaaaaaaaaacaaaaaadcaaaaakhcaabaaaabaaaaaaegiccaaa
aaaaaaaaabaaaaaaagaabaaaaaaaaaaaegacbaaaabaaaaaadcaaaaakhcaabaaa
aaaaaaaaegiccaaaaaaaaaaaadaaaaaakgakbaaaaaaaaaaaegacbaaaabaaaaaa
dcaaaaakhccabaaaadaaaaaaegiccaaaaaaaaaaaaeaaaaaapgapbaaaaaaaaaaa
egacbaaaaaaaaaaadoaaaaabejfdeheoeiaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaaebaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaafaepfdejfeejepeoaaeoepfcenebemaaepfdeheo
iaaaaaaaaeaaaaaaaiaaaaaagiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaaheaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahaiaaaaheaaaaaa
abaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaheaaaaaaacaaaaaaaaaaaaaa
adaaaaaaadaaaaaaahaiaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfcee
aaklklkl"
}

SubProgram "gles3 " {
Keywords { "POINT_COOKIE" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal _glesNormal
in vec3 _glesNormal;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 325
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec3 _LightCoord;
};
#line 319
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform highp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform samplerCube _LightTexture0;
uniform highp mat4 _LightMatrix0;
uniform sampler2D _LightTextureB0;
uniform highp vec4 _LightColor0;
#line 333
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return ((objSpaceLightPos.xyz * unity_Scale.w) - v.xyz);
}
#line 333
vertex2fragment vertShadow( in vertexInput v ) {
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    #line 337
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    o._LightCoord = (_LightMatrix0 * (_Object2World * v.vertex)).xyz;
    #line 341
    return o;
}
out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec3 xlv_TEXCOORD2;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec3(xl_retval._LightCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 325
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec3 _LightCoord;
};
#line 319
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform highp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform samplerCube _LightTexture0;
uniform highp mat4 _LightMatrix0;
uniform sampler2D _LightTextureB0;
uniform highp vec4 _LightColor0;
#line 333
#line 343
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 345
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((NdotL * _LightColor0) * ((texture( _LightTextureB0, vec2( dot( i._LightCoord, i._LightCoord))).w * texture( _LightTexture0, i._LightCoord).w) * 1.0)) * 2.0);
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec3 xlv_TEXCOORD2;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i._LightCoord = vec3(xlv_TEXCOORD2);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL_COOKIE" }
Bind "vertex" Vertex
Bind "normal" Normal
Vector 17 [_WorldSpaceLightPos0]
Matrix 5 [_Object2World]
Matrix 9 [_World2Object]
Matrix 13 [_LightMatrix0]
"!!ARBvp1.0
# 17 ALU
PARAM c[18] = { program.local[0],
		state.matrix.mvp,
		program.local[5..17] };
TEMP R0;
TEMP R1;
MOV R0, c[17];
DP3 R1.x, vertex.normal, vertex.normal;
RSQ R1.x, R1.x;
DP4 result.texcoord[1].z, R0, c[11];
DP4 result.texcoord[1].y, R0, c[10];
DP4 result.texcoord[1].x, R0, c[9];
DP4 R0.w, vertex.position, c[8];
DP4 R0.z, vertex.position, c[7];
DP4 R0.x, vertex.position, c[5];
DP4 R0.y, vertex.position, c[6];
DP4 result.texcoord[2].y, R0, c[14];
DP4 result.texcoord[2].x, R0, c[13];
MUL result.texcoord[0].xyz, R1.x, vertex.normal;
DP4 result.position.w, vertex.position, c[4];
DP4 result.position.z, vertex.position, c[3];
DP4 result.position.y, vertex.position, c[2];
DP4 result.position.x, vertex.position, c[1];
END
# 17 instructions, 2 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL_COOKIE" }
Bind "vertex" Vertex
Bind "normal" Normal
Matrix 0 [glstate_matrix_mvp]
Vector 16 [_WorldSpaceLightPos0]
Matrix 4 [_Object2World]
Matrix 8 [_World2Object]
Matrix 12 [_LightMatrix0]
"vs_2_0
; 19 ALU
dcl_position0 v0
dcl_normal0 v1
mov r0, c10
mov r1, c8
dp4 oT1.z, c16, r0
mov r0, c9
dp4 oT1.y, c16, r0
dp4 oT1.x, c16, r1
dp3 r1.x, v1, v1
rsq r1.x, r1.x
dp4 r0.w, v0, c7
dp4 r0.z, v0, c6
dp4 r0.x, v0, c4
dp4 r0.y, v0, c5
dp4 oT2.y, r0, c13
dp4 oT2.x, r0, c12
mul oT0.xyz, r1.x, v1
dp4 oPos.w, v0, c3
dp4 oPos.z, v0, c2
dp4 oPos.y, v0, c1
dp4 oPos.x, v0, c0
"
}

SubProgram "d3d11 " {
Keywords { "DIRECTIONAL_COOKIE" }
Bind "vertex" Vertex
Bind "normal" Normal
ConstBuffer "$Globals" 96 // 80 used size, 3 vars
Matrix 16 [_LightMatrix0] 4
ConstBuffer "UnityLighting" 720 // 16 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
ConstBuffer "UnityPerDraw" 336 // 320 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
Matrix 256 [_World2Object] 4
BindCB "$Globals" 0
BindCB "UnityLighting" 1
BindCB "UnityPerDraw" 2
// 20 instructions, 2 temp regs, 0 temp arrays:
// ALU 19 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0
eefiecedfdoekpleemlpfjafcfolfdlpmklhabdnabaaaaaafiaeaaaaadaaaaaa
cmaaaaaahmaaaaaaaeabaaaaejfdeheoeiaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaaebaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaafaepfdejfeejepeoaaeoepfcenebemaaepfdeheo
iaaaaaaaaeaaaaaaaiaaaaaagiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaaheaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahaiaaaaheaaaaaa
abaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaheaaaaaaacaaaaaaaaaaaaaa
adaaaaaaadaaaaaaadamaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfcee
aaklklklfdeieefcemadaaaaeaaaabaandaaaaaafjaaaaaeegiocaaaaaaaaaaa
afaaaaaafjaaaaaeegiocaaaabaaaaaaabaaaaaafjaaaaaeegiocaaaacaaaaaa
beaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaadhcbabaaaabaaaaaaghaaaaae
pccabaaaaaaaaaaaabaaaaaagfaaaaadhccabaaaabaaaaaagfaaaaadhccabaaa
acaaaaaagfaaaaaddccabaaaadaaaaaagiaaaaacacaaaaaadiaaaaaipcaabaaa
aaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaaabaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaacaaaaaaaaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaacaaaaaakgbkbaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpccabaaaaaaaaaaaegiocaaaacaaaaaaadaaaaaa
pgbpbaaaaaaaaaaaegaobaaaaaaaaaaabaaaaaahbcaabaaaaaaaaaaaegbcbaaa
abaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaa
diaaaaahhccabaaaabaaaaaaagaabaaaaaaaaaaaegbcbaaaabaaaaaadiaaaaaj
hcaabaaaaaaaaaaafgifcaaaabaaaaaaaaaaaaaaegiccaaaacaaaaaabbaaaaaa
dcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaabaaaaaaaagiacaaaabaaaaaa
aaaaaaaaegacbaaaaaaaaaaadcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaa
bcaaaaaakgikcaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaadcaaaaalhccabaaa
acaaaaaaegiccaaaacaaaaaabdaaaaaapgipcaaaabaaaaaaaaaaaaaaegacbaaa
aaaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaa
anaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaamaaaaaaagbabaaa
aaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaa
aoaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaa
egiocaaaacaaaaaaapaaaaaapgbpbaaaaaaaaaaaegaobaaaaaaaaaaadiaaaaai
dcaabaaaabaaaaaafgafbaaaaaaaaaaaegiacaaaaaaaaaaaacaaaaaadcaaaaak
dcaabaaaaaaaaaaaegiacaaaaaaaaaaaabaaaaaaagaabaaaaaaaaaaaegaabaaa
abaaaaaadcaaaaakdcaabaaaaaaaaaaaegiacaaaaaaaaaaaadaaaaaakgakbaaa
aaaaaaaaegaabaaaaaaaaaaadcaaaaakdccabaaaadaaaaaaegiacaaaaaaaaaaa
aeaaaaaapgapbaaaaaaaaaaaegaabaaaaaaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL_COOKIE" }
"!!GLES


#ifdef VERTEX

varying highp vec2 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _LightMatrix0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = normalize(_glesNormal);
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (_LightMatrix0 * (_Object2World * _glesVertex)).xy;
}



#endif
#ifdef FRAGMENT

varying highp vec2 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform sampler2D _LightTexture0;
void main ()
{
  lowp vec4 tmpvar_1;
  tmpvar_1 = texture2D (_LightTexture0, xlv_TEXCOORD2);
  gl_FragData[0] = (((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_1.w) * 2.0);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL_COOKIE" }
"!!GLES


#ifdef VERTEX

varying highp vec2 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp mat4 _LightMatrix0;
uniform highp mat4 _World2Object;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform lowp vec4 _WorldSpaceLightPos0;
attribute vec3 _glesNormal;
attribute vec4 _glesVertex;
void main ()
{
  gl_Position = (glstate_matrix_mvp * _glesVertex);
  xlv_TEXCOORD0 = normalize(_glesNormal);
  xlv_TEXCOORD1 = (_World2Object * _WorldSpaceLightPos0).xyz;
  xlv_TEXCOORD2 = (_LightMatrix0 * (_Object2World * _glesVertex)).xy;
}



#endif
#ifdef FRAGMENT

varying highp vec2 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD0;
uniform highp vec4 _LightColor0;
uniform sampler2D _LightTexture0;
void main ()
{
  lowp vec4 tmpvar_1;
  tmpvar_1 = texture2D (_LightTexture0, xlv_TEXCOORD2);
  gl_FragData[0] = (((clamp (dot (normalize(xlv_TEXCOORD0), normalize(xlv_TEXCOORD1)), 0.0, 1.0) * _LightColor0) * tmpvar_1.w) * 2.0);
}



#endif"
}

SubProgram "d3d11_9x " {
Keywords { "DIRECTIONAL_COOKIE" }
Bind "vertex" Vertex
Bind "normal" Normal
ConstBuffer "$Globals" 96 // 80 used size, 3 vars
Matrix 16 [_LightMatrix0] 4
ConstBuffer "UnityLighting" 720 // 16 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
ConstBuffer "UnityPerDraw" 336 // 320 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
Matrix 256 [_World2Object] 4
BindCB "$Globals" 0
BindCB "UnityLighting" 1
BindCB "UnityPerDraw" 2
// 20 instructions, 2 temp regs, 0 temp arrays:
// ALU 19 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0_level_9_1
eefiecedfilnannhiofohjbjkcjhedaomoilbacnabaaaaaageagaaaaaeaaaaaa
daaaaaaadiacaaaaimafaaaanmafaaaaebgpgodjaaacaaaaaaacaaaaaaacpopp
kiabaaaafiaaaaaaaeaaceaaaaaafeaaaaaafeaaaaaaceaaabaafeaaaaaaabaa
aeaaabaaaaaaaaaaabaaaaaaabaaafaaaaaaaaaaacaaaaaaaeaaagaaaaaaaaaa
acaaamaaaiaaakaaaaaaaaaaaaaaaaaaaaacpoppbpaaaaacafaaaaiaaaaaapja
bpaaaaacafaaabiaabaaapjaaiaaaaadaaaaabiaabaaoejaabaaoejaahaaaaac
aaaaabiaaaaaaaiaafaaaaadaaaaahoaaaaaaaiaabaaoejaabaaaaacaaaaapia
afaaoekaafaaaaadabaaahiaaaaaffiaapaaoekaaeaaaaaeabaaahiaaoaaoeka
aaaaaaiaabaaoeiaaeaaaaaeaaaaahiabaaaoekaaaaakkiaabaaoeiaaeaaaaae
abaaahoabbaaoekaaaaappiaaaaaoeiaafaaaaadaaaaapiaaaaaffjaalaaoeka
aeaaaaaeaaaaapiaakaaoekaaaaaaajaaaaaoeiaaeaaaaaeaaaaapiaamaaoeka
aaaakkjaaaaaoeiaaeaaaaaeaaaaapiaanaaoekaaaaappjaaaaaoeiaafaaaaad
abaaadiaaaaaffiaacaaoekaaeaaaaaeaaaaadiaabaaoekaaaaaaaiaabaaoeia
aeaaaaaeaaaaadiaadaaoekaaaaakkiaaaaaoeiaaeaaaaaeacaaadoaaeaaoeka
aaaappiaaaaaoeiaafaaaaadaaaaapiaaaaaffjaahaaoekaaeaaaaaeaaaaapia
agaaoekaaaaaaajaaaaaoeiaaeaaaaaeaaaaapiaaiaaoekaaaaakkjaaaaaoeia
aeaaaaaeaaaaapiaajaaoekaaaaappjaaaaaoeiaaeaaaaaeaaaaadmaaaaappia
aaaaoekaaaaaoeiaabaaaaacaaaaammaaaaaoeiappppaaaafdeieefcemadaaaa
eaaaabaandaaaaaafjaaaaaeegiocaaaaaaaaaaaafaaaaaafjaaaaaeegiocaaa
abaaaaaaabaaaaaafjaaaaaeegiocaaaacaaaaaabeaaaaaafpaaaaadpcbabaaa
aaaaaaaafpaaaaadhcbabaaaabaaaaaaghaaaaaepccabaaaaaaaaaaaabaaaaaa
gfaaaaadhccabaaaabaaaaaagfaaaaadhccabaaaacaaaaaagfaaaaaddccabaaa
adaaaaaagiaaaaacacaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaa
egiocaaaacaaaaaaabaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaa
aaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaa
egiocaaaacaaaaaaacaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaak
pccabaaaaaaaaaaaegiocaaaacaaaaaaadaaaaaapgbpbaaaaaaaaaaaegaobaaa
aaaaaaaabaaaaaahbcaabaaaaaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaa
eeaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaadiaaaaahhccabaaaabaaaaaa
agaabaaaaaaaaaaaegbcbaaaabaaaaaadiaaaaajhcaabaaaaaaaaaaafgifcaaa
abaaaaaaaaaaaaaaegiccaaaacaaaaaabbaaaaaadcaaaaalhcaabaaaaaaaaaaa
egiccaaaacaaaaaabaaaaaaaagiacaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaa
dcaaaaalhcaabaaaaaaaaaaaegiccaaaacaaaaaabcaaaaaakgikcaaaabaaaaaa
aaaaaaaaegacbaaaaaaaaaaadcaaaaalhccabaaaacaaaaaaegiccaaaacaaaaaa
bdaaaaaapgipcaaaabaaaaaaaaaaaaaaegacbaaaaaaaaaaadiaaaaaipcaabaaa
aaaaaaaafgbfbaaaaaaaaaaaegiocaaaacaaaaaaanaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaacaaaaaaamaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaaoaaaaaakgbkbaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaapaaaaaa
pgbpbaaaaaaaaaaaegaobaaaaaaaaaaadiaaaaaidcaabaaaabaaaaaafgafbaaa
aaaaaaaaegiacaaaaaaaaaaaacaaaaaadcaaaaakdcaabaaaaaaaaaaaegiacaaa
aaaaaaaaabaaaaaaagaabaaaaaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaa
aaaaaaaaegiacaaaaaaaaaaaadaaaaaakgakbaaaaaaaaaaaegaabaaaaaaaaaaa
dcaaaaakdccabaaaadaaaaaaegiacaaaaaaaaaaaaeaaaaaapgapbaaaaaaaaaaa
egaabaaaaaaaaaaadoaaaaabejfdeheoeiaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaaebaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaafaepfdejfeejepeoaaeoepfcenebemaaepfdeheo
iaaaaaaaaeaaaaaaaiaaaaaagiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaaheaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahaiaaaaheaaaaaa
abaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaheaaaaaaacaaaaaaaaaaaaaa
adaaaaaaadaaaaaaadamaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfcee
aaklklkl"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL_COOKIE" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal _glesNormal
in vec3 _glesNormal;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 324
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec2 _LightCoord;
};
#line 318
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _LightTexture0;
uniform highp mat4 _LightMatrix0;
uniform highp vec4 _LightColor0;
#line 332
#line 82
highp vec3 ObjSpaceLightDir( in highp vec4 v ) {
    highp vec3 objSpaceLightPos = (_World2Object * _WorldSpaceLightPos0).xyz;
    return objSpaceLightPos.xyz;
}
#line 332
vertex2fragment vertShadow( in vertexInput v ) {
    vertex2fragment o;
    o.pos = (glstate_matrix_mvp * v.vertex);
    #line 336
    o.normal = normalize(v.normal.xyz);
    o.lightDir = ObjSpaceLightDir( v.vertex);
    o._LightCoord = (_LightMatrix0 * (_Object2World * v.vertex)).xy;
    #line 340
    return o;
}
out highp vec3 xlv_TEXCOORD0;
out highp vec3 xlv_TEXCOORD1;
out highp vec2 xlv_TEXCOORD2;
void main() {
    vertex2fragment xl_retval;
    vertexInput xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vertShadow( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec3(xl_retval.normal);
    xlv_TEXCOORD1 = vec3(xl_retval.lightDir);
    xlv_TEXCOORD2 = vec2(xl_retval._LightCoord);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 324
struct vertex2fragment {
    highp vec4 pos;
    highp vec3 normal;
    highp vec3 lightDir;
    highp vec2 _LightCoord;
};
#line 318
struct vertexInput {
    highp vec4 vertex;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 315
uniform sampler2D _LightTexture0;
uniform highp mat4 _LightMatrix0;
uniform highp vec4 _LightColor0;
#line 332
#line 342
highp vec4 fragShadow( in vertex2fragment i ) {
    #line 344
    highp float NdotL = xll_saturate_f(dot( normalize(i.normal), normalize(i.lightDir)));
    return (((NdotL * _LightColor0) * (texture( _LightTexture0, i._LightCoord).w * 1.0)) * 2.0);
}
in highp vec3 xlv_TEXCOORD0;
in highp vec3 xlv_TEXCOORD1;
in highp vec2 xlv_TEXCOORD2;
void main() {
    highp vec4 xl_retval;
    vertex2fragment xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.normal = vec3(xlv_TEXCOORD0);
    xlt_i.lightDir = vec3(xlv_TEXCOORD1);
    xlt_i._LightCoord = vec2(xlv_TEXCOORD2);
    xl_retval = fragShadow( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

}
Program "fp" {
// Fragment combos: 5
//   opengl - ALU: 9 to 18, TEX: 0 to 2
//   d3d9 - ALU: 10 to 18, TEX: 1 to 2
//   d3d11 - ALU: 9 to 17, TEX: 0 to 2, FLOW: 1 to 1
//   d3d11_9x - ALU: 9 to 17, TEX: 0 to 2, FLOW: 1 to 1
SubProgram "opengl " {
Keywords { "POINT" }
Vector 0 [_LightColor0]
SetTexture 0 [_LightTexture0] 2D
"!!ARBfp1.0
OPTION ARB_precision_hint_fastest;
# 12 ALU, 1 TEX
PARAM c[2] = { program.local[0],
		{ 2 } };
TEMP R0;
TEMP R1;
DP3 R0.x, fragment.texcoord[2], fragment.texcoord[2];
DP3 R0.y, fragment.texcoord[1], fragment.texcoord[1];
RSQ R0.y, R0.y;
MUL R1.xyz, R0.y, fragment.texcoord[1];
TEX R0.w, R0.x, texture[0], 2D;
DP3 R0.x, fragment.texcoord[0], fragment.texcoord[0];
RSQ R0.x, R0.x;
MUL R0.xyz, R0.x, fragment.texcoord[0];
DP3_SAT R0.x, R0, R1;
MUL R1, R0.x, c[0];
MUL R0, R1, R0.w;
MUL result.color, R0, c[1].x;
END
# 12 instructions, 2 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "POINT" }
Vector 0 [_LightColor0]
SetTexture 0 [_LightTexture0] 2D
"ps_2_0
; 13 ALU, 1 TEX
dcl_2d s0
def c1, 2.00000000, 0, 0, 0
dcl t0.xyz
dcl t1.xyz
dcl t2.xyz
dp3 r0.x, t2, t2
mov r0.xy, r0.x
dp3 r1.x, t1, t1
rsq r1.x, r1.x
mul r1.xyz, r1.x, t1
texld r2, r0, s0
dp3 r0.x, t0, t0
rsq r0.x, r0.x
mul r0.xyz, r0.x, t0
dp3_sat r0.x, r0, r1
mul r0, r0.x, c0
mul r0, r0, r2.x
mul r0, r0, c1.x
mov oC0, r0
"
}

SubProgram "d3d11 " {
Keywords { "POINT" }
ConstBuffer "$Globals" 96 // 96 used size, 3 vars
Vector 80 [_LightColor0] 4
BindCB "$Globals" 0
SetTexture 0 [_LightTexture0] 2D 0
// 13 instructions, 2 temp regs, 0 temp arrays:
// ALU 11 float, 0 int, 0 uint
// TEX 1 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0
eefiecedenpoemaejjhdejfgdmeafcmgbgpdbdnkabaaaaaakmacaaaaadaaaaaa
cmaaaaaaleaaaaaaoiaaaaaaejfdeheoiaaaaaaaaeaaaaaaaiaaaaaagiaaaaaa
aaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaaheaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaaheaaaaaaabaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahahaaaaheaaaaaaacaaaaaaaaaaaaaaadaaaaaaadaaaaaaahahaaaafdfgfpfa
epfdejfeejepeoaafeeffiedepepfceeaaklklklepfdeheocmaaaaaaabaaaaaa
aiaaaaaacaaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfe
gbhcghgfheaaklklfdeieefclmabaaaaeaaaaaaagpaaaaaafjaaaaaeegiocaaa
aaaaaaaaagaaaaaafkaaaaadaagabaaaaaaaaaaafibiaaaeaahabaaaaaaaaaaa
ffffaaaagcbaaaadhcbabaaaabaaaaaagcbaaaadhcbabaaaacaaaaaagcbaaaad
hcbabaaaadaaaaaagfaaaaadpccabaaaaaaaaaaagiaaaaacacaaaaaabaaaaaah
bcaabaaaaaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaa
aaaaaaaaakaabaaaaaaaaaaadiaaaaahhcaabaaaaaaaaaaaagaabaaaaaaaaaaa
egbcbaaaabaaaaaabaaaaaahicaabaaaaaaaaaaaegbcbaaaacaaaaaaegbcbaaa
acaaaaaaeeaaaaaficaabaaaaaaaaaaadkaabaaaaaaaaaaadiaaaaahhcaabaaa
abaaaaaapgapbaaaaaaaaaaaegbcbaaaacaaaaaabacaaaahbcaabaaaaaaaaaaa
egacbaaaaaaaaaaaegacbaaaabaaaaaadiaaaaaipcaabaaaaaaaaaaaagaabaaa
aaaaaaaaegiocaaaaaaaaaaaafaaaaaabaaaaaahbcaabaaaabaaaaaaegbcbaaa
adaaaaaaegbcbaaaadaaaaaaefaaaaajpcaabaaaabaaaaaaagaabaaaabaaaaaa
eghobaaaaaaaaaaaaagabaaaaaaaaaaadiaaaaahpcaabaaaaaaaaaaaegaobaaa
aaaaaaaaagaabaaaabaaaaaaaaaaaaahpccabaaaaaaaaaaaegaobaaaaaaaaaaa
egaobaaaaaaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { "POINT" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "POINT" }
"!!GLES"
}

SubProgram "d3d11_9x " {
Keywords { "POINT" }
ConstBuffer "$Globals" 96 // 96 used size, 3 vars
Vector 80 [_LightColor0] 4
BindCB "$Globals" 0
SetTexture 0 [_LightTexture0] 2D 0
// 13 instructions, 2 temp regs, 0 temp arrays:
// ALU 11 float, 0 int, 0 uint
// TEX 1 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0_level_9_1
eefiecededannaoofckicanbbhfchjddajcfdiccabaaaaaakiadaaaaaeaaaaaa
daaaaaaaciabaaaaomacaaaaheadaaaaebgpgodjpaaaaaaapaaaaaaaaaacpppp
lmaaaaaadeaaaaaaabaaciaaaaaadeaaaaaadeaaabaaceaaaaaadeaaaaaaaaaa
aaaaafaaabaaaaaaaaaaaaaaaaacppppbpaaaaacaaaaaaiaaaaaahlabpaaaaac
aaaaaaiaabaaahlabpaaaaacaaaaaaiaacaaahlabpaaaaacaaaaaajaaaaiapka
ceaaaaacaaaaahiaaaaaoelaceaaaaacabaaahiaabaaoelaaiaaaaadaaaabbia
aaaaoeiaabaaoeiaafaaaaadaaaaapiaaaaaaaiaaaaaoekaaiaaaaadabaaadia
acaaoelaacaaoelaecaaaaadabaaapiaabaaoeiaaaaioekaafaaaaadaaaaapia
aaaaoeiaabaaaaiaacaaaaadaaaaapiaaaaaoeiaaaaaoeiaabaaaaacaaaiapia
aaaaoeiappppaaaafdeieefclmabaaaaeaaaaaaagpaaaaaafjaaaaaeegiocaaa
aaaaaaaaagaaaaaafkaaaaadaagabaaaaaaaaaaafibiaaaeaahabaaaaaaaaaaa
ffffaaaagcbaaaadhcbabaaaabaaaaaagcbaaaadhcbabaaaacaaaaaagcbaaaad
hcbabaaaadaaaaaagfaaaaadpccabaaaaaaaaaaagiaaaaacacaaaaaabaaaaaah
bcaabaaaaaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaa
aaaaaaaaakaabaaaaaaaaaaadiaaaaahhcaabaaaaaaaaaaaagaabaaaaaaaaaaa
egbcbaaaabaaaaaabaaaaaahicaabaaaaaaaaaaaegbcbaaaacaaaaaaegbcbaaa
acaaaaaaeeaaaaaficaabaaaaaaaaaaadkaabaaaaaaaaaaadiaaaaahhcaabaaa
abaaaaaapgapbaaaaaaaaaaaegbcbaaaacaaaaaabacaaaahbcaabaaaaaaaaaaa
egacbaaaaaaaaaaaegacbaaaabaaaaaadiaaaaaipcaabaaaaaaaaaaaagaabaaa
aaaaaaaaegiocaaaaaaaaaaaafaaaaaabaaaaaahbcaabaaaabaaaaaaegbcbaaa
adaaaaaaegbcbaaaadaaaaaaefaaaaajpcaabaaaabaaaaaaagaabaaaabaaaaaa
eghobaaaaaaaaaaaaagabaaaaaaaaaaadiaaaaahpcaabaaaaaaaaaaaegaobaaa
aaaaaaaaagaabaaaabaaaaaaaaaaaaahpccabaaaaaaaaaaaegaobaaaaaaaaaaa
egaobaaaaaaaaaaadoaaaaabejfdeheoiaaaaaaaaeaaaaaaaiaaaaaagiaaaaaa
aaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaaheaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaaheaaaaaaabaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahahaaaaheaaaaaaacaaaaaaaaaaaaaaadaaaaaaadaaaaaaahahaaaafdfgfpfa
epfdejfeejepeoaafeeffiedepepfceeaaklklklepfdeheocmaaaaaaabaaaaaa
aiaaaaaacaaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfe
gbhcghgfheaaklkl"
}

SubProgram "gles3 " {
Keywords { "POINT" }
"!!GLES3"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" }
Vector 0 [_LightColor0]
"!!ARBfp1.0
OPTION ARB_precision_hint_fastest;
# 9 ALU, 0 TEX
PARAM c[2] = { program.local[0],
		{ 2 } };
TEMP R0;
TEMP R1;
DP3 R0.y, fragment.texcoord[1], fragment.texcoord[1];
RSQ R0.y, R0.y;
DP3 R0.x, fragment.texcoord[0], fragment.texcoord[0];
MUL R1.xyz, R0.y, fragment.texcoord[1];
RSQ R0.x, R0.x;
MUL R0.xyz, R0.x, fragment.texcoord[0];
DP3_SAT R0.x, R0, R1;
MUL R0, R0.x, c[0];
MUL result.color, R0, c[1].x;
END
# 9 instructions, 2 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" }
Vector 0 [_LightColor0]
"ps_2_0
; 10 ALU
def c1, 2.00000000, 0, 0, 0
dcl t0.xyz
dcl t1.xyz
dp3 r1.x, t1, t1
dp3 r0.x, t0, t0
rsq r1.x, r1.x
rsq r0.x, r0.x
mul r0.xyz, r0.x, t0
mul r1.xyz, r1.x, t1
dp3_sat r0.x, r0, r1
mul r0, r0.x, c0
mul r0, r0, c1.x
mov oC0, r0
"
}

SubProgram "d3d11 " {
Keywords { "DIRECTIONAL" }
ConstBuffer "$Globals" 32 // 32 used size, 2 vars
Vector 16 [_LightColor0] 4
BindCB "$Globals" 0
// 10 instructions, 2 temp regs, 0 temp arrays:
// ALU 9 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0
eefiecedlnodboofillbhecipmdponfdgbaegfaeabaaaaaabaacaaaaadaaaaaa
cmaaaaaajmaaaaaanaaaaaaaejfdeheogiaaaaaaadaaaaaaaiaaaaaafaaaaaaa
aaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaafmaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaafmaaaaaaabaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahahaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfceeaaklklklepfdeheo
cmaaaaaaabaaaaaaaiaaaaaacaaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaa
apaaaaaafdfgfpfegbhcghgfheaaklklfdeieefcdiabaaaaeaaaaaaaeoaaaaaa
fjaaaaaeegiocaaaaaaaaaaaacaaaaaagcbaaaadhcbabaaaabaaaaaagcbaaaad
hcbabaaaacaaaaaagfaaaaadpccabaaaaaaaaaaagiaaaaacacaaaaaabaaaaaah
bcaabaaaaaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaa
aaaaaaaaakaabaaaaaaaaaaadiaaaaahhcaabaaaaaaaaaaaagaabaaaaaaaaaaa
egbcbaaaabaaaaaabaaaaaahicaabaaaaaaaaaaaegbcbaaaacaaaaaaegbcbaaa
acaaaaaaeeaaaaaficaabaaaaaaaaaaadkaabaaaaaaaaaaadiaaaaahhcaabaaa
abaaaaaapgapbaaaaaaaaaaaegbcbaaaacaaaaaabacaaaahbcaabaaaaaaaaaaa
egacbaaaaaaaaaaaegacbaaaabaaaaaadiaaaaaipcaabaaaaaaaaaaaagaabaaa
aaaaaaaaegiocaaaaaaaaaaaabaaaaaaaaaaaaahpccabaaaaaaaaaaaegaobaaa
aaaaaaaaegaobaaaaaaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" }
"!!GLES"
}

SubProgram "d3d11_9x " {
Keywords { "DIRECTIONAL" }
ConstBuffer "$Globals" 32 // 32 used size, 2 vars
Vector 16 [_LightColor0] 4
BindCB "$Globals" 0
// 10 instructions, 2 temp regs, 0 temp arrays:
// ALU 9 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0_level_9_1
eefiecedljdnmfcmphpkmfjghlmkibkkocejmaokabaaaaaamaacaaaaaeaaaaaa
daaaaaaanmaaaaaabmacaaaaimacaaaaebgpgodjkeaaaaaakeaaaaaaaaacpppp
heaaaaaadaaaaaaaabaaceaaaaaadaaaaaaadaaaaaaaceaaaaaadaaaaaaaabaa
abaaaaaaaaaaaaaaaaacppppbpaaaaacaaaaaaiaaaaaahlabpaaaaacaaaaaaia
abaaahlaceaaaaacaaaaahiaaaaaoelaceaaaaacabaaahiaabaaoelaaiaaaaad
aaaabbiaaaaaoeiaabaaoeiaafaaaaadaaaaapiaaaaaaaiaaaaaoekaacaaaaad
aaaaapiaaaaaoeiaaaaaoeiaabaaaaacaaaiapiaaaaaoeiappppaaaafdeieefc
diabaaaaeaaaaaaaeoaaaaaafjaaaaaeegiocaaaaaaaaaaaacaaaaaagcbaaaad
hcbabaaaabaaaaaagcbaaaadhcbabaaaacaaaaaagfaaaaadpccabaaaaaaaaaaa
giaaaaacacaaaaaabaaaaaahbcaabaaaaaaaaaaaegbcbaaaabaaaaaaegbcbaaa
abaaaaaaeeaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaadiaaaaahhcaabaaa
aaaaaaaaagaabaaaaaaaaaaaegbcbaaaabaaaaaabaaaaaahicaabaaaaaaaaaaa
egbcbaaaacaaaaaaegbcbaaaacaaaaaaeeaaaaaficaabaaaaaaaaaaadkaabaaa
aaaaaaaadiaaaaahhcaabaaaabaaaaaapgapbaaaaaaaaaaaegbcbaaaacaaaaaa
bacaaaahbcaabaaaaaaaaaaaegacbaaaaaaaaaaaegacbaaaabaaaaaadiaaaaai
pcaabaaaaaaaaaaaagaabaaaaaaaaaaaegiocaaaaaaaaaaaabaaaaaaaaaaaaah
pccabaaaaaaaaaaaegaobaaaaaaaaaaaegaobaaaaaaaaaaadoaaaaabejfdeheo
giaaaaaaadaaaaaaaiaaaaaafaaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaafmaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahahaaaafmaaaaaa
abaaaaaaaaaaaaaaadaaaaaaacaaaaaaahahaaaafdfgfpfaepfdejfeejepeoaa
feeffiedepepfceeaaklklklepfdeheocmaaaaaaabaaaaaaaiaaaaaacaaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfegbhcghgfheaaklkl
"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" }
"!!GLES3"
}

SubProgram "opengl " {
Keywords { "SPOT" }
Vector 0 [_LightColor0]
SetTexture 0 [_LightTexture0] 2D
SetTexture 1 [_LightTextureB0] 2D
"!!ARBfp1.0
OPTION ARB_precision_hint_fastest;
# 18 ALU, 2 TEX
PARAM c[2] = { program.local[0],
		{ 0, 0.5, 2 } };
TEMP R0;
TEMP R1;
DP3 R0.z, fragment.texcoord[2], fragment.texcoord[2];
RCP R0.x, fragment.texcoord[2].w;
MAD R0.xy, fragment.texcoord[2], R0.x, c[1].y;
TEX R0.w, R0, texture[0], 2D;
TEX R1.w, R0.z, texture[1], 2D;
DP3 R0.y, fragment.texcoord[1], fragment.texcoord[1];
RSQ R0.y, R0.y;
DP3 R0.x, fragment.texcoord[0], fragment.texcoord[0];
MUL R1.xyz, R0.y, fragment.texcoord[1];
RSQ R0.x, R0.x;
MUL R0.xyz, R0.x, fragment.texcoord[0];
DP3_SAT R0.x, R0, R1;
SLT R0.y, c[1].x, fragment.texcoord[2].z;
MUL R0.y, R0, R0.w;
MUL R1.x, R0.y, R1.w;
MUL R0, R0.x, c[0];
MUL R0, R0, R1.x;
MUL result.color, R0, c[1].z;
END
# 18 instructions, 2 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "SPOT" }
Vector 0 [_LightColor0]
SetTexture 0 [_LightTexture0] 2D
SetTexture 1 [_LightTextureB0] 2D
"ps_2_0
; 18 ALU, 2 TEX
dcl_2d s0
dcl_2d s1
def c1, 0.50000000, 0.00000000, 1.00000000, 2.00000000
dcl t0.xyz
dcl t1.xyz
dcl t2
rcp r1.x, t2.w
mad r2.xy, t2, r1.x, c1.x
dp3 r0.x, t2, t2
mov r1.xy, r0.x
texld r0, r2, s0
texld r2, r1, s1
dp3 r1.x, t1, t1
dp3 r0.x, t0, t0
rsq r1.x, r1.x
rsq r0.x, r0.x
mul r0.xyz, r0.x, t0
mul r1.xyz, r1.x, t1
dp3_sat r0.x, r0, r1
cmp r1.x, -t2.z, c1.y, c1.z
mul_pp r1.x, r1, r0.w
mul_pp r1.x, r1, r2
mul r0, r0.x, c0
mul r0, r0, r1.x
mul r0, r0, c1.w
mov oC0, r0
"
}

SubProgram "d3d11 " {
Keywords { "SPOT" }
ConstBuffer "$Globals" 96 // 96 used size, 3 vars
Vector 80 [_LightColor0] 4
BindCB "$Globals" 0
SetTexture 0 [_LightTexture0] 2D 0
SetTexture 1 [_LightTextureB0] 2D 1
// 20 instructions, 2 temp regs, 0 temp arrays:
// ALU 16 float, 0 int, 1 uint
// TEX 2 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0
eefiecedgcnjpcabedflagnonpbdmphmcjadjgfnabaaaaaakaadaaaaadaaaaaa
cmaaaaaaleaaaaaaoiaaaaaaejfdeheoiaaaaaaaaeaaaaaaaiaaaaaagiaaaaaa
aaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaaheaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaaheaaaaaaabaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahahaaaaheaaaaaaacaaaaaaaaaaaaaaadaaaaaaadaaaaaaapapaaaafdfgfpfa
epfdejfeejepeoaafeeffiedepepfceeaaklklklepfdeheocmaaaaaaabaaaaaa
aiaaaaaacaaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfe
gbhcghgfheaaklklfdeieefclaacaaaaeaaaaaaakmaaaaaafjaaaaaeegiocaaa
aaaaaaaaagaaaaaafkaaaaadaagabaaaaaaaaaaafkaaaaadaagabaaaabaaaaaa
fibiaaaeaahabaaaaaaaaaaaffffaaaafibiaaaeaahabaaaabaaaaaaffffaaaa
gcbaaaadhcbabaaaabaaaaaagcbaaaadhcbabaaaacaaaaaagcbaaaadpcbabaaa
adaaaaaagfaaaaadpccabaaaaaaaaaaagiaaaaacacaaaaaaaoaaaaahdcaabaaa
aaaaaaaaegbabaaaadaaaaaapgbpbaaaadaaaaaaaaaaaaakdcaabaaaaaaaaaaa
egaabaaaaaaaaaaaaceaaaaaaaaaaadpaaaaaadpaaaaaaaaaaaaaaaaefaaaaaj
pcaabaaaaaaaaaaaegaabaaaaaaaaaaaeghobaaaaaaaaaaaaagabaaaaaaaaaaa
dbaaaaahbcaabaaaaaaaaaaaabeaaaaaaaaaaaaackbabaaaadaaaaaaabaaaaah
bcaabaaaaaaaaaaaakaabaaaaaaaaaaaabeaaaaaaaaaiadpdiaaaaahbcaabaaa
aaaaaaaadkaabaaaaaaaaaaaakaabaaaaaaaaaaabaaaaaahccaabaaaaaaaaaaa
egbcbaaaadaaaaaaegbcbaaaadaaaaaaefaaaaajpcaabaaaabaaaaaafgafbaaa
aaaaaaaaeghobaaaabaaaaaaaagabaaaabaaaaaadiaaaaahbcaabaaaaaaaaaaa
akaabaaaaaaaaaaaakaabaaaabaaaaaabaaaaaahccaabaaaaaaaaaaaegbcbaaa
abaaaaaaegbcbaaaabaaaaaaeeaaaaafccaabaaaaaaaaaaabkaabaaaaaaaaaaa
diaaaaahocaabaaaaaaaaaaafgafbaaaaaaaaaaaagbjbaaaabaaaaaabaaaaaah
bcaabaaaabaaaaaaegbcbaaaacaaaaaaegbcbaaaacaaaaaaeeaaaaafbcaabaaa
abaaaaaaakaabaaaabaaaaaadiaaaaahhcaabaaaabaaaaaaagaabaaaabaaaaaa
egbcbaaaacaaaaaabacaaaahccaabaaaaaaaaaaajgahbaaaaaaaaaaaegacbaaa
abaaaaaadiaaaaaipcaabaaaabaaaaaafgafbaaaaaaaaaaaegiocaaaaaaaaaaa
afaaaaaadiaaaaahpcaabaaaaaaaaaaaagaabaaaaaaaaaaaegaobaaaabaaaaaa
aaaaaaahpccabaaaaaaaaaaaegaobaaaaaaaaaaaegaobaaaaaaaaaaadoaaaaab
"
}

SubProgram "gles " {
Keywords { "SPOT" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "SPOT" }
"!!GLES"
}

SubProgram "d3d11_9x " {
Keywords { "SPOT" }
ConstBuffer "$Globals" 96 // 96 used size, 3 vars
Vector 80 [_LightColor0] 4
BindCB "$Globals" 0
SetTexture 0 [_LightTexture0] 2D 0
SetTexture 1 [_LightTextureB0] 2D 1
// 20 instructions, 2 temp regs, 0 temp arrays:
// ALU 16 float, 0 int, 1 uint
// TEX 2 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0_level_9_1
eefiecedcoiphnmegklpeclapjolgdakbjagpdomabaaaaaaceafaaaaaeaaaaaa
daaaaaaalaabaaaagiaeaaaapaaeaaaaebgpgodjhiabaaaahiabaaaaaaacpppp
eaabaaaadiaaaaaaabaacmaaaaaadiaaaaaadiaaacaaceaaaaaadiaaaaaaaaaa
abababaaaaaaafaaabaaaaaaaaaaaaaaaaacppppfbaaaaafabaaapkaaaaaaadp
aaaaaaaaaaaaaaaaaaaaaaaabpaaaaacaaaaaaiaaaaaahlabpaaaaacaaaaaaia
abaaahlabpaaaaacaaaaaaiaacaaaplabpaaaaacaaaaaajaaaaiapkabpaaaaac
aaaaaajaabaiapkaagaaaaacaaaaaiiaacaapplaaeaaaaaeaaaaadiaacaaoela
aaaappiaabaaaakaaiaaaaadabaaaiiaacaaoelaacaaoelaabaaaaacabaaadia
abaappiaecaaaaadaaaacpiaaaaaoeiaaaaioekaecaaaaadabaacpiaabaaoeia
abaioekaafaaaaadaaaaabiaaaaappiaabaaaaiaceaaaaacabaaahiaaaaaoela
ceaaaaacacaaahiaabaaoelaaiaaaaadaaaabciaabaaoeiaacaaoeiaafaaaaad
abaaapiaaaaaffiaaaaaoekaafaaaaadaaaaapiaaaaaaaiaabaaoeiafiaaaaae
aaaaapiaacaakklbabaaffkaaaaaoeiaacaaaaadaaaaapiaaaaaoeiaaaaaoeia
abaaaaacaaaiapiaaaaaoeiappppaaaafdeieefclaacaaaaeaaaaaaakmaaaaaa
fjaaaaaeegiocaaaaaaaaaaaagaaaaaafkaaaaadaagabaaaaaaaaaaafkaaaaad
aagabaaaabaaaaaafibiaaaeaahabaaaaaaaaaaaffffaaaafibiaaaeaahabaaa
abaaaaaaffffaaaagcbaaaadhcbabaaaabaaaaaagcbaaaadhcbabaaaacaaaaaa
gcbaaaadpcbabaaaadaaaaaagfaaaaadpccabaaaaaaaaaaagiaaaaacacaaaaaa
aoaaaaahdcaabaaaaaaaaaaaegbabaaaadaaaaaapgbpbaaaadaaaaaaaaaaaaak
dcaabaaaaaaaaaaaegaabaaaaaaaaaaaaceaaaaaaaaaaadpaaaaaadpaaaaaaaa
aaaaaaaaefaaaaajpcaabaaaaaaaaaaaegaabaaaaaaaaaaaeghobaaaaaaaaaaa
aagabaaaaaaaaaaadbaaaaahbcaabaaaaaaaaaaaabeaaaaaaaaaaaaackbabaaa
adaaaaaaabaaaaahbcaabaaaaaaaaaaaakaabaaaaaaaaaaaabeaaaaaaaaaiadp
diaaaaahbcaabaaaaaaaaaaadkaabaaaaaaaaaaaakaabaaaaaaaaaaabaaaaaah
ccaabaaaaaaaaaaaegbcbaaaadaaaaaaegbcbaaaadaaaaaaefaaaaajpcaabaaa
abaaaaaafgafbaaaaaaaaaaaeghobaaaabaaaaaaaagabaaaabaaaaaadiaaaaah
bcaabaaaaaaaaaaaakaabaaaaaaaaaaaakaabaaaabaaaaaabaaaaaahccaabaaa
aaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaaeeaaaaafccaabaaaaaaaaaaa
bkaabaaaaaaaaaaadiaaaaahocaabaaaaaaaaaaafgafbaaaaaaaaaaaagbjbaaa
abaaaaaabaaaaaahbcaabaaaabaaaaaaegbcbaaaacaaaaaaegbcbaaaacaaaaaa
eeaaaaafbcaabaaaabaaaaaaakaabaaaabaaaaaadiaaaaahhcaabaaaabaaaaaa
agaabaaaabaaaaaaegbcbaaaacaaaaaabacaaaahccaabaaaaaaaaaaajgahbaaa
aaaaaaaaegacbaaaabaaaaaadiaaaaaipcaabaaaabaaaaaafgafbaaaaaaaaaaa
egiocaaaaaaaaaaaafaaaaaadiaaaaahpcaabaaaaaaaaaaaagaabaaaaaaaaaaa
egaobaaaabaaaaaaaaaaaaahpccabaaaaaaaaaaaegaobaaaaaaaaaaaegaobaaa
aaaaaaaadoaaaaabejfdeheoiaaaaaaaaeaaaaaaaiaaaaaagiaaaaaaaaaaaaaa
abaaaaaaadaaaaaaaaaaaaaaapaaaaaaheaaaaaaaaaaaaaaaaaaaaaaadaaaaaa
abaaaaaaahahaaaaheaaaaaaabaaaaaaaaaaaaaaadaaaaaaacaaaaaaahahaaaa
heaaaaaaacaaaaaaaaaaaaaaadaaaaaaadaaaaaaapapaaaafdfgfpfaepfdejfe
ejepeoaafeeffiedepepfceeaaklklklepfdeheocmaaaaaaabaaaaaaaiaaaaaa
caaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfegbhcghgf
heaaklkl"
}

SubProgram "gles3 " {
Keywords { "SPOT" }
"!!GLES3"
}

SubProgram "opengl " {
Keywords { "POINT_COOKIE" }
Vector 0 [_LightColor0]
SetTexture 0 [_LightTextureB0] 2D
SetTexture 1 [_LightTexture0] CUBE
"!!ARBfp1.0
OPTION ARB_precision_hint_fastest;
# 14 ALU, 2 TEX
PARAM c[2] = { program.local[0],
		{ 2 } };
TEMP R0;
TEMP R1;
TEX R1.w, fragment.texcoord[2], texture[1], CUBE;
DP3 R0.x, fragment.texcoord[2], fragment.texcoord[2];
DP3 R0.y, fragment.texcoord[1], fragment.texcoord[1];
RSQ R0.y, R0.y;
MUL R1.xyz, R0.y, fragment.texcoord[1];
TEX R0.w, R0.x, texture[0], 2D;
DP3 R0.x, fragment.texcoord[0], fragment.texcoord[0];
RSQ R0.x, R0.x;
MUL R0.xyz, R0.x, fragment.texcoord[0];
DP3_SAT R0.x, R0, R1;
MUL R1.x, R0.w, R1.w;
MUL R0, R0.x, c[0];
MUL R0, R0, R1.x;
MUL result.color, R0, c[1].x;
END
# 14 instructions, 2 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "POINT_COOKIE" }
Vector 0 [_LightColor0]
SetTexture 0 [_LightTextureB0] 2D
SetTexture 1 [_LightTexture0] CUBE
"ps_2_0
; 14 ALU, 2 TEX
dcl_2d s0
dcl_cube s1
def c1, 2.00000000, 0, 0, 0
dcl t0.xyz
dcl t1.xyz
dcl t2.xyz
dp3 r0.x, t2, t2
mov r0.xy, r0.x
dp3 r1.x, t1, t1
rsq r1.x, r1.x
mul r1.xyz, r1.x, t1
texld r2, r0, s0
texld r0, t2, s1
dp3 r0.x, t0, t0
rsq r0.x, r0.x
mul r0.xyz, r0.x, t0
dp3_sat r1.x, r0, r1
mul r0.x, r2, r0.w
mul r1, r1.x, c0
mul r0, r1, r0.x
mul r0, r0, c1.x
mov oC0, r0
"
}

SubProgram "d3d11 " {
Keywords { "POINT_COOKIE" }
ConstBuffer "$Globals" 96 // 96 used size, 3 vars
Vector 80 [_LightColor0] 4
BindCB "$Globals" 0
SetTexture 0 [_LightTextureB0] 2D 1
SetTexture 1 [_LightTexture0] CUBE 0
// 15 instructions, 3 temp regs, 0 temp arrays:
// ALU 12 float, 0 int, 0 uint
// TEX 2 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0
eefiecedngnpdcmkeokddnkgcmhkgmdoigigajipabaaaaaaaiadaaaaadaaaaaa
cmaaaaaaleaaaaaaoiaaaaaaejfdeheoiaaaaaaaaeaaaaaaaiaaaaaagiaaaaaa
aaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaaheaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaaheaaaaaaabaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahahaaaaheaaaaaaacaaaaaaaaaaaaaaadaaaaaaadaaaaaaahahaaaafdfgfpfa
epfdejfeejepeoaafeeffiedepepfceeaaklklklepfdeheocmaaaaaaabaaaaaa
aiaaaaaacaaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfe
gbhcghgfheaaklklfdeieefcbiacaaaaeaaaaaaaigaaaaaafjaaaaaeegiocaaa
aaaaaaaaagaaaaaafkaaaaadaagabaaaaaaaaaaafkaaaaadaagabaaaabaaaaaa
fibiaaaeaahabaaaaaaaaaaaffffaaaafidaaaaeaahabaaaabaaaaaaffffaaaa
gcbaaaadhcbabaaaabaaaaaagcbaaaadhcbabaaaacaaaaaagcbaaaadhcbabaaa
adaaaaaagfaaaaadpccabaaaaaaaaaaagiaaaaacadaaaaaabaaaaaahbcaabaaa
aaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaaaaaaaaaa
akaabaaaaaaaaaaadiaaaaahhcaabaaaaaaaaaaaagaabaaaaaaaaaaaegbcbaaa
abaaaaaabaaaaaahicaabaaaaaaaaaaaegbcbaaaacaaaaaaegbcbaaaacaaaaaa
eeaaaaaficaabaaaaaaaaaaadkaabaaaaaaaaaaadiaaaaahhcaabaaaabaaaaaa
pgapbaaaaaaaaaaaegbcbaaaacaaaaaabacaaaahbcaabaaaaaaaaaaaegacbaaa
aaaaaaaaegacbaaaabaaaaaadiaaaaaipcaabaaaaaaaaaaaagaabaaaaaaaaaaa
egiocaaaaaaaaaaaafaaaaaabaaaaaahbcaabaaaabaaaaaaegbcbaaaadaaaaaa
egbcbaaaadaaaaaaefaaaaajpcaabaaaabaaaaaaagaabaaaabaaaaaaeghobaaa
aaaaaaaaaagabaaaabaaaaaaefaaaaajpcaabaaaacaaaaaaegbcbaaaadaaaaaa
eghobaaaabaaaaaaaagabaaaaaaaaaaadiaaaaahbcaabaaaabaaaaaaakaabaaa
abaaaaaadkaabaaaacaaaaaadiaaaaahpcaabaaaaaaaaaaaegaobaaaaaaaaaaa
agaabaaaabaaaaaaaaaaaaahpccabaaaaaaaaaaaegaobaaaaaaaaaaaegaobaaa
aaaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { "POINT_COOKIE" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "POINT_COOKIE" }
"!!GLES"
}

SubProgram "d3d11_9x " {
Keywords { "POINT_COOKIE" }
ConstBuffer "$Globals" 96 // 96 used size, 3 vars
Vector 80 [_LightColor0] 4
BindCB "$Globals" 0
SetTexture 0 [_LightTextureB0] 2D 1
SetTexture 1 [_LightTexture0] CUBE 0
// 15 instructions, 3 temp regs, 0 temp arrays:
// ALU 12 float, 0 int, 0 uint
// TEX 2 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0_level_9_1
eefiecedkijddfbknpgnmgfpibkjfncdelklmielabaaaaaaeaaeaaaaaeaaaaaa
daaaaaaageabaaaaieadaaaaamaeaaaaebgpgodjcmabaaaacmabaaaaaaacpppp
peaaaaaadiaaaaaaabaacmaaaaaadiaaaaaadiaaacaaceaaaaaadiaaabaaaaaa
aaababaaaaaaafaaabaaaaaaaaaaaaaaaaacppppbpaaaaacaaaaaaiaaaaaahla
bpaaaaacaaaaaaiaabaaahlabpaaaaacaaaaaaiaacaaahlabpaaaaacaaaaaaji
aaaiapkabpaaaaacaaaaaajaabaiapkaaiaaaaadaaaaaiiaacaaoelaacaaoela
abaaaaacaaaaadiaaaaappiaecaaaaadaaaaapiaaaaaoeiaabaioekaecaaaaad
abaaapiaacaaoelaaaaioekaafaaaaadaaaaabiaaaaaaaiaabaappiaceaaaaac
abaaahiaaaaaoelaceaaaaacacaaahiaabaaoelaaiaaaaadaaaabciaabaaoeia
acaaoeiaafaaaaadabaaapiaaaaaffiaaaaaoekaafaaaaadaaaaapiaaaaaaaia
abaaoeiaacaaaaadaaaaapiaaaaaoeiaaaaaoeiaabaaaaacaaaiapiaaaaaoeia
ppppaaaafdeieefcbiacaaaaeaaaaaaaigaaaaaafjaaaaaeegiocaaaaaaaaaaa
agaaaaaafkaaaaadaagabaaaaaaaaaaafkaaaaadaagabaaaabaaaaaafibiaaae
aahabaaaaaaaaaaaffffaaaafidaaaaeaahabaaaabaaaaaaffffaaaagcbaaaad
hcbabaaaabaaaaaagcbaaaadhcbabaaaacaaaaaagcbaaaadhcbabaaaadaaaaaa
gfaaaaadpccabaaaaaaaaaaagiaaaaacadaaaaaabaaaaaahbcaabaaaaaaaaaaa
egbcbaaaabaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaaaaaaaaaaakaabaaa
aaaaaaaadiaaaaahhcaabaaaaaaaaaaaagaabaaaaaaaaaaaegbcbaaaabaaaaaa
baaaaaahicaabaaaaaaaaaaaegbcbaaaacaaaaaaegbcbaaaacaaaaaaeeaaaaaf
icaabaaaaaaaaaaadkaabaaaaaaaaaaadiaaaaahhcaabaaaabaaaaaapgapbaaa
aaaaaaaaegbcbaaaacaaaaaabacaaaahbcaabaaaaaaaaaaaegacbaaaaaaaaaaa
egacbaaaabaaaaaadiaaaaaipcaabaaaaaaaaaaaagaabaaaaaaaaaaaegiocaaa
aaaaaaaaafaaaaaabaaaaaahbcaabaaaabaaaaaaegbcbaaaadaaaaaaegbcbaaa
adaaaaaaefaaaaajpcaabaaaabaaaaaaagaabaaaabaaaaaaeghobaaaaaaaaaaa
aagabaaaabaaaaaaefaaaaajpcaabaaaacaaaaaaegbcbaaaadaaaaaaeghobaaa
abaaaaaaaagabaaaaaaaaaaadiaaaaahbcaabaaaabaaaaaaakaabaaaabaaaaaa
dkaabaaaacaaaaaadiaaaaahpcaabaaaaaaaaaaaegaobaaaaaaaaaaaagaabaaa
abaaaaaaaaaaaaahpccabaaaaaaaaaaaegaobaaaaaaaaaaaegaobaaaaaaaaaaa
doaaaaabejfdeheoiaaaaaaaaeaaaaaaaiaaaaaagiaaaaaaaaaaaaaaabaaaaaa
adaaaaaaaaaaaaaaapaaaaaaheaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaa
ahahaaaaheaaaaaaabaaaaaaaaaaaaaaadaaaaaaacaaaaaaahahaaaaheaaaaaa
acaaaaaaaaaaaaaaadaaaaaaadaaaaaaahahaaaafdfgfpfaepfdejfeejepeoaa
feeffiedepepfceeaaklklklepfdeheocmaaaaaaabaaaaaaaiaaaaaacaaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfegbhcghgfheaaklkl
"
}

SubProgram "gles3 " {
Keywords { "POINT_COOKIE" }
"!!GLES3"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL_COOKIE" }
Vector 0 [_LightColor0]
SetTexture 0 [_LightTexture0] 2D
"!!ARBfp1.0
OPTION ARB_precision_hint_fastest;
# 11 ALU, 1 TEX
PARAM c[2] = { program.local[0],
		{ 2 } };
TEMP R0;
TEMP R1;
TEX R0.w, fragment.texcoord[2], texture[0], 2D;
DP3 R0.y, fragment.texcoord[1], fragment.texcoord[1];
RSQ R0.y, R0.y;
DP3 R0.x, fragment.texcoord[0], fragment.texcoord[0];
MUL R1.xyz, R0.y, fragment.texcoord[1];
RSQ R0.x, R0.x;
MUL R0.xyz, R0.x, fragment.texcoord[0];
DP3_SAT R0.x, R0, R1;
MUL R1, R0.x, c[0];
MUL R0, R1, R0.w;
MUL result.color, R0, c[1].x;
END
# 11 instructions, 2 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL_COOKIE" }
Vector 0 [_LightColor0]
SetTexture 0 [_LightTexture0] 2D
"ps_2_0
; 11 ALU, 1 TEX
dcl_2d s0
def c1, 2.00000000, 0, 0, 0
dcl t0.xyz
dcl t1.xyz
dcl t2.xy
texld r0, t2, s0
dp3 r1.x, t1, t1
dp3 r0.x, t0, t0
rsq r1.x, r1.x
rsq r0.x, r0.x
mul r0.xyz, r0.x, t0
mul r1.xyz, r1.x, t1
dp3_sat r0.x, r0, r1
mul r1, r0.x, c0
mul r0, r1, r0.w
mul r0, r0, c1.x
mov oC0, r0
"
}

SubProgram "d3d11 " {
Keywords { "DIRECTIONAL_COOKIE" }
ConstBuffer "$Globals" 96 // 96 used size, 3 vars
Vector 80 [_LightColor0] 4
BindCB "$Globals" 0
SetTexture 0 [_LightTexture0] 2D 0
// 12 instructions, 2 temp regs, 0 temp arrays:
// ALU 10 float, 0 int, 0 uint
// TEX 1 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0
eefieceddidccfhjoedlfmjglcjlcnkfffmlmfjjabaaaaaajaacaaaaadaaaaaa
cmaaaaaaleaaaaaaoiaaaaaaejfdeheoiaaaaaaaaeaaaaaaaiaaaaaagiaaaaaa
aaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaaheaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaahahaaaaheaaaaaaabaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahahaaaaheaaaaaaacaaaaaaaaaaaaaaadaaaaaaadaaaaaaadadaaaafdfgfpfa
epfdejfeejepeoaafeeffiedepepfceeaaklklklepfdeheocmaaaaaaabaaaaaa
aiaaaaaacaaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfe
gbhcghgfheaaklklfdeieefckaabaaaaeaaaaaaagiaaaaaafjaaaaaeegiocaaa
aaaaaaaaagaaaaaafkaaaaadaagabaaaaaaaaaaafibiaaaeaahabaaaaaaaaaaa
ffffaaaagcbaaaadhcbabaaaabaaaaaagcbaaaadhcbabaaaacaaaaaagcbaaaad
dcbabaaaadaaaaaagfaaaaadpccabaaaaaaaaaaagiaaaaacacaaaaaabaaaaaah
bcaabaaaaaaaaaaaegbcbaaaabaaaaaaegbcbaaaabaaaaaaeeaaaaafbcaabaaa
aaaaaaaaakaabaaaaaaaaaaadiaaaaahhcaabaaaaaaaaaaaagaabaaaaaaaaaaa
egbcbaaaabaaaaaabaaaaaahicaabaaaaaaaaaaaegbcbaaaacaaaaaaegbcbaaa
acaaaaaaeeaaaaaficaabaaaaaaaaaaadkaabaaaaaaaaaaadiaaaaahhcaabaaa
abaaaaaapgapbaaaaaaaaaaaegbcbaaaacaaaaaabacaaaahbcaabaaaaaaaaaaa
egacbaaaaaaaaaaaegacbaaaabaaaaaadiaaaaaipcaabaaaaaaaaaaaagaabaaa
aaaaaaaaegiocaaaaaaaaaaaafaaaaaaefaaaaajpcaabaaaabaaaaaaegbabaaa
adaaaaaaeghobaaaaaaaaaaaaagabaaaaaaaaaaadiaaaaahpcaabaaaaaaaaaaa
egaobaaaaaaaaaaapgapbaaaabaaaaaaaaaaaaahpccabaaaaaaaaaaaegaobaaa
aaaaaaaaegaobaaaaaaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL_COOKIE" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL_COOKIE" }
"!!GLES"
}

SubProgram "d3d11_9x " {
Keywords { "DIRECTIONAL_COOKIE" }
ConstBuffer "$Globals" 96 // 96 used size, 3 vars
Vector 80 [_LightColor0] 4
BindCB "$Globals" 0
SetTexture 0 [_LightTexture0] 2D 0
// 12 instructions, 2 temp regs, 0 temp arrays:
// ALU 10 float, 0 int, 0 uint
// TEX 1 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0_level_9_1
eefiecedocpfdodjcclmanodeajphfpilfmebpaoabaaaaaahmadaaaaaeaaaaaa
daaaaaaabiabaaaamaacaaaaeiadaaaaebgpgodjoaaaaaaaoaaaaaaaaaacpppp
kmaaaaaadeaaaaaaabaaciaaaaaadeaaaaaadeaaabaaceaaaaaadeaaaaaaaaaa
aaaaafaaabaaaaaaaaaaaaaaaaacppppbpaaaaacaaaaaaiaaaaaahlabpaaaaac
aaaaaaiaabaaahlabpaaaaacaaaaaaiaacaaadlabpaaaaacaaaaaajaaaaiapka
ecaaaaadaaaaapiaacaaoelaaaaioekaceaaaaacaaaaahiaaaaaoelaceaaaaac
abaaahiaabaaoelaaiaaaaadaaaabbiaaaaaoeiaabaaoeiaafaaaaadabaaapia
aaaaaaiaaaaaoekaafaaaaadaaaaapiaaaaappiaabaaoeiaacaaaaadaaaaapia
aaaaoeiaaaaaoeiaabaaaaacaaaiapiaaaaaoeiappppaaaafdeieefckaabaaaa
eaaaaaaagiaaaaaafjaaaaaeegiocaaaaaaaaaaaagaaaaaafkaaaaadaagabaaa
aaaaaaaafibiaaaeaahabaaaaaaaaaaaffffaaaagcbaaaadhcbabaaaabaaaaaa
gcbaaaadhcbabaaaacaaaaaagcbaaaaddcbabaaaadaaaaaagfaaaaadpccabaaa
aaaaaaaagiaaaaacacaaaaaabaaaaaahbcaabaaaaaaaaaaaegbcbaaaabaaaaaa
egbcbaaaabaaaaaaeeaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaadiaaaaah
hcaabaaaaaaaaaaaagaabaaaaaaaaaaaegbcbaaaabaaaaaabaaaaaahicaabaaa
aaaaaaaaegbcbaaaacaaaaaaegbcbaaaacaaaaaaeeaaaaaficaabaaaaaaaaaaa
dkaabaaaaaaaaaaadiaaaaahhcaabaaaabaaaaaapgapbaaaaaaaaaaaegbcbaaa
acaaaaaabacaaaahbcaabaaaaaaaaaaaegacbaaaaaaaaaaaegacbaaaabaaaaaa
diaaaaaipcaabaaaaaaaaaaaagaabaaaaaaaaaaaegiocaaaaaaaaaaaafaaaaaa
efaaaaajpcaabaaaabaaaaaaegbabaaaadaaaaaaeghobaaaaaaaaaaaaagabaaa
aaaaaaaadiaaaaahpcaabaaaaaaaaaaaegaobaaaaaaaaaaapgapbaaaabaaaaaa
aaaaaaahpccabaaaaaaaaaaaegaobaaaaaaaaaaaegaobaaaaaaaaaaadoaaaaab
ejfdeheoiaaaaaaaaeaaaaaaaiaaaaaagiaaaaaaaaaaaaaaabaaaaaaadaaaaaa
aaaaaaaaapaaaaaaheaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaahahaaaa
heaaaaaaabaaaaaaaaaaaaaaadaaaaaaacaaaaaaahahaaaaheaaaaaaacaaaaaa
aaaaaaaaadaaaaaaadaaaaaaadadaaaafdfgfpfaepfdejfeejepeoaafeeffied
epepfceeaaklklklepfdeheocmaaaaaaabaaaaaaaiaaaaaacaaaaaaaaaaaaaaa
aaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfegbhcghgfheaaklkl"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL_COOKIE" }
"!!GLES3"
}

}

#LINE 176

		}
		
		UsePass "VertexLit/SHADOWCOLLECTOR"
		UsePass "VertexLit/SHADOWCASTER"
	}
	
	//FallBack "Diffuse"
}