﻿Shader "Mithril/WaterShader"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		
		_DepthFactor ("DepthFactor", float) = 0.9
		_Scale ("Scale", float) = 0.8
	}
	SubShader
	{
		Tags { "Queue"="Transparent-2" "RenderType"="Transparent" "LightMode" = "ForwardBase" }
		
		Pass
		{
        	Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Lighting On
			
			Program "vp" {
// Vertex combos: 6
//   opengl - ALU: 55 to 57
//   d3d9 - ALU: 52 to 54
//   d3d11 - ALU: 33 to 34, TEX: 0 to 0, FLOW: 1 to 1
//   d3d11_9x - ALU: 33 to 34, TEX: 0 to 0, FLOW: 1 to 1
SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Vector 9 [_Time]
Vector 10 [_ProjectionParams]
Vector 11 [_WorldSpaceLightPos0]
Vector 12 [unity_SHAr]
Vector 13 [unity_SHAg]
Vector 14 [unity_SHAb]
Vector 15 [unity_SHBr]
Vector 16 [unity_SHBg]
Vector 17 [unity_SHBb]
Vector 18 [unity_SHC]
Matrix 5 [_Object2World]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"!!ARBvp1.0
# 55 ALU
PARAM c[25] = { { 0.79577452, 0, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..20],
		{ 24.980801, -24.980801, 0.25 },
		{ -60.145809, 60.145809, 85.453789, -85.453789 },
		{ -64.939346, 64.939346, 19.73921, -19.73921 },
		{ -1, 1, -9, 0.75 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
MOV R0.x, c[0];
MUL R0.x, R0, c[9];
ADD R0.x, R0, -c[21].z;
FRC R2.w, R0.x;
ADD R0.xyz, -R2.w, c[0].yzww;
MUL R2.xyz, R0, R0;
MUL R0.xyz, R2, c[21].xyxw;
ADD R0.xyz, R0, c[22].xyxw;
MAD R1.xyz, R0, R2, c[22].zwzw;
MAD R3.xyz, R1, R2, c[23].xyxw;
MAD R3.xyz, R3, R2, c[23].zwzw;
MAD R3.xyz, R3, R2, c[24].xyxw;
MOV R1.w, c[0];
MOV R0.z, c[7].y;
MOV R0.x, c[5].y;
MOV R0.y, c[6];
DP3 R0.w, R0, R0;
RSQ R0.w, R0.w;
MUL R1.xyz, R0.w, R0;
MUL R0, R1.xyzz, R1.yzzx;
DP4 R4.z, R1, c[14];
DP4 R4.y, R1, c[13];
DP4 R4.x, R1, c[12];
DP4 R5.z, R0, c[17];
DP4 R5.x, R0, c[15];
DP4 R5.y, R0, c[16];
ADD R0.xyz, R4, R5;
MUL R0.w, R1.y, R1.y;
MAD R0.w, R1.x, R1.x, -R0;
DP3 R1.x, R1, c[11];
MAX R1.x, R1, c[0].y;
SLT R4.x, R2.w, c[21].z;
SGE R4.yz, R2.w, c[24].xzww;
MOV R2.xz, R4;
DP3 R2.y, R4, c[24].xyxw;
DP3 R1.w, R3, -R2;
MUL R2.xyz, R0.w, c[18];
ADD result.color.secondary.xyz, R0, R2;
MUL R0.w, R1, c[0].z;
ABS R2.w, R0;
DP4 R0.z, vertex.position, c[7];
MAD R3.y, R0.z, c[20].x, vertex.texcoord[0];
DP4 R1.w, vertex.position, c[5];
MAD R3.x, R1.w, c[20], vertex.texcoord[0];
DP4 R0.w, vertex.position, c[4];
DP4 R0.z, vertex.position, c[3];
DP4 R0.x, vertex.position, c[1];
DP4 R0.y, vertex.position, c[2];
MUL R2.xyz, R0.xyww, c[0].z;
MUL R2.y, R2, c[10].x;
ADD result.texcoord[0].xy, R2.w, R3;
ADD result.texcoord[1].xy, R2, R2.z;
MOV result.position, R0;
MUL result.color.xyz, R1.x, c[19];
MOV result.texcoord[1].zw, R0;
END
# 55 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 0 [glstate_matrix_mvp]
Vector 8 [_Time]
Vector 9 [_ProjectionParams]
Vector 10 [_ScreenParams]
Vector 11 [_WorldSpaceLightPos0]
Vector 12 [unity_SHAr]
Vector 13 [unity_SHAg]
Vector 14 [unity_SHAb]
Vector 15 [unity_SHBr]
Vector 16 [unity_SHBg]
Vector 17 [unity_SHBb]
Vector 18 [unity_SHC]
Matrix 4 [_Object2World]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"vs_2_0
; 52 ALU
def c21, -0.02083333, -0.12500000, 1.00000000, 0.50000000
def c22, -0.00000155, -0.00002170, 0.00260417, 0.00026042
def c23, 0.00000000, 0.79577452, 0.50000000, 0
def c24, 6.28318501, -3.14159298, 0, 0
dcl_position0 v0
dcl_texcoord0 v1
dp4 r2.w, v0, c3
mov r0.z, c6.y
mov r0.x, c4.y
mov r0.y, c5
dp3 r0.w, r0, r0
rsq r0.w, r0.w
mul r0.xyz, r0.w, r0
mul r0.w, r0.y, r0.y
mad r1.x, r0, r0, -r0.w
mov r0.w, c21.z
mul r2.xyz, r1.x, c18
mul r1, r0.xyzz, r0.yzzx
dp4 r3.z, r0, c14
dp4 r3.y, r0, c13
dp4 r3.x, r0, c12
mov r0.w, c8.x
mad r0.w, r0, c23.y, c23.z
frc r0.w, r0
dp3 r0.y, r0, c11
dp4 r4.z, r1, c17
dp4 r4.x, r1, c15
dp4 r4.y, r1, c16
add r1.xyz, r3, r4
add oD1.xyz, r1, r2
dp4 r2.z, v0, c2
dp4 r2.x, v0, c0
dp4 r2.y, v0, c1
mul r1.xyz, r2.xyww, c21.w
mul r1.y, r1, c9.x
mad r0.w, r0, c24.x, c24.y
mad oT1.xy, r1.z, c10.zwzw, r1
sincos r1.xy, r0.w, c22.xyzw, c21.xyzw
max r0.w, r0.y, c23.x
mul r0.x, r1.y, c21.w
abs r0.z, r0.x
dp4 r0.y, v0, c6
dp4 r0.x, v0, c4
mad r0.y, r0, c20.x, v1
mad r0.x, r0, c20, v1
mov oPos, r2
add oT0.xy, r0.z, r0
mul oD0.xyz, r0.w, c19
mov oT1.zw, r2
"
}

SubProgram "d3d11 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
ConstBuffer "$Globals" 64 // 52 used size, 5 vars
Vector 16 [_LightColor0] 4
Float 48 [_Scale]
ConstBuffer "UnityPerCamera" 128 // 96 used size, 8 vars
Vector 0 [_Time] 4
Vector 80 [_ProjectionParams] 4
ConstBuffer "UnityLighting" 720 // 720 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
Vector 608 [unity_SHAr] 4
Vector 624 [unity_SHAg] 4
Vector 640 [unity_SHAb] 4
Vector 656 [unity_SHBr] 4
Vector 672 [unity_SHBg] 4
Vector 688 [unity_SHBb] 4
Vector 704 [unity_SHC] 4
ConstBuffer "UnityPerDraw" 336 // 256 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
BindCB "UnityLighting" 2
BindCB "UnityPerDraw" 3
// 37 instructions, 4 temp regs, 0 temp arrays:
// ALU 33 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0
eefiecedmmjogfpagdenonoikogclepdoekejgkaabaaaaaahmagaaaaadaaaaaa
cmaaaaaakaaaaaaaeeabaaaaejfdeheogmaaaaaaadaaaaaaaiaaaaaafaaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaafjaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaadadaaaagcaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahaaaaaafaepfdejfeejepeoaafeeffiedepepfceeaaeoepfcenebemaaklklkl
epfdeheojmaaaaaaafaaaaaaaiaaaaaaiaaaaaaaaaaaaaaaabaaaaaaadaaaaaa
aaaaaaaaapaaaaaaimaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaadamaaaa
jfaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaimaaaaaaabaaaaaa
aaaaaaaaadaaaaaaadaaaaaaapaaaaaajfaaaaaaabaaaaaaaaaaaaaaadaaaaaa
aeaaaaaaahaiaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfceeaaedepem
epfcaaklfdeieefcdaafaaaaeaaaabaaemabaaaafjaaaaaeegiocaaaaaaaaaaa
aeaaaaaafjaaaaaeegiocaaaabaaaaaaagaaaaaafjaaaaaeegiocaaaacaaaaaa
cnaaaaaafjaaaaaeegiocaaaadaaaaaabaaaaaaafpaaaaadpcbabaaaaaaaaaaa
fpaaaaaddcbabaaaabaaaaaaghaaaaaepccabaaaaaaaaaaaabaaaaaagfaaaaad
dccabaaaabaaaaaagfaaaaadhccabaaaacaaaaaagfaaaaadpccabaaaadaaaaaa
gfaaaaadhccabaaaaeaaaaaagiaaaaacaeaaaaaadiaaaaaipcaabaaaaaaaaaaa
fgbfbaaaaaaaaaaaegiocaaaadaaaaaaabaaaaaadcaaaaakpcaabaaaaaaaaaaa
egiocaaaadaaaaaaaaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaak
pcaabaaaaaaaaaaaegiocaaaadaaaaaaacaaaaaakgbkbaaaaaaaaaaaegaobaaa
aaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaadaaaaaaadaaaaaapgbpbaaa
aaaaaaaaegaobaaaaaaaaaaadgaaaaafpccabaaaaaaaaaaaegaobaaaaaaaaaaa
diaaaaaidcaabaaaabaaaaaafgbfbaaaaaaaaaaaigiacaaaadaaaaaaanaaaaaa
dcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaamaaaaaaagbabaaaaaaaaaaa
egaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaaoaaaaaa
kgbkbaaaaaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaa
adaaaaaaapaaaaaapgbpbaaaaaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaa
abaaaaaaegaabaaaabaaaaaaagiacaaaaaaaaaaaadaaaaaaegbabaaaabaaaaaa
diaaaaaiecaabaaaabaaaaaaakiacaaaabaaaaaaaaaaaaaaabeaaaaaaaaakaea
enaaaaagecaabaaaabaaaaaaaanaaaaackaabaaaabaaaaaadiaaaaahecaabaaa
abaaaaaackaabaaaabaaaaaaabeaaaaaaaaaaadpaaaaaaaidccabaaaabaaaaaa
egaabaaaabaaaaaakgakbaiaibaaaaaaabaaaaaabaaaaaajbcaabaaaabaaaaaa
egiccaaaadaaaaaaanaaaaaaegiccaaaadaaaaaaanaaaaaaeeaaaaafbcaabaaa
abaaaaaaakaabaaaabaaaaaadiaaaaaihcaabaaaabaaaaaaagaabaaaabaaaaaa
egiccaaaadaaaaaaanaaaaaabaaaaaaibcaabaaaacaaaaaaegacbaaaabaaaaaa
egiccaaaacaaaaaaaaaaaaaadeaaaaahbcaabaaaacaaaaaaakaabaaaacaaaaaa
abeaaaaaaaaaaaaadiaaaaaihccabaaaacaaaaaaagaabaaaacaaaaaaegiccaaa
aaaaaaaaabaaaaaadiaaaaaiccaabaaaaaaaaaaabkaabaaaaaaaaaaaakiacaaa
abaaaaaaafaaaaaadiaaaaakncaabaaaacaaaaaaagahbaaaaaaaaaaaaceaaaaa
aaaaaadpaaaaaaaaaaaaaadpaaaaaadpdgaaaaafmccabaaaadaaaaaakgaobaaa
aaaaaaaaaaaaaaahdccabaaaadaaaaaakgakbaaaacaaaaaamgaabaaaacaaaaaa
dgaaaaaficaabaaaabaaaaaaabeaaaaaaaaaiadpbbaaaaaibcaabaaaaaaaaaaa
egiocaaaacaaaaaacgaaaaaaegaobaaaabaaaaaabbaaaaaiccaabaaaaaaaaaaa
egiocaaaacaaaaaachaaaaaaegaobaaaabaaaaaabbaaaaaiecaabaaaaaaaaaaa
egiocaaaacaaaaaaciaaaaaaegaobaaaabaaaaaadiaaaaahpcaabaaaacaaaaaa
jgacbaaaabaaaaaaegakbaaaabaaaaaabbaaaaaibcaabaaaadaaaaaaegiocaaa
acaaaaaacjaaaaaaegaobaaaacaaaaaabbaaaaaiccaabaaaadaaaaaaegiocaaa
acaaaaaackaaaaaaegaobaaaacaaaaaabbaaaaaiecaabaaaadaaaaaaegiocaaa
acaaaaaaclaaaaaaegaobaaaacaaaaaaaaaaaaahhcaabaaaaaaaaaaaegacbaaa
aaaaaaaaegacbaaaadaaaaaadiaaaaahicaabaaaaaaaaaaabkaabaaaabaaaaaa
bkaabaaaabaaaaaadcaaaaakicaabaaaaaaaaaaaakaabaaaabaaaaaaakaabaaa
abaaaaaadkaabaiaebaaaaaaaaaaaaaadcaaaaakhccabaaaaeaaaaaaegiccaaa
acaaaaaacmaaaaaapgapbaaaaaaaaaaaegacbaaaaaaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES


#ifdef VERTEX

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _Scale;
uniform lowp vec4 _LightColor0;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_SHC;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAr;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
uniform highp vec4 _Time;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  mediump vec2 wrlPos_1;
  mediump vec3 worldNormal_2;
  lowp vec3 tmpvar_3;
  lowp vec3 tmpvar_4;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_Object2World * vec4(0.0, 1.0, 0.0, 0.0)).xyz);
  worldNormal_2 = tmpvar_5;
  mediump vec4 tmpvar_6;
  tmpvar_6.w = 1.0;
  tmpvar_6.xyz = worldNormal_2;
  mediump vec3 tmpvar_7;
  highp float vC_8;
  mediump vec3 x3_9;
  mediump vec3 x2_10;
  mediump vec3 x1_11;
  highp float tmpvar_12;
  tmpvar_12 = dot (unity_SHAr, tmpvar_6);
  x1_11.x = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = dot (unity_SHAg, tmpvar_6);
  x1_11.y = tmpvar_13;
  highp float tmpvar_14;
  tmpvar_14 = dot (unity_SHAb, tmpvar_6);
  x1_11.z = tmpvar_14;
  mediump vec4 tmpvar_15;
  tmpvar_15 = (worldNormal_2.xyzz * worldNormal_2.yzzx);
  highp float tmpvar_16;
  tmpvar_16 = dot (unity_SHBr, tmpvar_15);
  x2_10.x = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = dot (unity_SHBg, tmpvar_15);
  x2_10.y = tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = dot (unity_SHBb, tmpvar_15);
  x2_10.z = tmpvar_18;
  mediump float tmpvar_19;
  tmpvar_19 = ((worldNormal_2.x * worldNormal_2.x) - (worldNormal_2.y * worldNormal_2.y));
  vC_8 = tmpvar_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (unity_SHC.xyz * vC_8);
  x3_9 = tmpvar_20;
  tmpvar_7 = ((x1_11 + x2_10) + x3_9);
  tmpvar_4 = tmpvar_7;
  mediump vec3 tmpvar_21;
  tmpvar_21 = (max (0.0, dot (worldNormal_2, _WorldSpaceLightPos0.xyz)) * _LightColor0.xyz);
  tmpvar_3 = tmpvar_21;
  highp vec4 tmpvar_22;
  tmpvar_22 = (glstate_matrix_mvp * _glesVertex);
  highp vec4 o_23;
  highp vec4 tmpvar_24;
  tmpvar_24 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_25;
  tmpvar_25.x = tmpvar_24.x;
  tmpvar_25.y = (tmpvar_24.y * _ProjectionParams.x);
  o_23.xy = (tmpvar_25 + tmpvar_24.w);
  o_23.zw = tmpvar_22.zw;
  highp vec2 tmpvar_26;
  tmpvar_26 = (_Object2World * _glesVertex).xz;
  wrlPos_1 = tmpvar_26;
  highp vec2 tmpvar_27;
  tmpvar_27.x = (_glesMultiTexCoord0.x + (wrlPos_1.x * _Scale));
  tmpvar_27.y = (_glesMultiTexCoord0.y + (wrlPos_1.y * _Scale));
  gl_Position = tmpvar_22;
  xlv_TEXCOORD0 = (abs((sin((_Time.x * 5.0)) / 2.0)) + tmpvar_27);
  xlv_COLOR0 = tmpvar_3;
  xlv_TEXCOORD1 = o_23;
  xlv_COLOR1 = tmpvar_4;
}



#endif
#ifdef FRAGMENT

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _DepthFactor;
uniform sampler2D _CameraDepthTexture;
uniform sampler2D _MainTex;
uniform highp vec4 _ZBufferParams;
void main ()
{
  lowp vec4 tmpvar_1;
  lowp vec4 col_2;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD0);
  col_2.w = tmpvar_3.w;
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2DProj (_CameraDepthTexture, xlv_TEXCOORD1);
  highp float z_5;
  z_5 = tmpvar_4.x;
  col_2.xyz = (tmpvar_3.xyz * (xlv_COLOR0 + xlv_COLOR1));
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = col_2.xyz;
  tmpvar_6.w = clamp ((_DepthFactor * ((1.0/(((_ZBufferParams.z * z_5) + _ZBufferParams.w))) - xlv_TEXCOORD1.w)), 0.0, 1.0);
  tmpvar_1 = tmpvar_6;
  gl_FragData[0] = tmpvar_1;
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES


#ifdef VERTEX

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _Scale;
uniform lowp vec4 _LightColor0;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_SHC;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAr;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
uniform highp vec4 _Time;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  mediump vec2 wrlPos_1;
  mediump vec3 worldNormal_2;
  lowp vec3 tmpvar_3;
  lowp vec3 tmpvar_4;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_Object2World * vec4(0.0, 1.0, 0.0, 0.0)).xyz);
  worldNormal_2 = tmpvar_5;
  mediump vec4 tmpvar_6;
  tmpvar_6.w = 1.0;
  tmpvar_6.xyz = worldNormal_2;
  mediump vec3 tmpvar_7;
  highp float vC_8;
  mediump vec3 x3_9;
  mediump vec3 x2_10;
  mediump vec3 x1_11;
  highp float tmpvar_12;
  tmpvar_12 = dot (unity_SHAr, tmpvar_6);
  x1_11.x = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = dot (unity_SHAg, tmpvar_6);
  x1_11.y = tmpvar_13;
  highp float tmpvar_14;
  tmpvar_14 = dot (unity_SHAb, tmpvar_6);
  x1_11.z = tmpvar_14;
  mediump vec4 tmpvar_15;
  tmpvar_15 = (worldNormal_2.xyzz * worldNormal_2.yzzx);
  highp float tmpvar_16;
  tmpvar_16 = dot (unity_SHBr, tmpvar_15);
  x2_10.x = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = dot (unity_SHBg, tmpvar_15);
  x2_10.y = tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = dot (unity_SHBb, tmpvar_15);
  x2_10.z = tmpvar_18;
  mediump float tmpvar_19;
  tmpvar_19 = ((worldNormal_2.x * worldNormal_2.x) - (worldNormal_2.y * worldNormal_2.y));
  vC_8 = tmpvar_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (unity_SHC.xyz * vC_8);
  x3_9 = tmpvar_20;
  tmpvar_7 = ((x1_11 + x2_10) + x3_9);
  tmpvar_4 = tmpvar_7;
  mediump vec3 tmpvar_21;
  tmpvar_21 = (max (0.0, dot (worldNormal_2, _WorldSpaceLightPos0.xyz)) * _LightColor0.xyz);
  tmpvar_3 = tmpvar_21;
  highp vec4 tmpvar_22;
  tmpvar_22 = (glstate_matrix_mvp * _glesVertex);
  highp vec4 o_23;
  highp vec4 tmpvar_24;
  tmpvar_24 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_25;
  tmpvar_25.x = tmpvar_24.x;
  tmpvar_25.y = (tmpvar_24.y * _ProjectionParams.x);
  o_23.xy = (tmpvar_25 + tmpvar_24.w);
  o_23.zw = tmpvar_22.zw;
  highp vec2 tmpvar_26;
  tmpvar_26 = (_Object2World * _glesVertex).xz;
  wrlPos_1 = tmpvar_26;
  highp vec2 tmpvar_27;
  tmpvar_27.x = (_glesMultiTexCoord0.x + (wrlPos_1.x * _Scale));
  tmpvar_27.y = (_glesMultiTexCoord0.y + (wrlPos_1.y * _Scale));
  gl_Position = tmpvar_22;
  xlv_TEXCOORD0 = (abs((sin((_Time.x * 5.0)) / 2.0)) + tmpvar_27);
  xlv_COLOR0 = tmpvar_3;
  xlv_TEXCOORD1 = o_23;
  xlv_COLOR1 = tmpvar_4;
}



#endif
#ifdef FRAGMENT

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _DepthFactor;
uniform sampler2D _CameraDepthTexture;
uniform sampler2D _MainTex;
uniform highp vec4 _ZBufferParams;
void main ()
{
  lowp vec4 tmpvar_1;
  lowp vec4 col_2;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD0);
  col_2.w = tmpvar_3.w;
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2DProj (_CameraDepthTexture, xlv_TEXCOORD1);
  highp float z_5;
  z_5 = tmpvar_4.x;
  col_2.xyz = (tmpvar_3.xyz * (xlv_COLOR0 + xlv_COLOR1));
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = col_2.xyz;
  tmpvar_6.w = clamp ((_DepthFactor * ((1.0/(((_ZBufferParams.z * z_5) + _ZBufferParams.w))) - xlv_TEXCOORD1.w)), 0.0, 1.0);
  tmpvar_1 = tmpvar_6;
  gl_FragData[0] = tmpvar_1;
}



#endif"
}

SubProgram "flash " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 0 [glstate_matrix_mvp]
Vector 8 [_Time]
Vector 9 [_ProjectionParams]
Vector 10 [_WorldSpaceLightPos0]
Vector 11 [unity_SHAr]
Vector 12 [unity_SHAg]
Vector 13 [unity_SHAb]
Vector 14 [unity_SHBr]
Vector 15 [unity_SHBg]
Vector 16 [unity_SHBb]
Vector 17 [unity_SHC]
Matrix 4 [_Object2World]
Vector 18 [unity_NPOTScale]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"agal_vs
c21 -0.020833 -0.125 1.0 0.5
c22 -0.000002 -0.000022 0.002604 0.00026
c23 0.0 0.795775 0.5 0.0
c24 6.283185 -3.141593 0.0 0.0
[bc]
aaaaaaaaaaaaaeacagaaaaffabaaaaaaaaaaaaaaaaaaaaaa mov r0.z, c6.y
aaaaaaaaaaaaabacaeaaaaffabaaaaaaaaaaaaaaaaaaaaaa mov r0.x, c4.y
aaaaaaaaaaaaacacafaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov r0.y, c5
bcaaaaaaaaaaaiacaaaaaakeacaaaaaaaaaaaakeacaaaaaa dp3 r0.w, r0.xyzz, r0.xyzz
akaaaaaaaaaaaiacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa rsq r0.w, r0.w
adaaaaaaaaaaahacaaaaaappacaaaaaaaaaaaakeacaaaaaa mul r0.xyz, r0.w, r0.xyzz
aaaaaaaaaaaaaiacbfaaaakkabaaaaaaaaaaaaaaaaaaaaaa mov r0.w, c21.z
adaaaaaaabaaabacaaaaaaffacaaaaaaaaaaaaffacaaaaaa mul r1.x, r0.y, r0.y
adaaaaaaacaaaiacaaaaaaaaacaaaaaaaaaaaaaaacaaaaaa mul r2.w, r0.x, r0.x
acaaaaaaacaaaiacacaaaappacaaaaaaabaaaaaaacaaaaaa sub r2.w, r2.w, r1.x
adaaaaaaabaaapacaaaaaakeacaaaaaaaaaaaacjacaaaaaa mul r1, r0.xyzz, r0.yzzx
bdaaaaaaacaaaeacaaaaaaoeacaaaaaaanaaaaoeabaaaaaa dp4 r2.z, r0, c13
bdaaaaaaacaaacacaaaaaaoeacaaaaaaamaaaaoeabaaaaaa dp4 r2.y, r0, c12
bdaaaaaaacaaabacaaaaaaoeacaaaaaaalaaaaoeabaaaaaa dp4 r2.x, r0, c11
aaaaaaaaaaaaaiacaiaaaaaaabaaaaaaaaaaaaaaaaaaaaaa mov r0.w, c8.x
adaaaaaaaaaaaiacaaaaaappacaaaaaabhaaaaffabaaaaaa mul r0.w, r0.w, c23.y
abaaaaaaaaaaaiacaaaaaappacaaaaaabhaaaakkabaaaaaa add r0.w, r0.w, c23.z
aiaaaaaaaaaaaiacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa frc r0.w, r0.w
bdaaaaaaadaaaeacabaaaaoeacaaaaaabaaaaaoeabaaaaaa dp4 r3.z, r1, c16
bdaaaaaaadaaabacabaaaaoeacaaaaaaaoaaaaoeabaaaaaa dp4 r3.x, r1, c14
bdaaaaaaadaaacacabaaaaoeacaaaaaaapaaaaoeabaaaaaa dp4 r3.y, r1, c15
bcaaaaaaaaaaacacaaaaaakeacaaaaaaakaaaaoeabaaaaaa dp3 r0.y, r0.xyzz, c10
bdaaaaaaabaaaiacaaaaaaoeaaaaaaaaadaaaaoeabaaaaaa dp4 r1.w, a0, c3
bdaaaaaaabaaaeacaaaaaaoeaaaaaaaaacaaaaoeabaaaaaa dp4 r1.z, a0, c2
abaaaaaaadaaahacacaaaakeacaaaaaaadaaaakeacaaaaaa add r3.xyz, r2.xyzz, r3.xyzz
adaaaaaaaeaaahacacaaaappacaaaaaabbaaaaoeabaaaaaa mul r4.xyz, r2.w, c17
bdaaaaaaabaaabacaaaaaaoeaaaaaaaaaaaaaaoeabaaaaaa dp4 r1.x, a0, c0
bdaaaaaaabaaacacaaaaaaoeaaaaaaaaabaaaaoeabaaaaaa dp4 r1.y, a0, c1
adaaaaaaacaaahacabaaaapeacaaaaaabfaaaappabaaaaaa mul r2.xyz, r1.xyww, c21.w
adaaaaaaacaaacacacaaaaffacaaaaaaajaaaaaaabaaaaaa mul r2.y, r2.y, c9.x
abaaaaaaacaaadacacaaaafeacaaaaaaacaaaakkacaaaaaa add r2.xy, r2.xyyy, r2.z
adaaaaaaaaaaaiacaaaaaappacaaaaaabiaaaaaaabaaaaaa mul r0.w, r0.w, c24.x
abaaaaaaaaaaaiacaaaaaappacaaaaaabiaaaaffabaaaaaa add r0.w, r0.w, c24.y
adaaaaaaabaaadaeacaaaafeacaaaaaabcaaaaoeabaaaaaa mul v1.xy, r2.xyyy, c18
apaaaaaaacaaabacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa sin r2.x, r0.w
baaaaaaaacaaacacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa cos r2.y, r0.w
ahaaaaaaaaaaaiacaaaaaaffacaaaaaabhaaaaaaabaaaaaa max r0.w, r0.y, c23.x
adaaaaaaaaaaabacacaaaaffacaaaaaabfaaaappabaaaaaa mul r0.x, r2.y, c21.w
beaaaaaaaaaaaeacaaaaaaaaacaaaaaaaaaaaaaaaaaaaaaa abs r0.z, r0.x
bdaaaaaaaaaaacacaaaaaaoeaaaaaaaaagaaaaoeabaaaaaa dp4 r0.y, a0, c6
bdaaaaaaaaaaabacaaaaaaoeaaaaaaaaaeaaaaoeabaaaaaa dp4 r0.x, a0, c4
adaaaaaaaaaaacacaaaaaaffacaaaaaabeaaaaaaabaaaaaa mul r0.y, r0.y, c20.x
abaaaaaaaaaaacacaaaaaaffacaaaaaaadaaaaoeaaaaaaaa add r0.y, r0.y, a3
adaaaaaaaaaaabacaaaaaaaaacaaaaaabeaaaaoeabaaaaaa mul r0.x, r0.x, c20
abaaaaaaaaaaabacaaaaaaaaacaaaaaaadaaaaoeaaaaaaaa add r0.x, r0.x, a3
abaaaaaaagaaahaeadaaaakeacaaaaaaaeaaaakeacaaaaaa add v6.xyz, r3.xyzz, r4.xyzz
aaaaaaaaaaaaapadabaaaaoeacaaaaaaaaaaaaaaaaaaaaaa mov o0, r1
abaaaaaaaaaaadaeaaaaaakkacaaaaaaaaaaaafeacaaaaaa add v0.xy, r0.z, r0.xyyy
adaaaaaaahaaahaeaaaaaappacaaaaaabdaaaaoeabaaaaaa mul v7.xyz, r0.w, c19
aaaaaaaaabaaamaeabaaaaopacaaaaaaaaaaaaaaaaaaaaaa mov v1.zw, r1.wwzw
aaaaaaaaaaaaamaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v0.zw, c0
aaaaaaaaagaaaiaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v6.w, c0
aaaaaaaaahaaaiaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v7.w, c0
"
}

SubProgram "d3d11_9x " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
ConstBuffer "$Globals" 64 // 52 used size, 5 vars
Vector 16 [_LightColor0] 4
Float 48 [_Scale]
ConstBuffer "UnityPerCamera" 128 // 96 used size, 8 vars
Vector 0 [_Time] 4
Vector 80 [_ProjectionParams] 4
ConstBuffer "UnityLighting" 720 // 720 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
Vector 608 [unity_SHAr] 4
Vector 624 [unity_SHAg] 4
Vector 640 [unity_SHAb] 4
Vector 656 [unity_SHBr] 4
Vector 672 [unity_SHBg] 4
Vector 688 [unity_SHBb] 4
Vector 704 [unity_SHC] 4
ConstBuffer "UnityPerDraw" 336 // 256 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
BindCB "UnityLighting" 2
BindCB "UnityPerDraw" 3
// 37 instructions, 4 temp regs, 0 temp arrays:
// ALU 33 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0_level_9_1
eefiecedpehalmanahacfidnogggdgpgnmigniocabaaaaaaciakaaaaaeaaaaaa
daaaaaaaniadaaaabaajaaaaieajaaaaebgpgodjkaadaaaakaadaaaaaaacpopp
biadaaaaiiaaaaaaaiaaceaaaaaaieaaaaaaieaaaaaaceaaabaaieaaaaaaabaa
abaaabaaaaaaaaaaaaaaadaaabaaacaaaaaaaaaaabaaaaaaabaaadaaaaaaaaaa
abaaafaaabaaaeaaaaaaaaaaacaaaaaaabaaafaaaaaaaaaaacaacgaaahaaagaa
aaaaaaaaadaaaaaaaeaaanaaaaaaaaaaadaaamaaaeaabbaaaaaaaaaaaaaaaaaa
aaacpoppfbaaaaafbfaaapkaaaaaiadpaaaaaaaaaaaaaadpoelheldpfbaaaaaf
bgaaapkanlapmjeanlapejmaaaaaaaaaaaaaaaaafbaaaaafbhaaapkaabannalf
gballglhklkkckdlijiiiidjfbaaaaafbiaaapkaklkkkklmaaaaaaloaaaaiadp
aaaaaadpbpaaaaacafaaaaiaaaaaapjabpaaaaacafaaabiaabaaapjaabaaaaac
aaaaaiiabfaaaakaceaaaaacaaaaahiabcaaoekaajaaaaadabaaabiaagaaoeka
aaaaoeiaajaaaaadabaaaciaahaaoekaaaaaoeiaajaaaaadabaaaeiaaiaaoeka
aaaaoeiaafaaaaadacaaapiaaaaacjiaaaaakeiaajaaaaadadaaabiaajaaoeka
acaaoeiaajaaaaadadaaaciaakaaoekaacaaoeiaajaaaaadadaaaeiaalaaoeka
acaaoeiaacaaaaadabaaahiaabaaoeiaadaaoeiaafaaaaadaaaaaiiaaaaaffia
aaaaffiaaeaaaaaeaaaaaiiaaaaaaaiaaaaaaaiaaaaappibaiaaaaadaaaaabia
aaaaoeiaafaaoekaalaaaaadaaaaabiaaaaaaaiabfaaffkaafaaaaadabaaahoa
aaaaaaiaabaaoekaaeaaaaaeadaaahoaamaaoekaaaaappiaabaaoeiaafaaaaad
aaaaapiaaaaaffjaaoaaoekaaeaaaaaeaaaaapiaanaaoekaaaaaaajaaaaaoeia
aeaaaaaeaaaaapiaapaaoekaaaaakkjaaaaaoeiaaeaaaaaeaaaaapiabaaaoeka
aaaappjaaaaaoeiaafaaaaadabaaabiaaaaaffiaaeaaaakaafaaaaadabaaaiia
abaaaaiabfaakkkaafaaaaadabaaafiaaaaapeiabfaakkkaacaaaaadacaaadoa
abaakkiaabaaomiaabaaaaacabaaamiabfaaoekaaeaaaaaeabaaabiaadaaaaka
abaappiaabaakkiabdaaaaacabaaabiaabaaaaiaaeaaaaaeabaaabiaabaaaaia
bgaaaakabgaaffkacfaaaaaeacaaaciaabaaaaiabhaaoekabiaaoekaafaaaaad
abaaabiaacaaffiabfaakkkacdaaaaacabaaabiaabaaaaiaafaaaaadabaaagia
aaaaffjabcaaoakaaeaaaaaeabaaagiabbaaoakaaaaaaajaabaaoeiaaeaaaaae
abaaagiabdaaoakaaaaakkjaabaaoeiaaeaaaaaeabaaagiabeaaoakaaaaappja
abaaoeiaaeaaaaaeabaaagiaabaaoeiaacaaaakaabaanajaacaaaaadaaaaadoa
abaaojiaabaaaaiaaeaaaaaeaaaaadmaaaaappiaaaaaoekaaaaaoeiaabaaaaac
aaaaammaaaaaoeiaabaaaaacacaaamoaaaaaoeiappppaaaafdeieefcdaafaaaa
eaaaabaaemabaaaafjaaaaaeegiocaaaaaaaaaaaaeaaaaaafjaaaaaeegiocaaa
abaaaaaaagaaaaaafjaaaaaeegiocaaaacaaaaaacnaaaaaafjaaaaaeegiocaaa
adaaaaaabaaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaaddcbabaaaabaaaaaa
ghaaaaaepccabaaaaaaaaaaaabaaaaaagfaaaaaddccabaaaabaaaaaagfaaaaad
hccabaaaacaaaaaagfaaaaadpccabaaaadaaaaaagfaaaaadhccabaaaaeaaaaaa
giaaaaacaeaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaaegiocaaa
adaaaaaaabaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaadaaaaaaaaaaaaaa
agbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaa
adaaaaaaacaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaadaaaaaaadaaaaaapgbpbaaaaaaaaaaaegaobaaaaaaaaaaa
dgaaaaafpccabaaaaaaaaaaaegaobaaaaaaaaaaadiaaaaaidcaabaaaabaaaaaa
fgbfbaaaaaaaaaaaigiacaaaadaaaaaaanaaaaaadcaaaaakdcaabaaaabaaaaaa
igiacaaaadaaaaaaamaaaaaaagbabaaaaaaaaaaaegaabaaaabaaaaaadcaaaaak
dcaabaaaabaaaaaaigiacaaaadaaaaaaaoaaaaaakgbkbaaaaaaaaaaaegaabaaa
abaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaapaaaaaapgbpbaaa
aaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaegaabaaaabaaaaaa
agiacaaaaaaaaaaaadaaaaaaegbabaaaabaaaaaadiaaaaaiecaabaaaabaaaaaa
akiacaaaabaaaaaaaaaaaaaaabeaaaaaaaaakaeaenaaaaagecaabaaaabaaaaaa
aanaaaaackaabaaaabaaaaaadiaaaaahecaabaaaabaaaaaackaabaaaabaaaaaa
abeaaaaaaaaaaadpaaaaaaaidccabaaaabaaaaaaegaabaaaabaaaaaakgakbaia
ibaaaaaaabaaaaaabaaaaaajbcaabaaaabaaaaaaegiccaaaadaaaaaaanaaaaaa
egiccaaaadaaaaaaanaaaaaaeeaaaaafbcaabaaaabaaaaaaakaabaaaabaaaaaa
diaaaaaihcaabaaaabaaaaaaagaabaaaabaaaaaaegiccaaaadaaaaaaanaaaaaa
baaaaaaibcaabaaaacaaaaaaegacbaaaabaaaaaaegiccaaaacaaaaaaaaaaaaaa
deaaaaahbcaabaaaacaaaaaaakaabaaaacaaaaaaabeaaaaaaaaaaaaadiaaaaai
hccabaaaacaaaaaaagaabaaaacaaaaaaegiccaaaaaaaaaaaabaaaaaadiaaaaai
ccaabaaaaaaaaaaabkaabaaaaaaaaaaaakiacaaaabaaaaaaafaaaaaadiaaaaak
ncaabaaaacaaaaaaagahbaaaaaaaaaaaaceaaaaaaaaaaadpaaaaaaaaaaaaaadp
aaaaaadpdgaaaaafmccabaaaadaaaaaakgaobaaaaaaaaaaaaaaaaaahdccabaaa
adaaaaaakgakbaaaacaaaaaamgaabaaaacaaaaaadgaaaaaficaabaaaabaaaaaa
abeaaaaaaaaaiadpbbaaaaaibcaabaaaaaaaaaaaegiocaaaacaaaaaacgaaaaaa
egaobaaaabaaaaaabbaaaaaiccaabaaaaaaaaaaaegiocaaaacaaaaaachaaaaaa
egaobaaaabaaaaaabbaaaaaiecaabaaaaaaaaaaaegiocaaaacaaaaaaciaaaaaa
egaobaaaabaaaaaadiaaaaahpcaabaaaacaaaaaajgacbaaaabaaaaaaegakbaaa
abaaaaaabbaaaaaibcaabaaaadaaaaaaegiocaaaacaaaaaacjaaaaaaegaobaaa
acaaaaaabbaaaaaiccaabaaaadaaaaaaegiocaaaacaaaaaackaaaaaaegaobaaa
acaaaaaabbaaaaaiecaabaaaadaaaaaaegiocaaaacaaaaaaclaaaaaaegaobaaa
acaaaaaaaaaaaaahhcaabaaaaaaaaaaaegacbaaaaaaaaaaaegacbaaaadaaaaaa
diaaaaahicaabaaaaaaaaaaabkaabaaaabaaaaaabkaabaaaabaaaaaadcaaaaak
icaabaaaaaaaaaaaakaabaaaabaaaaaaakaabaaaabaaaaaadkaabaiaebaaaaaa
aaaaaaaadcaaaaakhccabaaaaeaaaaaaegiccaaaacaaaaaacmaaaaaapgapbaaa
aaaaaaaaegacbaaaaaaaaaaadoaaaaabejfdeheogmaaaaaaadaaaaaaaiaaaaaa
faaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaafjaaaaaaaaaaaaaa
aaaaaaaaadaaaaaaabaaaaaaadadaaaagcaaaaaaaaaaaaaaaaaaaaaaadaaaaaa
acaaaaaaahaaaaaafaepfdejfeejepeoaafeeffiedepepfceeaaeoepfcenebem
aaklklklepfdeheojmaaaaaaafaaaaaaaiaaaaaaiaaaaaaaaaaaaaaaabaaaaaa
adaaaaaaaaaaaaaaapaaaaaaimaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaa
adamaaaajfaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaimaaaaaa
abaaaaaaaaaaaaaaadaaaaaaadaaaaaaapaaaaaajfaaaaaaabaaaaaaaaaaaaaa
adaaaaaaaeaaaaaaahaiaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfcee
aaedepemepfcaakl"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal (normalize(_glesNormal))
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 399
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 392
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform sampler2D _MainTex;
#line 408
#line 412
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
uniform highp float _DepthFactor;
#line 428
#line 284
highp vec4 ComputeScreenPos( in highp vec4 pos ) {
    #line 286
    highp vec4 o = (pos * 0.5);
    o.xy = (vec2( o.x, (o.y * _ProjectionParams.x)) + o.w);
    o.zw = pos.zw;
    return o;
}
#line 137
mediump vec3 ShadeSH9( in mediump vec4 normal ) {
    mediump vec3 x1;
    mediump vec3 x2;
    mediump vec3 x3;
    x1.x = dot( unity_SHAr, normal);
    #line 141
    x1.y = dot( unity_SHAg, normal);
    x1.z = dot( unity_SHAb, normal);
    mediump vec4 vB = (normal.xyzz * normal.yzzx);
    x2.x = dot( unity_SHBr, vB);
    #line 145
    x2.y = dot( unity_SHBg, vB);
    x2.z = dot( unity_SHBb, vB);
    highp float vC = ((normal.x * normal.x) - (normal.y * normal.y));
    x3 = (unity_SHC.xyz * vC);
    #line 149
    return ((x1 + x2) + x3);
}
#line 408
highp vec2 offset( in highp vec2 uv ) {
    return (abs((sin((_Time.x * 5.0)) / 2.0)) + uv);
}
#line 413
v2f vert( in appdata_t v ) {
    v2f o;
    #line 416
    mediump vec3 worldNormal = normalize((_Object2World * vec4( 0.0, 1.0, 0.0, 0.0)).xyz);
    o.ambient = ShadeSH9( vec4( worldNormal, 1.0));
    mediump float nl = max( 0.0, dot( worldNormal, _WorldSpaceLightPos0.xyz));
    o.diff = (nl * _LightColor0.xyz);
    #line 420
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.scrPos = ComputeScreenPos( o.pos);
    mediump vec2 wrlPos = (_Object2World * v.vertex).xz;
    o.uv = offset( vec2( (v.texcoord.x + (wrlPos.x * _Scale)), (v.texcoord.y + (wrlPos.y * _Scale))));
    #line 424
    return o;
}
out highp vec2 xlv_TEXCOORD0;
out lowp vec3 xlv_COLOR0;
out highp vec4 xlv_TEXCOORD1;
out lowp vec3 xlv_COLOR1;
void main() {
    v2f xl_retval;
    appdata_t xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.texcoord = vec2(gl_MultiTexCoord0);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vert( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec2(xl_retval.uv);
    xlv_COLOR0 = vec3(xl_retval.diff);
    xlv_TEXCOORD1 = vec4(xl_retval.scrPos);
    xlv_COLOR1 = vec3(xl_retval.ambient);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 399
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 392
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform sampler2D _MainTex;
#line 408
#line 412
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
uniform highp float _DepthFactor;
#line 428
#line 280
highp float LinearEyeDepth( in highp float z ) {
    #line 282
    return (1.0 / ((_ZBufferParams.z * z) + _ZBufferParams.w));
}
#line 428
lowp vec4 frag( in v2f i ) {
    lowp vec4 col = texture( _MainTex, i.uv);
    highp float depth = LinearEyeDepth( float( textureProj( _CameraDepthTexture, i.scrPos).xyzw));
    #line 432
    highp float diff = xll_saturate_f((_DepthFactor * (depth - i.scrPos.w)));
    lowp float shadow = 1.0;
    lowp vec3 lighting = ((i.diff * shadow) + i.ambient);
    col.xyz *= lighting;
    #line 436
    return vec4( col.xyz, diff);
}
in highp vec2 xlv_TEXCOORD0;
in lowp vec3 xlv_COLOR0;
in highp vec4 xlv_TEXCOORD1;
in lowp vec3 xlv_COLOR1;
void main() {
    lowp vec4 xl_retval;
    v2f xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.uv = vec2(xlv_TEXCOORD0);
    xlt_i.diff = vec3(xlv_COLOR0);
    xlt_i.scrPos = vec4(xlv_TEXCOORD1);
    xlt_i.ambient = vec3(xlv_COLOR1);
    xl_retval = frag( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Vector 9 [_Time]
Vector 10 [_ProjectionParams]
Vector 11 [_WorldSpaceLightPos0]
Vector 12 [unity_SHAr]
Vector 13 [unity_SHAg]
Vector 14 [unity_SHAb]
Vector 15 [unity_SHBr]
Vector 16 [unity_SHBg]
Vector 17 [unity_SHBb]
Vector 18 [unity_SHC]
Matrix 5 [_Object2World]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"!!ARBvp1.0
# 57 ALU
PARAM c[25] = { { 0.79577452, 0, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..20],
		{ 24.980801, -24.980801, 0.25 },
		{ -60.145809, 60.145809, 85.453789, -85.453789 },
		{ -64.939346, 64.939346, 19.73921, -19.73921 },
		{ -1, 1, -9, 0.75 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
MOV R0.x, c[0];
MUL R0.x, R0, c[9];
ADD R0.x, R0, -c[21].z;
FRC R2.w, R0.x;
ADD R0.xyz, -R2.w, c[0].yzww;
MUL R2.xyz, R0, R0;
MUL R0.xyz, R2, c[21].xyxw;
ADD R0.xyz, R0, c[22].xyxw;
MAD R1.xyz, R0, R2, c[22].zwzw;
MAD R3.xyz, R1, R2, c[23].xyxw;
MAD R3.xyz, R3, R2, c[23].zwzw;
MAD R3.xyz, R3, R2, c[24].xyxw;
MOV R1.w, c[0];
MOV R0.z, c[7].y;
MOV R0.x, c[5].y;
MOV R0.y, c[6];
DP3 R0.w, R0, R0;
RSQ R0.w, R0.w;
MUL R1.xyz, R0.w, R0;
MUL R0, R1.xyzz, R1.yzzx;
DP4 R4.z, R1, c[14];
DP4 R4.y, R1, c[13];
DP4 R4.x, R1, c[12];
DP4 R5.z, R0, c[17];
DP4 R5.x, R0, c[15];
DP4 R5.y, R0, c[16];
ADD R0.xyz, R4, R5;
MUL R0.w, R1.y, R1.y;
MAD R0.w, R1.x, R1.x, -R0;
SLT R4.x, R2.w, c[21].z;
SGE R4.yz, R2.w, c[24].xzww;
DP4 R2.w, vertex.position, c[4];
MOV R2.xz, R4;
DP3 R2.y, R4, c[24].xyxw;
DP3 R1.w, R3, -R2;
MUL R2.xyz, R0.w, c[18];
ADD result.color.secondary.xyz, R0, R2;
MUL R0.w, R1, c[0].z;
ABS R2.z, R0.w;
DP4 R0.w, vertex.position, c[7];
DP4 R1.w, vertex.position, c[5];
DP4 R2.x, vertex.position, c[1];
DP4 R2.y, vertex.position, c[2];
MUL R0.xyz, R2.xyww, c[0].z;
MUL R0.y, R0, c[10].x;
ADD R0.xy, R0, R0.z;
MAD R3.y, R0.w, c[20].x, vertex.texcoord[0];
MAD R3.x, R1.w, c[20], vertex.texcoord[0];
ADD result.texcoord[0].xy, R2.z, R3;
DP4 R2.z, vertex.position, c[3];
MOV R0.zw, R2;
MOV result.texcoord[2], R0;
DP3 R1.x, R1, c[11];
MOV result.texcoord[1], R0;
MAX R0.x, R1, c[0].y;
MOV result.position, R2;
MUL result.color.xyz, R0.x, c[19];
END
# 57 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 0 [glstate_matrix_mvp]
Vector 8 [_Time]
Vector 9 [_ProjectionParams]
Vector 10 [_ScreenParams]
Vector 11 [_WorldSpaceLightPos0]
Vector 12 [unity_SHAr]
Vector 13 [unity_SHAg]
Vector 14 [unity_SHAb]
Vector 15 [unity_SHBr]
Vector 16 [unity_SHBg]
Vector 17 [unity_SHBb]
Vector 18 [unity_SHC]
Matrix 4 [_Object2World]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"vs_2_0
; 54 ALU
def c21, -0.02083333, -0.12500000, 1.00000000, 0.50000000
def c22, -0.00000155, -0.00002170, 0.00260417, 0.00026042
def c23, 0.00000000, 0.79577452, 0.50000000, 0
def c24, 6.28318501, -3.14159298, 0, 0
dcl_position0 v0
dcl_texcoord0 v1
mov r0.z, c6.y
mov r0.x, c4.y
mov r0.y, c5
dp3 r0.w, r0, r0
rsq r0.w, r0.w
mul r0.xyz, r0.w, r0
mov r0.w, c21.z
mul r1.x, r0.y, r0.y
mad r2.w, r0.x, r0.x, -r1.x
mul r1, r0.xyzz, r0.yzzx
dp4 r2.z, r0, c14
dp4 r2.y, r0, c13
dp4 r2.x, r0, c12
mov r0.w, c8.x
mad r0.w, r0, c23.y, c23.z
frc r0.w, r0
dp4 r3.z, r1, c17
dp4 r3.x, r1, c15
dp4 r3.y, r1, c16
dp3 r0.y, r0, c11
dp4 r1.w, v0, c3
add r3.xyz, r2, r3
mul r4.xyz, r2.w, c18
dp4 r1.x, v0, c0
dp4 r1.y, v0, c1
mul r2.xyz, r1.xyww, c21.w
dp4 r1.z, v0, c2
mul r2.y, r2, c9.x
mad r2.xy, r2.z, c10.zwzw, r2
mov r2.zw, r1
mad r0.w, r0, c24.x, c24.y
mov oT2, r2
mov oT1, r2
sincos r2.xy, r0.w, c22.xyzw, c21.xyzw
max r0.w, r0.y, c23.x
mul r0.x, r2.y, c21.w
abs r0.z, r0.x
dp4 r0.y, v0, c6
dp4 r0.x, v0, c4
mad r0.y, r0, c20.x, v1
mad r0.x, r0, c20, v1
add oD1.xyz, r3, r4
mov oPos, r1
add oT0.xy, r0.z, r0
mul oD0.xyz, r0.w, c19
"
}

SubProgram "d3d11 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
ConstBuffer "$Globals" 128 // 116 used size, 6 vars
Vector 16 [_LightColor0] 4
Float 112 [_Scale]
ConstBuffer "UnityPerCamera" 128 // 96 used size, 8 vars
Vector 0 [_Time] 4
Vector 80 [_ProjectionParams] 4
ConstBuffer "UnityLighting" 720 // 720 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
Vector 608 [unity_SHAr] 4
Vector 624 [unity_SHAg] 4
Vector 640 [unity_SHAb] 4
Vector 656 [unity_SHBr] 4
Vector 672 [unity_SHBg] 4
Vector 688 [unity_SHBb] 4
Vector 704 [unity_SHC] 4
ConstBuffer "UnityPerDraw" 336 // 256 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
BindCB "UnityLighting" 2
BindCB "UnityPerDraw" 3
// 39 instructions, 4 temp regs, 0 temp arrays:
// ALU 34 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0
eefiecedkmbjjcbgbnmcgdihdafokpehmgpabbboabaaaaaanaagaaaaadaaaaaa
cmaaaaaakaaaaaaafmabaaaaejfdeheogmaaaaaaadaaaaaaaiaaaaaafaaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaafjaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaadadaaaagcaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahaaaaaafaepfdejfeejepeoaafeeffiedepepfceeaaeoepfcenebemaaklklkl
epfdeheoleaaaaaaagaaaaaaaiaaaaaajiaaaaaaaaaaaaaaabaaaaaaadaaaaaa
aaaaaaaaapaaaaaakeaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaadamaaaa
keaaaaaaacaaaaaaaaaaaaaaadaaaaaaacaaaaaaapaaaaaaknaaaaaaaaaaaaaa
aaaaaaaaadaaaaaaadaaaaaaahaiaaaakeaaaaaaabaaaaaaaaaaaaaaadaaaaaa
aeaaaaaaapaaaaaaknaaaaaaabaaaaaaaaaaaaaaadaaaaaaafaaaaaaahaiaaaa
fdfgfpfaepfdejfeejepeoaafeeffiedepepfceeaaedepemepfcaaklfdeieefc
gmafaaaaeaaaabaaflabaaaafjaaaaaeegiocaaaaaaaaaaaaiaaaaaafjaaaaae
egiocaaaabaaaaaaagaaaaaafjaaaaaeegiocaaaacaaaaaacnaaaaaafjaaaaae
egiocaaaadaaaaaabaaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaaddcbabaaa
abaaaaaaghaaaaaepccabaaaaaaaaaaaabaaaaaagfaaaaaddccabaaaabaaaaaa
gfaaaaadpccabaaaacaaaaaagfaaaaadhccabaaaadaaaaaagfaaaaadpccabaaa
aeaaaaaagfaaaaadhccabaaaafaaaaaagiaaaaacaeaaaaaadiaaaaaipcaabaaa
aaaaaaaafgbfbaaaaaaaaaaaegiocaaaadaaaaaaabaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaadaaaaaaaaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaadaaaaaaacaaaaaakgbkbaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaadaaaaaaadaaaaaa
pgbpbaaaaaaaaaaaegaobaaaaaaaaaaadgaaaaafpccabaaaaaaaaaaaegaobaaa
aaaaaaaadiaaaaaidcaabaaaabaaaaaafgbfbaaaaaaaaaaaigiacaaaadaaaaaa
anaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaamaaaaaaagbabaaa
aaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaa
aoaaaaaakgbkbaaaaaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaa
igiacaaaadaaaaaaapaaaaaapgbpbaaaaaaaaaaaegaabaaaabaaaaaadcaaaaak
dcaabaaaabaaaaaaegaabaaaabaaaaaaagiacaaaaaaaaaaaahaaaaaaegbabaaa
abaaaaaadiaaaaaiecaabaaaabaaaaaaakiacaaaabaaaaaaaaaaaaaaabeaaaaa
aaaakaeaenaaaaagecaabaaaabaaaaaaaanaaaaackaabaaaabaaaaaadiaaaaah
ecaabaaaabaaaaaackaabaaaabaaaaaaabeaaaaaaaaaaadpaaaaaaaidccabaaa
abaaaaaaegaabaaaabaaaaaakgakbaiaibaaaaaaabaaaaaadiaaaaaibcaabaaa
abaaaaaabkaabaaaaaaaaaaaakiacaaaabaaaaaaafaaaaaadiaaaaahicaabaaa
abaaaaaaakaabaaaabaaaaaaabeaaaaaaaaaaadpdiaaaaakfcaabaaaabaaaaaa
agadbaaaaaaaaaaaaceaaaaaaaaaaadpaaaaaaaaaaaaaadpaaaaaaaaaaaaaaah
dcaabaaaaaaaaaaakgakbaaaabaaaaaamgaabaaaabaaaaaadgaaaaafpccabaaa
acaaaaaaegaobaaaaaaaaaaadgaaaaafpccabaaaaeaaaaaaegaobaaaaaaaaaaa
baaaaaajbcaabaaaaaaaaaaaegiccaaaadaaaaaaanaaaaaaegiccaaaadaaaaaa
anaaaaaaeeaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaadiaaaaaihcaabaaa
aaaaaaaaagaabaaaaaaaaaaaegiccaaaadaaaaaaanaaaaaabaaaaaaibcaabaaa
abaaaaaaegacbaaaaaaaaaaaegiccaaaacaaaaaaaaaaaaaadeaaaaahbcaabaaa
abaaaaaaakaabaaaabaaaaaaabeaaaaaaaaaaaaadiaaaaaihccabaaaadaaaaaa
agaabaaaabaaaaaaegiccaaaaaaaaaaaabaaaaaadgaaaaaficaabaaaaaaaaaaa
abeaaaaaaaaaiadpbbaaaaaibcaabaaaabaaaaaaegiocaaaacaaaaaacgaaaaaa
egaobaaaaaaaaaaabbaaaaaiccaabaaaabaaaaaaegiocaaaacaaaaaachaaaaaa
egaobaaaaaaaaaaabbaaaaaiecaabaaaabaaaaaaegiocaaaacaaaaaaciaaaaaa
egaobaaaaaaaaaaadiaaaaahpcaabaaaacaaaaaajgacbaaaaaaaaaaaegakbaaa
aaaaaaaabbaaaaaibcaabaaaadaaaaaaegiocaaaacaaaaaacjaaaaaaegaobaaa
acaaaaaabbaaaaaiccaabaaaadaaaaaaegiocaaaacaaaaaackaaaaaaegaobaaa
acaaaaaabbaaaaaiecaabaaaadaaaaaaegiocaaaacaaaaaaclaaaaaaegaobaaa
acaaaaaaaaaaaaahhcaabaaaabaaaaaaegacbaaaabaaaaaaegacbaaaadaaaaaa
diaaaaahccaabaaaaaaaaaaabkaabaaaaaaaaaaabkaabaaaaaaaaaaadcaaaaak
bcaabaaaaaaaaaaaakaabaaaaaaaaaaaakaabaaaaaaaaaaabkaabaiaebaaaaaa
aaaaaaaadcaaaaakhccabaaaafaaaaaaegiccaaaacaaaaaacmaaaaaaagaabaaa
aaaaaaaaegacbaaaabaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES


#ifdef VERTEX

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _Scale;
uniform lowp vec4 _LightColor0;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_SHC;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAr;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
uniform highp vec4 _Time;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  mediump vec2 wrlPos_1;
  mediump vec3 worldNormal_2;
  lowp vec3 tmpvar_3;
  lowp vec3 tmpvar_4;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_Object2World * vec4(0.0, 1.0, 0.0, 0.0)).xyz);
  worldNormal_2 = tmpvar_5;
  mediump vec4 tmpvar_6;
  tmpvar_6.w = 1.0;
  tmpvar_6.xyz = worldNormal_2;
  mediump vec3 tmpvar_7;
  highp float vC_8;
  mediump vec3 x3_9;
  mediump vec3 x2_10;
  mediump vec3 x1_11;
  highp float tmpvar_12;
  tmpvar_12 = dot (unity_SHAr, tmpvar_6);
  x1_11.x = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = dot (unity_SHAg, tmpvar_6);
  x1_11.y = tmpvar_13;
  highp float tmpvar_14;
  tmpvar_14 = dot (unity_SHAb, tmpvar_6);
  x1_11.z = tmpvar_14;
  mediump vec4 tmpvar_15;
  tmpvar_15 = (worldNormal_2.xyzz * worldNormal_2.yzzx);
  highp float tmpvar_16;
  tmpvar_16 = dot (unity_SHBr, tmpvar_15);
  x2_10.x = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = dot (unity_SHBg, tmpvar_15);
  x2_10.y = tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = dot (unity_SHBb, tmpvar_15);
  x2_10.z = tmpvar_18;
  mediump float tmpvar_19;
  tmpvar_19 = ((worldNormal_2.x * worldNormal_2.x) - (worldNormal_2.y * worldNormal_2.y));
  vC_8 = tmpvar_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (unity_SHC.xyz * vC_8);
  x3_9 = tmpvar_20;
  tmpvar_7 = ((x1_11 + x2_10) + x3_9);
  tmpvar_4 = tmpvar_7;
  mediump vec3 tmpvar_21;
  tmpvar_21 = (max (0.0, dot (worldNormal_2, _WorldSpaceLightPos0.xyz)) * _LightColor0.xyz);
  tmpvar_3 = tmpvar_21;
  highp vec4 tmpvar_22;
  tmpvar_22 = (glstate_matrix_mvp * _glesVertex);
  highp vec4 o_23;
  highp vec4 tmpvar_24;
  tmpvar_24 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_25;
  tmpvar_25.x = tmpvar_24.x;
  tmpvar_25.y = (tmpvar_24.y * _ProjectionParams.x);
  o_23.xy = (tmpvar_25 + tmpvar_24.w);
  o_23.zw = tmpvar_22.zw;
  highp vec2 tmpvar_26;
  tmpvar_26 = (_Object2World * _glesVertex).xz;
  wrlPos_1 = tmpvar_26;
  highp vec2 tmpvar_27;
  tmpvar_27.x = (_glesMultiTexCoord0.x + (wrlPos_1.x * _Scale));
  tmpvar_27.y = (_glesMultiTexCoord0.y + (wrlPos_1.y * _Scale));
  gl_Position = tmpvar_22;
  xlv_TEXCOORD0 = (abs((sin((_Time.x * 5.0)) / 2.0)) + tmpvar_27);
  xlv_TEXCOORD2 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
  xlv_COLOR0 = tmpvar_3;
  xlv_TEXCOORD1 = o_23;
  xlv_COLOR1 = tmpvar_4;
}



#endif
#ifdef FRAGMENT

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _DepthFactor;
uniform sampler2D _CameraDepthTexture;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 _LightShadowData;
uniform highp vec4 _ZBufferParams;
void main ()
{
  lowp vec4 tmpvar_1;
  lowp vec4 col_2;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD0);
  col_2.w = tmpvar_3.w;
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2DProj (_CameraDepthTexture, xlv_TEXCOORD1);
  highp float z_5;
  z_5 = tmpvar_4.x;
  lowp float tmpvar_6;
  mediump float lightShadowDataX_7;
  highp float dist_8;
  lowp float tmpvar_9;
  tmpvar_9 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD2).x;
  dist_8 = tmpvar_9;
  highp float tmpvar_10;
  tmpvar_10 = _LightShadowData.x;
  lightShadowDataX_7 = tmpvar_10;
  highp float tmpvar_11;
  tmpvar_11 = max (float((dist_8 > (xlv_TEXCOORD2.z / xlv_TEXCOORD2.w))), lightShadowDataX_7);
  tmpvar_6 = tmpvar_11;
  col_2.xyz = (tmpvar_3.xyz * ((xlv_COLOR0 * tmpvar_6) + xlv_COLOR1));
  highp vec4 tmpvar_12;
  tmpvar_12.xyz = col_2.xyz;
  tmpvar_12.w = clamp ((_DepthFactor * ((1.0/(((_ZBufferParams.z * z_5) + _ZBufferParams.w))) - xlv_TEXCOORD1.w)), 0.0, 1.0);
  tmpvar_1 = tmpvar_12;
  gl_FragData[0] = tmpvar_1;
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES


#ifdef VERTEX

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _Scale;
uniform lowp vec4 _LightColor0;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_SHC;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAr;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
uniform highp vec4 _Time;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  mediump vec2 wrlPos_1;
  mediump vec3 worldNormal_2;
  lowp vec3 tmpvar_3;
  lowp vec3 tmpvar_4;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_Object2World * vec4(0.0, 1.0, 0.0, 0.0)).xyz);
  worldNormal_2 = tmpvar_5;
  mediump vec4 tmpvar_6;
  tmpvar_6.w = 1.0;
  tmpvar_6.xyz = worldNormal_2;
  mediump vec3 tmpvar_7;
  highp float vC_8;
  mediump vec3 x3_9;
  mediump vec3 x2_10;
  mediump vec3 x1_11;
  highp float tmpvar_12;
  tmpvar_12 = dot (unity_SHAr, tmpvar_6);
  x1_11.x = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = dot (unity_SHAg, tmpvar_6);
  x1_11.y = tmpvar_13;
  highp float tmpvar_14;
  tmpvar_14 = dot (unity_SHAb, tmpvar_6);
  x1_11.z = tmpvar_14;
  mediump vec4 tmpvar_15;
  tmpvar_15 = (worldNormal_2.xyzz * worldNormal_2.yzzx);
  highp float tmpvar_16;
  tmpvar_16 = dot (unity_SHBr, tmpvar_15);
  x2_10.x = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = dot (unity_SHBg, tmpvar_15);
  x2_10.y = tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = dot (unity_SHBb, tmpvar_15);
  x2_10.z = tmpvar_18;
  mediump float tmpvar_19;
  tmpvar_19 = ((worldNormal_2.x * worldNormal_2.x) - (worldNormal_2.y * worldNormal_2.y));
  vC_8 = tmpvar_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (unity_SHC.xyz * vC_8);
  x3_9 = tmpvar_20;
  tmpvar_7 = ((x1_11 + x2_10) + x3_9);
  tmpvar_4 = tmpvar_7;
  mediump vec3 tmpvar_21;
  tmpvar_21 = (max (0.0, dot (worldNormal_2, _WorldSpaceLightPos0.xyz)) * _LightColor0.xyz);
  tmpvar_3 = tmpvar_21;
  highp vec4 tmpvar_22;
  tmpvar_22 = (glstate_matrix_mvp * _glesVertex);
  highp vec4 o_23;
  highp vec4 tmpvar_24;
  tmpvar_24 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_25;
  tmpvar_25.x = tmpvar_24.x;
  tmpvar_25.y = (tmpvar_24.y * _ProjectionParams.x);
  o_23.xy = (tmpvar_25 + tmpvar_24.w);
  o_23.zw = tmpvar_22.zw;
  highp vec2 tmpvar_26;
  tmpvar_26 = (_Object2World * _glesVertex).xz;
  wrlPos_1 = tmpvar_26;
  highp vec2 tmpvar_27;
  tmpvar_27.x = (_glesMultiTexCoord0.x + (wrlPos_1.x * _Scale));
  tmpvar_27.y = (_glesMultiTexCoord0.y + (wrlPos_1.y * _Scale));
  highp vec4 o_28;
  highp vec4 tmpvar_29;
  tmpvar_29 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_30;
  tmpvar_30.x = tmpvar_29.x;
  tmpvar_30.y = (tmpvar_29.y * _ProjectionParams.x);
  o_28.xy = (tmpvar_30 + tmpvar_29.w);
  o_28.zw = tmpvar_22.zw;
  gl_Position = tmpvar_22;
  xlv_TEXCOORD0 = (abs((sin((_Time.x * 5.0)) / 2.0)) + tmpvar_27);
  xlv_TEXCOORD2 = o_28;
  xlv_COLOR0 = tmpvar_3;
  xlv_TEXCOORD1 = o_23;
  xlv_COLOR1 = tmpvar_4;
}



#endif
#ifdef FRAGMENT

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _DepthFactor;
uniform sampler2D _CameraDepthTexture;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 _ZBufferParams;
void main ()
{
  lowp vec4 tmpvar_1;
  lowp vec4 col_2;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD0);
  col_2.w = tmpvar_3.w;
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2DProj (_CameraDepthTexture, xlv_TEXCOORD1);
  highp float z_5;
  z_5 = tmpvar_4.x;
  col_2.xyz = (tmpvar_3.xyz * ((xlv_COLOR0 * texture2DProj (_ShadowMapTexture, xlv_TEXCOORD2).x) + xlv_COLOR1));
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = col_2.xyz;
  tmpvar_6.w = clamp ((_DepthFactor * ((1.0/(((_ZBufferParams.z * z_5) + _ZBufferParams.w))) - xlv_TEXCOORD1.w)), 0.0, 1.0);
  tmpvar_1 = tmpvar_6;
  gl_FragData[0] = tmpvar_1;
}



#endif"
}

SubProgram "flash " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 0 [glstate_matrix_mvp]
Vector 8 [_Time]
Vector 9 [_ProjectionParams]
Vector 10 [_WorldSpaceLightPos0]
Vector 11 [unity_SHAr]
Vector 12 [unity_SHAg]
Vector 13 [unity_SHAb]
Vector 14 [unity_SHBr]
Vector 15 [unity_SHBg]
Vector 16 [unity_SHBb]
Vector 17 [unity_SHC]
Matrix 4 [_Object2World]
Vector 18 [unity_NPOTScale]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"agal_vs
c21 -0.020833 -0.125 1.0 0.5
c22 -0.000002 -0.000022 0.002604 0.00026
c23 0.0 0.795775 0.5 0.0
c24 6.283185 -3.141593 0.0 0.0
[bc]
aaaaaaaaaaaaaeacagaaaaffabaaaaaaaaaaaaaaaaaaaaaa mov r0.z, c6.y
aaaaaaaaaaaaabacaeaaaaffabaaaaaaaaaaaaaaaaaaaaaa mov r0.x, c4.y
aaaaaaaaaaaaacacafaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov r0.y, c5
bcaaaaaaaaaaaiacaaaaaakeacaaaaaaaaaaaakeacaaaaaa dp3 r0.w, r0.xyzz, r0.xyzz
akaaaaaaaaaaaiacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa rsq r0.w, r0.w
adaaaaaaaaaaahacaaaaaappacaaaaaaaaaaaakeacaaaaaa mul r0.xyz, r0.w, r0.xyzz
adaaaaaaabaaapacaaaaaakeacaaaaaaaaaaaacjacaaaaaa mul r1, r0.xyzz, r0.yzzx
aaaaaaaaaaaaaiacbfaaaakkabaaaaaaaaaaaaaaaaaaaaaa mov r0.w, c21.z
bdaaaaaaacaaaeacaaaaaaoeacaaaaaaanaaaaoeabaaaaaa dp4 r2.z, r0, c13
bdaaaaaaacaaacacaaaaaaoeacaaaaaaamaaaaoeabaaaaaa dp4 r2.y, r0, c12
bdaaaaaaacaaabacaaaaaaoeacaaaaaaalaaaaoeabaaaaaa dp4 r2.x, r0, c11
adaaaaaaacaaaiacaaaaaaffacaaaaaaaaaaaaffacaaaaaa mul r2.w, r0.y, r0.y
adaaaaaaadaaaiacaaaaaaaaacaaaaaaaaaaaaaaacaaaaaa mul r3.w, r0.x, r0.x
acaaaaaaaaaaaiacadaaaappacaaaaaaacaaaappacaaaaaa sub r0.w, r3.w, r2.w
adaaaaaaaeaaahacaaaaaappacaaaaaabbaaaaoeabaaaaaa mul r4.xyz, r0.w, c17
aaaaaaaaaaaaaiacaiaaaaaaabaaaaaaaaaaaaaaaaaaaaaa mov r0.w, c8.x
adaaaaaaaaaaaiacaaaaaappacaaaaaabhaaaaffabaaaaaa mul r0.w, r0.w, c23.y
abaaaaaaaaaaaiacaaaaaappacaaaaaabhaaaakkabaaaaaa add r0.w, r0.w, c23.z
aiaaaaaaaaaaaiacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa frc r0.w, r0.w
bdaaaaaaadaaaeacabaaaaoeacaaaaaabaaaaaoeabaaaaaa dp4 r3.z, r1, c16
bdaaaaaaadaaabacabaaaaoeacaaaaaaaoaaaaoeabaaaaaa dp4 r3.x, r1, c14
bdaaaaaaadaaacacabaaaaoeacaaaaaaapaaaaoeabaaaaaa dp4 r3.y, r1, c15
abaaaaaaadaaahacacaaaakeacaaaaaaadaaaakeacaaaaaa add r3.xyz, r2.xyzz, r3.xyzz
bcaaaaaaaaaaacacaaaaaakeacaaaaaaakaaaaoeabaaaaaa dp3 r0.y, r0.xyzz, c10
bdaaaaaaabaaaiacaaaaaaoeaaaaaaaaadaaaaoeabaaaaaa dp4 r1.w, a0, c3
bdaaaaaaabaaabacaaaaaaoeaaaaaaaaaaaaaaoeabaaaaaa dp4 r1.x, a0, c0
bdaaaaaaabaaacacaaaaaaoeaaaaaaaaabaaaaoeabaaaaaa dp4 r1.y, a0, c1
adaaaaaaacaaahacabaaaapeacaaaaaabfaaaappabaaaaaa mul r2.xyz, r1.xyww, c21.w
bdaaaaaaabaaaeacaaaaaaoeaaaaaaaaacaaaaoeabaaaaaa dp4 r1.z, a0, c2
adaaaaaaacaaacacacaaaaffacaaaaaaajaaaaaaabaaaaaa mul r2.y, r2.y, c9.x
abaaaaaaacaaadacacaaaafeacaaaaaaacaaaakkacaaaaaa add r2.xy, r2.xyyy, r2.z
adaaaaaaacaaadacacaaaafeacaaaaaabcaaaaoeabaaaaaa mul r2.xy, r2.xyyy, c18
aaaaaaaaacaaamacabaaaaopacaaaaaaaaaaaaaaaaaaaaaa mov r2.zw, r1.wwzw
adaaaaaaaaaaaiacaaaaaappacaaaaaabiaaaaaaabaaaaaa mul r0.w, r0.w, c24.x
abaaaaaaaaaaaiacaaaaaappacaaaaaabiaaaaffabaaaaaa add r0.w, r0.w, c24.y
aaaaaaaaacaaapaeacaaaaoeacaaaaaaaaaaaaaaaaaaaaaa mov v2, r2
aaaaaaaaabaaapaeacaaaaoeacaaaaaaaaaaaaaaaaaaaaaa mov v1, r2
apaaaaaaacaaabacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa sin r2.x, r0.w
baaaaaaaacaaacacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa cos r2.y, r0.w
ahaaaaaaaaaaaiacaaaaaaffacaaaaaabhaaaaaaabaaaaaa max r0.w, r0.y, c23.x
adaaaaaaaaaaabacacaaaaffacaaaaaabfaaaappabaaaaaa mul r0.x, r2.y, c21.w
beaaaaaaaaaaaeacaaaaaaaaacaaaaaaaaaaaaaaaaaaaaaa abs r0.z, r0.x
bdaaaaaaaaaaacacaaaaaaoeaaaaaaaaagaaaaoeabaaaaaa dp4 r0.y, a0, c6
bdaaaaaaaaaaabacaaaaaaoeaaaaaaaaaeaaaaoeabaaaaaa dp4 r0.x, a0, c4
adaaaaaaaaaaacacaaaaaaffacaaaaaabeaaaaaaabaaaaaa mul r0.y, r0.y, c20.x
abaaaaaaaaaaacacaaaaaaffacaaaaaaadaaaaoeaaaaaaaa add r0.y, r0.y, a3
adaaaaaaaaaaabacaaaaaaaaacaaaaaabeaaaaoeabaaaaaa mul r0.x, r0.x, c20
abaaaaaaaaaaabacaaaaaaaaacaaaaaaadaaaaoeaaaaaaaa add r0.x, r0.x, a3
abaaaaaaagaaahaeadaaaakeacaaaaaaaeaaaakeacaaaaaa add v6.xyz, r3.xyzz, r4.xyzz
aaaaaaaaaaaaapadabaaaaoeacaaaaaaaaaaaaaaaaaaaaaa mov o0, r1
abaaaaaaaaaaadaeaaaaaakkacaaaaaaaaaaaafeacaaaaaa add v0.xy, r0.z, r0.xyyy
adaaaaaaahaaahaeaaaaaappacaaaaaabdaaaaoeabaaaaaa mul v7.xyz, r0.w, c19
aaaaaaaaaaaaamaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v0.zw, c0
aaaaaaaaagaaaiaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v6.w, c0
aaaaaaaaahaaaiaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v7.w, c0
"
}

SubProgram "d3d11_9x " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
ConstBuffer "$Globals" 128 // 116 used size, 6 vars
Vector 16 [_LightColor0] 4
Float 112 [_Scale]
ConstBuffer "UnityPerCamera" 128 // 96 used size, 8 vars
Vector 0 [_Time] 4
Vector 80 [_ProjectionParams] 4
ConstBuffer "UnityLighting" 720 // 720 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
Vector 608 [unity_SHAr] 4
Vector 624 [unity_SHAg] 4
Vector 640 [unity_SHAb] 4
Vector 656 [unity_SHBr] 4
Vector 672 [unity_SHBg] 4
Vector 688 [unity_SHBb] 4
Vector 704 [unity_SHC] 4
ConstBuffer "UnityPerDraw" 336 // 256 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
BindCB "UnityLighting" 2
BindCB "UnityPerDraw" 3
// 39 instructions, 4 temp regs, 0 temp arrays:
// ALU 34 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0_level_9_1
eefieceddenomlclbbglbdbkafieepgfcmceckdkabaaaaaajeakaaaaaeaaaaaa
daaaaaaapaadaaaageajaaaaniajaaaaebgpgodjliadaaaaliadaaaaaaacpopp
daadaaaaiiaaaaaaaiaaceaaaaaaieaaaaaaieaaaaaaceaaabaaieaaaaaaabaa
abaaabaaaaaaaaaaaaaaahaaabaaacaaaaaaaaaaabaaaaaaabaaadaaaaaaaaaa
abaaafaaabaaaeaaaaaaaaaaacaaaaaaabaaafaaaaaaaaaaacaacgaaahaaagaa
aaaaaaaaadaaaaaaaeaaanaaaaaaaaaaadaaamaaaeaabbaaaaaaaaaaaaaaaaaa
aaacpoppfbaaaaafbfaaapkaaaaaiadpaaaaaaaaaaaaaadpoelheldpfbaaaaaf
bgaaapkanlapmjeanlapejmaaaaaaaaaaaaaaaaafbaaaaafbhaaapkaabannalf
gballglhklkkckdlijiiiidjfbaaaaafbiaaapkaklkkkklmaaaaaaloaaaaiadp
aaaaaadpbpaaaaacafaaaaiaaaaaapjabpaaaaacafaaabiaabaaapjaabaaaaac
aaaaaiiabfaaaakaceaaaaacaaaaahiabcaaoekaajaaaaadabaaabiaagaaoeka
aaaaoeiaajaaaaadabaaaciaahaaoekaaaaaoeiaajaaaaadabaaaeiaaiaaoeka
aaaaoeiaafaaaaadacaaapiaaaaacjiaaaaakeiaajaaaaadadaaabiaajaaoeka
acaaoeiaajaaaaadadaaaciaakaaoekaacaaoeiaajaaaaadadaaaeiaalaaoeka
acaaoeiaacaaaaadabaaahiaabaaoeiaadaaoeiaafaaaaadaaaaaiiaaaaaffia
aaaaffiaaeaaaaaeaaaaaiiaaaaaaaiaaaaaaaiaaaaappibaiaaaaadaaaaabia
aaaaoeiaafaaoekaalaaaaadaaaaabiaaaaaaaiabfaaffkaafaaaaadacaaahoa
aaaaaaiaabaaoekaaeaaaaaeaeaaahoaamaaoekaaaaappiaabaaoeiaabaaaaac
aaaaamiabfaaoekaaeaaaaaeaaaaabiaadaaaakaaaaappiaaaaakkiabdaaaaac
aaaaabiaaaaaaaiaaeaaaaaeaaaaabiaaaaaaaiabgaaaakabgaaffkacfaaaaae
abaaaciaaaaaaaiabhaaoekabiaaoekaafaaaaadaaaaabiaabaaffiabfaakkka
cdaaaaacaaaaabiaaaaaaaiaafaaaaadaaaaagiaaaaaffjabcaaoakaaeaaaaae
aaaaagiabbaaoakaaaaaaajaaaaaoeiaaeaaaaaeaaaaagiabdaaoakaaaaakkja
aaaaoeiaaeaaaaaeaaaaagiabeaaoakaaaaappjaaaaaoeiaaeaaaaaeaaaaagia
aaaaoeiaacaaaakaabaanajaacaaaaadaaaaadoaaaaaojiaaaaaaaiaafaaaaad
aaaaapiaaaaaffjaaoaaoekaaeaaaaaeaaaaapiaanaaoekaaaaaaajaaaaaoeia
aeaaaaaeaaaaapiaapaaoekaaaaakkjaaaaaoeiaaeaaaaaeaaaaapiabaaaoeka
aaaappjaaaaaoeiaaeaaaaaeaaaaadmaaaaappiaaaaaoekaaaaaoeiaabaaaaac
abaaamiaaaaaoeiaabaaaaacaaaaammaabaaoeiaafaaaaadaaaaaciaaaaaffia
aeaaaakaafaaaaadaaaaafiaaaaapeiabfaakkkaafaaaaadaaaaaiiaaaaaffia
bfaakkkaacaaaaadabaaadiaaaaakkiaaaaaomiaabaaaaacabaaapoaabaaoeia
abaaaaacadaaapoaabaaoeiappppaaaafdeieefcgmafaaaaeaaaabaaflabaaaa
fjaaaaaeegiocaaaaaaaaaaaaiaaaaaafjaaaaaeegiocaaaabaaaaaaagaaaaaa
fjaaaaaeegiocaaaacaaaaaacnaaaaaafjaaaaaeegiocaaaadaaaaaabaaaaaaa
fpaaaaadpcbabaaaaaaaaaaafpaaaaaddcbabaaaabaaaaaaghaaaaaepccabaaa
aaaaaaaaabaaaaaagfaaaaaddccabaaaabaaaaaagfaaaaadpccabaaaacaaaaaa
gfaaaaadhccabaaaadaaaaaagfaaaaadpccabaaaaeaaaaaagfaaaaadhccabaaa
afaaaaaagiaaaaacaeaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaa
egiocaaaadaaaaaaabaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaadaaaaaa
aaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaa
egiocaaaadaaaaaaacaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaak
pcaabaaaaaaaaaaaegiocaaaadaaaaaaadaaaaaapgbpbaaaaaaaaaaaegaobaaa
aaaaaaaadgaaaaafpccabaaaaaaaaaaaegaobaaaaaaaaaaadiaaaaaidcaabaaa
abaaaaaafgbfbaaaaaaaaaaaigiacaaaadaaaaaaanaaaaaadcaaaaakdcaabaaa
abaaaaaaigiacaaaadaaaaaaamaaaaaaagbabaaaaaaaaaaaegaabaaaabaaaaaa
dcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaaoaaaaaakgbkbaaaaaaaaaaa
egaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaapaaaaaa
pgbpbaaaaaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaegaabaaa
abaaaaaaagiacaaaaaaaaaaaahaaaaaaegbabaaaabaaaaaadiaaaaaiecaabaaa
abaaaaaaakiacaaaabaaaaaaaaaaaaaaabeaaaaaaaaakaeaenaaaaagecaabaaa
abaaaaaaaanaaaaackaabaaaabaaaaaadiaaaaahecaabaaaabaaaaaackaabaaa
abaaaaaaabeaaaaaaaaaaadpaaaaaaaidccabaaaabaaaaaaegaabaaaabaaaaaa
kgakbaiaibaaaaaaabaaaaaadiaaaaaibcaabaaaabaaaaaabkaabaaaaaaaaaaa
akiacaaaabaaaaaaafaaaaaadiaaaaahicaabaaaabaaaaaaakaabaaaabaaaaaa
abeaaaaaaaaaaadpdiaaaaakfcaabaaaabaaaaaaagadbaaaaaaaaaaaaceaaaaa
aaaaaadpaaaaaaaaaaaaaadpaaaaaaaaaaaaaaahdcaabaaaaaaaaaaakgakbaaa
abaaaaaamgaabaaaabaaaaaadgaaaaafpccabaaaacaaaaaaegaobaaaaaaaaaaa
dgaaaaafpccabaaaaeaaaaaaegaobaaaaaaaaaaabaaaaaajbcaabaaaaaaaaaaa
egiccaaaadaaaaaaanaaaaaaegiccaaaadaaaaaaanaaaaaaeeaaaaafbcaabaaa
aaaaaaaaakaabaaaaaaaaaaadiaaaaaihcaabaaaaaaaaaaaagaabaaaaaaaaaaa
egiccaaaadaaaaaaanaaaaaabaaaaaaibcaabaaaabaaaaaaegacbaaaaaaaaaaa
egiccaaaacaaaaaaaaaaaaaadeaaaaahbcaabaaaabaaaaaaakaabaaaabaaaaaa
abeaaaaaaaaaaaaadiaaaaaihccabaaaadaaaaaaagaabaaaabaaaaaaegiccaaa
aaaaaaaaabaaaaaadgaaaaaficaabaaaaaaaaaaaabeaaaaaaaaaiadpbbaaaaai
bcaabaaaabaaaaaaegiocaaaacaaaaaacgaaaaaaegaobaaaaaaaaaaabbaaaaai
ccaabaaaabaaaaaaegiocaaaacaaaaaachaaaaaaegaobaaaaaaaaaaabbaaaaai
ecaabaaaabaaaaaaegiocaaaacaaaaaaciaaaaaaegaobaaaaaaaaaaadiaaaaah
pcaabaaaacaaaaaajgacbaaaaaaaaaaaegakbaaaaaaaaaaabbaaaaaibcaabaaa
adaaaaaaegiocaaaacaaaaaacjaaaaaaegaobaaaacaaaaaabbaaaaaiccaabaaa
adaaaaaaegiocaaaacaaaaaackaaaaaaegaobaaaacaaaaaabbaaaaaiecaabaaa
adaaaaaaegiocaaaacaaaaaaclaaaaaaegaobaaaacaaaaaaaaaaaaahhcaabaaa
abaaaaaaegacbaaaabaaaaaaegacbaaaadaaaaaadiaaaaahccaabaaaaaaaaaaa
bkaabaaaaaaaaaaabkaabaaaaaaaaaaadcaaaaakbcaabaaaaaaaaaaaakaabaaa
aaaaaaaaakaabaaaaaaaaaaabkaabaiaebaaaaaaaaaaaaaadcaaaaakhccabaaa
afaaaaaaegiccaaaacaaaaaacmaaaaaaagaabaaaaaaaaaaaegacbaaaabaaaaaa
doaaaaabejfdeheogmaaaaaaadaaaaaaaiaaaaaafaaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaaaaaaaaaapapaaaafjaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaa
adadaaaagcaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaaaaaafaepfdej
feejepeoaafeeffiedepepfceeaaeoepfcenebemaaklklklepfdeheoleaaaaaa
agaaaaaaaiaaaaaajiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaa
keaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaadamaaaakeaaaaaaacaaaaaa
aaaaaaaaadaaaaaaacaaaaaaapaaaaaaknaaaaaaaaaaaaaaaaaaaaaaadaaaaaa
adaaaaaaahaiaaaakeaaaaaaabaaaaaaaaaaaaaaadaaaaaaaeaaaaaaapaaaaaa
knaaaaaaabaaaaaaaaaaaaaaadaaaaaaafaaaaaaahaiaaaafdfgfpfaepfdejfe
ejepeoaafeeffiedepepfceeaaedepemepfcaakl"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal (normalize(_glesNormal))
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 407
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    highp vec4 _ShadowCoord;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 400
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 393
uniform sampler2D _MainTex;
#line 417
#line 421
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
#line 437
uniform highp float _DepthFactor;
#line 284
highp vec4 ComputeScreenPos( in highp vec4 pos ) {
    #line 286
    highp vec4 o = (pos * 0.5);
    o.xy = (vec2( o.x, (o.y * _ProjectionParams.x)) + o.w);
    o.zw = pos.zw;
    return o;
}
#line 137
mediump vec3 ShadeSH9( in mediump vec4 normal ) {
    mediump vec3 x1;
    mediump vec3 x2;
    mediump vec3 x3;
    x1.x = dot( unity_SHAr, normal);
    #line 141
    x1.y = dot( unity_SHAg, normal);
    x1.z = dot( unity_SHAb, normal);
    mediump vec4 vB = (normal.xyzz * normal.yzzx);
    x2.x = dot( unity_SHBr, vB);
    #line 145
    x2.y = dot( unity_SHBg, vB);
    x2.z = dot( unity_SHBb, vB);
    highp float vC = ((normal.x * normal.x) - (normal.y * normal.y));
    x3 = (unity_SHC.xyz * vC);
    #line 149
    return ((x1 + x2) + x3);
}
#line 417
highp vec2 offset( in highp vec2 uv ) {
    return (abs((sin((_Time.x * 5.0)) / 2.0)) + uv);
}
#line 422
v2f vert( in appdata_t v ) {
    v2f o;
    #line 425
    mediump vec3 worldNormal = normalize((_Object2World * vec4( 0.0, 1.0, 0.0, 0.0)).xyz);
    o.ambient = ShadeSH9( vec4( worldNormal, 1.0));
    mediump float nl = max( 0.0, dot( worldNormal, _WorldSpaceLightPos0.xyz));
    o.diff = (nl * _LightColor0.xyz);
    #line 429
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.scrPos = ComputeScreenPos( o.pos);
    mediump vec2 wrlPos = (_Object2World * v.vertex).xz;
    o.uv = offset( vec2( (v.texcoord.x + (wrlPos.x * _Scale)), (v.texcoord.y + (wrlPos.y * _Scale))));
    #line 433
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    return o;
}
out highp vec2 xlv_TEXCOORD0;
out highp vec4 xlv_TEXCOORD2;
out lowp vec3 xlv_COLOR0;
out highp vec4 xlv_TEXCOORD1;
out lowp vec3 xlv_COLOR1;
void main() {
    v2f xl_retval;
    appdata_t xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.texcoord = vec2(gl_MultiTexCoord0);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vert( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec2(xl_retval.uv);
    xlv_TEXCOORD2 = vec4(xl_retval._ShadowCoord);
    xlv_COLOR0 = vec3(xl_retval.diff);
    xlv_TEXCOORD1 = vec4(xl_retval.scrPos);
    xlv_COLOR1 = vec3(xl_retval.ambient);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 407
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    highp vec4 _ShadowCoord;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 400
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 393
uniform sampler2D _MainTex;
#line 417
#line 421
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
#line 437
uniform highp float _DepthFactor;
#line 280
highp float LinearEyeDepth( in highp float z ) {
    #line 282
    return (1.0 / ((_ZBufferParams.z * z) + _ZBufferParams.w));
}
#line 393
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    highp float dist = textureProj( _ShadowMapTexture, shadowCoord).x;
    mediump float lightShadowDataX = _LightShadowData.x;
    #line 397
    return max( float((dist > (shadowCoord.z / shadowCoord.w))), lightShadowDataX);
}
#line 438
lowp vec4 frag( in v2f i ) {
    lowp vec4 col = texture( _MainTex, i.uv);
    #line 441
    highp float depth = LinearEyeDepth( float( textureProj( _CameraDepthTexture, i.scrPos).xyzw));
    highp float diff = xll_saturate_f((_DepthFactor * (depth - i.scrPos.w)));
    lowp float shadow = unitySampleShadow( i._ShadowCoord);
    lowp vec3 lighting = ((i.diff * shadow) + i.ambient);
    #line 445
    col.xyz *= lighting;
    return vec4( col.xyz, diff);
}
in highp vec2 xlv_TEXCOORD0;
in highp vec4 xlv_TEXCOORD2;
in lowp vec3 xlv_COLOR0;
in highp vec4 xlv_TEXCOORD1;
in lowp vec3 xlv_COLOR1;
void main() {
    lowp vec4 xl_retval;
    v2f xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.uv = vec2(xlv_TEXCOORD0);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD2);
    xlt_i.diff = vec3(xlv_COLOR0);
    xlt_i.scrPos = vec4(xlv_TEXCOORD1);
    xlt_i.ambient = vec3(xlv_COLOR1);
    xl_retval = frag( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Vector 9 [_Time]
Vector 10 [_ProjectionParams]
Vector 11 [_WorldSpaceLightPos0]
Vector 12 [unity_SHAr]
Vector 13 [unity_SHAg]
Vector 14 [unity_SHAb]
Vector 15 [unity_SHBr]
Vector 16 [unity_SHBg]
Vector 17 [unity_SHBb]
Vector 18 [unity_SHC]
Matrix 5 [_Object2World]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"!!ARBvp1.0
# 55 ALU
PARAM c[25] = { { 0.79577452, 0, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..20],
		{ 24.980801, -24.980801, 0.25 },
		{ -60.145809, 60.145809, 85.453789, -85.453789 },
		{ -64.939346, 64.939346, 19.73921, -19.73921 },
		{ -1, 1, -9, 0.75 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
MOV R0.x, c[0];
MUL R0.x, R0, c[9];
ADD R0.x, R0, -c[21].z;
FRC R2.w, R0.x;
ADD R0.xyz, -R2.w, c[0].yzww;
MUL R2.xyz, R0, R0;
MUL R0.xyz, R2, c[21].xyxw;
ADD R0.xyz, R0, c[22].xyxw;
MAD R1.xyz, R0, R2, c[22].zwzw;
MAD R3.xyz, R1, R2, c[23].xyxw;
MAD R3.xyz, R3, R2, c[23].zwzw;
MAD R3.xyz, R3, R2, c[24].xyxw;
MOV R1.w, c[0];
MOV R0.z, c[7].y;
MOV R0.x, c[5].y;
MOV R0.y, c[6];
DP3 R0.w, R0, R0;
RSQ R0.w, R0.w;
MUL R1.xyz, R0.w, R0;
MUL R0, R1.xyzz, R1.yzzx;
DP4 R4.z, R1, c[14];
DP4 R4.y, R1, c[13];
DP4 R4.x, R1, c[12];
DP4 R5.z, R0, c[17];
DP4 R5.x, R0, c[15];
DP4 R5.y, R0, c[16];
ADD R0.xyz, R4, R5;
MUL R0.w, R1.y, R1.y;
MAD R0.w, R1.x, R1.x, -R0;
DP3 R1.x, R1, c[11];
MAX R1.x, R1, c[0].y;
SLT R4.x, R2.w, c[21].z;
SGE R4.yz, R2.w, c[24].xzww;
MOV R2.xz, R4;
DP3 R2.y, R4, c[24].xyxw;
DP3 R1.w, R3, -R2;
MUL R2.xyz, R0.w, c[18];
ADD result.color.secondary.xyz, R0, R2;
MUL R0.w, R1, c[0].z;
ABS R2.w, R0;
DP4 R0.z, vertex.position, c[7];
MAD R3.y, R0.z, c[20].x, vertex.texcoord[0];
DP4 R1.w, vertex.position, c[5];
MAD R3.x, R1.w, c[20], vertex.texcoord[0];
DP4 R0.w, vertex.position, c[4];
DP4 R0.z, vertex.position, c[3];
DP4 R0.x, vertex.position, c[1];
DP4 R0.y, vertex.position, c[2];
MUL R2.xyz, R0.xyww, c[0].z;
MUL R2.y, R2, c[10].x;
ADD result.texcoord[0].xy, R2.w, R3;
ADD result.texcoord[1].xy, R2, R2.z;
MOV result.position, R0;
MUL result.color.xyz, R1.x, c[19];
MOV result.texcoord[1].zw, R0;
END
# 55 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 0 [glstate_matrix_mvp]
Vector 8 [_Time]
Vector 9 [_ProjectionParams]
Vector 10 [_ScreenParams]
Vector 11 [_WorldSpaceLightPos0]
Vector 12 [unity_SHAr]
Vector 13 [unity_SHAg]
Vector 14 [unity_SHAb]
Vector 15 [unity_SHBr]
Vector 16 [unity_SHBg]
Vector 17 [unity_SHBb]
Vector 18 [unity_SHC]
Matrix 4 [_Object2World]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"vs_2_0
; 52 ALU
def c21, -0.02083333, -0.12500000, 1.00000000, 0.50000000
def c22, -0.00000155, -0.00002170, 0.00260417, 0.00026042
def c23, 0.00000000, 0.79577452, 0.50000000, 0
def c24, 6.28318501, -3.14159298, 0, 0
dcl_position0 v0
dcl_texcoord0 v1
dp4 r2.w, v0, c3
mov r0.z, c6.y
mov r0.x, c4.y
mov r0.y, c5
dp3 r0.w, r0, r0
rsq r0.w, r0.w
mul r0.xyz, r0.w, r0
mul r0.w, r0.y, r0.y
mad r1.x, r0, r0, -r0.w
mov r0.w, c21.z
mul r2.xyz, r1.x, c18
mul r1, r0.xyzz, r0.yzzx
dp4 r3.z, r0, c14
dp4 r3.y, r0, c13
dp4 r3.x, r0, c12
mov r0.w, c8.x
mad r0.w, r0, c23.y, c23.z
frc r0.w, r0
dp3 r0.y, r0, c11
dp4 r4.z, r1, c17
dp4 r4.x, r1, c15
dp4 r4.y, r1, c16
add r1.xyz, r3, r4
add oD1.xyz, r1, r2
dp4 r2.z, v0, c2
dp4 r2.x, v0, c0
dp4 r2.y, v0, c1
mul r1.xyz, r2.xyww, c21.w
mul r1.y, r1, c9.x
mad r0.w, r0, c24.x, c24.y
mad oT1.xy, r1.z, c10.zwzw, r1
sincos r1.xy, r0.w, c22.xyzw, c21.xyzw
max r0.w, r0.y, c23.x
mul r0.x, r1.y, c21.w
abs r0.z, r0.x
dp4 r0.y, v0, c6
dp4 r0.x, v0, c4
mad r0.y, r0, c20.x, v1
mad r0.x, r0, c20, v1
mov oPos, r2
add oT0.xy, r0.z, r0
mul oD0.xyz, r0.w, c19
mov oT1.zw, r2
"
}

SubProgram "d3d11 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
ConstBuffer "$Globals" 64 // 52 used size, 5 vars
Vector 16 [_LightColor0] 4
Float 48 [_Scale]
ConstBuffer "UnityPerCamera" 128 // 96 used size, 8 vars
Vector 0 [_Time] 4
Vector 80 [_ProjectionParams] 4
ConstBuffer "UnityLighting" 720 // 720 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
Vector 608 [unity_SHAr] 4
Vector 624 [unity_SHAg] 4
Vector 640 [unity_SHAb] 4
Vector 656 [unity_SHBr] 4
Vector 672 [unity_SHBg] 4
Vector 688 [unity_SHBb] 4
Vector 704 [unity_SHC] 4
ConstBuffer "UnityPerDraw" 336 // 256 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
BindCB "UnityLighting" 2
BindCB "UnityPerDraw" 3
// 37 instructions, 4 temp regs, 0 temp arrays:
// ALU 33 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0
eefiecedmmjogfpagdenonoikogclepdoekejgkaabaaaaaahmagaaaaadaaaaaa
cmaaaaaakaaaaaaaeeabaaaaejfdeheogmaaaaaaadaaaaaaaiaaaaaafaaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaafjaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaadadaaaagcaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahaaaaaafaepfdejfeejepeoaafeeffiedepepfceeaaeoepfcenebemaaklklkl
epfdeheojmaaaaaaafaaaaaaaiaaaaaaiaaaaaaaaaaaaaaaabaaaaaaadaaaaaa
aaaaaaaaapaaaaaaimaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaadamaaaa
jfaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaimaaaaaaabaaaaaa
aaaaaaaaadaaaaaaadaaaaaaapaaaaaajfaaaaaaabaaaaaaaaaaaaaaadaaaaaa
aeaaaaaaahaiaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfceeaaedepem
epfcaaklfdeieefcdaafaaaaeaaaabaaemabaaaafjaaaaaeegiocaaaaaaaaaaa
aeaaaaaafjaaaaaeegiocaaaabaaaaaaagaaaaaafjaaaaaeegiocaaaacaaaaaa
cnaaaaaafjaaaaaeegiocaaaadaaaaaabaaaaaaafpaaaaadpcbabaaaaaaaaaaa
fpaaaaaddcbabaaaabaaaaaaghaaaaaepccabaaaaaaaaaaaabaaaaaagfaaaaad
dccabaaaabaaaaaagfaaaaadhccabaaaacaaaaaagfaaaaadpccabaaaadaaaaaa
gfaaaaadhccabaaaaeaaaaaagiaaaaacaeaaaaaadiaaaaaipcaabaaaaaaaaaaa
fgbfbaaaaaaaaaaaegiocaaaadaaaaaaabaaaaaadcaaaaakpcaabaaaaaaaaaaa
egiocaaaadaaaaaaaaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaak
pcaabaaaaaaaaaaaegiocaaaadaaaaaaacaaaaaakgbkbaaaaaaaaaaaegaobaaa
aaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaadaaaaaaadaaaaaapgbpbaaa
aaaaaaaaegaobaaaaaaaaaaadgaaaaafpccabaaaaaaaaaaaegaobaaaaaaaaaaa
diaaaaaidcaabaaaabaaaaaafgbfbaaaaaaaaaaaigiacaaaadaaaaaaanaaaaaa
dcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaamaaaaaaagbabaaaaaaaaaaa
egaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaaoaaaaaa
kgbkbaaaaaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaa
adaaaaaaapaaaaaapgbpbaaaaaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaa
abaaaaaaegaabaaaabaaaaaaagiacaaaaaaaaaaaadaaaaaaegbabaaaabaaaaaa
diaaaaaiecaabaaaabaaaaaaakiacaaaabaaaaaaaaaaaaaaabeaaaaaaaaakaea
enaaaaagecaabaaaabaaaaaaaanaaaaackaabaaaabaaaaaadiaaaaahecaabaaa
abaaaaaackaabaaaabaaaaaaabeaaaaaaaaaaadpaaaaaaaidccabaaaabaaaaaa
egaabaaaabaaaaaakgakbaiaibaaaaaaabaaaaaabaaaaaajbcaabaaaabaaaaaa
egiccaaaadaaaaaaanaaaaaaegiccaaaadaaaaaaanaaaaaaeeaaaaafbcaabaaa
abaaaaaaakaabaaaabaaaaaadiaaaaaihcaabaaaabaaaaaaagaabaaaabaaaaaa
egiccaaaadaaaaaaanaaaaaabaaaaaaibcaabaaaacaaaaaaegacbaaaabaaaaaa
egiccaaaacaaaaaaaaaaaaaadeaaaaahbcaabaaaacaaaaaaakaabaaaacaaaaaa
abeaaaaaaaaaaaaadiaaaaaihccabaaaacaaaaaaagaabaaaacaaaaaaegiccaaa
aaaaaaaaabaaaaaadiaaaaaiccaabaaaaaaaaaaabkaabaaaaaaaaaaaakiacaaa
abaaaaaaafaaaaaadiaaaaakncaabaaaacaaaaaaagahbaaaaaaaaaaaaceaaaaa
aaaaaadpaaaaaaaaaaaaaadpaaaaaadpdgaaaaafmccabaaaadaaaaaakgaobaaa
aaaaaaaaaaaaaaahdccabaaaadaaaaaakgakbaaaacaaaaaamgaabaaaacaaaaaa
dgaaaaaficaabaaaabaaaaaaabeaaaaaaaaaiadpbbaaaaaibcaabaaaaaaaaaaa
egiocaaaacaaaaaacgaaaaaaegaobaaaabaaaaaabbaaaaaiccaabaaaaaaaaaaa
egiocaaaacaaaaaachaaaaaaegaobaaaabaaaaaabbaaaaaiecaabaaaaaaaaaaa
egiocaaaacaaaaaaciaaaaaaegaobaaaabaaaaaadiaaaaahpcaabaaaacaaaaaa
jgacbaaaabaaaaaaegakbaaaabaaaaaabbaaaaaibcaabaaaadaaaaaaegiocaaa
acaaaaaacjaaaaaaegaobaaaacaaaaaabbaaaaaiccaabaaaadaaaaaaegiocaaa
acaaaaaackaaaaaaegaobaaaacaaaaaabbaaaaaiecaabaaaadaaaaaaegiocaaa
acaaaaaaclaaaaaaegaobaaaacaaaaaaaaaaaaahhcaabaaaaaaaaaaaegacbaaa
aaaaaaaaegacbaaaadaaaaaadiaaaaahicaabaaaaaaaaaaabkaabaaaabaaaaaa
bkaabaaaabaaaaaadcaaaaakicaabaaaaaaaaaaaakaabaaaabaaaaaaakaabaaa
abaaaaaadkaabaiaebaaaaaaaaaaaaaadcaaaaakhccabaaaaeaaaaaaegiccaaa
acaaaaaacmaaaaaapgapbaaaaaaaaaaaegacbaaaaaaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
"!!GLES


#ifdef VERTEX

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _Scale;
uniform lowp vec4 _LightColor0;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_SHC;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAr;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
uniform highp vec4 _Time;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  mediump vec2 wrlPos_1;
  mediump vec3 worldNormal_2;
  lowp vec3 tmpvar_3;
  lowp vec3 tmpvar_4;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_Object2World * vec4(0.0, 1.0, 0.0, 0.0)).xyz);
  worldNormal_2 = tmpvar_5;
  mediump vec4 tmpvar_6;
  tmpvar_6.w = 1.0;
  tmpvar_6.xyz = worldNormal_2;
  mediump vec3 tmpvar_7;
  highp float vC_8;
  mediump vec3 x3_9;
  mediump vec3 x2_10;
  mediump vec3 x1_11;
  highp float tmpvar_12;
  tmpvar_12 = dot (unity_SHAr, tmpvar_6);
  x1_11.x = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = dot (unity_SHAg, tmpvar_6);
  x1_11.y = tmpvar_13;
  highp float tmpvar_14;
  tmpvar_14 = dot (unity_SHAb, tmpvar_6);
  x1_11.z = tmpvar_14;
  mediump vec4 tmpvar_15;
  tmpvar_15 = (worldNormal_2.xyzz * worldNormal_2.yzzx);
  highp float tmpvar_16;
  tmpvar_16 = dot (unity_SHBr, tmpvar_15);
  x2_10.x = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = dot (unity_SHBg, tmpvar_15);
  x2_10.y = tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = dot (unity_SHBb, tmpvar_15);
  x2_10.z = tmpvar_18;
  mediump float tmpvar_19;
  tmpvar_19 = ((worldNormal_2.x * worldNormal_2.x) - (worldNormal_2.y * worldNormal_2.y));
  vC_8 = tmpvar_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (unity_SHC.xyz * vC_8);
  x3_9 = tmpvar_20;
  tmpvar_7 = ((x1_11 + x2_10) + x3_9);
  tmpvar_4 = tmpvar_7;
  mediump vec3 tmpvar_21;
  tmpvar_21 = (max (0.0, dot (worldNormal_2, _WorldSpaceLightPos0.xyz)) * _LightColor0.xyz);
  tmpvar_3 = tmpvar_21;
  highp vec4 tmpvar_22;
  tmpvar_22 = (glstate_matrix_mvp * _glesVertex);
  highp vec4 o_23;
  highp vec4 tmpvar_24;
  tmpvar_24 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_25;
  tmpvar_25.x = tmpvar_24.x;
  tmpvar_25.y = (tmpvar_24.y * _ProjectionParams.x);
  o_23.xy = (tmpvar_25 + tmpvar_24.w);
  o_23.zw = tmpvar_22.zw;
  highp vec2 tmpvar_26;
  tmpvar_26 = (_Object2World * _glesVertex).xz;
  wrlPos_1 = tmpvar_26;
  highp vec2 tmpvar_27;
  tmpvar_27.x = (_glesMultiTexCoord0.x + (wrlPos_1.x * _Scale));
  tmpvar_27.y = (_glesMultiTexCoord0.y + (wrlPos_1.y * _Scale));
  gl_Position = tmpvar_22;
  xlv_TEXCOORD0 = (abs((sin((_Time.x * 5.0)) / 2.0)) + tmpvar_27);
  xlv_COLOR0 = tmpvar_3;
  xlv_TEXCOORD1 = o_23;
  xlv_COLOR1 = tmpvar_4;
}



#endif
#ifdef FRAGMENT

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _DepthFactor;
uniform sampler2D _CameraDepthTexture;
uniform sampler2D _MainTex;
uniform highp vec4 _ZBufferParams;
void main ()
{
  lowp vec4 tmpvar_1;
  lowp vec4 col_2;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD0);
  col_2.w = tmpvar_3.w;
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2DProj (_CameraDepthTexture, xlv_TEXCOORD1);
  highp float z_5;
  z_5 = tmpvar_4.x;
  col_2.xyz = (tmpvar_3.xyz * (xlv_COLOR0 + xlv_COLOR1));
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = col_2.xyz;
  tmpvar_6.w = clamp ((_DepthFactor * ((1.0/(((_ZBufferParams.z * z_5) + _ZBufferParams.w))) - xlv_TEXCOORD1.w)), 0.0, 1.0);
  tmpvar_1 = tmpvar_6;
  gl_FragData[0] = tmpvar_1;
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
"!!GLES


#ifdef VERTEX

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _Scale;
uniform lowp vec4 _LightColor0;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_SHC;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAr;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
uniform highp vec4 _Time;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  mediump vec2 wrlPos_1;
  mediump vec3 worldNormal_2;
  lowp vec3 tmpvar_3;
  lowp vec3 tmpvar_4;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_Object2World * vec4(0.0, 1.0, 0.0, 0.0)).xyz);
  worldNormal_2 = tmpvar_5;
  mediump vec4 tmpvar_6;
  tmpvar_6.w = 1.0;
  tmpvar_6.xyz = worldNormal_2;
  mediump vec3 tmpvar_7;
  highp float vC_8;
  mediump vec3 x3_9;
  mediump vec3 x2_10;
  mediump vec3 x1_11;
  highp float tmpvar_12;
  tmpvar_12 = dot (unity_SHAr, tmpvar_6);
  x1_11.x = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = dot (unity_SHAg, tmpvar_6);
  x1_11.y = tmpvar_13;
  highp float tmpvar_14;
  tmpvar_14 = dot (unity_SHAb, tmpvar_6);
  x1_11.z = tmpvar_14;
  mediump vec4 tmpvar_15;
  tmpvar_15 = (worldNormal_2.xyzz * worldNormal_2.yzzx);
  highp float tmpvar_16;
  tmpvar_16 = dot (unity_SHBr, tmpvar_15);
  x2_10.x = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = dot (unity_SHBg, tmpvar_15);
  x2_10.y = tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = dot (unity_SHBb, tmpvar_15);
  x2_10.z = tmpvar_18;
  mediump float tmpvar_19;
  tmpvar_19 = ((worldNormal_2.x * worldNormal_2.x) - (worldNormal_2.y * worldNormal_2.y));
  vC_8 = tmpvar_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (unity_SHC.xyz * vC_8);
  x3_9 = tmpvar_20;
  tmpvar_7 = ((x1_11 + x2_10) + x3_9);
  tmpvar_4 = tmpvar_7;
  mediump vec3 tmpvar_21;
  tmpvar_21 = (max (0.0, dot (worldNormal_2, _WorldSpaceLightPos0.xyz)) * _LightColor0.xyz);
  tmpvar_3 = tmpvar_21;
  highp vec4 tmpvar_22;
  tmpvar_22 = (glstate_matrix_mvp * _glesVertex);
  highp vec4 o_23;
  highp vec4 tmpvar_24;
  tmpvar_24 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_25;
  tmpvar_25.x = tmpvar_24.x;
  tmpvar_25.y = (tmpvar_24.y * _ProjectionParams.x);
  o_23.xy = (tmpvar_25 + tmpvar_24.w);
  o_23.zw = tmpvar_22.zw;
  highp vec2 tmpvar_26;
  tmpvar_26 = (_Object2World * _glesVertex).xz;
  wrlPos_1 = tmpvar_26;
  highp vec2 tmpvar_27;
  tmpvar_27.x = (_glesMultiTexCoord0.x + (wrlPos_1.x * _Scale));
  tmpvar_27.y = (_glesMultiTexCoord0.y + (wrlPos_1.y * _Scale));
  gl_Position = tmpvar_22;
  xlv_TEXCOORD0 = (abs((sin((_Time.x * 5.0)) / 2.0)) + tmpvar_27);
  xlv_COLOR0 = tmpvar_3;
  xlv_TEXCOORD1 = o_23;
  xlv_COLOR1 = tmpvar_4;
}



#endif
#ifdef FRAGMENT

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _DepthFactor;
uniform sampler2D _CameraDepthTexture;
uniform sampler2D _MainTex;
uniform highp vec4 _ZBufferParams;
void main ()
{
  lowp vec4 tmpvar_1;
  lowp vec4 col_2;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD0);
  col_2.w = tmpvar_3.w;
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2DProj (_CameraDepthTexture, xlv_TEXCOORD1);
  highp float z_5;
  z_5 = tmpvar_4.x;
  col_2.xyz = (tmpvar_3.xyz * (xlv_COLOR0 + xlv_COLOR1));
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = col_2.xyz;
  tmpvar_6.w = clamp ((_DepthFactor * ((1.0/(((_ZBufferParams.z * z_5) + _ZBufferParams.w))) - xlv_TEXCOORD1.w)), 0.0, 1.0);
  tmpvar_1 = tmpvar_6;
  gl_FragData[0] = tmpvar_1;
}



#endif"
}

SubProgram "flash " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 0 [glstate_matrix_mvp]
Vector 8 [_Time]
Vector 9 [_ProjectionParams]
Vector 10 [_WorldSpaceLightPos0]
Vector 11 [unity_SHAr]
Vector 12 [unity_SHAg]
Vector 13 [unity_SHAb]
Vector 14 [unity_SHBr]
Vector 15 [unity_SHBg]
Vector 16 [unity_SHBb]
Vector 17 [unity_SHC]
Matrix 4 [_Object2World]
Vector 18 [unity_NPOTScale]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"agal_vs
c21 -0.020833 -0.125 1.0 0.5
c22 -0.000002 -0.000022 0.002604 0.00026
c23 0.0 0.795775 0.5 0.0
c24 6.283185 -3.141593 0.0 0.0
[bc]
aaaaaaaaaaaaaeacagaaaaffabaaaaaaaaaaaaaaaaaaaaaa mov r0.z, c6.y
aaaaaaaaaaaaabacaeaaaaffabaaaaaaaaaaaaaaaaaaaaaa mov r0.x, c4.y
aaaaaaaaaaaaacacafaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov r0.y, c5
bcaaaaaaaaaaaiacaaaaaakeacaaaaaaaaaaaakeacaaaaaa dp3 r0.w, r0.xyzz, r0.xyzz
akaaaaaaaaaaaiacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa rsq r0.w, r0.w
adaaaaaaaaaaahacaaaaaappacaaaaaaaaaaaakeacaaaaaa mul r0.xyz, r0.w, r0.xyzz
aaaaaaaaaaaaaiacbfaaaakkabaaaaaaaaaaaaaaaaaaaaaa mov r0.w, c21.z
adaaaaaaabaaabacaaaaaaffacaaaaaaaaaaaaffacaaaaaa mul r1.x, r0.y, r0.y
adaaaaaaacaaaiacaaaaaaaaacaaaaaaaaaaaaaaacaaaaaa mul r2.w, r0.x, r0.x
acaaaaaaacaaaiacacaaaappacaaaaaaabaaaaaaacaaaaaa sub r2.w, r2.w, r1.x
adaaaaaaabaaapacaaaaaakeacaaaaaaaaaaaacjacaaaaaa mul r1, r0.xyzz, r0.yzzx
bdaaaaaaacaaaeacaaaaaaoeacaaaaaaanaaaaoeabaaaaaa dp4 r2.z, r0, c13
bdaaaaaaacaaacacaaaaaaoeacaaaaaaamaaaaoeabaaaaaa dp4 r2.y, r0, c12
bdaaaaaaacaaabacaaaaaaoeacaaaaaaalaaaaoeabaaaaaa dp4 r2.x, r0, c11
aaaaaaaaaaaaaiacaiaaaaaaabaaaaaaaaaaaaaaaaaaaaaa mov r0.w, c8.x
adaaaaaaaaaaaiacaaaaaappacaaaaaabhaaaaffabaaaaaa mul r0.w, r0.w, c23.y
abaaaaaaaaaaaiacaaaaaappacaaaaaabhaaaakkabaaaaaa add r0.w, r0.w, c23.z
aiaaaaaaaaaaaiacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa frc r0.w, r0.w
bdaaaaaaadaaaeacabaaaaoeacaaaaaabaaaaaoeabaaaaaa dp4 r3.z, r1, c16
bdaaaaaaadaaabacabaaaaoeacaaaaaaaoaaaaoeabaaaaaa dp4 r3.x, r1, c14
bdaaaaaaadaaacacabaaaaoeacaaaaaaapaaaaoeabaaaaaa dp4 r3.y, r1, c15
bcaaaaaaaaaaacacaaaaaakeacaaaaaaakaaaaoeabaaaaaa dp3 r0.y, r0.xyzz, c10
bdaaaaaaabaaaiacaaaaaaoeaaaaaaaaadaaaaoeabaaaaaa dp4 r1.w, a0, c3
bdaaaaaaabaaaeacaaaaaaoeaaaaaaaaacaaaaoeabaaaaaa dp4 r1.z, a0, c2
abaaaaaaadaaahacacaaaakeacaaaaaaadaaaakeacaaaaaa add r3.xyz, r2.xyzz, r3.xyzz
adaaaaaaaeaaahacacaaaappacaaaaaabbaaaaoeabaaaaaa mul r4.xyz, r2.w, c17
bdaaaaaaabaaabacaaaaaaoeaaaaaaaaaaaaaaoeabaaaaaa dp4 r1.x, a0, c0
bdaaaaaaabaaacacaaaaaaoeaaaaaaaaabaaaaoeabaaaaaa dp4 r1.y, a0, c1
adaaaaaaacaaahacabaaaapeacaaaaaabfaaaappabaaaaaa mul r2.xyz, r1.xyww, c21.w
adaaaaaaacaaacacacaaaaffacaaaaaaajaaaaaaabaaaaaa mul r2.y, r2.y, c9.x
abaaaaaaacaaadacacaaaafeacaaaaaaacaaaakkacaaaaaa add r2.xy, r2.xyyy, r2.z
adaaaaaaaaaaaiacaaaaaappacaaaaaabiaaaaaaabaaaaaa mul r0.w, r0.w, c24.x
abaaaaaaaaaaaiacaaaaaappacaaaaaabiaaaaffabaaaaaa add r0.w, r0.w, c24.y
adaaaaaaabaaadaeacaaaafeacaaaaaabcaaaaoeabaaaaaa mul v1.xy, r2.xyyy, c18
apaaaaaaacaaabacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa sin r2.x, r0.w
baaaaaaaacaaacacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa cos r2.y, r0.w
ahaaaaaaaaaaaiacaaaaaaffacaaaaaabhaaaaaaabaaaaaa max r0.w, r0.y, c23.x
adaaaaaaaaaaabacacaaaaffacaaaaaabfaaaappabaaaaaa mul r0.x, r2.y, c21.w
beaaaaaaaaaaaeacaaaaaaaaacaaaaaaaaaaaaaaaaaaaaaa abs r0.z, r0.x
bdaaaaaaaaaaacacaaaaaaoeaaaaaaaaagaaaaoeabaaaaaa dp4 r0.y, a0, c6
bdaaaaaaaaaaabacaaaaaaoeaaaaaaaaaeaaaaoeabaaaaaa dp4 r0.x, a0, c4
adaaaaaaaaaaacacaaaaaaffacaaaaaabeaaaaaaabaaaaaa mul r0.y, r0.y, c20.x
abaaaaaaaaaaacacaaaaaaffacaaaaaaadaaaaoeaaaaaaaa add r0.y, r0.y, a3
adaaaaaaaaaaabacaaaaaaaaacaaaaaabeaaaaoeabaaaaaa mul r0.x, r0.x, c20
abaaaaaaaaaaabacaaaaaaaaacaaaaaaadaaaaoeaaaaaaaa add r0.x, r0.x, a3
abaaaaaaagaaahaeadaaaakeacaaaaaaaeaaaakeacaaaaaa add v6.xyz, r3.xyzz, r4.xyzz
aaaaaaaaaaaaapadabaaaaoeacaaaaaaaaaaaaaaaaaaaaaa mov o0, r1
abaaaaaaaaaaadaeaaaaaakkacaaaaaaaaaaaafeacaaaaaa add v0.xy, r0.z, r0.xyyy
adaaaaaaahaaahaeaaaaaappacaaaaaabdaaaaoeabaaaaaa mul v7.xyz, r0.w, c19
aaaaaaaaabaaamaeabaaaaopacaaaaaaaaaaaaaaaaaaaaaa mov v1.zw, r1.wwzw
aaaaaaaaaaaaamaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v0.zw, c0
aaaaaaaaagaaaiaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v6.w, c0
aaaaaaaaahaaaiaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v7.w, c0
"
}

SubProgram "d3d11_9x " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
ConstBuffer "$Globals" 64 // 52 used size, 5 vars
Vector 16 [_LightColor0] 4
Float 48 [_Scale]
ConstBuffer "UnityPerCamera" 128 // 96 used size, 8 vars
Vector 0 [_Time] 4
Vector 80 [_ProjectionParams] 4
ConstBuffer "UnityLighting" 720 // 720 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
Vector 608 [unity_SHAr] 4
Vector 624 [unity_SHAg] 4
Vector 640 [unity_SHAb] 4
Vector 656 [unity_SHBr] 4
Vector 672 [unity_SHBg] 4
Vector 688 [unity_SHBb] 4
Vector 704 [unity_SHC] 4
ConstBuffer "UnityPerDraw" 336 // 256 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
BindCB "UnityLighting" 2
BindCB "UnityPerDraw" 3
// 37 instructions, 4 temp regs, 0 temp arrays:
// ALU 33 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0_level_9_1
eefiecedpehalmanahacfidnogggdgpgnmigniocabaaaaaaciakaaaaaeaaaaaa
daaaaaaaniadaaaabaajaaaaieajaaaaebgpgodjkaadaaaakaadaaaaaaacpopp
biadaaaaiiaaaaaaaiaaceaaaaaaieaaaaaaieaaaaaaceaaabaaieaaaaaaabaa
abaaabaaaaaaaaaaaaaaadaaabaaacaaaaaaaaaaabaaaaaaabaaadaaaaaaaaaa
abaaafaaabaaaeaaaaaaaaaaacaaaaaaabaaafaaaaaaaaaaacaacgaaahaaagaa
aaaaaaaaadaaaaaaaeaaanaaaaaaaaaaadaaamaaaeaabbaaaaaaaaaaaaaaaaaa
aaacpoppfbaaaaafbfaaapkaaaaaiadpaaaaaaaaaaaaaadpoelheldpfbaaaaaf
bgaaapkanlapmjeanlapejmaaaaaaaaaaaaaaaaafbaaaaafbhaaapkaabannalf
gballglhklkkckdlijiiiidjfbaaaaafbiaaapkaklkkkklmaaaaaaloaaaaiadp
aaaaaadpbpaaaaacafaaaaiaaaaaapjabpaaaaacafaaabiaabaaapjaabaaaaac
aaaaaiiabfaaaakaceaaaaacaaaaahiabcaaoekaajaaaaadabaaabiaagaaoeka
aaaaoeiaajaaaaadabaaaciaahaaoekaaaaaoeiaajaaaaadabaaaeiaaiaaoeka
aaaaoeiaafaaaaadacaaapiaaaaacjiaaaaakeiaajaaaaadadaaabiaajaaoeka
acaaoeiaajaaaaadadaaaciaakaaoekaacaaoeiaajaaaaadadaaaeiaalaaoeka
acaaoeiaacaaaaadabaaahiaabaaoeiaadaaoeiaafaaaaadaaaaaiiaaaaaffia
aaaaffiaaeaaaaaeaaaaaiiaaaaaaaiaaaaaaaiaaaaappibaiaaaaadaaaaabia
aaaaoeiaafaaoekaalaaaaadaaaaabiaaaaaaaiabfaaffkaafaaaaadabaaahoa
aaaaaaiaabaaoekaaeaaaaaeadaaahoaamaaoekaaaaappiaabaaoeiaafaaaaad
aaaaapiaaaaaffjaaoaaoekaaeaaaaaeaaaaapiaanaaoekaaaaaaajaaaaaoeia
aeaaaaaeaaaaapiaapaaoekaaaaakkjaaaaaoeiaaeaaaaaeaaaaapiabaaaoeka
aaaappjaaaaaoeiaafaaaaadabaaabiaaaaaffiaaeaaaakaafaaaaadabaaaiia
abaaaaiabfaakkkaafaaaaadabaaafiaaaaapeiabfaakkkaacaaaaadacaaadoa
abaakkiaabaaomiaabaaaaacabaaamiabfaaoekaaeaaaaaeabaaabiaadaaaaka
abaappiaabaakkiabdaaaaacabaaabiaabaaaaiaaeaaaaaeabaaabiaabaaaaia
bgaaaakabgaaffkacfaaaaaeacaaaciaabaaaaiabhaaoekabiaaoekaafaaaaad
abaaabiaacaaffiabfaakkkacdaaaaacabaaabiaabaaaaiaafaaaaadabaaagia
aaaaffjabcaaoakaaeaaaaaeabaaagiabbaaoakaaaaaaajaabaaoeiaaeaaaaae
abaaagiabdaaoakaaaaakkjaabaaoeiaaeaaaaaeabaaagiabeaaoakaaaaappja
abaaoeiaaeaaaaaeabaaagiaabaaoeiaacaaaakaabaanajaacaaaaadaaaaadoa
abaaojiaabaaaaiaaeaaaaaeaaaaadmaaaaappiaaaaaoekaaaaaoeiaabaaaaac
aaaaammaaaaaoeiaabaaaaacacaaamoaaaaaoeiappppaaaafdeieefcdaafaaaa
eaaaabaaemabaaaafjaaaaaeegiocaaaaaaaaaaaaeaaaaaafjaaaaaeegiocaaa
abaaaaaaagaaaaaafjaaaaaeegiocaaaacaaaaaacnaaaaaafjaaaaaeegiocaaa
adaaaaaabaaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaaddcbabaaaabaaaaaa
ghaaaaaepccabaaaaaaaaaaaabaaaaaagfaaaaaddccabaaaabaaaaaagfaaaaad
hccabaaaacaaaaaagfaaaaadpccabaaaadaaaaaagfaaaaadhccabaaaaeaaaaaa
giaaaaacaeaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaaegiocaaa
adaaaaaaabaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaadaaaaaaaaaaaaaa
agbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaa
adaaaaaaacaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaadaaaaaaadaaaaaapgbpbaaaaaaaaaaaegaobaaaaaaaaaaa
dgaaaaafpccabaaaaaaaaaaaegaobaaaaaaaaaaadiaaaaaidcaabaaaabaaaaaa
fgbfbaaaaaaaaaaaigiacaaaadaaaaaaanaaaaaadcaaaaakdcaabaaaabaaaaaa
igiacaaaadaaaaaaamaaaaaaagbabaaaaaaaaaaaegaabaaaabaaaaaadcaaaaak
dcaabaaaabaaaaaaigiacaaaadaaaaaaaoaaaaaakgbkbaaaaaaaaaaaegaabaaa
abaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaapaaaaaapgbpbaaa
aaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaegaabaaaabaaaaaa
agiacaaaaaaaaaaaadaaaaaaegbabaaaabaaaaaadiaaaaaiecaabaaaabaaaaaa
akiacaaaabaaaaaaaaaaaaaaabeaaaaaaaaakaeaenaaaaagecaabaaaabaaaaaa
aanaaaaackaabaaaabaaaaaadiaaaaahecaabaaaabaaaaaackaabaaaabaaaaaa
abeaaaaaaaaaaadpaaaaaaaidccabaaaabaaaaaaegaabaaaabaaaaaakgakbaia
ibaaaaaaabaaaaaabaaaaaajbcaabaaaabaaaaaaegiccaaaadaaaaaaanaaaaaa
egiccaaaadaaaaaaanaaaaaaeeaaaaafbcaabaaaabaaaaaaakaabaaaabaaaaaa
diaaaaaihcaabaaaabaaaaaaagaabaaaabaaaaaaegiccaaaadaaaaaaanaaaaaa
baaaaaaibcaabaaaacaaaaaaegacbaaaabaaaaaaegiccaaaacaaaaaaaaaaaaaa
deaaaaahbcaabaaaacaaaaaaakaabaaaacaaaaaaabeaaaaaaaaaaaaadiaaaaai
hccabaaaacaaaaaaagaabaaaacaaaaaaegiccaaaaaaaaaaaabaaaaaadiaaaaai
ccaabaaaaaaaaaaabkaabaaaaaaaaaaaakiacaaaabaaaaaaafaaaaaadiaaaaak
ncaabaaaacaaaaaaagahbaaaaaaaaaaaaceaaaaaaaaaaadpaaaaaaaaaaaaaadp
aaaaaadpdgaaaaafmccabaaaadaaaaaakgaobaaaaaaaaaaaaaaaaaahdccabaaa
adaaaaaakgakbaaaacaaaaaamgaabaaaacaaaaaadgaaaaaficaabaaaabaaaaaa
abeaaaaaaaaaiadpbbaaaaaibcaabaaaaaaaaaaaegiocaaaacaaaaaacgaaaaaa
egaobaaaabaaaaaabbaaaaaiccaabaaaaaaaaaaaegiocaaaacaaaaaachaaaaaa
egaobaaaabaaaaaabbaaaaaiecaabaaaaaaaaaaaegiocaaaacaaaaaaciaaaaaa
egaobaaaabaaaaaadiaaaaahpcaabaaaacaaaaaajgacbaaaabaaaaaaegakbaaa
abaaaaaabbaaaaaibcaabaaaadaaaaaaegiocaaaacaaaaaacjaaaaaaegaobaaa
acaaaaaabbaaaaaiccaabaaaadaaaaaaegiocaaaacaaaaaackaaaaaaegaobaaa
acaaaaaabbaaaaaiecaabaaaadaaaaaaegiocaaaacaaaaaaclaaaaaaegaobaaa
acaaaaaaaaaaaaahhcaabaaaaaaaaaaaegacbaaaaaaaaaaaegacbaaaadaaaaaa
diaaaaahicaabaaaaaaaaaaabkaabaaaabaaaaaabkaabaaaabaaaaaadcaaaaak
icaabaaaaaaaaaaaakaabaaaabaaaaaaakaabaaaabaaaaaadkaabaiaebaaaaaa
aaaaaaaadcaaaaakhccabaaaaeaaaaaaegiccaaaacaaaaaacmaaaaaapgapbaaa
aaaaaaaaegacbaaaaaaaaaaadoaaaaabejfdeheogmaaaaaaadaaaaaaaiaaaaaa
faaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaafjaaaaaaaaaaaaaa
aaaaaaaaadaaaaaaabaaaaaaadadaaaagcaaaaaaaaaaaaaaaaaaaaaaadaaaaaa
acaaaaaaahaaaaaafaepfdejfeejepeoaafeeffiedepepfceeaaeoepfcenebem
aaklklklepfdeheojmaaaaaaafaaaaaaaiaaaaaaiaaaaaaaaaaaaaaaabaaaaaa
adaaaaaaaaaaaaaaapaaaaaaimaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaa
adamaaaajfaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaiaaaaimaaaaaa
abaaaaaaaaaaaaaaadaaaaaaadaaaaaaapaaaaaajfaaaaaaabaaaaaaaaaaaaaa
adaaaaaaaeaaaaaaahaiaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfcee
aaedepemepfcaakl"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" "VERTEXLIGHT_ON" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal (normalize(_glesNormal))
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 399
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 392
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform sampler2D _MainTex;
#line 408
#line 412
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
uniform highp float _DepthFactor;
#line 428
#line 284
highp vec4 ComputeScreenPos( in highp vec4 pos ) {
    #line 286
    highp vec4 o = (pos * 0.5);
    o.xy = (vec2( o.x, (o.y * _ProjectionParams.x)) + o.w);
    o.zw = pos.zw;
    return o;
}
#line 137
mediump vec3 ShadeSH9( in mediump vec4 normal ) {
    mediump vec3 x1;
    mediump vec3 x2;
    mediump vec3 x3;
    x1.x = dot( unity_SHAr, normal);
    #line 141
    x1.y = dot( unity_SHAg, normal);
    x1.z = dot( unity_SHAb, normal);
    mediump vec4 vB = (normal.xyzz * normal.yzzx);
    x2.x = dot( unity_SHBr, vB);
    #line 145
    x2.y = dot( unity_SHBg, vB);
    x2.z = dot( unity_SHBb, vB);
    highp float vC = ((normal.x * normal.x) - (normal.y * normal.y));
    x3 = (unity_SHC.xyz * vC);
    #line 149
    return ((x1 + x2) + x3);
}
#line 408
highp vec2 offset( in highp vec2 uv ) {
    return (abs((sin((_Time.x * 5.0)) / 2.0)) + uv);
}
#line 413
v2f vert( in appdata_t v ) {
    v2f o;
    #line 416
    mediump vec3 worldNormal = normalize((_Object2World * vec4( 0.0, 1.0, 0.0, 0.0)).xyz);
    o.ambient = ShadeSH9( vec4( worldNormal, 1.0));
    mediump float nl = max( 0.0, dot( worldNormal, _WorldSpaceLightPos0.xyz));
    o.diff = (nl * _LightColor0.xyz);
    #line 420
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.scrPos = ComputeScreenPos( o.pos);
    mediump vec2 wrlPos = (_Object2World * v.vertex).xz;
    o.uv = offset( vec2( (v.texcoord.x + (wrlPos.x * _Scale)), (v.texcoord.y + (wrlPos.y * _Scale))));
    #line 424
    return o;
}
out highp vec2 xlv_TEXCOORD0;
out lowp vec3 xlv_COLOR0;
out highp vec4 xlv_TEXCOORD1;
out lowp vec3 xlv_COLOR1;
void main() {
    v2f xl_retval;
    appdata_t xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.texcoord = vec2(gl_MultiTexCoord0);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vert( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec2(xl_retval.uv);
    xlv_COLOR0 = vec3(xl_retval.diff);
    xlv_TEXCOORD1 = vec4(xl_retval.scrPos);
    xlv_COLOR1 = vec3(xl_retval.ambient);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 399
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 392
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform sampler2D _MainTex;
#line 408
#line 412
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
uniform highp float _DepthFactor;
#line 428
#line 280
highp float LinearEyeDepth( in highp float z ) {
    #line 282
    return (1.0 / ((_ZBufferParams.z * z) + _ZBufferParams.w));
}
#line 428
lowp vec4 frag( in v2f i ) {
    lowp vec4 col = texture( _MainTex, i.uv);
    highp float depth = LinearEyeDepth( float( textureProj( _CameraDepthTexture, i.scrPos).xyzw));
    #line 432
    highp float diff = xll_saturate_f((_DepthFactor * (depth - i.scrPos.w)));
    lowp float shadow = 1.0;
    lowp vec3 lighting = ((i.diff * shadow) + i.ambient);
    col.xyz *= lighting;
    #line 436
    return vec4( col.xyz, diff);
}
in highp vec2 xlv_TEXCOORD0;
in lowp vec3 xlv_COLOR0;
in highp vec4 xlv_TEXCOORD1;
in lowp vec3 xlv_COLOR1;
void main() {
    lowp vec4 xl_retval;
    v2f xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.uv = vec2(xlv_TEXCOORD0);
    xlt_i.diff = vec3(xlv_COLOR0);
    xlt_i.scrPos = vec4(xlv_TEXCOORD1);
    xlt_i.ambient = vec3(xlv_COLOR1);
    xl_retval = frag( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Vector 9 [_Time]
Vector 10 [_ProjectionParams]
Vector 11 [_WorldSpaceLightPos0]
Vector 12 [unity_SHAr]
Vector 13 [unity_SHAg]
Vector 14 [unity_SHAb]
Vector 15 [unity_SHBr]
Vector 16 [unity_SHBg]
Vector 17 [unity_SHBb]
Vector 18 [unity_SHC]
Matrix 5 [_Object2World]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"!!ARBvp1.0
# 57 ALU
PARAM c[25] = { { 0.79577452, 0, 0.5, 1 },
		state.matrix.mvp,
		program.local[5..20],
		{ 24.980801, -24.980801, 0.25 },
		{ -60.145809, 60.145809, 85.453789, -85.453789 },
		{ -64.939346, 64.939346, 19.73921, -19.73921 },
		{ -1, 1, -9, 0.75 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
MOV R0.x, c[0];
MUL R0.x, R0, c[9];
ADD R0.x, R0, -c[21].z;
FRC R2.w, R0.x;
ADD R0.xyz, -R2.w, c[0].yzww;
MUL R2.xyz, R0, R0;
MUL R0.xyz, R2, c[21].xyxw;
ADD R0.xyz, R0, c[22].xyxw;
MAD R1.xyz, R0, R2, c[22].zwzw;
MAD R3.xyz, R1, R2, c[23].xyxw;
MAD R3.xyz, R3, R2, c[23].zwzw;
MAD R3.xyz, R3, R2, c[24].xyxw;
MOV R1.w, c[0];
MOV R0.z, c[7].y;
MOV R0.x, c[5].y;
MOV R0.y, c[6];
DP3 R0.w, R0, R0;
RSQ R0.w, R0.w;
MUL R1.xyz, R0.w, R0;
MUL R0, R1.xyzz, R1.yzzx;
DP4 R4.z, R1, c[14];
DP4 R4.y, R1, c[13];
DP4 R4.x, R1, c[12];
DP4 R5.z, R0, c[17];
DP4 R5.x, R0, c[15];
DP4 R5.y, R0, c[16];
ADD R0.xyz, R4, R5;
MUL R0.w, R1.y, R1.y;
MAD R0.w, R1.x, R1.x, -R0;
SLT R4.x, R2.w, c[21].z;
SGE R4.yz, R2.w, c[24].xzww;
DP4 R2.w, vertex.position, c[4];
MOV R2.xz, R4;
DP3 R2.y, R4, c[24].xyxw;
DP3 R1.w, R3, -R2;
MUL R2.xyz, R0.w, c[18];
ADD result.color.secondary.xyz, R0, R2;
MUL R0.w, R1, c[0].z;
ABS R2.z, R0.w;
DP4 R0.w, vertex.position, c[7];
DP4 R1.w, vertex.position, c[5];
DP4 R2.x, vertex.position, c[1];
DP4 R2.y, vertex.position, c[2];
MUL R0.xyz, R2.xyww, c[0].z;
MUL R0.y, R0, c[10].x;
ADD R0.xy, R0, R0.z;
MAD R3.y, R0.w, c[20].x, vertex.texcoord[0];
MAD R3.x, R1.w, c[20], vertex.texcoord[0];
ADD result.texcoord[0].xy, R2.z, R3;
DP4 R2.z, vertex.position, c[3];
MOV R0.zw, R2;
MOV result.texcoord[2], R0;
DP3 R1.x, R1, c[11];
MOV result.texcoord[1], R0;
MAX R0.x, R1, c[0].y;
MOV result.position, R2;
MUL result.color.xyz, R0.x, c[19];
END
# 57 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 0 [glstate_matrix_mvp]
Vector 8 [_Time]
Vector 9 [_ProjectionParams]
Vector 10 [_ScreenParams]
Vector 11 [_WorldSpaceLightPos0]
Vector 12 [unity_SHAr]
Vector 13 [unity_SHAg]
Vector 14 [unity_SHAb]
Vector 15 [unity_SHBr]
Vector 16 [unity_SHBg]
Vector 17 [unity_SHBb]
Vector 18 [unity_SHC]
Matrix 4 [_Object2World]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"vs_2_0
; 54 ALU
def c21, -0.02083333, -0.12500000, 1.00000000, 0.50000000
def c22, -0.00000155, -0.00002170, 0.00260417, 0.00026042
def c23, 0.00000000, 0.79577452, 0.50000000, 0
def c24, 6.28318501, -3.14159298, 0, 0
dcl_position0 v0
dcl_texcoord0 v1
mov r0.z, c6.y
mov r0.x, c4.y
mov r0.y, c5
dp3 r0.w, r0, r0
rsq r0.w, r0.w
mul r0.xyz, r0.w, r0
mov r0.w, c21.z
mul r1.x, r0.y, r0.y
mad r2.w, r0.x, r0.x, -r1.x
mul r1, r0.xyzz, r0.yzzx
dp4 r2.z, r0, c14
dp4 r2.y, r0, c13
dp4 r2.x, r0, c12
mov r0.w, c8.x
mad r0.w, r0, c23.y, c23.z
frc r0.w, r0
dp4 r3.z, r1, c17
dp4 r3.x, r1, c15
dp4 r3.y, r1, c16
dp3 r0.y, r0, c11
dp4 r1.w, v0, c3
add r3.xyz, r2, r3
mul r4.xyz, r2.w, c18
dp4 r1.x, v0, c0
dp4 r1.y, v0, c1
mul r2.xyz, r1.xyww, c21.w
dp4 r1.z, v0, c2
mul r2.y, r2, c9.x
mad r2.xy, r2.z, c10.zwzw, r2
mov r2.zw, r1
mad r0.w, r0, c24.x, c24.y
mov oT2, r2
mov oT1, r2
sincos r2.xy, r0.w, c22.xyzw, c21.xyzw
max r0.w, r0.y, c23.x
mul r0.x, r2.y, c21.w
abs r0.z, r0.x
dp4 r0.y, v0, c6
dp4 r0.x, v0, c4
mad r0.y, r0, c20.x, v1
mad r0.x, r0, c20, v1
add oD1.xyz, r3, r4
mov oPos, r1
add oT0.xy, r0.z, r0
mul oD0.xyz, r0.w, c19
"
}

SubProgram "d3d11 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
ConstBuffer "$Globals" 128 // 116 used size, 6 vars
Vector 16 [_LightColor0] 4
Float 112 [_Scale]
ConstBuffer "UnityPerCamera" 128 // 96 used size, 8 vars
Vector 0 [_Time] 4
Vector 80 [_ProjectionParams] 4
ConstBuffer "UnityLighting" 720 // 720 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
Vector 608 [unity_SHAr] 4
Vector 624 [unity_SHAg] 4
Vector 640 [unity_SHAb] 4
Vector 656 [unity_SHBr] 4
Vector 672 [unity_SHBg] 4
Vector 688 [unity_SHBb] 4
Vector 704 [unity_SHC] 4
ConstBuffer "UnityPerDraw" 336 // 256 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
BindCB "UnityLighting" 2
BindCB "UnityPerDraw" 3
// 39 instructions, 4 temp regs, 0 temp arrays:
// ALU 34 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0
eefiecedkmbjjcbgbnmcgdihdafokpehmgpabbboabaaaaaanaagaaaaadaaaaaa
cmaaaaaakaaaaaaafmabaaaaejfdeheogmaaaaaaadaaaaaaaiaaaaaafaaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaafjaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaadadaaaagcaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahaaaaaafaepfdejfeejepeoaafeeffiedepepfceeaaeoepfcenebemaaklklkl
epfdeheoleaaaaaaagaaaaaaaiaaaaaajiaaaaaaaaaaaaaaabaaaaaaadaaaaaa
aaaaaaaaapaaaaaakeaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaadamaaaa
keaaaaaaacaaaaaaaaaaaaaaadaaaaaaacaaaaaaapaaaaaaknaaaaaaaaaaaaaa
aaaaaaaaadaaaaaaadaaaaaaahaiaaaakeaaaaaaabaaaaaaaaaaaaaaadaaaaaa
aeaaaaaaapaaaaaaknaaaaaaabaaaaaaaaaaaaaaadaaaaaaafaaaaaaahaiaaaa
fdfgfpfaepfdejfeejepeoaafeeffiedepepfceeaaedepemepfcaaklfdeieefc
gmafaaaaeaaaabaaflabaaaafjaaaaaeegiocaaaaaaaaaaaaiaaaaaafjaaaaae
egiocaaaabaaaaaaagaaaaaafjaaaaaeegiocaaaacaaaaaacnaaaaaafjaaaaae
egiocaaaadaaaaaabaaaaaaafpaaaaadpcbabaaaaaaaaaaafpaaaaaddcbabaaa
abaaaaaaghaaaaaepccabaaaaaaaaaaaabaaaaaagfaaaaaddccabaaaabaaaaaa
gfaaaaadpccabaaaacaaaaaagfaaaaadhccabaaaadaaaaaagfaaaaadpccabaaa
aeaaaaaagfaaaaadhccabaaaafaaaaaagiaaaaacaeaaaaaadiaaaaaipcaabaaa
aaaaaaaafgbfbaaaaaaaaaaaegiocaaaadaaaaaaabaaaaaadcaaaaakpcaabaaa
aaaaaaaaegiocaaaadaaaaaaaaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaadaaaaaaacaaaaaakgbkbaaaaaaaaaaa
egaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaadaaaaaaadaaaaaa
pgbpbaaaaaaaaaaaegaobaaaaaaaaaaadgaaaaafpccabaaaaaaaaaaaegaobaaa
aaaaaaaadiaaaaaidcaabaaaabaaaaaafgbfbaaaaaaaaaaaigiacaaaadaaaaaa
anaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaamaaaaaaagbabaaa
aaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaa
aoaaaaaakgbkbaaaaaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaa
igiacaaaadaaaaaaapaaaaaapgbpbaaaaaaaaaaaegaabaaaabaaaaaadcaaaaak
dcaabaaaabaaaaaaegaabaaaabaaaaaaagiacaaaaaaaaaaaahaaaaaaegbabaaa
abaaaaaadiaaaaaiecaabaaaabaaaaaaakiacaaaabaaaaaaaaaaaaaaabeaaaaa
aaaakaeaenaaaaagecaabaaaabaaaaaaaanaaaaackaabaaaabaaaaaadiaaaaah
ecaabaaaabaaaaaackaabaaaabaaaaaaabeaaaaaaaaaaadpaaaaaaaidccabaaa
abaaaaaaegaabaaaabaaaaaakgakbaiaibaaaaaaabaaaaaadiaaaaaibcaabaaa
abaaaaaabkaabaaaaaaaaaaaakiacaaaabaaaaaaafaaaaaadiaaaaahicaabaaa
abaaaaaaakaabaaaabaaaaaaabeaaaaaaaaaaadpdiaaaaakfcaabaaaabaaaaaa
agadbaaaaaaaaaaaaceaaaaaaaaaaadpaaaaaaaaaaaaaadpaaaaaaaaaaaaaaah
dcaabaaaaaaaaaaakgakbaaaabaaaaaamgaabaaaabaaaaaadgaaaaafpccabaaa
acaaaaaaegaobaaaaaaaaaaadgaaaaafpccabaaaaeaaaaaaegaobaaaaaaaaaaa
baaaaaajbcaabaaaaaaaaaaaegiccaaaadaaaaaaanaaaaaaegiccaaaadaaaaaa
anaaaaaaeeaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaadiaaaaaihcaabaaa
aaaaaaaaagaabaaaaaaaaaaaegiccaaaadaaaaaaanaaaaaabaaaaaaibcaabaaa
abaaaaaaegacbaaaaaaaaaaaegiccaaaacaaaaaaaaaaaaaadeaaaaahbcaabaaa
abaaaaaaakaabaaaabaaaaaaabeaaaaaaaaaaaaadiaaaaaihccabaaaadaaaaaa
agaabaaaabaaaaaaegiccaaaaaaaaaaaabaaaaaadgaaaaaficaabaaaaaaaaaaa
abeaaaaaaaaaiadpbbaaaaaibcaabaaaabaaaaaaegiocaaaacaaaaaacgaaaaaa
egaobaaaaaaaaaaabbaaaaaiccaabaaaabaaaaaaegiocaaaacaaaaaachaaaaaa
egaobaaaaaaaaaaabbaaaaaiecaabaaaabaaaaaaegiocaaaacaaaaaaciaaaaaa
egaobaaaaaaaaaaadiaaaaahpcaabaaaacaaaaaajgacbaaaaaaaaaaaegakbaaa
aaaaaaaabbaaaaaibcaabaaaadaaaaaaegiocaaaacaaaaaacjaaaaaaegaobaaa
acaaaaaabbaaaaaiccaabaaaadaaaaaaegiocaaaacaaaaaackaaaaaaegaobaaa
acaaaaaabbaaaaaiecaabaaaadaaaaaaegiocaaaacaaaaaaclaaaaaaegaobaaa
acaaaaaaaaaaaaahhcaabaaaabaaaaaaegacbaaaabaaaaaaegacbaaaadaaaaaa
diaaaaahccaabaaaaaaaaaaabkaabaaaaaaaaaaabkaabaaaaaaaaaaadcaaaaak
bcaabaaaaaaaaaaaakaabaaaaaaaaaaaakaabaaaaaaaaaaabkaabaiaebaaaaaa
aaaaaaaadcaaaaakhccabaaaafaaaaaaegiccaaaacaaaaaacmaaaaaaagaabaaa
aaaaaaaaegacbaaaabaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
"!!GLES


#ifdef VERTEX

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _Scale;
uniform lowp vec4 _LightColor0;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_SHC;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAr;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
uniform highp vec4 _Time;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  mediump vec2 wrlPos_1;
  mediump vec3 worldNormal_2;
  lowp vec3 tmpvar_3;
  lowp vec3 tmpvar_4;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_Object2World * vec4(0.0, 1.0, 0.0, 0.0)).xyz);
  worldNormal_2 = tmpvar_5;
  mediump vec4 tmpvar_6;
  tmpvar_6.w = 1.0;
  tmpvar_6.xyz = worldNormal_2;
  mediump vec3 tmpvar_7;
  highp float vC_8;
  mediump vec3 x3_9;
  mediump vec3 x2_10;
  mediump vec3 x1_11;
  highp float tmpvar_12;
  tmpvar_12 = dot (unity_SHAr, tmpvar_6);
  x1_11.x = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = dot (unity_SHAg, tmpvar_6);
  x1_11.y = tmpvar_13;
  highp float tmpvar_14;
  tmpvar_14 = dot (unity_SHAb, tmpvar_6);
  x1_11.z = tmpvar_14;
  mediump vec4 tmpvar_15;
  tmpvar_15 = (worldNormal_2.xyzz * worldNormal_2.yzzx);
  highp float tmpvar_16;
  tmpvar_16 = dot (unity_SHBr, tmpvar_15);
  x2_10.x = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = dot (unity_SHBg, tmpvar_15);
  x2_10.y = tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = dot (unity_SHBb, tmpvar_15);
  x2_10.z = tmpvar_18;
  mediump float tmpvar_19;
  tmpvar_19 = ((worldNormal_2.x * worldNormal_2.x) - (worldNormal_2.y * worldNormal_2.y));
  vC_8 = tmpvar_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (unity_SHC.xyz * vC_8);
  x3_9 = tmpvar_20;
  tmpvar_7 = ((x1_11 + x2_10) + x3_9);
  tmpvar_4 = tmpvar_7;
  mediump vec3 tmpvar_21;
  tmpvar_21 = (max (0.0, dot (worldNormal_2, _WorldSpaceLightPos0.xyz)) * _LightColor0.xyz);
  tmpvar_3 = tmpvar_21;
  highp vec4 tmpvar_22;
  tmpvar_22 = (glstate_matrix_mvp * _glesVertex);
  highp vec4 o_23;
  highp vec4 tmpvar_24;
  tmpvar_24 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_25;
  tmpvar_25.x = tmpvar_24.x;
  tmpvar_25.y = (tmpvar_24.y * _ProjectionParams.x);
  o_23.xy = (tmpvar_25 + tmpvar_24.w);
  o_23.zw = tmpvar_22.zw;
  highp vec2 tmpvar_26;
  tmpvar_26 = (_Object2World * _glesVertex).xz;
  wrlPos_1 = tmpvar_26;
  highp vec2 tmpvar_27;
  tmpvar_27.x = (_glesMultiTexCoord0.x + (wrlPos_1.x * _Scale));
  tmpvar_27.y = (_glesMultiTexCoord0.y + (wrlPos_1.y * _Scale));
  gl_Position = tmpvar_22;
  xlv_TEXCOORD0 = (abs((sin((_Time.x * 5.0)) / 2.0)) + tmpvar_27);
  xlv_TEXCOORD2 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
  xlv_COLOR0 = tmpvar_3;
  xlv_TEXCOORD1 = o_23;
  xlv_COLOR1 = tmpvar_4;
}



#endif
#ifdef FRAGMENT

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _DepthFactor;
uniform sampler2D _CameraDepthTexture;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 _LightShadowData;
uniform highp vec4 _ZBufferParams;
void main ()
{
  lowp vec4 tmpvar_1;
  lowp vec4 col_2;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD0);
  col_2.w = tmpvar_3.w;
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2DProj (_CameraDepthTexture, xlv_TEXCOORD1);
  highp float z_5;
  z_5 = tmpvar_4.x;
  lowp float tmpvar_6;
  mediump float lightShadowDataX_7;
  highp float dist_8;
  lowp float tmpvar_9;
  tmpvar_9 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD2).x;
  dist_8 = tmpvar_9;
  highp float tmpvar_10;
  tmpvar_10 = _LightShadowData.x;
  lightShadowDataX_7 = tmpvar_10;
  highp float tmpvar_11;
  tmpvar_11 = max (float((dist_8 > (xlv_TEXCOORD2.z / xlv_TEXCOORD2.w))), lightShadowDataX_7);
  tmpvar_6 = tmpvar_11;
  col_2.xyz = (tmpvar_3.xyz * ((xlv_COLOR0 * tmpvar_6) + xlv_COLOR1));
  highp vec4 tmpvar_12;
  tmpvar_12.xyz = col_2.xyz;
  tmpvar_12.w = clamp ((_DepthFactor * ((1.0/(((_ZBufferParams.z * z_5) + _ZBufferParams.w))) - xlv_TEXCOORD1.w)), 0.0, 1.0);
  tmpvar_1 = tmpvar_12;
  gl_FragData[0] = tmpvar_1;
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
"!!GLES


#ifdef VERTEX

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _Scale;
uniform lowp vec4 _LightColor0;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp vec4 unity_SHC;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAr;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
uniform highp vec4 _Time;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  mediump vec2 wrlPos_1;
  mediump vec3 worldNormal_2;
  lowp vec3 tmpvar_3;
  lowp vec3 tmpvar_4;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_Object2World * vec4(0.0, 1.0, 0.0, 0.0)).xyz);
  worldNormal_2 = tmpvar_5;
  mediump vec4 tmpvar_6;
  tmpvar_6.w = 1.0;
  tmpvar_6.xyz = worldNormal_2;
  mediump vec3 tmpvar_7;
  highp float vC_8;
  mediump vec3 x3_9;
  mediump vec3 x2_10;
  mediump vec3 x1_11;
  highp float tmpvar_12;
  tmpvar_12 = dot (unity_SHAr, tmpvar_6);
  x1_11.x = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = dot (unity_SHAg, tmpvar_6);
  x1_11.y = tmpvar_13;
  highp float tmpvar_14;
  tmpvar_14 = dot (unity_SHAb, tmpvar_6);
  x1_11.z = tmpvar_14;
  mediump vec4 tmpvar_15;
  tmpvar_15 = (worldNormal_2.xyzz * worldNormal_2.yzzx);
  highp float tmpvar_16;
  tmpvar_16 = dot (unity_SHBr, tmpvar_15);
  x2_10.x = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = dot (unity_SHBg, tmpvar_15);
  x2_10.y = tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = dot (unity_SHBb, tmpvar_15);
  x2_10.z = tmpvar_18;
  mediump float tmpvar_19;
  tmpvar_19 = ((worldNormal_2.x * worldNormal_2.x) - (worldNormal_2.y * worldNormal_2.y));
  vC_8 = tmpvar_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (unity_SHC.xyz * vC_8);
  x3_9 = tmpvar_20;
  tmpvar_7 = ((x1_11 + x2_10) + x3_9);
  tmpvar_4 = tmpvar_7;
  mediump vec3 tmpvar_21;
  tmpvar_21 = (max (0.0, dot (worldNormal_2, _WorldSpaceLightPos0.xyz)) * _LightColor0.xyz);
  tmpvar_3 = tmpvar_21;
  highp vec4 tmpvar_22;
  tmpvar_22 = (glstate_matrix_mvp * _glesVertex);
  highp vec4 o_23;
  highp vec4 tmpvar_24;
  tmpvar_24 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_25;
  tmpvar_25.x = tmpvar_24.x;
  tmpvar_25.y = (tmpvar_24.y * _ProjectionParams.x);
  o_23.xy = (tmpvar_25 + tmpvar_24.w);
  o_23.zw = tmpvar_22.zw;
  highp vec2 tmpvar_26;
  tmpvar_26 = (_Object2World * _glesVertex).xz;
  wrlPos_1 = tmpvar_26;
  highp vec2 tmpvar_27;
  tmpvar_27.x = (_glesMultiTexCoord0.x + (wrlPos_1.x * _Scale));
  tmpvar_27.y = (_glesMultiTexCoord0.y + (wrlPos_1.y * _Scale));
  highp vec4 o_28;
  highp vec4 tmpvar_29;
  tmpvar_29 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_30;
  tmpvar_30.x = tmpvar_29.x;
  tmpvar_30.y = (tmpvar_29.y * _ProjectionParams.x);
  o_28.xy = (tmpvar_30 + tmpvar_29.w);
  o_28.zw = tmpvar_22.zw;
  gl_Position = tmpvar_22;
  xlv_TEXCOORD0 = (abs((sin((_Time.x * 5.0)) / 2.0)) + tmpvar_27);
  xlv_TEXCOORD2 = o_28;
  xlv_COLOR0 = tmpvar_3;
  xlv_TEXCOORD1 = o_23;
  xlv_COLOR1 = tmpvar_4;
}



#endif
#ifdef FRAGMENT

varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _DepthFactor;
uniform sampler2D _CameraDepthTexture;
uniform sampler2D _MainTex;
uniform sampler2D _ShadowMapTexture;
uniform highp vec4 _ZBufferParams;
void main ()
{
  lowp vec4 tmpvar_1;
  lowp vec4 col_2;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD0);
  col_2.w = tmpvar_3.w;
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2DProj (_CameraDepthTexture, xlv_TEXCOORD1);
  highp float z_5;
  z_5 = tmpvar_4.x;
  col_2.xyz = (tmpvar_3.xyz * ((xlv_COLOR0 * texture2DProj (_ShadowMapTexture, xlv_TEXCOORD2).x) + xlv_COLOR1));
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = col_2.xyz;
  tmpvar_6.w = clamp ((_DepthFactor * ((1.0/(((_ZBufferParams.z * z_5) + _ZBufferParams.w))) - xlv_TEXCOORD1.w)), 0.0, 1.0);
  tmpvar_1 = tmpvar_6;
  gl_FragData[0] = tmpvar_1;
}



#endif"
}

SubProgram "flash " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 0 [glstate_matrix_mvp]
Vector 8 [_Time]
Vector 9 [_ProjectionParams]
Vector 10 [_WorldSpaceLightPos0]
Vector 11 [unity_SHAr]
Vector 12 [unity_SHAg]
Vector 13 [unity_SHAb]
Vector 14 [unity_SHBr]
Vector 15 [unity_SHBg]
Vector 16 [unity_SHBb]
Vector 17 [unity_SHC]
Matrix 4 [_Object2World]
Vector 18 [unity_NPOTScale]
Vector 19 [_LightColor0]
Float 20 [_Scale]
"agal_vs
c21 -0.020833 -0.125 1.0 0.5
c22 -0.000002 -0.000022 0.002604 0.00026
c23 0.0 0.795775 0.5 0.0
c24 6.283185 -3.141593 0.0 0.0
[bc]
aaaaaaaaaaaaaeacagaaaaffabaaaaaaaaaaaaaaaaaaaaaa mov r0.z, c6.y
aaaaaaaaaaaaabacaeaaaaffabaaaaaaaaaaaaaaaaaaaaaa mov r0.x, c4.y
aaaaaaaaaaaaacacafaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov r0.y, c5
bcaaaaaaaaaaaiacaaaaaakeacaaaaaaaaaaaakeacaaaaaa dp3 r0.w, r0.xyzz, r0.xyzz
akaaaaaaaaaaaiacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa rsq r0.w, r0.w
adaaaaaaaaaaahacaaaaaappacaaaaaaaaaaaakeacaaaaaa mul r0.xyz, r0.w, r0.xyzz
adaaaaaaabaaapacaaaaaakeacaaaaaaaaaaaacjacaaaaaa mul r1, r0.xyzz, r0.yzzx
aaaaaaaaaaaaaiacbfaaaakkabaaaaaaaaaaaaaaaaaaaaaa mov r0.w, c21.z
bdaaaaaaacaaaeacaaaaaaoeacaaaaaaanaaaaoeabaaaaaa dp4 r2.z, r0, c13
bdaaaaaaacaaacacaaaaaaoeacaaaaaaamaaaaoeabaaaaaa dp4 r2.y, r0, c12
bdaaaaaaacaaabacaaaaaaoeacaaaaaaalaaaaoeabaaaaaa dp4 r2.x, r0, c11
adaaaaaaacaaaiacaaaaaaffacaaaaaaaaaaaaffacaaaaaa mul r2.w, r0.y, r0.y
adaaaaaaadaaaiacaaaaaaaaacaaaaaaaaaaaaaaacaaaaaa mul r3.w, r0.x, r0.x
acaaaaaaaaaaaiacadaaaappacaaaaaaacaaaappacaaaaaa sub r0.w, r3.w, r2.w
adaaaaaaaeaaahacaaaaaappacaaaaaabbaaaaoeabaaaaaa mul r4.xyz, r0.w, c17
aaaaaaaaaaaaaiacaiaaaaaaabaaaaaaaaaaaaaaaaaaaaaa mov r0.w, c8.x
adaaaaaaaaaaaiacaaaaaappacaaaaaabhaaaaffabaaaaaa mul r0.w, r0.w, c23.y
abaaaaaaaaaaaiacaaaaaappacaaaaaabhaaaakkabaaaaaa add r0.w, r0.w, c23.z
aiaaaaaaaaaaaiacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa frc r0.w, r0.w
bdaaaaaaadaaaeacabaaaaoeacaaaaaabaaaaaoeabaaaaaa dp4 r3.z, r1, c16
bdaaaaaaadaaabacabaaaaoeacaaaaaaaoaaaaoeabaaaaaa dp4 r3.x, r1, c14
bdaaaaaaadaaacacabaaaaoeacaaaaaaapaaaaoeabaaaaaa dp4 r3.y, r1, c15
abaaaaaaadaaahacacaaaakeacaaaaaaadaaaakeacaaaaaa add r3.xyz, r2.xyzz, r3.xyzz
bcaaaaaaaaaaacacaaaaaakeacaaaaaaakaaaaoeabaaaaaa dp3 r0.y, r0.xyzz, c10
bdaaaaaaabaaaiacaaaaaaoeaaaaaaaaadaaaaoeabaaaaaa dp4 r1.w, a0, c3
bdaaaaaaabaaabacaaaaaaoeaaaaaaaaaaaaaaoeabaaaaaa dp4 r1.x, a0, c0
bdaaaaaaabaaacacaaaaaaoeaaaaaaaaabaaaaoeabaaaaaa dp4 r1.y, a0, c1
adaaaaaaacaaahacabaaaapeacaaaaaabfaaaappabaaaaaa mul r2.xyz, r1.xyww, c21.w
bdaaaaaaabaaaeacaaaaaaoeaaaaaaaaacaaaaoeabaaaaaa dp4 r1.z, a0, c2
adaaaaaaacaaacacacaaaaffacaaaaaaajaaaaaaabaaaaaa mul r2.y, r2.y, c9.x
abaaaaaaacaaadacacaaaafeacaaaaaaacaaaakkacaaaaaa add r2.xy, r2.xyyy, r2.z
adaaaaaaacaaadacacaaaafeacaaaaaabcaaaaoeabaaaaaa mul r2.xy, r2.xyyy, c18
aaaaaaaaacaaamacabaaaaopacaaaaaaaaaaaaaaaaaaaaaa mov r2.zw, r1.wwzw
adaaaaaaaaaaaiacaaaaaappacaaaaaabiaaaaaaabaaaaaa mul r0.w, r0.w, c24.x
abaaaaaaaaaaaiacaaaaaappacaaaaaabiaaaaffabaaaaaa add r0.w, r0.w, c24.y
aaaaaaaaacaaapaeacaaaaoeacaaaaaaaaaaaaaaaaaaaaaa mov v2, r2
aaaaaaaaabaaapaeacaaaaoeacaaaaaaaaaaaaaaaaaaaaaa mov v1, r2
apaaaaaaacaaabacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa sin r2.x, r0.w
baaaaaaaacaaacacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa cos r2.y, r0.w
ahaaaaaaaaaaaiacaaaaaaffacaaaaaabhaaaaaaabaaaaaa max r0.w, r0.y, c23.x
adaaaaaaaaaaabacacaaaaffacaaaaaabfaaaappabaaaaaa mul r0.x, r2.y, c21.w
beaaaaaaaaaaaeacaaaaaaaaacaaaaaaaaaaaaaaaaaaaaaa abs r0.z, r0.x
bdaaaaaaaaaaacacaaaaaaoeaaaaaaaaagaaaaoeabaaaaaa dp4 r0.y, a0, c6
bdaaaaaaaaaaabacaaaaaaoeaaaaaaaaaeaaaaoeabaaaaaa dp4 r0.x, a0, c4
adaaaaaaaaaaacacaaaaaaffacaaaaaabeaaaaaaabaaaaaa mul r0.y, r0.y, c20.x
abaaaaaaaaaaacacaaaaaaffacaaaaaaadaaaaoeaaaaaaaa add r0.y, r0.y, a3
adaaaaaaaaaaabacaaaaaaaaacaaaaaabeaaaaoeabaaaaaa mul r0.x, r0.x, c20
abaaaaaaaaaaabacaaaaaaaaacaaaaaaadaaaaoeaaaaaaaa add r0.x, r0.x, a3
abaaaaaaagaaahaeadaaaakeacaaaaaaaeaaaakeacaaaaaa add v6.xyz, r3.xyzz, r4.xyzz
aaaaaaaaaaaaapadabaaaaoeacaaaaaaaaaaaaaaaaaaaaaa mov o0, r1
abaaaaaaaaaaadaeaaaaaakkacaaaaaaaaaaaafeacaaaaaa add v0.xy, r0.z, r0.xyyy
adaaaaaaahaaahaeaaaaaappacaaaaaabdaaaaoeabaaaaaa mul v7.xyz, r0.w, c19
aaaaaaaaaaaaamaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v0.zw, c0
aaaaaaaaagaaaiaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v6.w, c0
aaaaaaaaahaaaiaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v7.w, c0
"
}

SubProgram "d3d11_9x " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
ConstBuffer "$Globals" 128 // 116 used size, 6 vars
Vector 16 [_LightColor0] 4
Float 112 [_Scale]
ConstBuffer "UnityPerCamera" 128 // 96 used size, 8 vars
Vector 0 [_Time] 4
Vector 80 [_ProjectionParams] 4
ConstBuffer "UnityLighting" 720 // 720 used size, 17 vars
Vector 0 [_WorldSpaceLightPos0] 4
Vector 608 [unity_SHAr] 4
Vector 624 [unity_SHAg] 4
Vector 640 [unity_SHAb] 4
Vector 656 [unity_SHBr] 4
Vector 672 [unity_SHBg] 4
Vector 688 [unity_SHBb] 4
Vector 704 [unity_SHC] 4
ConstBuffer "UnityPerDraw" 336 // 256 used size, 6 vars
Matrix 0 [glstate_matrix_mvp] 4
Matrix 192 [_Object2World] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
BindCB "UnityLighting" 2
BindCB "UnityPerDraw" 3
// 39 instructions, 4 temp regs, 0 temp arrays:
// ALU 34 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0_level_9_1
eefieceddenomlclbbglbdbkafieepgfcmceckdkabaaaaaajeakaaaaaeaaaaaa
daaaaaaapaadaaaageajaaaaniajaaaaebgpgodjliadaaaaliadaaaaaaacpopp
daadaaaaiiaaaaaaaiaaceaaaaaaieaaaaaaieaaaaaaceaaabaaieaaaaaaabaa
abaaabaaaaaaaaaaaaaaahaaabaaacaaaaaaaaaaabaaaaaaabaaadaaaaaaaaaa
abaaafaaabaaaeaaaaaaaaaaacaaaaaaabaaafaaaaaaaaaaacaacgaaahaaagaa
aaaaaaaaadaaaaaaaeaaanaaaaaaaaaaadaaamaaaeaabbaaaaaaaaaaaaaaaaaa
aaacpoppfbaaaaafbfaaapkaaaaaiadpaaaaaaaaaaaaaadpoelheldpfbaaaaaf
bgaaapkanlapmjeanlapejmaaaaaaaaaaaaaaaaafbaaaaafbhaaapkaabannalf
gballglhklkkckdlijiiiidjfbaaaaafbiaaapkaklkkkklmaaaaaaloaaaaiadp
aaaaaadpbpaaaaacafaaaaiaaaaaapjabpaaaaacafaaabiaabaaapjaabaaaaac
aaaaaiiabfaaaakaceaaaaacaaaaahiabcaaoekaajaaaaadabaaabiaagaaoeka
aaaaoeiaajaaaaadabaaaciaahaaoekaaaaaoeiaajaaaaadabaaaeiaaiaaoeka
aaaaoeiaafaaaaadacaaapiaaaaacjiaaaaakeiaajaaaaadadaaabiaajaaoeka
acaaoeiaajaaaaadadaaaciaakaaoekaacaaoeiaajaaaaadadaaaeiaalaaoeka
acaaoeiaacaaaaadabaaahiaabaaoeiaadaaoeiaafaaaaadaaaaaiiaaaaaffia
aaaaffiaaeaaaaaeaaaaaiiaaaaaaaiaaaaaaaiaaaaappibaiaaaaadaaaaabia
aaaaoeiaafaaoekaalaaaaadaaaaabiaaaaaaaiabfaaffkaafaaaaadacaaahoa
aaaaaaiaabaaoekaaeaaaaaeaeaaahoaamaaoekaaaaappiaabaaoeiaabaaaaac
aaaaamiabfaaoekaaeaaaaaeaaaaabiaadaaaakaaaaappiaaaaakkiabdaaaaac
aaaaabiaaaaaaaiaaeaaaaaeaaaaabiaaaaaaaiabgaaaakabgaaffkacfaaaaae
abaaaciaaaaaaaiabhaaoekabiaaoekaafaaaaadaaaaabiaabaaffiabfaakkka
cdaaaaacaaaaabiaaaaaaaiaafaaaaadaaaaagiaaaaaffjabcaaoakaaeaaaaae
aaaaagiabbaaoakaaaaaaajaaaaaoeiaaeaaaaaeaaaaagiabdaaoakaaaaakkja
aaaaoeiaaeaaaaaeaaaaagiabeaaoakaaaaappjaaaaaoeiaaeaaaaaeaaaaagia
aaaaoeiaacaaaakaabaanajaacaaaaadaaaaadoaaaaaojiaaaaaaaiaafaaaaad
aaaaapiaaaaaffjaaoaaoekaaeaaaaaeaaaaapiaanaaoekaaaaaaajaaaaaoeia
aeaaaaaeaaaaapiaapaaoekaaaaakkjaaaaaoeiaaeaaaaaeaaaaapiabaaaoeka
aaaappjaaaaaoeiaaeaaaaaeaaaaadmaaaaappiaaaaaoekaaaaaoeiaabaaaaac
abaaamiaaaaaoeiaabaaaaacaaaaammaabaaoeiaafaaaaadaaaaaciaaaaaffia
aeaaaakaafaaaaadaaaaafiaaaaapeiabfaakkkaafaaaaadaaaaaiiaaaaaffia
bfaakkkaacaaaaadabaaadiaaaaakkiaaaaaomiaabaaaaacabaaapoaabaaoeia
abaaaaacadaaapoaabaaoeiappppaaaafdeieefcgmafaaaaeaaaabaaflabaaaa
fjaaaaaeegiocaaaaaaaaaaaaiaaaaaafjaaaaaeegiocaaaabaaaaaaagaaaaaa
fjaaaaaeegiocaaaacaaaaaacnaaaaaafjaaaaaeegiocaaaadaaaaaabaaaaaaa
fpaaaaadpcbabaaaaaaaaaaafpaaaaaddcbabaaaabaaaaaaghaaaaaepccabaaa
aaaaaaaaabaaaaaagfaaaaaddccabaaaabaaaaaagfaaaaadpccabaaaacaaaaaa
gfaaaaadhccabaaaadaaaaaagfaaaaadpccabaaaaeaaaaaagfaaaaadhccabaaa
afaaaaaagiaaaaacaeaaaaaadiaaaaaipcaabaaaaaaaaaaafgbfbaaaaaaaaaaa
egiocaaaadaaaaaaabaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaadaaaaaa
aaaaaaaaagbabaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaakpcaabaaaaaaaaaaa
egiocaaaadaaaaaaacaaaaaakgbkbaaaaaaaaaaaegaobaaaaaaaaaaadcaaaaak
pcaabaaaaaaaaaaaegiocaaaadaaaaaaadaaaaaapgbpbaaaaaaaaaaaegaobaaa
aaaaaaaadgaaaaafpccabaaaaaaaaaaaegaobaaaaaaaaaaadiaaaaaidcaabaaa
abaaaaaafgbfbaaaaaaaaaaaigiacaaaadaaaaaaanaaaaaadcaaaaakdcaabaaa
abaaaaaaigiacaaaadaaaaaaamaaaaaaagbabaaaaaaaaaaaegaabaaaabaaaaaa
dcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaaoaaaaaakgbkbaaaaaaaaaaa
egaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaigiacaaaadaaaaaaapaaaaaa
pgbpbaaaaaaaaaaaegaabaaaabaaaaaadcaaaaakdcaabaaaabaaaaaaegaabaaa
abaaaaaaagiacaaaaaaaaaaaahaaaaaaegbabaaaabaaaaaadiaaaaaiecaabaaa
abaaaaaaakiacaaaabaaaaaaaaaaaaaaabeaaaaaaaaakaeaenaaaaagecaabaaa
abaaaaaaaanaaaaackaabaaaabaaaaaadiaaaaahecaabaaaabaaaaaackaabaaa
abaaaaaaabeaaaaaaaaaaadpaaaaaaaidccabaaaabaaaaaaegaabaaaabaaaaaa
kgakbaiaibaaaaaaabaaaaaadiaaaaaibcaabaaaabaaaaaabkaabaaaaaaaaaaa
akiacaaaabaaaaaaafaaaaaadiaaaaahicaabaaaabaaaaaaakaabaaaabaaaaaa
abeaaaaaaaaaaadpdiaaaaakfcaabaaaabaaaaaaagadbaaaaaaaaaaaaceaaaaa
aaaaaadpaaaaaaaaaaaaaadpaaaaaaaaaaaaaaahdcaabaaaaaaaaaaakgakbaaa
abaaaaaamgaabaaaabaaaaaadgaaaaafpccabaaaacaaaaaaegaobaaaaaaaaaaa
dgaaaaafpccabaaaaeaaaaaaegaobaaaaaaaaaaabaaaaaajbcaabaaaaaaaaaaa
egiccaaaadaaaaaaanaaaaaaegiccaaaadaaaaaaanaaaaaaeeaaaaafbcaabaaa
aaaaaaaaakaabaaaaaaaaaaadiaaaaaihcaabaaaaaaaaaaaagaabaaaaaaaaaaa
egiccaaaadaaaaaaanaaaaaabaaaaaaibcaabaaaabaaaaaaegacbaaaaaaaaaaa
egiccaaaacaaaaaaaaaaaaaadeaaaaahbcaabaaaabaaaaaaakaabaaaabaaaaaa
abeaaaaaaaaaaaaadiaaaaaihccabaaaadaaaaaaagaabaaaabaaaaaaegiccaaa
aaaaaaaaabaaaaaadgaaaaaficaabaaaaaaaaaaaabeaaaaaaaaaiadpbbaaaaai
bcaabaaaabaaaaaaegiocaaaacaaaaaacgaaaaaaegaobaaaaaaaaaaabbaaaaai
ccaabaaaabaaaaaaegiocaaaacaaaaaachaaaaaaegaobaaaaaaaaaaabbaaaaai
ecaabaaaabaaaaaaegiocaaaacaaaaaaciaaaaaaegaobaaaaaaaaaaadiaaaaah
pcaabaaaacaaaaaajgacbaaaaaaaaaaaegakbaaaaaaaaaaabbaaaaaibcaabaaa
adaaaaaaegiocaaaacaaaaaacjaaaaaaegaobaaaacaaaaaabbaaaaaiccaabaaa
adaaaaaaegiocaaaacaaaaaackaaaaaaegaobaaaacaaaaaabbaaaaaiecaabaaa
adaaaaaaegiocaaaacaaaaaaclaaaaaaegaobaaaacaaaaaaaaaaaaahhcaabaaa
abaaaaaaegacbaaaabaaaaaaegacbaaaadaaaaaadiaaaaahccaabaaaaaaaaaaa
bkaabaaaaaaaaaaabkaabaaaaaaaaaaadcaaaaakbcaabaaaaaaaaaaaakaabaaa
aaaaaaaaakaabaaaaaaaaaaabkaabaiaebaaaaaaaaaaaaaadcaaaaakhccabaaa
afaaaaaaegiccaaaacaaaaaacmaaaaaaagaabaaaaaaaaaaaegacbaaaabaaaaaa
doaaaaabejfdeheogmaaaaaaadaaaaaaaiaaaaaafaaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaaaaaaaaaapapaaaafjaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaa
adadaaaagcaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaaahaaaaaafaepfdej
feejepeoaafeeffiedepepfceeaaeoepfcenebemaaklklklepfdeheoleaaaaaa
agaaaaaaaiaaaaaajiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaa
keaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaadamaaaakeaaaaaaacaaaaaa
aaaaaaaaadaaaaaaacaaaaaaapaaaaaaknaaaaaaaaaaaaaaaaaaaaaaadaaaaaa
adaaaaaaahaiaaaakeaaaaaaabaaaaaaaaaaaaaaadaaaaaaaeaaaaaaapaaaaaa
knaaaaaaabaaaaaaaaaaaaaaadaaaaaaafaaaaaaahaiaaaafdfgfpfaepfdejfe
ejepeoaafeeffiedepepfceeaaedepemepfcaakl"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal (normalize(_glesNormal))
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 407
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    highp vec4 _ShadowCoord;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 400
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 393
uniform sampler2D _MainTex;
#line 417
#line 421
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
#line 437
uniform highp float _DepthFactor;
#line 284
highp vec4 ComputeScreenPos( in highp vec4 pos ) {
    #line 286
    highp vec4 o = (pos * 0.5);
    o.xy = (vec2( o.x, (o.y * _ProjectionParams.x)) + o.w);
    o.zw = pos.zw;
    return o;
}
#line 137
mediump vec3 ShadeSH9( in mediump vec4 normal ) {
    mediump vec3 x1;
    mediump vec3 x2;
    mediump vec3 x3;
    x1.x = dot( unity_SHAr, normal);
    #line 141
    x1.y = dot( unity_SHAg, normal);
    x1.z = dot( unity_SHAb, normal);
    mediump vec4 vB = (normal.xyzz * normal.yzzx);
    x2.x = dot( unity_SHBr, vB);
    #line 145
    x2.y = dot( unity_SHBg, vB);
    x2.z = dot( unity_SHBb, vB);
    highp float vC = ((normal.x * normal.x) - (normal.y * normal.y));
    x3 = (unity_SHC.xyz * vC);
    #line 149
    return ((x1 + x2) + x3);
}
#line 417
highp vec2 offset( in highp vec2 uv ) {
    return (abs((sin((_Time.x * 5.0)) / 2.0)) + uv);
}
#line 422
v2f vert( in appdata_t v ) {
    v2f o;
    #line 425
    mediump vec3 worldNormal = normalize((_Object2World * vec4( 0.0, 1.0, 0.0, 0.0)).xyz);
    o.ambient = ShadeSH9( vec4( worldNormal, 1.0));
    mediump float nl = max( 0.0, dot( worldNormal, _WorldSpaceLightPos0.xyz));
    o.diff = (nl * _LightColor0.xyz);
    #line 429
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.scrPos = ComputeScreenPos( o.pos);
    mediump vec2 wrlPos = (_Object2World * v.vertex).xz;
    o.uv = offset( vec2( (v.texcoord.x + (wrlPos.x * _Scale)), (v.texcoord.y + (wrlPos.y * _Scale))));
    #line 433
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    return o;
}
out highp vec2 xlv_TEXCOORD0;
out highp vec4 xlv_TEXCOORD2;
out lowp vec3 xlv_COLOR0;
out highp vec4 xlv_TEXCOORD1;
out lowp vec3 xlv_COLOR1;
void main() {
    v2f xl_retval;
    appdata_t xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.texcoord = vec2(gl_MultiTexCoord0);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vert( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec2(xl_retval.uv);
    xlv_TEXCOORD2 = vec4(xl_retval._ShadowCoord);
    xlv_COLOR0 = vec3(xl_retval.diff);
    xlv_TEXCOORD1 = vec4(xl_retval.scrPos);
    xlv_COLOR1 = vec3(xl_retval.ambient);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 407
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    highp vec4 _ShadowCoord;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 400
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform highp vec4 _ShadowOffsets[4];
uniform sampler2D _ShadowMapTexture;
#line 393
uniform sampler2D _MainTex;
#line 417
#line 421
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
#line 437
uniform highp float _DepthFactor;
#line 280
highp float LinearEyeDepth( in highp float z ) {
    #line 282
    return (1.0 / ((_ZBufferParams.z * z) + _ZBufferParams.w));
}
#line 393
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    highp float dist = textureProj( _ShadowMapTexture, shadowCoord).x;
    mediump float lightShadowDataX = _LightShadowData.x;
    #line 397
    return max( float((dist > (shadowCoord.z / shadowCoord.w))), lightShadowDataX);
}
#line 438
lowp vec4 frag( in v2f i ) {
    lowp vec4 col = texture( _MainTex, i.uv);
    #line 441
    highp float depth = LinearEyeDepth( float( textureProj( _CameraDepthTexture, i.scrPos).xyzw));
    highp float diff = xll_saturate_f((_DepthFactor * (depth - i.scrPos.w)));
    lowp float shadow = unitySampleShadow( i._ShadowCoord);
    lowp vec3 lighting = ((i.diff * shadow) + i.ambient);
    #line 445
    col.xyz *= lighting;
    return vec4( col.xyz, diff);
}
in highp vec2 xlv_TEXCOORD0;
in highp vec4 xlv_TEXCOORD2;
in lowp vec3 xlv_COLOR0;
in highp vec4 xlv_TEXCOORD1;
in lowp vec3 xlv_COLOR1;
void main() {
    lowp vec4 xl_retval;
    v2f xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.uv = vec2(xlv_TEXCOORD0);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD2);
    xlt_i.diff = vec3(xlv_COLOR0);
    xlt_i.scrPos = vec4(xlv_TEXCOORD1);
    xlt_i.ambient = vec3(xlv_COLOR1);
    xl_retval = frag( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "SHADOWS_NATIVE" }
"!!GLES


#ifdef VERTEX

#extension GL_EXT_shadow_samplers : enable
varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _Scale;
uniform lowp vec4 _LightColor0;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_SHC;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAr;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
uniform highp vec4 _Time;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  mediump vec2 wrlPos_1;
  mediump vec3 worldNormal_2;
  lowp vec3 tmpvar_3;
  lowp vec3 tmpvar_4;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_Object2World * vec4(0.0, 1.0, 0.0, 0.0)).xyz);
  worldNormal_2 = tmpvar_5;
  mediump vec4 tmpvar_6;
  tmpvar_6.w = 1.0;
  tmpvar_6.xyz = worldNormal_2;
  mediump vec3 tmpvar_7;
  highp float vC_8;
  mediump vec3 x3_9;
  mediump vec3 x2_10;
  mediump vec3 x1_11;
  highp float tmpvar_12;
  tmpvar_12 = dot (unity_SHAr, tmpvar_6);
  x1_11.x = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = dot (unity_SHAg, tmpvar_6);
  x1_11.y = tmpvar_13;
  highp float tmpvar_14;
  tmpvar_14 = dot (unity_SHAb, tmpvar_6);
  x1_11.z = tmpvar_14;
  mediump vec4 tmpvar_15;
  tmpvar_15 = (worldNormal_2.xyzz * worldNormal_2.yzzx);
  highp float tmpvar_16;
  tmpvar_16 = dot (unity_SHBr, tmpvar_15);
  x2_10.x = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = dot (unity_SHBg, tmpvar_15);
  x2_10.y = tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = dot (unity_SHBb, tmpvar_15);
  x2_10.z = tmpvar_18;
  mediump float tmpvar_19;
  tmpvar_19 = ((worldNormal_2.x * worldNormal_2.x) - (worldNormal_2.y * worldNormal_2.y));
  vC_8 = tmpvar_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (unity_SHC.xyz * vC_8);
  x3_9 = tmpvar_20;
  tmpvar_7 = ((x1_11 + x2_10) + x3_9);
  tmpvar_4 = tmpvar_7;
  mediump vec3 tmpvar_21;
  tmpvar_21 = (max (0.0, dot (worldNormal_2, _WorldSpaceLightPos0.xyz)) * _LightColor0.xyz);
  tmpvar_3 = tmpvar_21;
  highp vec4 tmpvar_22;
  tmpvar_22 = (glstate_matrix_mvp * _glesVertex);
  highp vec4 o_23;
  highp vec4 tmpvar_24;
  tmpvar_24 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_25;
  tmpvar_25.x = tmpvar_24.x;
  tmpvar_25.y = (tmpvar_24.y * _ProjectionParams.x);
  o_23.xy = (tmpvar_25 + tmpvar_24.w);
  o_23.zw = tmpvar_22.zw;
  highp vec2 tmpvar_26;
  tmpvar_26 = (_Object2World * _glesVertex).xz;
  wrlPos_1 = tmpvar_26;
  highp vec2 tmpvar_27;
  tmpvar_27.x = (_glesMultiTexCoord0.x + (wrlPos_1.x * _Scale));
  tmpvar_27.y = (_glesMultiTexCoord0.y + (wrlPos_1.y * _Scale));
  gl_Position = tmpvar_22;
  xlv_TEXCOORD0 = (abs((sin((_Time.x * 5.0)) / 2.0)) + tmpvar_27);
  xlv_TEXCOORD2 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
  xlv_COLOR0 = tmpvar_3;
  xlv_TEXCOORD1 = o_23;
  xlv_COLOR1 = tmpvar_4;
}



#endif
#ifdef FRAGMENT

#extension GL_EXT_shadow_samplers : enable
varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _DepthFactor;
uniform sampler2D _CameraDepthTexture;
uniform sampler2D _MainTex;
uniform lowp sampler2DShadow _ShadowMapTexture;
uniform highp vec4 _LightShadowData;
uniform highp vec4 _ZBufferParams;
void main ()
{
  lowp vec4 tmpvar_1;
  lowp vec4 col_2;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD0);
  col_2.w = tmpvar_3.w;
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2DProj (_CameraDepthTexture, xlv_TEXCOORD1);
  highp float z_5;
  z_5 = tmpvar_4.x;
  lowp float shadow_6;
  lowp float tmpvar_7;
  tmpvar_7 = shadow2DEXT (_ShadowMapTexture, xlv_TEXCOORD2.xyz);
  highp float tmpvar_8;
  tmpvar_8 = (_LightShadowData.x + (tmpvar_7 * (1.0 - _LightShadowData.x)));
  shadow_6 = tmpvar_8;
  col_2.xyz = (tmpvar_3.xyz * ((xlv_COLOR0 * shadow_6) + xlv_COLOR1));
  highp vec4 tmpvar_9;
  tmpvar_9.xyz = col_2.xyz;
  tmpvar_9.w = clamp ((_DepthFactor * ((1.0/(((_ZBufferParams.z * z_5) + _ZBufferParams.w))) - xlv_TEXCOORD1.w)), 0.0, 1.0);
  tmpvar_1 = tmpvar_9;
  gl_FragData[0] = tmpvar_1;
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "SHADOWS_NATIVE" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal (normalize(_glesNormal))
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 407
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    highp vec4 _ShadowCoord;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 400
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 393
uniform sampler2D _MainTex;
#line 417
#line 421
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
#line 437
uniform highp float _DepthFactor;
#line 284
highp vec4 ComputeScreenPos( in highp vec4 pos ) {
    #line 286
    highp vec4 o = (pos * 0.5);
    o.xy = (vec2( o.x, (o.y * _ProjectionParams.x)) + o.w);
    o.zw = pos.zw;
    return o;
}
#line 137
mediump vec3 ShadeSH9( in mediump vec4 normal ) {
    mediump vec3 x1;
    mediump vec3 x2;
    mediump vec3 x3;
    x1.x = dot( unity_SHAr, normal);
    #line 141
    x1.y = dot( unity_SHAg, normal);
    x1.z = dot( unity_SHAb, normal);
    mediump vec4 vB = (normal.xyzz * normal.yzzx);
    x2.x = dot( unity_SHBr, vB);
    #line 145
    x2.y = dot( unity_SHBg, vB);
    x2.z = dot( unity_SHBb, vB);
    highp float vC = ((normal.x * normal.x) - (normal.y * normal.y));
    x3 = (unity_SHC.xyz * vC);
    #line 149
    return ((x1 + x2) + x3);
}
#line 417
highp vec2 offset( in highp vec2 uv ) {
    return (abs((sin((_Time.x * 5.0)) / 2.0)) + uv);
}
#line 422
v2f vert( in appdata_t v ) {
    v2f o;
    #line 425
    mediump vec3 worldNormal = normalize((_Object2World * vec4( 0.0, 1.0, 0.0, 0.0)).xyz);
    o.ambient = ShadeSH9( vec4( worldNormal, 1.0));
    mediump float nl = max( 0.0, dot( worldNormal, _WorldSpaceLightPos0.xyz));
    o.diff = (nl * _LightColor0.xyz);
    #line 429
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.scrPos = ComputeScreenPos( o.pos);
    mediump vec2 wrlPos = (_Object2World * v.vertex).xz;
    o.uv = offset( vec2( (v.texcoord.x + (wrlPos.x * _Scale)), (v.texcoord.y + (wrlPos.y * _Scale))));
    #line 433
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    return o;
}
out highp vec2 xlv_TEXCOORD0;
out highp vec4 xlv_TEXCOORD2;
out lowp vec3 xlv_COLOR0;
out highp vec4 xlv_TEXCOORD1;
out lowp vec3 xlv_COLOR1;
void main() {
    v2f xl_retval;
    appdata_t xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.texcoord = vec2(gl_MultiTexCoord0);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vert( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec2(xl_retval.uv);
    xlv_TEXCOORD2 = vec4(xl_retval._ShadowCoord);
    xlv_COLOR0 = vec3(xl_retval.diff);
    xlv_TEXCOORD1 = vec4(xl_retval.scrPos);
    xlv_COLOR1 = vec3(xl_retval.ambient);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_shadow2D(mediump sampler2DShadow s, vec3 coord) { return texture (s, coord); }
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 407
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    highp vec4 _ShadowCoord;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 400
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 393
uniform sampler2D _MainTex;
#line 417
#line 421
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
#line 437
uniform highp float _DepthFactor;
#line 280
highp float LinearEyeDepth( in highp float z ) {
    #line 282
    return (1.0 / ((_ZBufferParams.z * z) + _ZBufferParams.w));
}
#line 393
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    lowp float shadow = xll_shadow2D( _ShadowMapTexture, shadowCoord.xyz.xyz);
    shadow = (_LightShadowData.x + (shadow * (1.0 - _LightShadowData.x)));
    #line 397
    return shadow;
}
#line 438
lowp vec4 frag( in v2f i ) {
    lowp vec4 col = texture( _MainTex, i.uv);
    #line 441
    highp float depth = LinearEyeDepth( float( textureProj( _CameraDepthTexture, i.scrPos).xyzw));
    highp float diff = xll_saturate_f((_DepthFactor * (depth - i.scrPos.w)));
    lowp float shadow = unitySampleShadow( i._ShadowCoord);
    lowp vec3 lighting = ((i.diff * shadow) + i.ambient);
    #line 445
    col.xyz *= lighting;
    return vec4( col.xyz, diff);
}
in highp vec2 xlv_TEXCOORD0;
in highp vec4 xlv_TEXCOORD2;
in lowp vec3 xlv_COLOR0;
in highp vec4 xlv_TEXCOORD1;
in lowp vec3 xlv_COLOR1;
void main() {
    lowp vec4 xl_retval;
    v2f xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.uv = vec2(xlv_TEXCOORD0);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD2);
    xlt_i.diff = vec3(xlv_COLOR0);
    xlt_i.scrPos = vec4(xlv_TEXCOORD1);
    xlt_i.ambient = vec3(xlv_COLOR1);
    xl_retval = frag( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "SHADOWS_NATIVE" "VERTEXLIGHT_ON" }
"!!GLES


#ifdef VERTEX

#extension GL_EXT_shadow_samplers : enable
varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _Scale;
uniform lowp vec4 _LightColor0;
uniform highp mat4 _Object2World;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 unity_SHC;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAr;
uniform lowp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _ProjectionParams;
uniform highp vec4 _Time;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  mediump vec2 wrlPos_1;
  mediump vec3 worldNormal_2;
  lowp vec3 tmpvar_3;
  lowp vec3 tmpvar_4;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_Object2World * vec4(0.0, 1.0, 0.0, 0.0)).xyz);
  worldNormal_2 = tmpvar_5;
  mediump vec4 tmpvar_6;
  tmpvar_6.w = 1.0;
  tmpvar_6.xyz = worldNormal_2;
  mediump vec3 tmpvar_7;
  highp float vC_8;
  mediump vec3 x3_9;
  mediump vec3 x2_10;
  mediump vec3 x1_11;
  highp float tmpvar_12;
  tmpvar_12 = dot (unity_SHAr, tmpvar_6);
  x1_11.x = tmpvar_12;
  highp float tmpvar_13;
  tmpvar_13 = dot (unity_SHAg, tmpvar_6);
  x1_11.y = tmpvar_13;
  highp float tmpvar_14;
  tmpvar_14 = dot (unity_SHAb, tmpvar_6);
  x1_11.z = tmpvar_14;
  mediump vec4 tmpvar_15;
  tmpvar_15 = (worldNormal_2.xyzz * worldNormal_2.yzzx);
  highp float tmpvar_16;
  tmpvar_16 = dot (unity_SHBr, tmpvar_15);
  x2_10.x = tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = dot (unity_SHBg, tmpvar_15);
  x2_10.y = tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = dot (unity_SHBb, tmpvar_15);
  x2_10.z = tmpvar_18;
  mediump float tmpvar_19;
  tmpvar_19 = ((worldNormal_2.x * worldNormal_2.x) - (worldNormal_2.y * worldNormal_2.y));
  vC_8 = tmpvar_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (unity_SHC.xyz * vC_8);
  x3_9 = tmpvar_20;
  tmpvar_7 = ((x1_11 + x2_10) + x3_9);
  tmpvar_4 = tmpvar_7;
  mediump vec3 tmpvar_21;
  tmpvar_21 = (max (0.0, dot (worldNormal_2, _WorldSpaceLightPos0.xyz)) * _LightColor0.xyz);
  tmpvar_3 = tmpvar_21;
  highp vec4 tmpvar_22;
  tmpvar_22 = (glstate_matrix_mvp * _glesVertex);
  highp vec4 o_23;
  highp vec4 tmpvar_24;
  tmpvar_24 = (tmpvar_22 * 0.5);
  highp vec2 tmpvar_25;
  tmpvar_25.x = tmpvar_24.x;
  tmpvar_25.y = (tmpvar_24.y * _ProjectionParams.x);
  o_23.xy = (tmpvar_25 + tmpvar_24.w);
  o_23.zw = tmpvar_22.zw;
  highp vec2 tmpvar_26;
  tmpvar_26 = (_Object2World * _glesVertex).xz;
  wrlPos_1 = tmpvar_26;
  highp vec2 tmpvar_27;
  tmpvar_27.x = (_glesMultiTexCoord0.x + (wrlPos_1.x * _Scale));
  tmpvar_27.y = (_glesMultiTexCoord0.y + (wrlPos_1.y * _Scale));
  gl_Position = tmpvar_22;
  xlv_TEXCOORD0 = (abs((sin((_Time.x * 5.0)) / 2.0)) + tmpvar_27);
  xlv_TEXCOORD2 = (unity_World2Shadow[0] * (_Object2World * _glesVertex));
  xlv_COLOR0 = tmpvar_3;
  xlv_TEXCOORD1 = o_23;
  xlv_COLOR1 = tmpvar_4;
}



#endif
#ifdef FRAGMENT

#extension GL_EXT_shadow_samplers : enable
varying lowp vec3 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD1;
varying lowp vec3 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD0;
uniform highp float _DepthFactor;
uniform sampler2D _CameraDepthTexture;
uniform sampler2D _MainTex;
uniform lowp sampler2DShadow _ShadowMapTexture;
uniform highp vec4 _LightShadowData;
uniform highp vec4 _ZBufferParams;
void main ()
{
  lowp vec4 tmpvar_1;
  lowp vec4 col_2;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_MainTex, xlv_TEXCOORD0);
  col_2.w = tmpvar_3.w;
  lowp vec4 tmpvar_4;
  tmpvar_4 = texture2DProj (_CameraDepthTexture, xlv_TEXCOORD1);
  highp float z_5;
  z_5 = tmpvar_4.x;
  lowp float shadow_6;
  lowp float tmpvar_7;
  tmpvar_7 = shadow2DEXT (_ShadowMapTexture, xlv_TEXCOORD2.xyz);
  highp float tmpvar_8;
  tmpvar_8 = (_LightShadowData.x + (tmpvar_7 * (1.0 - _LightShadowData.x)));
  shadow_6 = tmpvar_8;
  col_2.xyz = (tmpvar_3.xyz * ((xlv_COLOR0 * shadow_6) + xlv_COLOR1));
  highp vec4 tmpvar_9;
  tmpvar_9.xyz = col_2.xyz;
  tmpvar_9.w = clamp ((_DepthFactor * ((1.0/(((_ZBufferParams.z * z_5) + _ZBufferParams.w))) - xlv_TEXCOORD1.w)), 0.0, 1.0);
  tmpvar_1 = tmpvar_9;
  gl_FragData[0] = tmpvar_1;
}



#endif"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" "SHADOWS_NATIVE" "VERTEXLIGHT_ON" }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_Normal (normalize(_glesNormal))
in vec3 _glesNormal;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;

#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 407
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    highp vec4 _ShadowCoord;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 400
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 393
uniform sampler2D _MainTex;
#line 417
#line 421
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
#line 437
uniform highp float _DepthFactor;
#line 284
highp vec4 ComputeScreenPos( in highp vec4 pos ) {
    #line 286
    highp vec4 o = (pos * 0.5);
    o.xy = (vec2( o.x, (o.y * _ProjectionParams.x)) + o.w);
    o.zw = pos.zw;
    return o;
}
#line 137
mediump vec3 ShadeSH9( in mediump vec4 normal ) {
    mediump vec3 x1;
    mediump vec3 x2;
    mediump vec3 x3;
    x1.x = dot( unity_SHAr, normal);
    #line 141
    x1.y = dot( unity_SHAg, normal);
    x1.z = dot( unity_SHAb, normal);
    mediump vec4 vB = (normal.xyzz * normal.yzzx);
    x2.x = dot( unity_SHBr, vB);
    #line 145
    x2.y = dot( unity_SHBg, vB);
    x2.z = dot( unity_SHBb, vB);
    highp float vC = ((normal.x * normal.x) - (normal.y * normal.y));
    x3 = (unity_SHC.xyz * vC);
    #line 149
    return ((x1 + x2) + x3);
}
#line 417
highp vec2 offset( in highp vec2 uv ) {
    return (abs((sin((_Time.x * 5.0)) / 2.0)) + uv);
}
#line 422
v2f vert( in appdata_t v ) {
    v2f o;
    #line 425
    mediump vec3 worldNormal = normalize((_Object2World * vec4( 0.0, 1.0, 0.0, 0.0)).xyz);
    o.ambient = ShadeSH9( vec4( worldNormal, 1.0));
    mediump float nl = max( 0.0, dot( worldNormal, _WorldSpaceLightPos0.xyz));
    o.diff = (nl * _LightColor0.xyz);
    #line 429
    o.pos = (glstate_matrix_mvp * v.vertex);
    o.scrPos = ComputeScreenPos( o.pos);
    mediump vec2 wrlPos = (_Object2World * v.vertex).xz;
    o.uv = offset( vec2( (v.texcoord.x + (wrlPos.x * _Scale)), (v.texcoord.y + (wrlPos.y * _Scale))));
    #line 433
    o._ShadowCoord = (unity_World2Shadow[0] * (_Object2World * v.vertex));
    return o;
}
out highp vec2 xlv_TEXCOORD0;
out highp vec4 xlv_TEXCOORD2;
out lowp vec3 xlv_COLOR0;
out highp vec4 xlv_TEXCOORD1;
out lowp vec3 xlv_COLOR1;
void main() {
    v2f xl_retval;
    appdata_t xlt_v;
    xlt_v.vertex = vec4(gl_Vertex);
    xlt_v.texcoord = vec2(gl_MultiTexCoord0);
    xlt_v.normal = vec3(gl_Normal);
    xl_retval = vert( xlt_v);
    gl_Position = vec4(xl_retval.pos);
    xlv_TEXCOORD0 = vec2(xl_retval.uv);
    xlv_TEXCOORD2 = vec4(xl_retval._ShadowCoord);
    xlv_COLOR0 = vec3(xl_retval.diff);
    xlv_TEXCOORD1 = vec4(xl_retval.scrPos);
    xlv_COLOR1 = vec3(xl_retval.ambient);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];
float xll_shadow2D(mediump sampler2DShadow s, vec3 coord) { return texture (s, coord); }
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 151
struct v2f_vertex_lit {
    highp vec2 uv;
    lowp vec4 diff;
    lowp vec4 spec;
};
#line 187
struct v2f_img {
    highp vec4 pos;
    mediump vec2 uv;
};
#line 181
struct appdata_img {
    highp vec4 vertex;
    mediump vec2 texcoord;
};
#line 315
struct SurfaceOutput {
    lowp vec3 Albedo;
    lowp vec3 Normal;
    lowp vec3 Emission;
    mediump float Specular;
    lowp float Gloss;
    lowp float Alpha;
};
#line 407
struct v2f {
    highp vec4 pos;
    highp vec2 uv;
    highp vec4 _ShadowCoord;
    lowp vec3 diff;
    highp vec4 scrPos;
    lowp vec3 ambient;
};
#line 400
struct appdata_t {
    highp vec4 vertex;
    highp vec2 texcoord;
    highp vec3 normal;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform lowp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[8];
uniform highp vec4 unity_LightPosition[8];
uniform highp vec4 unity_LightAtten[8];
#line 19
uniform highp vec4 unity_SpotDirection[8];
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
#line 23
uniform highp vec4 unity_SHBr;
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
#line 27
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
#line 31
uniform highp vec4 _LightSplitsNear;
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
#line 35
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
#line 39
uniform highp mat4 _Object2World;
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
#line 43
uniform highp mat4 glstate_matrix_texture0;
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
#line 47
uniform highp mat4 glstate_matrix_projection;
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 51
uniform lowp vec4 unity_ColorSpaceGrey;
#line 77
#line 82
#line 87
#line 91
#line 96
#line 120
#line 137
#line 158
#line 166
#line 193
#line 206
#line 215
#line 220
#line 229
#line 234
#line 243
#line 260
#line 265
#line 291
#line 299
#line 307
#line 311
#line 325
uniform lowp vec4 _LightColor0;
uniform lowp vec4 _SpecColor;
#line 338
#line 346
#line 360
uniform highp vec4 _ShadowOffsets[4];
uniform lowp sampler2DShadow _ShadowMapTexture;
#line 393
uniform sampler2D _MainTex;
#line 417
#line 421
uniform highp float _Scale;
uniform sampler2D _CameraDepthTexture;
#line 437
uniform highp float _DepthFactor;
#line 280
highp float LinearEyeDepth( in highp float z ) {
    #line 282
    return (1.0 / ((_ZBufferParams.z * z) + _ZBufferParams.w));
}
#line 393
lowp float unitySampleShadow( in highp vec4 shadowCoord ) {
    lowp float shadow = xll_shadow2D( _ShadowMapTexture, shadowCoord.xyz.xyz);
    shadow = (_LightShadowData.x + (shadow * (1.0 - _LightShadowData.x)));
    #line 397
    return shadow;
}
#line 438
lowp vec4 frag( in v2f i ) {
    lowp vec4 col = texture( _MainTex, i.uv);
    #line 441
    highp float depth = LinearEyeDepth( float( textureProj( _CameraDepthTexture, i.scrPos).xyzw));
    highp float diff = xll_saturate_f((_DepthFactor * (depth - i.scrPos.w)));
    lowp float shadow = unitySampleShadow( i._ShadowCoord);
    lowp vec3 lighting = ((i.diff * shadow) + i.ambient);
    #line 445
    col.xyz *= lighting;
    return vec4( col.xyz, diff);
}
in highp vec2 xlv_TEXCOORD0;
in highp vec4 xlv_TEXCOORD2;
in lowp vec3 xlv_COLOR0;
in highp vec4 xlv_TEXCOORD1;
in lowp vec3 xlv_COLOR1;
void main() {
    lowp vec4 xl_retval;
    v2f xlt_i;
    xlt_i.pos = vec4(0.0);
    xlt_i.uv = vec2(xlv_TEXCOORD0);
    xlt_i._ShadowCoord = vec4(xlv_TEXCOORD2);
    xlt_i.diff = vec3(xlv_COLOR0);
    xlt_i.scrPos = vec4(xlv_TEXCOORD1);
    xlt_i.ambient = vec3(xlv_COLOR1);
    xl_retval = frag( xlt_i);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

}
Program "fp" {
// Fragment combos: 2
//   opengl - ALU: 9 to 10, TEX: 2 to 3
//   d3d9 - ALU: 8 to 8, TEX: 2 to 3
//   d3d11 - ALU: 7 to 8, TEX: 2 to 3, FLOW: 1 to 1
//   d3d11_9x - ALU: 7 to 8, TEX: 2 to 3, FLOW: 1 to 1
SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Vector 0 [_ZBufferParams]
Float 1 [_DepthFactor]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_CameraDepthTexture] 2D
"!!ARBfp1.0
# 9 ALU, 2 TEX
PARAM c[2] = { program.local[0..1] };
TEMP R0;
TEMP R1;
TXP R1.x, fragment.texcoord[1], texture[1], 2D;
TEX R0.xyz, fragment.texcoord[0], texture[0], 2D;
MAD R0.w, R1.x, c[0].z, c[0];
MOV R1.xyz, fragment.color.secondary;
RCP R0.w, R0.w;
ADD R1.xyz, fragment.color.primary, R1;
ADD R0.w, R0, -fragment.texcoord[1];
MUL result.color.xyz, R0, R1;
MUL_SAT result.color.w, R0, c[1].x;
END
# 9 instructions, 2 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Vector 0 [_ZBufferParams]
Float 1 [_DepthFactor]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_CameraDepthTexture] 2D
"ps_2_0
; 8 ALU, 2 TEX
dcl_2d s0
dcl_2d s1
dcl t0.xy
dcl v0.xyz
dcl t1
dcl v1.xyz
texldp r0, t1, s1
texld r1, t0, s0
mad r0.x, r0, c0.z, c0.w
rcp r0.x, r0.x
add r0.x, r0, -t1.w
mul_sat r0.w, r0.x, c1.x
mov_pp r2.xyz, v1
add_pp r0.xyz, v0, r2
mul_pp r0.xyz, r1, r0
mov_pp oC0, r0
"
}

SubProgram "d3d11 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
ConstBuffer "$Globals" 64 // 56 used size, 5 vars
Float 52 [_DepthFactor]
ConstBuffer "UnityPerCamera" 128 // 128 used size, 8 vars
Vector 112 [_ZBufferParams] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
SetTexture 0 [_MainTex] 2D 0
SetTexture 1 [_CameraDepthTexture] 2D 1
// 10 instructions, 2 temp regs, 0 temp arrays:
// ALU 7 float, 0 int, 0 uint
// TEX 2 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0
eefiecedenjnmofembphhkfhniabmfjmeifgaedjabaaaaaaoeacaaaaadaaaaaa
cmaaaaaanaaaaaaaaeabaaaaejfdeheojmaaaaaaafaaaaaaaiaaaaaaiaaaaaaa
aaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaaimaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaadadaaaajfaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaa
ahahaaaaimaaaaaaabaaaaaaaaaaaaaaadaaaaaaadaaaaaaapalaaaajfaaaaaa
abaaaaaaaaaaaaaaadaaaaaaaeaaaaaaahahaaaafdfgfpfaepfdejfeejepeoaa
feeffiedepepfceeaaedepemepfcaaklepfdeheocmaaaaaaabaaaaaaaiaaaaaa
caaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfegbhcghgf
heaaklklfdeieefcniabaaaaeaaaaaaahgaaaaaafjaaaaaeegiocaaaaaaaaaaa
aeaaaaaafjaaaaaeegiocaaaabaaaaaaaiaaaaaafkaaaaadaagabaaaaaaaaaaa
fkaaaaadaagabaaaabaaaaaafibiaaaeaahabaaaaaaaaaaaffffaaaafibiaaae
aahabaaaabaaaaaaffffaaaagcbaaaaddcbabaaaabaaaaaagcbaaaadhcbabaaa
acaaaaaagcbaaaadlcbabaaaadaaaaaagcbaaaadhcbabaaaaeaaaaaagfaaaaad
pccabaaaaaaaaaaagiaaaaacacaaaaaaaoaaaaahdcaabaaaaaaaaaaaegbabaaa
adaaaaaapgbpbaaaadaaaaaaefaaaaajpcaabaaaaaaaaaaaegaabaaaaaaaaaaa
eghobaaaabaaaaaaaagabaaaabaaaaaadcaaaaalbcaabaaaaaaaaaaackiacaaa
abaaaaaaahaaaaaaakaabaaaaaaaaaaadkiacaaaabaaaaaaahaaaaaaaoaaaaak
bcaabaaaaaaaaaaaaceaaaaaaaaaiadpaaaaiadpaaaaiadpaaaaiadpakaabaaa
aaaaaaaaaaaaaaaibcaabaaaaaaaaaaaakaabaaaaaaaaaaadkbabaiaebaaaaaa
adaaaaaadicaaaaiiccabaaaaaaaaaaaakaabaaaaaaaaaaabkiacaaaaaaaaaaa
adaaaaaaefaaaaajpcaabaaaaaaaaaaaegbabaaaabaaaaaaeghobaaaaaaaaaaa
aagabaaaaaaaaaaaaaaaaaahhcaabaaaabaaaaaaegbcbaaaacaaaaaaegbcbaaa
aeaaaaaadiaaaaahhccabaaaaaaaaaaaegacbaaaaaaaaaaaegacbaaaabaaaaaa
doaaaaab"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES"
}

SubProgram "flash " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
Vector 0 [_ZBufferParams]
Float 1 [_DepthFactor]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_CameraDepthTexture] 2D
"agal_ps
[bc]
aeaaaaaaaaaaapacabaaaaoeaeaaaaaaabaaaappaeaaaaaa div r0, v1, v1.w
ciaaaaaaaaaaapacaaaaaafeacaaaaaaabaaaaaaafaababb tex r0, r0.xyyy, s1 <2d wrap linear point>
ciaaaaaaabaaapacaaaaaaoeaeaaaaaaaaaaaaaaafaababb tex r1, v0, s0 <2d wrap linear point>
adaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaakkabaaaaaa mul r0.x, r0.x, c0.z
abaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaappabaaaaaa add r0.x, r0.x, c0.w
afaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaaaaaaaaaaaa rcp r0.x, r0.x
acaaaaaaaaaaabacaaaaaaaaacaaaaaaabaaaappaeaaaaaa sub r0.x, r0.x, v1.w
adaaaaaaaaaaaiacaaaaaaaaacaaaaaaabaaaaaaabaaaaaa mul r0.w, r0.x, c1.x
bgaaaaaaaaaaaiacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa sat r0.w, r0.w
aaaaaaaaacaaahacagaaaaoeaeaaaaaaaaaaaaaaaaaaaaaa mov r2.xyz, v6
abaaaaaaaaaaahacahaaaaoeaeaaaaaaacaaaakeacaaaaaa add r0.xyz, v7, r2.xyzz
adaaaaaaaaaaahacabaaaakeacaaaaaaaaaaaakeacaaaaaa mul r0.xyz, r1.xyzz, r0.xyzz
aaaaaaaaaaaaapadaaaaaaoeacaaaaaaaaaaaaaaaaaaaaaa mov o0, r0
"
}

SubProgram "d3d11_9x " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
ConstBuffer "$Globals" 64 // 56 used size, 5 vars
Float 52 [_DepthFactor]
ConstBuffer "UnityPerCamera" 128 // 128 used size, 8 vars
Vector 112 [_ZBufferParams] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
SetTexture 0 [_MainTex] 2D 0
SetTexture 1 [_CameraDepthTexture] 2D 1
// 10 instructions, 2 temp regs, 0 temp arrays:
// ALU 7 float, 0 int, 0 uint
// TEX 2 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0_level_9_1
eefiecedbgblmfbadocfllhacdbiombgggakkaajabaaaaaadiaeaaaaaeaaaaaa
daaaaaaaiaabaaaagaadaaaaaeaeaaaaebgpgodjeiabaaaaeiabaaaaaaacpppp
aeabaaaaeeaaaaaaacaacmaaaaaaeeaaaaaaeeaaacaaceaaaaaaeeaaaaaaaaaa
abababaaaaaaadaaabaaaaaaaaaaaaaaabaaahaaabaaabaaaaaaaaaaaaacpppp
bpaaaaacaaaaaaiaaaaaaplabpaaaaacaaaaaaiaabaachlabpaaaaacaaaaaaia
acaaaplabpaaaaacaaaaaaiaadaachlabpaaaaacaaaaaajaaaaiapkabpaaaaac
aaaaaajaabaiapkaagaaaaacaaaaaiiaacaapplaafaaaaadaaaaadiaaaaappia
acaaoelaecaaaaadaaaaapiaaaaaoeiaabaioekaecaaaaadabaacpiaaaaaoela
aaaioekaaeaaaaaeabaaaiiaabaakkkaaaaaaaiaabaappkaagaaaaacabaaaiia
abaappiaacaaaaadabaaaiiaabaappiaacaapplbafaaaaadaaaadiiaabaappia
aaaaffkaabaaaaacacaaahiaabaaoelaacaaaaadacaachiaacaaoeiaadaaoela
afaaaaadaaaachiaabaaoeiaacaaoeiaabaaaaacaaaicpiaaaaaoeiappppaaaa
fdeieefcniabaaaaeaaaaaaahgaaaaaafjaaaaaeegiocaaaaaaaaaaaaeaaaaaa
fjaaaaaeegiocaaaabaaaaaaaiaaaaaafkaaaaadaagabaaaaaaaaaaafkaaaaad
aagabaaaabaaaaaafibiaaaeaahabaaaaaaaaaaaffffaaaafibiaaaeaahabaaa
abaaaaaaffffaaaagcbaaaaddcbabaaaabaaaaaagcbaaaadhcbabaaaacaaaaaa
gcbaaaadlcbabaaaadaaaaaagcbaaaadhcbabaaaaeaaaaaagfaaaaadpccabaaa
aaaaaaaagiaaaaacacaaaaaaaoaaaaahdcaabaaaaaaaaaaaegbabaaaadaaaaaa
pgbpbaaaadaaaaaaefaaaaajpcaabaaaaaaaaaaaegaabaaaaaaaaaaaeghobaaa
abaaaaaaaagabaaaabaaaaaadcaaaaalbcaabaaaaaaaaaaackiacaaaabaaaaaa
ahaaaaaaakaabaaaaaaaaaaadkiacaaaabaaaaaaahaaaaaaaoaaaaakbcaabaaa
aaaaaaaaaceaaaaaaaaaiadpaaaaiadpaaaaiadpaaaaiadpakaabaaaaaaaaaaa
aaaaaaaibcaabaaaaaaaaaaaakaabaaaaaaaaaaadkbabaiaebaaaaaaadaaaaaa
dicaaaaiiccabaaaaaaaaaaaakaabaaaaaaaaaaabkiacaaaaaaaaaaaadaaaaaa
efaaaaajpcaabaaaaaaaaaaaegbabaaaabaaaaaaeghobaaaaaaaaaaaaagabaaa
aaaaaaaaaaaaaaahhcaabaaaabaaaaaaegbcbaaaacaaaaaaegbcbaaaaeaaaaaa
diaaaaahhccabaaaaaaaaaaaegacbaaaaaaaaaaaegacbaaaabaaaaaadoaaaaab
ejfdeheojmaaaaaaafaaaaaaaiaaaaaaiaaaaaaaaaaaaaaaabaaaaaaadaaaaaa
aaaaaaaaapaaaaaaimaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaadadaaaa
jfaaaaaaaaaaaaaaaaaaaaaaadaaaaaaacaaaaaaahahaaaaimaaaaaaabaaaaaa
aaaaaaaaadaaaaaaadaaaaaaapalaaaajfaaaaaaabaaaaaaaaaaaaaaadaaaaaa
aeaaaaaaahahaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfceeaaedepem
epfcaaklepfdeheocmaaaaaaabaaaaaaaiaaaaaacaaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaaaaaaaaaapaaaaaafdfgfpfegbhcghgfheaaklkl"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_OFF" }
"!!GLES3"
}

SubProgram "opengl " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Vector 0 [_ZBufferParams]
Float 1 [_DepthFactor]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_CameraDepthTexture] 2D
SetTexture 2 [_ShadowMapTexture] 2D
"!!ARBfp1.0
# 10 ALU, 3 TEX
PARAM c[2] = { program.local[0..1] };
TEMP R0;
TEMP R1;
TEMP R2;
TXP R1.x, fragment.texcoord[1], texture[1], 2D;
TEX R2.xyz, fragment.texcoord[0], texture[0], 2D;
TXP R0.x, fragment.texcoord[2], texture[2], 2D;
MAD R0.y, R1.x, c[0].z, c[0].w;
RCP R0.w, R0.y;
MOV R1.xyz, fragment.color.secondary;
MAD R0.xyz, fragment.color.primary, R0.x, R1;
ADD R0.w, R0, -fragment.texcoord[1];
MUL result.color.xyz, R2, R0;
MUL_SAT result.color.w, R0, c[1].x;
END
# 10 instructions, 3 R-regs
"
}

SubProgram "d3d9 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Vector 0 [_ZBufferParams]
Float 1 [_DepthFactor]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_CameraDepthTexture] 2D
SetTexture 2 [_ShadowMapTexture] 2D
"ps_2_0
; 8 ALU, 3 TEX
dcl_2d s0
dcl_2d s1
dcl_2d s2
dcl t0.xy
dcl t2
dcl v0.xyz
dcl t1
dcl v1.xyz
texldp r0, t1, s1
texld r1, t0, s0
texldp r2, t2, s2
mad r0.x, r0, c0.z, c0.w
rcp r0.x, r0.x
add r0.x, r0, -t1.w
mul_sat r0.w, r0.x, c1.x
mov_pp r0.xyz, v1
mad_pp r0.xyz, v0, r2.x, r0
mul_pp r0.xyz, r1, r0
mov_pp oC0, r0
"
}

SubProgram "d3d11 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
ConstBuffer "$Globals" 128 // 120 used size, 6 vars
Float 116 [_DepthFactor]
ConstBuffer "UnityPerCamera" 128 // 128 used size, 8 vars
Vector 112 [_ZBufferParams] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
SetTexture 0 [_MainTex] 2D 1
SetTexture 1 [_CameraDepthTexture] 2D 2
SetTexture 2 [_ShadowMapTexture] 2D 0
// 12 instructions, 2 temp regs, 0 temp arrays:
// ALU 8 float, 0 int, 0 uint
// TEX 3 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0
eefiecedhdkimhfmbcihheicpdimegcnbcgpjijmabaaaaaagmadaaaaadaaaaaa
cmaaaaaaoiaaaaaabmabaaaaejfdeheoleaaaaaaagaaaaaaaiaaaaaajiaaaaaa
aaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaakeaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaadadaaaakeaaaaaaacaaaaaaaaaaaaaaadaaaaaaacaaaaaa
apalaaaaknaaaaaaaaaaaaaaaaaaaaaaadaaaaaaadaaaaaaahahaaaakeaaaaaa
abaaaaaaaaaaaaaaadaaaaaaaeaaaaaaapalaaaaknaaaaaaabaaaaaaaaaaaaaa
adaaaaaaafaaaaaaahahaaaafdfgfpfaepfdejfeejepeoaafeeffiedepepfcee
aaedepemepfcaaklepfdeheocmaaaaaaabaaaaaaaiaaaaaacaaaaaaaaaaaaaaa
aaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfegbhcghgfheaaklklfdeieefc
eiacaaaaeaaaaaaajcaaaaaafjaaaaaeegiocaaaaaaaaaaaaiaaaaaafjaaaaae
egiocaaaabaaaaaaaiaaaaaafkaaaaadaagabaaaaaaaaaaafkaaaaadaagabaaa
abaaaaaafkaaaaadaagabaaaacaaaaaafibiaaaeaahabaaaaaaaaaaaffffaaaa
fibiaaaeaahabaaaabaaaaaaffffaaaafibiaaaeaahabaaaacaaaaaaffffaaaa
gcbaaaaddcbabaaaabaaaaaagcbaaaadlcbabaaaacaaaaaagcbaaaadhcbabaaa
adaaaaaagcbaaaadlcbabaaaaeaaaaaagcbaaaadhcbabaaaafaaaaaagfaaaaad
pccabaaaaaaaaaaagiaaaaacacaaaaaaaoaaaaahdcaabaaaaaaaaaaaegbabaaa
aeaaaaaapgbpbaaaaeaaaaaaefaaaaajpcaabaaaaaaaaaaaegaabaaaaaaaaaaa
eghobaaaabaaaaaaaagabaaaacaaaaaadcaaaaalbcaabaaaaaaaaaaackiacaaa
abaaaaaaahaaaaaaakaabaaaaaaaaaaadkiacaaaabaaaaaaahaaaaaaaoaaaaak
bcaabaaaaaaaaaaaaceaaaaaaaaaiadpaaaaiadpaaaaiadpaaaaiadpakaabaaa
aaaaaaaaaaaaaaaibcaabaaaaaaaaaaaakaabaaaaaaaaaaadkbabaiaebaaaaaa
aeaaaaaadicaaaaiiccabaaaaaaaaaaaakaabaaaaaaaaaaabkiacaaaaaaaaaaa
ahaaaaaaaoaaaaahdcaabaaaaaaaaaaaegbabaaaacaaaaaapgbpbaaaacaaaaaa
efaaaaajpcaabaaaaaaaaaaaegaabaaaaaaaaaaaeghobaaaacaaaaaaaagabaaa
aaaaaaaadcaaaaajhcaabaaaaaaaaaaaegbcbaaaadaaaaaaagaabaaaaaaaaaaa
egbcbaaaafaaaaaaefaaaaajpcaabaaaabaaaaaaegbabaaaabaaaaaaeghobaaa
aaaaaaaaaagabaaaabaaaaaadiaaaaahhccabaaaaaaaaaaaegacbaaaaaaaaaaa
egacbaaaabaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES"
}

SubProgram "flash " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
Vector 0 [_ZBufferParams]
Float 1 [_DepthFactor]
SetTexture 0 [_MainTex] 2D
SetTexture 1 [_CameraDepthTexture] 2D
SetTexture 2 [_ShadowMapTexture] 2D
"agal_ps
[bc]
aeaaaaaaaaaaapacabaaaaoeaeaaaaaaabaaaappaeaaaaaa div r0, v1, v1.w
ciaaaaaaaaaaapacaaaaaafeacaaaaaaabaaaaaaafaababb tex r0, r0.xyyy, s1 <2d wrap linear point>
ciaaaaaaabaaapacaaaaaaoeaeaaaaaaaaaaaaaaafaababb tex r1, v0, s0 <2d wrap linear point>
aeaaaaaaacaaapacacaaaaoeaeaaaaaaacaaaappaeaaaaaa div r2, v2, v2.w
ciaaaaaaacaaapacacaaaafeacaaaaaaacaaaaaaafaababb tex r2, r2.xyyy, s2 <2d wrap linear point>
adaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaakkabaaaaaa mul r0.x, r0.x, c0.z
abaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaappabaaaaaa add r0.x, r0.x, c0.w
afaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaaaaaaaaaaaa rcp r0.x, r0.x
acaaaaaaaaaaabacaaaaaaaaacaaaaaaabaaaappaeaaaaaa sub r0.x, r0.x, v1.w
adaaaaaaaaaaaiacaaaaaaaaacaaaaaaabaaaaaaabaaaaaa mul r0.w, r0.x, c1.x
bgaaaaaaaaaaaiacaaaaaappacaaaaaaaaaaaaaaaaaaaaaa sat r0.w, r0.w
aaaaaaaaaaaaahacagaaaaoeaeaaaaaaaaaaaaaaaaaaaaaa mov r0.xyz, v6
adaaaaaaacaaahacahaaaaoeaeaaaaaaacaaaaaaacaaaaaa mul r2.xyz, v7, r2.x
abaaaaaaaaaaahacacaaaakeacaaaaaaaaaaaakeacaaaaaa add r0.xyz, r2.xyzz, r0.xyzz
adaaaaaaaaaaahacabaaaakeacaaaaaaaaaaaakeacaaaaaa mul r0.xyz, r1.xyzz, r0.xyzz
aaaaaaaaaaaaapadaaaaaaoeacaaaaaaaaaaaaaaaaaaaaaa mov o0, r0
"
}

SubProgram "d3d11_9x " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
ConstBuffer "$Globals" 128 // 120 used size, 6 vars
Float 116 [_DepthFactor]
ConstBuffer "UnityPerCamera" 128 // 128 used size, 8 vars
Vector 112 [_ZBufferParams] 4
BindCB "$Globals" 0
BindCB "UnityPerCamera" 1
SetTexture 0 [_MainTex] 2D 1
SetTexture 1 [_CameraDepthTexture] 2D 2
SetTexture 2 [_ShadowMapTexture] 2D 0
// 12 instructions, 2 temp regs, 0 temp arrays:
// ALU 8 float, 0 int, 0 uint
// TEX 3 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0_level_9_1
eefiecedcmnepliobnnmmilpepcehebmbpnoahmpabaaaaaaamafaaaaaeaaaaaa
daaaaaaammabaaaabmaeaaaaniaeaaaaebgpgodjjeabaaaajeabaaaaaaacpppp
emabaaaaeiaaaaaaacaadaaaaaaaeiaaaaaaeiaaadaaceaaaaaaeiaaacaaaaaa
aaababaaabacacaaaaaaahaaabaaaaaaaaaaaaaaabaaahaaabaaabaaaaaaaaaa
aaacppppbpaaaaacaaaaaaiaaaaaaplabpaaaaacaaaaaaiaabaaaplabpaaaaac
aaaaaaiaacaachlabpaaaaacaaaaaaiaadaaaplabpaaaaacaaaaaaiaaeaachla
bpaaaaacaaaaaajaaaaiapkabpaaaaacaaaaaajaabaiapkabpaaaaacaaaaaaja
acaiapkaagaaaaacaaaaaiiaadaapplaafaaaaadaaaaadiaaaaappiaadaaoela
agaaaaacaaaaaeiaabaapplaafaaaaadabaaadiaaaaakkiaabaaoelaecaaaaad
aaaaapiaaaaaoeiaacaioekaecaaaaadabaacpiaabaaoeiaaaaioekaecaaaaad
acaacpiaaaaaoelaabaioekaaeaaaaaeacaaaiiaabaakkkaaaaaaaiaabaappka
agaaaaacacaaaiiaacaappiaacaaaaadacaaaiiaacaappiaadaapplbafaaaaad
aaaadiiaacaappiaaaaaffkaabaaaaacadaaahiaacaaoelaaeaaaaaeabaachia
adaaoeiaabaaaaiaaeaaoelaafaaaaadaaaachiaabaaoeiaacaaoeiaabaaaaac
aaaicpiaaaaaoeiappppaaaafdeieefceiacaaaaeaaaaaaajcaaaaaafjaaaaae
egiocaaaaaaaaaaaaiaaaaaafjaaaaaeegiocaaaabaaaaaaaiaaaaaafkaaaaad
aagabaaaaaaaaaaafkaaaaadaagabaaaabaaaaaafkaaaaadaagabaaaacaaaaaa
fibiaaaeaahabaaaaaaaaaaaffffaaaafibiaaaeaahabaaaabaaaaaaffffaaaa
fibiaaaeaahabaaaacaaaaaaffffaaaagcbaaaaddcbabaaaabaaaaaagcbaaaad
lcbabaaaacaaaaaagcbaaaadhcbabaaaadaaaaaagcbaaaadlcbabaaaaeaaaaaa
gcbaaaadhcbabaaaafaaaaaagfaaaaadpccabaaaaaaaaaaagiaaaaacacaaaaaa
aoaaaaahdcaabaaaaaaaaaaaegbabaaaaeaaaaaapgbpbaaaaeaaaaaaefaaaaaj
pcaabaaaaaaaaaaaegaabaaaaaaaaaaaeghobaaaabaaaaaaaagabaaaacaaaaaa
dcaaaaalbcaabaaaaaaaaaaackiacaaaabaaaaaaahaaaaaaakaabaaaaaaaaaaa
dkiacaaaabaaaaaaahaaaaaaaoaaaaakbcaabaaaaaaaaaaaaceaaaaaaaaaiadp
aaaaiadpaaaaiadpaaaaiadpakaabaaaaaaaaaaaaaaaaaaibcaabaaaaaaaaaaa
akaabaaaaaaaaaaadkbabaiaebaaaaaaaeaaaaaadicaaaaiiccabaaaaaaaaaaa
akaabaaaaaaaaaaabkiacaaaaaaaaaaaahaaaaaaaoaaaaahdcaabaaaaaaaaaaa
egbabaaaacaaaaaapgbpbaaaacaaaaaaefaaaaajpcaabaaaaaaaaaaaegaabaaa
aaaaaaaaeghobaaaacaaaaaaaagabaaaaaaaaaaadcaaaaajhcaabaaaaaaaaaaa
egbcbaaaadaaaaaaagaabaaaaaaaaaaaegbcbaaaafaaaaaaefaaaaajpcaabaaa
abaaaaaaegbabaaaabaaaaaaeghobaaaaaaaaaaaaagabaaaabaaaaaadiaaaaah
hccabaaaaaaaaaaaegacbaaaaaaaaaaaegacbaaaabaaaaaadoaaaaabejfdeheo
leaaaaaaagaaaaaaaiaaaaaajiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaakeaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaadadaaaakeaaaaaa
acaaaaaaaaaaaaaaadaaaaaaacaaaaaaapalaaaaknaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaadaaaaaaahahaaaakeaaaaaaabaaaaaaaaaaaaaaadaaaaaaaeaaaaaa
apalaaaaknaaaaaaabaaaaaaaaaaaaaaadaaaaaaafaaaaaaahahaaaafdfgfpfa
epfdejfeejepeoaafeeffiedepepfceeaaedepemepfcaaklepfdeheocmaaaaaa
abaaaaaaaiaaaaaacaaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapaaaaaa
fdfgfpfegbhcghgfheaaklkl"
}

SubProgram "gles3 " {
Keywords { "DIRECTIONAL" "LIGHTMAP_OFF" "DIRLIGHTMAP_OFF" "SHADOWS_SCREEN" }
"!!GLES3"
}

}

#LINE 90

        }
	} 
	FallBack "Diffuse"
}
