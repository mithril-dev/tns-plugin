﻿using MithrilAPI.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TNSPlugin.Shaders
{
    public class ChunkShader : iShader
    {
        public bool ApplyShader()
        {
            var shader = ShaderHandler.LoadShader("TerrainShader");
            if (shader == string.Empty) return false;

            Material mat = new Material(shader);

            mat.mainTexture = ChunkManager.getInstance().materials[0].mainTexture;
            ChunkManager.getInstance().materials[0] = mat;

            ChunkManager.getInstance().materials[0].SetTexture("_MainTex", TextureManager.getInstance().mainTexture);
            ChunkManager.getInstance().materials[0].SetTextureScale("_MainTex", new Vector2(1, 1));
            ChunkManager.getInstance().materials[0].SetTextureOffset("_MainTex", new Vector2(0, 0));

            ChunkManager.getInstance().materials[0].SetTexture("_ShdeTex", TextureManager.getInstance().mainTexture);
            ChunkManager.getInstance().materials[0].SetTextureScale("_ShdeTex", new Vector2(1, 1));
            ChunkManager.getInstance().materials[0].SetTextureOffset("_ShdeTex", new Vector2(0, 0));

            return true;
        }
    }
}
