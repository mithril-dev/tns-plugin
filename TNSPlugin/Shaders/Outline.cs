﻿//
//  Outline.cs
//  QuickOutline
//
//  Created by Chris Nolet on 3/30/18.
//  Copyright © 2018 Chris Nolet. All rights reserved.
//

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TNSPlugin.Shaders
{
	public class Outline : MonoBehaviour
	{
		private static HashSet<Mesh> registeredMeshes = new HashSet<Mesh>();

		public enum Mode
		{
			OutlineAll,
			OutlineVisible,
			OutlineHidden,
			OutlineAndSilhouette,
			SilhouetteOnly
		}

		public Mode OutlineMode
		{
			get { return outlineMode; }
			set
			{
				outlineMode = value;
				needsUpdate = true;
			}
		}

		public Color OutlineColor
		{
			get { return outlineColor; }
			set
			{
				outlineColor = value;
				needsUpdate = true;
			}
		}

		public float OutlineWidth
		{
			get { return outlineWidth; }
			set
			{
				outlineWidth = value;
				needsUpdate = true;
			}
		}

		[Serializable]
		private class ListVector3
		{
			public List<Vector3> data;
		}

		[SerializeField]
		private Mode outlineMode = Mode.OutlineAll;

		[SerializeField]
		private Color outlineColor = new Color(255, 255, 0, 0);

		[SerializeField, Range(0f, 2f)]
		private float outlineWidth = 0.02f;

		private bool precomputeOutline = true;

		[SerializeField, HideInInspector]
		private List<Mesh> bakeKeys = new List<Mesh>();

		[SerializeField, HideInInspector]
		private List<ListVector3> bakeValues = new List<ListVector3>();

		private Renderer[] renderers;
		//public Material outlineMaskMaterial;
		public Material outlineFillMaterial;

		private bool needsUpdate;

		private ControlPlayer player;

		private APlayableEntity entity;

		public static Material SightRangeShader;

		void Awake()
		{

			// Cache renderers
			renderers = GetComponentsInChildren<Renderer>();

			// Instantiate outline materials
			outlineFillMaterial = (Material)Instantiate(outlineFillMaterial);

			outlineFillMaterial.name = "Outline (Instance)";

			// Retrieve or generate smooth normals
			LoadSmoothNormals();

			// Apply material properties immediately
			needsUpdate = true;

			player = GUIManager.getInstance().controllerObj.GetComponent<ControlPlayer>();
		}

		void OnEnable()
		{
			foreach (var renderer in renderers)
			{
				// Append outline shaders
				var materials = renderer.sharedMaterials.ToList();

				materials.Add(outlineFillMaterial);

				renderer.materials = materials.ToArray();
			}
		}

		void OnValidate()
		{
			// Update material properties
			needsUpdate = true;

			// Clear cache when baking is disabled or corrupted
			if (!precomputeOutline && bakeKeys.Count != 0 || bakeKeys.Count != bakeValues.Count)
			{
				bakeKeys.Clear();
				bakeValues.Clear();
			}

			// Generate smooth normals when baking is enabled
			if (precomputeOutline && bakeKeys.Count == 0)
			{
				Bake();
			}
		}

		void Update()
		{
			if (needsUpdate)
			{
				needsUpdate = false;

				UpdateMaterialProperties();
			}

			if (entity == null)
            {
				entity = this.transform.root.GetComponent<APlayableEntity>();
			}

			// Add highlight to groups.
			//if (GroupManager.selectedUnits.Contains(entity) || (player.selectedObject is APlayableEntity && this.transform.root.transform == player.selectedObject.transform.root.transform))
			if ((player.selectedObject is APlayableEntity && this.transform.root.transform == player.selectedObject.transform.root.transform))
			{
				OutlineColor = new Color(OutlineColor.r, OutlineColor.g, OutlineColor.b, 1);
				player.entityHighlight.renderer.material.color = new Color(0, 0, 0, 0);
			}
			else
			{
				OutlineColor = new Color(OutlineColor.r, OutlineColor.g, OutlineColor.b, 0);
			}
        }

		void OnDisable()
		{
			foreach (var renderer in renderers)
			{

				// Remove outline shaders
				var materials = renderer.sharedMaterials.ToList();

				materials.Remove(outlineFillMaterial);

				renderer.materials = materials.ToArray();
			}
		}

		void OnDestroy()
		{
			// Destroy material instances
			Destroy(outlineFillMaterial);
		}

		void Bake()
		{
			// Generate smooth normals for each mesh
			var bakedMeshes = new HashSet<Mesh>();

			foreach (var meshFilter in GetComponentsInChildren<MeshFilter>())
			{

				// Skip duplicates
				if (!bakedMeshes.Add(meshFilter.sharedMesh))
				{
					continue;
				}

				// Serialize smooth normals
				var smoothNormals = SmoothNormals(meshFilter.sharedMesh);

				bakeKeys.Add(meshFilter.sharedMesh);
				bakeValues.Add(new ListVector3() { data = smoothNormals });
			}
		}

		void LoadSmoothNormals()
		{
			// Retrieve or generate smooth normals
			foreach (var meshFilter in GetComponentsInChildren<MeshFilter>())
			{

				// Skip if smooth normals have already been adopted
				if (!registeredMeshes.Add(meshFilter.sharedMesh))
				{
					continue;
				}

				// Retrieve or generate smooth normals
				var index = bakeKeys.IndexOf(meshFilter.sharedMesh);
				var smoothNormals = (index >= 0) ? bakeValues[index].data : SmoothNormals(meshFilter.sharedMesh);

				List<Vector4> smoothNrms = new List<Vector4>();

				foreach (Vector3 nrms in smoothNormals)
				{
					smoothNrms.Add(new Vector4(nrms.x, nrms.y, nrms.z, 0));
				}

				meshFilter.sharedMesh.tangents = smoothNrms.ToArray();

				// Combine submeshes
				var renderer = meshFilter.GetComponent<Renderer>();

				if (renderer != null)
				{
					CombineSubmeshes(meshFilter.sharedMesh, renderer.sharedMaterials);
				}
			}

			// Clear UV3 on skinned mesh renderers
			foreach (var skinnedMeshRenderer in GetComponentsInChildren<SkinnedMeshRenderer>())
			{

				// Skip if UV3 has already been reset
				if (!registeredMeshes.Add(skinnedMeshRenderer.sharedMesh))
				{
					continue;
				}

				// Clear UV3
				skinnedMeshRenderer.sharedMesh.tangents = new Vector4[skinnedMeshRenderer.sharedMesh.vertexCount];

				var index = bakeKeys.IndexOf(skinnedMeshRenderer.sharedMesh);
				var smoothNormals = (index >= 0) ? bakeValues[index].data : SmoothNormals(skinnedMeshRenderer.sharedMesh);

				// Combine submeshes
				CombineSubmeshes(skinnedMeshRenderer.sharedMesh, skinnedMeshRenderer.sharedMaterials);

				List<Vector4> smoothNrms = new List<Vector4>();

				foreach (Vector3 nrms in smoothNormals)
				{
					smoothNrms.Add(new Vector4(nrms.x, nrms.y, nrms.z, 0));
				}

				skinnedMeshRenderer.sharedMesh.tangents = smoothNrms.ToArray();
			}
		}

		List<Vector3> SmoothNormals(Mesh mesh)
		{

			// Group vertices by location
			var groups = mesh.vertices.Select((vertex, index) => new KeyValuePair<Vector3, int>(vertex, index)).GroupBy(pair => pair.Key);

			// Copy normals to a new list
			var smoothNormals = new List<Vector3>(mesh.normals);

			// Average normals for grouped vertices
			foreach (var group in groups)
			{

				// Skip single vertices
				if (group.Count() == 1)
				{
					continue;
				}

				// Calculate the average normal
				var smoothNormal = Vector3.zero;

				foreach (var pair in group)
				{
					smoothNormal += smoothNormals[pair.Value];
				}

				smoothNormal.Normalize();

				// Assign smooth normal to each vertex
				foreach (var pair in group)
				{
					smoothNormals[pair.Value] = smoothNormal;
				}
			}

			return smoothNormals;
		}

		void CombineSubmeshes(Mesh mesh, Material[] materials)
		{

			// Skip meshes with a single submesh
			if (mesh.subMeshCount == 1)
			{
				return;
			}

			// Skip if submesh count exceeds material count
			if (mesh.subMeshCount > materials.Length)
			{
				return;
			}

			// Append combined submesh
			mesh.subMeshCount++;
			mesh.SetTriangles(mesh.triangles, mesh.subMeshCount - 1);
		}

		void UpdateMaterialProperties()
		{

			// Apply properties according to mode
			outlineFillMaterial.SetColor("_OutlineColor", outlineColor);

			switch (outlineMode)
			{
				case Mode.OutlineAll:
					outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
					outlineFillMaterial.SetFloat("_OutlineWidth", outlineWidth);
					break;

				case Mode.OutlineVisible:
					outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
					outlineFillMaterial.SetFloat("_OutlineWidth", outlineWidth);
					break;

				case Mode.OutlineHidden:
					outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Greater);
					outlineFillMaterial.SetFloat("_OutlineWidth", outlineWidth);
					break;

				case Mode.OutlineAndSilhouette:
					outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
					outlineFillMaterial.SetFloat("_OutlineWidth", outlineWidth);
					break;

				case Mode.SilhouetteOnly:
					outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Greater);
					outlineFillMaterial.SetFloat("_OutlineWidth", 0f);
					break;
			}
		}
	}
}