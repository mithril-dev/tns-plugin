﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TNSPlugin.Shaders
{
    public class OutlineShader : iShader
    {
        public bool ApplyShader()
        {
            string outlineShader = ShaderHandler.LoadShader("OutlineShader");
            string settlerShader = ShaderHandler.LoadShader("SettlerShader");

            if (outlineShader == string.Empty || settlerShader == string.Empty)
                return false;

            Material OutlineMat = new Material(outlineShader);
            Material SettlerShader = new Material(settlerShader);

            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanBuilderModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanBuilderModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanAdventurerModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanAdventurerModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanArcherModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanArcherModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanBlacksmithModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanBlacksmithModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanCarpenterModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanCarpenterModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanEngineerModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanEngineerModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanFarmerModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanFarmerModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanFishermanModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanFishermanModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanForagerModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanForagerModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanHerderModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanHerderModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanInfantryModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanInfantryModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanMinerModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanMinerModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanStoneMasonModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanStoneMasonModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanTailorModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanTailorModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanTraderModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanTraderModel_M);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanWoodChopperModel_F);
            ApplyShaderToChilderen(OutlineMat, SettlerShader, AssetManager.getInstance().humanWoodChopperModel_M);

            return true;
        }

        private static void ApplyShaderToChilderen(Material Outline, Material Settler, Transform transform)
        {
            Outline outline = transform.gameObject.AddComponent<Outline>();
            outline.outlineFillMaterial = Outline;

            for (int i = 0; i < transform.childCount; i++)
            {
                var item = transform.GetChild(i);

                SkinnedMeshRenderer renderer = item.GetComponent<SkinnedMeshRenderer>();

                if (renderer != null)
                {
                    Settler.mainTexture = renderer.material.mainTexture;
                    renderer.material = Settler;
                }
            }
        }
    }
}
