﻿using System;
using System.IO;
using System.Reflection;
using MithrilAPI.Debug;

namespace TNSPlugin.Shaders
{
    public static class ShaderHandler
    {
        // Move to data class.
        public static TreeShader TreeShader = new TreeShader();
        public static SkyboxShader SkyboxShader = new SkyboxShader();
        public static OutlineShader OutlineShader = new OutlineShader();
        public static WaterShader WaterShader = new WaterShader();
        public static ChunkShader ChunkShader = new ChunkShader();

        public static void ApplyShaders()
        {
            TreeShader.ApplyShader();
            SkyboxShader.ApplyShader();
            OutlineShader.ApplyShader();
            WaterShader.ApplyShader();
            ChunkShader.ApplyShader();
        }


        /// <summary>
        /// Loads the shader resource into a string
        /// </summary>
        /// <param name="resourceName"></param>
        /// <returns></returns>
        public static string LoadShader(string resourceName)
        {
            try
            {
                Assembly assembly = Assembly.GetExecutingAssembly();

                using (Stream stream = assembly.GetManifestResourceStream($"TNSPlugin.Resources.Shaders.{resourceName}.shader"))
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }

            }
            catch (Exception ex)
            {
                DebugConsole.LogError($"{ex.GetType()} | {ex.Message}");
                foreach (string line in ex.StackTrace.Split('\n'))
                {
                    DebugConsole.LogError($"    {line}");
                }

                return string.Empty;
            }
        }
    }
}
