﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TNSPlugin.Shaders
{
    public class SkyboxShader : iShader
    {
        public bool ApplyShader()
        {
            string shader = ShaderHandler.LoadShader("SkyboxShader");
            if (shader == string.Empty) return false;

            DisableOldSkybox();

            Material SkyMaterial = new Material(shader);
            SkyMaterial.name = "Custom Sky Material";

            Texture2D clouds = TextureHandler.ConvertToTexture2D("noise_swirl_tex");
            clouds.filterMode = FilterMode.Point;

            Texture2D stars = TextureHandler.ConvertToTexture2D("star_tex");
            stars.filterMode = FilterMode.Point;

            SkyMaterial.SetTexture("_CloudsTexture", clouds);
            SkyMaterial.SetTexture("_Stars", stars);

            SkyController.SkyMaterial = SkyMaterial;

            Transform oldSky = WorldManager.getInstance().controllerObj.GetComponent<ControlPlayer>().sky;
            Transform sunLight = oldSky.GetChild(2).GetChild(1);
            sunLight.gameObject.AddComponent<SkyController>();

            return true;
        }

        public static void DisableOldSkybox()
        {
            Transform oldSky = WorldManager.getInstance().controllerObj.GetComponent<ControlPlayer>().sky;
            oldSky.GetChild(0).renderer.enabled = false;

            Transform sunLight = oldSky.GetChild(2).GetChild(1);
            Transform moonLight = oldSky.GetChild(2).GetChild(0);

            oldSky.GetChild(1).gameObject.SetActive(false);
            sunLight.GetChild(0).gameObject.SetActive(false);
            moonLight.GetChild(0).gameObject.SetActive(false);
        }
    }
}
