﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TNSPlugin.Shaders
{
    public class TreeShader : iShader
    {
        public bool ApplyShader()
        {
            string shader = ShaderHandler.LoadShader("TreeShader");
            if (shader == string.Empty) return false;

            Material TreeMat = new Material(shader);
            Material ChunkMat = new Material(shader);

            Transform tree = AssetManager.getInstance().tree;
            TreeMat.SetTexture("_MainTex", AssetManager.getInstance().tree.GetChild(2).GetComponent<MeshRenderer>().sharedMaterial.mainTexture);
            ChunkMat.SetTexture("_MainTex", TextureManager.getInstance().mainTexture);

            // Apply material with custom shader to trees
            for (int i = 0; i < tree.childCount; i++)
            {
                MeshRenderer renderer = tree.transform.GetChild(i).GetComponent<MeshRenderer>();
                if (renderer == null) continue;
                if (renderer.name.Contains("Stump")) continue;

                renderer.sharedMaterial = TreeMat;
            }

            AssetManager.getInstance().shrub.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial = TreeMat;
            AssetManager.getInstance().shrub.GetChild(1).GetComponent<MeshRenderer>().sharedMaterial = TreeMat;
            AssetManager.getInstance().shrub.GetChild(2).GetComponent<MeshRenderer>().sharedMaterial = TreeMat;

            ChunkManager.getInstance().materials[2].SetTexture("_MainTex", TextureManager.getInstance().mainTexture);

            return true;
        }
    }
}
