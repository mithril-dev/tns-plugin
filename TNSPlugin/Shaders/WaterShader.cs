﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TNSPlugin.Shaders
{
    public class WaterShader : iShader
    {
        public bool ApplyShader()
        {
            string rangeShader = ShaderHandler.LoadShader("SightRangeShader");
            string waterShader = ShaderHandler.LoadShader("WaterShader");

            if (rangeShader == string.Empty || waterShader == string.Empty) return false;

            // SightRange
            Material SightRangeShader = new Material(rangeShader);
            //GUIManager.getInstance().revealMapCube.renderer.material = SightRangeShader;
            AssetManager.getInstance().sightRangeMat = SightRangeShader;
            AssetManager.getInstance().highResBox.renderer.material = SightRangeShader;

            Outline.SightRangeShader = SightRangeShader;

            // Water
            Material WaterShader = new Material(waterShader);
            WaterShader.mainTexture = ChunkManager.getInstance().materials[1].mainTexture;
            WaterManager.getInstance().waterMaterial = WaterShader;
            ChunkManager.getInstance().materials[1] = WaterShader;

            return true;
        }
    }
}
