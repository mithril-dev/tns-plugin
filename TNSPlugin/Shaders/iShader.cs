﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TNSPlugin.Shaders
{
    public interface iShader
    {
        bool ApplyShader();
    }
}
