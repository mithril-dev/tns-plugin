﻿using MithrilAPI.Debug;
using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using Timber_and_Stone.Blocks;
using UnityEngine;

namespace TNSPlugin.Sounds
{
    public class AmbientSounds : MonoBehaviour
    {
        private ControlPlayer player;
        private GUIManager manager;
        private TimeManager time;
        private Transform cameraObject;

        private Dictionary<string, AudioSource> audioSources;
        private Dictionary<string, AudioClip> audioClips;

        public void Start()
        {
            time = TimeManager.getInstance();
            manager = GUIManager.getInstance();
            player = manager.controllerObj.GetComponent<ControlPlayer>();
            cameraObject = player.currentCamera.transform;

            audioSources = new Dictionary<string, AudioSource>();
            audioClips = new Dictionary<string, AudioClip>();

            audioSources.Add("day", player.currentCamera.gameObject.AddComponent<AudioSource>());
            audioSources.Add("height", player.currentCamera.gameObject.AddComponent<AudioSource>());

            // Make a sounds manager for this

            audioClips.Add("day", AudioClipResource.GetAudioClip("dayNoise0"));
            audioClips.Add("night", AudioClipResource.GetAudioClip("nightNoise0"));
            audioClips.Add("wind", AudioClipResource.GetAudioClip("wind0"));

            audioSources["height"].clip = audioClips["wind"];
        }

        void Update()
        {
            if (!manager.inGame) return;
            if (!audioSources["day"].isPlaying) audioSources["day"].Play();
            if (!audioSources["height"].isPlaying) audioSources["height"].Play();

            var elevation = Mathf.Clamp(Mathf.Clamp((-5 + cameraObject.position.y) / 100, 0, 1) / 8, 0, Options.getInstance().soundfxVolume);

            audioSources["day"].clip = (time.hour > 4 && time.hour < 18) ? audioClips["day"] : audioClips["night"];
            audioSources["day"].volume = Mathf.Clamp(Mathf.Clamp((20 - cameraObject.position.y) / 100, 0, 1), 0, Options.getInstance().soundfxVolume);

            audioSources["height"].volume = elevation;
        }
    }
}