﻿using MithrilAPI.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Timber_and_Stone.API.Event;
using Timber_and_Stone.Event;
using TNSPlugin.Utilities;
using UnityEngine;
using EventHandler = Timber_and_Stone.API.Event.EventHandler;

namespace TNSPlugin
{
    public class ButtonSounds : MonoBehaviour, IEventListener
    {
        AudioClip button;
        Transform camera;

        public void Start()
        {
            EventManager.getInstance().Register(this);

            camera = GUIManager.getInstance().controllerObj.GetComponent<ControlPlayer>().currentCamera.transform;

            button = AudioClipResource.GetAudioClip("Button");
        }

        [EventHandler(Priority.Low)]
        public void OnButtonPress(EventButtonPressed evt)
        {
            // Play Sound
            camera.audio.PlayOneShot(button);
        }
    }
}
