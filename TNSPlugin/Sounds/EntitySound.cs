﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TNSPlugin.Sounds
{
    public class EntitySound : MonoBehaviour
    {
        public ALivingEntity entity;
        private System.Random rand;

        private float time = 0.0f;
        private float interpolationPeriod;

        public void Start()
        {
            rand = new System.Random(entity.GetInstanceID());
            interpolationPeriod = 30;
        }

        public void Update()
        {
            if (!GUIManager.getInstance().inGame) return;

            if (Time.deltaTime == 0) entity.audio.Stop();

            time += Time.deltaTime;

            if (time >= interpolationPeriod && rand.Next(0, 200) == 0)
            {
                switch (entity.unitName)
                {
                    case "Sheep":
                            entity.audio.clip = EntitySoundManager.getInstance().SheepClips[rand.Next(0, EntitySoundManager.getInstance().SheepClips.Length)];
                            entity.audio.Play();
                        break;
                    case "Boar":
                            entity.audio.clip = EntitySoundManager.getInstance().BoarClips[rand.Next(0, EntitySoundManager.getInstance().BoarClips.Length)];
                            entity.audio.Play();
                        break;
                    case "Chicken":
                            entity.audio.clip = EntitySoundManager.getInstance().ChickClips[rand.Next(0, EntitySoundManager.getInstance().ChickClips.Length)];
                            entity.audio.Play();
                        break;
                }

                time = 0.0f;
            }
        }
    }
}
