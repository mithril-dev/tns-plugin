﻿using MithrilAPI.Debug;
using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using Timber_and_Stone.API.Event;
using Timber_and_Stone.Event;
using UnityEngine;

namespace TNSPlugin.Sounds
{
    public class EntitySoundManager : AManager<EntitySoundManager>, IEventListener
    {
        public AudioClip[] BoarClips;
        public AudioClip[] SheepClips;
        public AudioClip[] ChickClips;
        public AudioClip[] WalkingClips;

        private float time = 0.0f;
        public float interpolationPeriod = 0.8f;

        public void Start()
        {
            EventManager.getInstance().Register(this);
            WalkingClips = new AudioClip[2]
            {
                AudioClipResource.GetAudioClip("walk0", true),
                AudioClipResource.GetAudioClip("walk1", true)
            };

            BoarClips = new AudioClip[3]
            {
                AudioClipResource.GetAudioClip("oink0", true),
                AudioClipResource.GetAudioClip("oink1", true),
                AudioClipResource.GetAudioClip("oink2", true)
            };

            SheepClips = new AudioClip[3]
            {
                AudioClipResource.GetAudioClip("sheep0", true),
                AudioClipResource.GetAudioClip("sheep1", true),
                AudioClipResource.GetAudioClip("sheep2", true)
            };

            ChickClips = new AudioClip[2]
            {
                AudioClipResource.GetAudioClip("chick0", true),
                AudioClipResource.GetAudioClip("chick1", true)
            };
        }

        [Timber_and_Stone.API.Event.EventHandler(Priority.Monitor)]
        public void OnGameLoad(EventGameLoad evt)
        {
            foreach (var unit in UnitManager.getInstance().allAliveUnits)
            {
                if (!(unit is AAnimalEntity)) continue;

                unit.audio.volume = 0.04f;
                EntitySound sound = unit.gameObject.AddComponent<EntitySound>();
                sound.entity = unit;
            }
        }
    }
}
