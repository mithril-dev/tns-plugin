﻿using TNSPlugin.Utilities;
using UnityEngine;

namespace TNSPlugin.Sounds
{
    public class SoundManager
    {
        public SoundManager()
        {
            
            PluginObject.Object.AddComponent<ButtonSounds>();
            PluginObject.Object.AddComponent<AmbientSounds>();
            PluginObject.Object.AddComponent<EntitySoundManager>();
        }
    }
}
