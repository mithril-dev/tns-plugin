﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using UnityEngine;
using System.IO;
using System.Drawing.Imaging;
using System.Reflection;
using MithrilAPI.Debug;

namespace TNSPlugin
{
    public static class TextureHandler
    {
        public static Texture2D ConvertToTexture2D(string resourceName)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            try
            {
                using (Stream stream = assembly.GetManifestResourceStream($"TNSPlugin.Resources.{resourceName}.png"))
                {
                    byte[] buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    stream.Close();

                    Texture2D img = new Texture2D(0, 0, TextureFormat.RGBA32, false);
                    img.LoadImage(buffer);

                    return img;
                }
            }
            catch
            {
                DebugConsole.LogError($"{resourceName} doesn't exist.");
            }


            return null;
        }
    }
}
