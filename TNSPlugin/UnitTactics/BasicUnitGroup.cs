﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Timber_and_Stone;
//using Timber_and_Stone.Tasks;
//using UnityEngine;

//namespace TNSPlugin
//{
//    public class BasicUnitGroup : MonoBehaviour
//    {
//        ControlPlayer player;
//        GUIManager manager;

//        public void Start()
//        {
//            manager = GUIManager.getInstance();
//            player = manager.controllerObj.GetComponent<ControlPlayer>();
//        }

//        public void Update()
//        {
//            if (!GUIManager.getInstance().inGame)
//                return;


//            LayerMask layerMask;
//            RaycastHit hit;
//            if (Input.GetMouseButtonDown(1) && Input.GetKey(KeyCode.LeftShift))
//            {
//                layerMask = (1 << 8);
//                if (Physics.Raycast(player.currentCamera.ScreenPointToRay(Input.mousePosition), out hit, 50, layerMask))
//                {
//                    Vector3[] pickArray = ChunkManager.getInstance().Pick(hit.point, player.currentCamera.ScreenPointToRay(Input.mousePosition).direction, true);
//                    if (pickArray == null) { return; }

//                    if (ChunkManager.getInstance().WalkableBlock(pickArray[0], pickArray[1], pickArray[2], false, false, false, false) != 0)
//                    { return; }

//                    foreach (APlayableEntity unit in GroupManager.selectedUnits)
//                    {
//                        unit.interruptTask();
//                        unit.interruptTask(new TaskForcedMove(unit, (Vector3i)pickArray[0], (Vector3i)(pickArray[1] + pickArray[2])));
//                    }
//                }
//                else
//                {
//                    Vector3[] pickArray = ChunkManager.getInstance().Pick(player.currentCamera.transform.position, player.currentCamera.ScreenPointToRay(Input.mousePosition).direction, true);
//                    if (pickArray == null)
//                        return;

//                    if (ChunkManager.getInstance().WalkableBlock(pickArray[0], pickArray[1] + pickArray[2], Vector3.zero, false, false, false, false) != 0)
//                    { return; }

//                    foreach (APlayableEntity unit in GroupManager.selectedUnits)
//                    {
//                        Vector3i value = ((Vector3i)(pickArray[1] + pickArray[2]));

//                        unit.interruptTask();
//                        unit.interruptTask(new TaskForcedMove(unit, (Vector3i)pickArray[0], value));
//                    }
//                }
//            }
//        }

//        Rect window = new Rect(50, 50, 500, 500);
//        public void OnGUI()
//        {
//            #region old
//            //Rect windowTop = new Rect(window.x, window.y, 500, 32);
//            //manager.DrawWindow(window, "", false);

//            //if (Input.GetMouseButtonDown(0))
//            //{
//            //    click = Event.current.mousePosition;
//            //}

//            //if (beingDragged && Input.GetMouseButton(0))
//            //{
//            //    float x = origin.x + (Event.current.mousePosition.x - click.x);
//            //    float y = origin.y + (Event.current.mousePosition.y - click.y);
//            //    origin = new Vector2(x, y);

//            //    click = Event.current.mousePosition;
//            //}
//            //else if (Input.GetMouseButtonUp(0))
//            //{
//            //    beingDragged = false;
//            //}

//            //if (windowTop.Contains(Event.current.mousePosition) && Input.GetMouseButton(0))
//            //{
//            //    beingDragged = true;
//            //}
//            #endregion

//            //window = GUI.Window(21506, window, ResourceWindow, "");
//        }

//        void ResourceWindow(int windowID)
//        {
//            GUI.DrawTextureWithTexCoords(new Rect(0, 0, window.width, window.height), manager.windowBG, new Rect(0, 0, window.width / manager.windowBG.width, window.height / manager.windowBG.height));
//            GUI.Box(new Rect(0, 0, window.width, window.height), "", manager.windowStyle);
//            manager.DrawTextCenteredWhite(new Rect(0, 0 + 4, window.width, 32), "Unit Groups");
//            {

//            }
//            GUI.DragWindow(new Rect(0, 0, 100000, 32));
//        }
//    }
//}
