﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using MithrilAPI.Debug;

namespace TNSPlugin
{
    public static class Config
    {
        public static ConfigData config { get; private set; }

        private static readonly string name = "MithrilConfig.binary";

        public static void SaveSettings()
        {
            try
            {
                using (FileStream file = File.OpenWrite($@"{name}"))
                {
                    BinaryFormatter formatter = new BinaryFormatter();

                    config = new ConfigData()
                    {
                        UsingNewUI = GUIInGame.IsUsingNewUI
                    };

                    formatter.Serialize(file, config);
                }
            }
            catch (System.Runtime.Serialization.SerializationException ex)
            {
                DebugConsole.LogError(ex.Message);
            }
        }

        public static void LoadSettings()
        {
            if (File.Exists($@"{name}"))
            {
                using (FileStream r = File.OpenRead($@"{name}"))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    config = (ConfigData)formatter.Deserialize(r);
                }
            }
            else
            {
                config = defaultConfig();
            }
        }

        public static ConfigData defaultConfig()
        {
            return new ConfigData()
            {
                UsingNewUI = true
            };
        }
    }

    [Serializable]
    public struct ConfigData
    {
        public bool UsingNewUI;
    }
}
