﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TNSPlugin.Utilities
{
    public static class ExtensionMethods
    {
        public static void MoveToLast<T>(this List<T> list, T item)
        {
            if (item == null) return;

            var index = list.IndexOf(item);
            if (index == list.Count) return;

            list.Insert(list.Count, item);
            list.RemoveAt(index);
        }
    }
}
