﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TNSPlugin.Utilities
{
    public class PluginObject
    {
        public static GameObject Object { get; protected set; }

        public PluginObject()
        {
            Object = new GameObject();
            Object.AddComponent<GUIHandler>();
        }
    }
}
