﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TNSPlugin.Utilities
{
    public static class Styles
    {
        private static readonly GUIManager guiManager = AManager<GUIManager>.getInstance();

        public static GUIStyle SelectionGridStyle { get; private set; } = new GUIStyle();
        public static GUIStyle unreadBoxStyle { get; private set; } = new GUIStyle();
        public static GUIStyle readBoxStyle { get; private set; } = new GUIStyle();
        public static GUIStyle verticalScrollStyle { get; private set; } = new GUIStyle();
        public static GUIStyle verticalScrollThumbStyle { get; private set; } = new GUIStyle();

        public static void ImplimentStyles()
        {
            SelectionGridStyle.alignment = TextAnchor.MiddleCenter;
            SelectionGridStyle.onNormal.background = guiManager.gameSpeedSelector;
            SelectionGridStyle.fixedHeight = 128f;

            verticalScrollStyle = guiManager.skin.verticalScrollbar;
            verticalScrollThumbStyle = guiManager.skin.verticalScrollbarThumb;

            unreadBoxStyle.normal.background = guiManager.boxStyle.normal.background;
            unreadBoxStyle.font = guiManager.textlineStyle.font;
            unreadBoxStyle.normal.textColor = Color.white;
            unreadBoxStyle.padding = new RectOffset(5, 5, 5, 5);
            unreadBoxStyle.wordWrap = true;
            unreadBoxStyle.fixedWidth = 400 - 30;
            unreadBoxStyle.margin = new RectOffset(0, 0, 0, 5);

            readBoxStyle.font = guiManager.textlineStyle.font;
            readBoxStyle.normal.textColor = Color.white;
            readBoxStyle.padding = new RectOffset(5, 5, 5, 5);
            readBoxStyle.wordWrap = true;
            readBoxStyle.fixedWidth = 400 - 30;
            readBoxStyle.margin = new RectOffset(0, 0, 0, 5);
        }
    }
}
