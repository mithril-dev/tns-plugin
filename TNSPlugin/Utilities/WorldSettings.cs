﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using MithrilAPI.Debug;
using TNSPlugin.Events;
using static WorldManager;

namespace TNSPlugin
{
    public class WorldSettings
    {
        private string name;

        public WorldSettings(string name)
        {
            this.name = name;
        }

        public WorldSettingsData worldSettings { get; private set; }
        public void SaveSettings()
        {
            WorldManager.getInstance().AddMetaToGame($"{GUINewGameSettings.difficulty}/{GUINewGameSettings.fogOfWar}");
        }

        public void LoadSettings()
        {
            SaveFileData value;
            WorldSettingsData settings = new WorldSettingsData();

            if (ReadSavesSav().TryGetValue(name, out value))
            {
                if (value.meta == string.Empty)
                {
                    settings = defaultWorldSettings();
                }
                else
                {
                    if (value.meta.Contains('/'))
                    {
                        string[] values = value.meta.Split('/');
                        settings.difficulty = (Difficulty)Enum.Parse(typeof(Difficulty), values[0]);
                        settings.fogOfWar = bool.Parse(values[1]);
                    }
                    else
                    {
                        settings.difficulty = (Difficulty)Enum.Parse(typeof(Difficulty), value.meta);
                        settings.fogOfWar = true;
                    }
                }
            }
            else
            {
                settings = defaultWorldSettings();
            }

            GUINewGameSettings.difficulty = settings.difficulty;
            GUINewGameSettings.fogOfWar = settings.fogOfWar;
        }

        public static WorldSettingsData defaultWorldSettings()
        {
            return new WorldSettingsData()
            {
                difficulty = GUINewGameSettings.difficulty != Difficulty.Peaceful ? GUINewGameSettings.difficulty : Difficulty.Peaceful,
                fogOfWar = GUINewGameSettings.fogOfWar
            };
        }
    }

    [Serializable]
    public struct WorldSettingsData
    {
        public Difficulty difficulty;
        public bool fogOfWar;
    }
}
