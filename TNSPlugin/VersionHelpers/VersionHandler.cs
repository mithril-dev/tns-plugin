﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Timber_and_Stone;
using UnityEngine;

namespace TNSPlugin.VersionHelpers
{
    public class VersionHandler
    {
        public static bool CorrectAPIVersion { get; private set; }

        public static bool CheckCorrectAPIVersion(Version version)
        {
            if ((version.Major == 0 && version.Minor >= 5) && (version.Build >= 0 && version.Revision >= 0))
            {
                //Debug.DebugConsole.Log($"Mithril Mod version: {Assembly.GetExecutingAssembly().GetName().Version} loaded.");

                CorrectAPIVersion = true;
                return true;
            }

            IncorrectAPIVersion();
            return false;
        }

        public static void IncorrectAPIVersion()
        {
            CorrectAPIVersion = false;

            GUIWrongAPI api = new GameObject().AddComponent<GUIWrongAPI>();
            //MithrilGUIManager.GUIs.Add(api);
        }
    }
}
