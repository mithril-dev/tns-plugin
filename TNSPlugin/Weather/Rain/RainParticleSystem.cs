﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

namespace TNSPlugin.Weather
{
    public class RainParticleSystem : MonoBehaviour
    {
        GUIManager guiManager;
        ControlPlayer player;

        Transform origin;
        ParticleSystem particleSystem;

        void Start()
        {
            guiManager = GUIManager.getInstance();
            player = guiManager.controllerObj.GetComponent<ControlPlayer>();

            origin = player.currentCamera.transform.parent;
            
            GameObject ob = new GameObject();
            ob.transform.parent = origin;
            ob.name = "Weather";

            particleSystem = ob.AddComponent<ParticleSystem>();
            ob.AddComponent<ParticleRenderer>();

            ob.transform.position = Vector3.up * 4;

            particleSystem.Play();
            particleSystem.simulationSpace = ParticleSystemSimulationSpace.World;
            particleSystem.startSize = 0.02f;
            particleSystem.startSpeed = 0f;
            particleSystem.gravityModifier = 1.32f;
            particleSystem.emissionRate = 100;
        }

        void Update()
        {
        }
    }
}
