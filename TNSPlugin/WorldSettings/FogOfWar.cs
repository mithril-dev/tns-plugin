﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TNSPlugin
{
    public class FogOfWar : MonoBehaviour
    {
        private static PostProcess[] processes;

        public static void EnablePostProcessing(bool enabled)
        {
            if (processes == null)
            {
                processes = FindObjectsOfType<PostProcess>();
            }

            foreach (PostProcess pp in processes)
            {
                pp.enabled = enabled;
            }

            GUINewGameSettings.fogOfWar = enabled;

            if (GUIManager.getInstance().inGame)
            {
                WorldSettings settings = new WorldSettings(WorldManager.getInstance().settlementName);
                settings.SaveSettings();
            }
        }
    }
}
